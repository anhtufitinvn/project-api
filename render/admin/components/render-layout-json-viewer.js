(function () {
    let component = Object.create(require('../../render-object')());
    let config = global.configuration;
    let services = component.services;
    component.name = "layout-json-viewer";
    component.view = `admin/components/${component.name}`;

    component.loadData = async function (args) {
        let layout = services.data.getLayoutByLayoutID(args.layout_id || 0);
        return layout["unity_layout"] || {}
    };

    module.exports = component;
})();