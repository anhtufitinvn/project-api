(function () {
    let component = Object.create(require('../../render-object')());
    let config = global.configuration;
    let services = component.services;
    component.name = "project-list";
    component.view = `admin/components/${component.name}`;

    component.loadData = async function (args) {
        return null;
    };

    module.exports = component;
})();