(function () {
    let component = Object.create(require('../../render-object')());
    let config = global.configuration;
    let services = component.services;
    component.name = "layout-list";
    component.view = `admin/components/${component.name}`;

    component.loadData = async function (args) {
        let data = {layouts : []};
        let projectLayouts = await services.data.getLayoutListByProjectIdEx(args.project_id || 0);
        if(projectLayouts) {
            data.layouts = projectLayouts.layouts.map((l) => {
                return {
                    recid: l.layout_id,
                    layout_id: l.layout_id,
                    name: l.name,
                    style_name: l.style_name,
                    area: l.area,
                    bed_room_num: l.bed_room_num,
                    price: l.price,
                    construction_fee: l.construction_fee,
                    fitinfurniture_fee: l.fitinfurniture_fee,
                }
            });
        }

        return data
    };

    module.exports = component;
})();