(function () {
    let page = Object.create(require('../render-object')());
    let config = global.configuration;
    page.title = "Fitin Consultant - Admin";
    page.view = "admin/index";
    page.loadData = async function (args) {
        let projectList = await page.services.data.getProjectList();
        return {
            project_count : projectList.length,
            menu : {
                projects : projectList.map(p => {
                    return { id : p.project_id, text: p.project_name, icon: 'fa-picture', count: p.project_id };
                })
            }
        }
    };

    module.exports = page;
})();