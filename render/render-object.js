let Promise = require('bluebird');
const {logger, debug} = require('../core/logger');
module.exports = function () {
    var RenderObject = {
        view : null,
        title : "page title",
        name : "",
        metadata : [],
        services : {
            data :  require('../services/data-services'),
            renovation: require('../services/consultant-services'),
            ecom : require('../services/ecom-services'),
            quotation : require('../services/quotation-services'),
            homestyler : require('../services/homestyler-services'),
            debug: debug,
            logger: logger,
        }
    };

    RenderObject.getContent = async function (args) {
        if(this.loadData && typeof this.loadData === "function") {
            return  {name : this.name, title : this.title, metadata : this.metadata, data : await this.loadData(args || {})}
        }

        return Promise.resolve({title : this.title, metadata : this.metadata, data : null});
    };

    return RenderObject;
};