(function () {
    let page = Object.create(require('../render-object')());
    page.title = "Fitin quotation - Báo giá";
    page.view = "pages/layout-quotation";
    page.loadData = function (args) {
        if(args.layout_id) {
            return page.services.quotation.getUnityLayoutById(args.layout_id);
        }

        throw new Error("quotation page require cart signature");
    };

    module.exports = page;
})();