(function () {
    let page = Object.create(require('../render-object')());
    let config = global.configuration;
    page.title = "Fitin Consultant - Client Messenger";
    page.view = "pages/chat";
    page.loadData = async function (args) {
        return {
            hubSpotId: config.HUBSPOT['hubspot_id']
        }
    };

    module.exports = page;
})();