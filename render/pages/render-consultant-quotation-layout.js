(function () {
    let page = Object.create(require('../render-object')());
    page.title = "Fitin quotation - Báo giá";
    page.view = "pages/consultant-quotation-layout";

    let Providers = require('../../server/providers');
    let Handler = require('../../core/handler');
    let Utils = require('../../utils/utils');
    let ENUMS = global.ENUMS;
    let ResourceService = Providers.ResourceService;
    let DiscountService = Providers.DiscountService;
    let UrlService = Providers.UrlService;

    page.loadData = async function (args) {
        
        if (args.signature || args.layout_id) {
            let consultants = args.consultants;
            if (consultants === undefined || consultants === null) {
            
                consultants = await page.services.quotation.getConsultantLayoutData(args);
            }

            if (consultants === undefined || consultants === null) {
                throw new Error("consultant data with signature or id not found");
            }
            if(!args.signature){
                args.signature = consultants.cart_sign;
            }

            let discount = DiscountService.getDiscountByProjectID(consultants.project_id);
            consultants.discount = discount;

            if(args.is_build) {
                consultants.cart = await rebuidCartIfDataInterrupted(args.signature, consultants);
            } else {
                consultants.cart = await page.services.ecom.getCartBySignature(args.signature);
                if (consultants.cart === null || consultants.cart.length === 0) {
                    consultants.cart = await rebuidCartIfDataInterrupted(args.signature, consultants);
                }
            }
           
            if (consultants.cart) {
                // filter : remove services fee from cart
                consultants.cart = consultants.cart.filter(cart => {
                    for(let p of cart.product_list) {
                        if(p.sku === 'construction_fee' || p.sku === 'fitinfurniture_fee') {
                            return false;
                        }
                    }

                    return true;
                });

                //// convert items array to itemsMap
                let itemsMap = {};
                consultants.items.forEach(item => {
                    itemsMap[item.sku] = item;
                });
                
                /** MAPPING NON ECOM ITEMS WITH HOMESTYLER **/
                let homestylerItems = [];
                if(consultants.non_ecom_items && consultants.non_ecom_items.length > 0) {
                    homestylerItems = await page.services.homestyler.getItems(consultants.non_ecom_items);
                    homestylerItems = homestylerItems.list;
                }

                consultants.non_ecom_items.forEach(item => {
                    h_item = homestylerItems.find(i => i.key === item.sku);
                    if(h_item) {
                        item.name = h_item.display_name || null ;
                        item.thumbnail_url = h_item.thumb128 || null;
                        itemsMap[item.sku] = item;
                    }
                });

                /** END MAPPING **/
                let allItemList = [];

                let non_item_group = {
                    merchant_logo: "images/fitin-merchant.png",
                    merchant_name: "FITIN",
                    product_list : []
                };

                //// Tam thoi chua hien thi
                consultants.non_ecom_items.forEach(item => {
                    if(item.cost > 0) {
                        //// filter price > 0;
                        item.market_price = item.cost;
                        item.group_name = ResourceService.resolveRoomNameByGroupName(item.group_name);
                        item.reserved = true;
                        non_item_group.product_list.push(item);
                    }
                });

                consultants.cart.push(non_item_group);
                
                consultants.cart.forEach(cart => {
                    
                    let sum_market_price = 0;
                    cart.product_list.forEach(p => {
                        p.price = p.market_price - (p.market_price * discount);
                        p.total_price = p.quantity * p.price;
                        p.total_market_price = p.quantity * p.market_price;
                        sum_market_price += p.total_market_price;
                        //
                        let item = itemsMap[p.sku];
                        if (item) {
                            p.group_id = item.group_id;
                            p.group_name = ResourceService.resolveRoomNameByGroupName(item.group_name);
                        } else {
                            p.group_id = 0;
                            p.group_name = "Phòng khác";
                        }

                        allItemList.push(p);
                    });

                    cart.sum_market_price = sum_market_price;
                    cart.sum_price = sum_market_price - (sum_market_price * discount);
                });

                //// group item by group_id
                consultants.groups = groupItemByGroupId(allItemList, discount);
                if(consultants.groups && consultants.groups.length === 1) {
                    let gname = consultants.groups[0].group_name || "";
                    if(gname.toUpperCase() === 'OTHER FURNITURE') {
                        page.view = "pages/consultant-quotation-no-group";
                    }
                }

                /** sum all **/
                let sumAll = {
                    quotationPrice : 0,
                    quotationMarketPrice : 0
                };

                for(let item of allItemList) {
                    sumAll.quotationPrice += item.total_price;
                    sumAll.quotationMarketPrice += item.total_market_price;
                }

                //// services fee
                let services_fee = {
                    merchant_logo: "images/fitin-merchant.png",
                    merchant_name: "FITIN",
                    sum_price: 0,
                    sum_market_price: 0,
                    product_list: []
                };

                let construction_fee = parseInt(consultants.construction_fee);
                let fitinfurniture_fee = parseInt(consultants.fitinfurniture_fee);

                if (!isNaN(construction_fee) && construction_fee > 0) {
                    let fee = resolveServiceFee('construction_fee', 'Chi phí thi công', 1, construction_fee, discount);
                    if(fee) {
                        services_fee.product_list.push(fee);
                        services_fee.sum_price += fee.total_price;
                        services_fee.sum_market_price += fee.total_market_price;
                    }
                }

                if (!isNaN(fitinfurniture_fee) && fitinfurniture_fee > 0) {
                    let fee = resolveServiceFee('fitinfurniture_fee', 'Chi phí nguyên vật liệu', 1, fitinfurniture_fee, discount);
                    if(fee) {
                        services_fee.product_list.push(fee);
                        services_fee.sum_price += fee.total_price;
                        services_fee.sum_market_price += fee.total_market_price;
                    }
                }

                /*consultants.matDatas.forEach(item => {
                    let fee = resolveServiceFee(item.sku, item.sku, 1, item.cost, discount);
                    services_fee.product_list.push(fee);
                    services_fee.sum_price += fee.total_price;
                    services_fee.sum_market_price = fee.total_market_price;
                });*/

                if (services_fee.product_list.length > 0) {
                    sumAll.quotationMarketPrice += services_fee.sum_market_price;
                    sumAll.quotationPrice += services_fee.sum_price;
                    //
                    consultants.services_fee = services_fee;
                }


                consultants.quotation_price = sumAll.quotationPrice;
                consultants.quotation_market_price = sumAll.quotationMarketPrice;
                consultants.quotation_save_price = sumAll.quotationMarketPrice - sumAll.quotationPrice;
                consultants.quotation_discount_price = discount * sumAll.quotationMarketPrice;
                consultants.discount = (discount * 100) + "%";
                consultants.has_discount = discount > 0.0;

            }
       
            return consultants;
        }

        throw new Error("quotation page require cart signature");
    };

    async function rebuidCartIfDataInterrupted(signature, consultants) {
        let cart = await page.services.ecom.buildCart(consultants.items, signature, consultants.construction_fee, consultants.fitinfurniture_fee);
        if (cart && cart.signature && cart.list) {
            if(!signature || !consultants.cart_sign) {
                ///// assign new cart_sign.
                consultants.cart_sign = cart.signature;
                consultants.cart_url = UrlService.resolveCartLink(cart.signature);
            }

            return cart.list;
        }

        return null;
    }

    // market_price : la gia goc, chua discount
    function resolveServiceFee(sku, name, qty, market_price, discount) {
        let fee = {
            sku: sku,
            name: name,
            quantity: qty,
            discount: discount,
            market_price: market_price,
            price: market_price - (market_price * discount)
        };

        fee.total_price = qty * fee.price; // gia da discount
        fee.total_market_price = qty * fee.market_price; // gia chua discount

        if (sku === 'construction_fee') {
            fee.thumbnail_url = "images/fitin-construction-fee.jpg";
        } else if(sku === 'fitinfurniture_fee') {
            fee.thumbnail_url = "images/fitin-furniture-fee.jpg";
        }

        return fee;
    }

    function groupItemByGroupId(items, discount) {
        let groups = {};
        items.forEach(item => {
            let group = groups[item.group_id];
            if (group === undefined || group === null) {
                group = {
                    group_id: item.group_id,
                    group_name: item.group_name,
                    product_list: []
                };

                groups[item.group_id] = group;
            }

            group.product_list.push(item);
        });

        //// summary every group
        let list = Object.values(groups);
        list.forEach(g => {
            let sum_market_price = 0;
            g.product_list.forEach(item => {
                sum_market_price += item.total_market_price;
            });

            g.sum_price = sum_market_price - (sum_market_price * discount);
            g.sum_market_price = sum_market_price;
        });

        return list;
    }

    module.exports = page;
})();