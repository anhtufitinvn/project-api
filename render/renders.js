const Renders = {};
const Components = {};
Renders.ConsultantQuote = require('./pages/render-consultant-quotation');
Renders.ConsultantQuoteRenovation = require('./pages/render-consultant-quotation-renovation');
Renders.ConsultantLayoutQuote = require('./pages/render-consultant-quotation-layout');
Renders.LayoutQuote = require('./pages/render-layout-quotation');
Renders.ChatClient = require('./pages/render-ct-client');
Renders.AdminSite = require('./admin/render-admin-site');
//
Renders.Components = Components;
Components.LayoutViewer = require('./admin/components/render-layout-viewer');

module.exports = Renders;