const Components = {};
Components.LayoutViewer = require('./admin/components/render-layout-viewer');
Components.LayoutList = require('./admin/components/render-layout-list');
Components.ProjectList = require('./admin/components/render-project-list');
module.exports = Components;