const {debug, logger} = require('../../core/logger');
const {query, queryMaster} = require('./query');
const dbaccess = {};

dbaccess.getUserIDByEmail = async function (email) {
    try {
        let results = await query('select `id` from customers where `email` = ? limit 1', [email]);
        if (results && results.length > 0) {
            return results[0]['customers'].id;
        }
    } catch (e) {
        logger.error('getUserIDByEmail', e);
    }

    return 0;
};

dbaccess.getContactList = async function (page) {
    let offset = 0;
    const limit = 250;
    if (page !== undefined && page !== null) {
        if (page > 0) {
            offset = Math.abs(page - 1) * limit;
        }
    }

    let results = await query('select * from customers limit ?, ?', [offset, limit]);
    if (results) {
        return results;
    }

    throw new Error("contacts not found");
};

dbaccess.getContactListEx = async function (limit, page) {
    let offset = 0;
    if (page !== undefined && page !== null) {
        if (page > 0) {
            offset = Math.abs(page - 1) * limit;
        }
    }

    let results = await query('select * from customers limit ?, ?', [offset, limit]);
    if (results) {
        return results;
    }

    throw new Error("contacts not found");
};

dbaccess.getContactListFromTime = async function (limit, page, time) {
    let offset = 0;
    if (page !== undefined && page !== null) {
        if (page > 0) {
            offset = Math.abs(page - 1) * limit;
        }
    }

    let results = await query('select * from customers where UNIX_TIMESTAMP(`created_at`) > ? order by `created_at` DESC limit ?, ?', [time, offset, limit]);
    if (results) {
        return results;
    }

    throw new Error("contacts not found");
};

dbaccess.findEmailContactStartWith = async function (string) {
    let filter = `${string}%`;
    if (string.startsWith('*') && string.length > 1) {
        string = string.substring(1);
        filter = `%${string}%`;
    }

    let results = await query("select `name`, `email`, `normalized_phone`, `gender` from `customers` where `email` is not null and `email` like ? limit 100", [filter]);
    if (results) {
        return results;
    }

    throw new Error("contacts not found");
};

dbaccess.getProjectList = async function (page) {
    let offset = 0;
    const limit = 24;
    if (page !== undefined && page !== null) {
        if (page > 0) {
            offset = Math.abs(page - 1) * limit;
        }
    }

    let results = await query("select distinct * from `layout_projects` as `project` "
        + "where `project`.`status` = 'published' "
        + "order by `project`.`name` ASC limit ?, 24",
        [offset]);
    if (results) {
        return results;
    }

    throw new Error("projects not found");
};

dbaccess.getProjectListWithoutStatusFilter = async function (page) {
    let offset = 0;
    const limit = 24;
    if (page !== undefined && page !== null) {
        if (page > 0) {
            offset = Math.abs(page - 1) * limit;
        }
    }

    let results = await query("select distinct * from `layout_projects` as `project` "
        + "order by `project`.`name` ASC limit ?, ?",
        [offset, limit]);
    if (results) {
        return results;
    }

    throw new Error("projects not found");
};

dbaccess.getAllProjectList = async function () {
    let results = await query("select distinct * from `layout_projects` as `project` "
        + "where `project`.`status` = 'published' "
        + "order by `project`.`name` ASC limit ?",
        [10000]);
    if (results) {
        return results;
    }

    throw new Error("projects not found");
};

dbaccess.getAllProjectAsResources = async function () {
    let results = await query("select distinct `id`, `name`, `slug` from `layout_projects` as `project` "
        + "where `project`.`status` = 'published' "
        + "order by `project`.`name` ASC limit ?",
        [10000]);
    if (results) {
        return results;
    }

    throw new Error("projects not found");
};

dbaccess.getLayoutListByProjectId = async function (project_id, page) {
    let offset = 0;
    const limit = 24;
    if (page !== undefined && page !== null) {
        if (page > 0) {
            offset = Math.abs(page - 1) * limit;
        }
    }

    let results = await query("select distinct * from `layout_projects` as `project` "
        + "inner join `layouts` as `layout` on `project`.id = `layout`.`project_id` "
        + "inner join `layout_styles` as `style` on `style`.`id` = `layout`.`style_id` "
        + "left join `layouts_images` as `images` on `layout`.`id` = `images`.`layout_id` "
        + "where `layout`.`project_id`= ? and `layout`.`parent` = 0 and `layout`.`layout_json` is not null "
        + "and `layout`.`global`= 1 and `layout`.`status` = 1 "
        + "order by `layout`.`name` DESC limit ?, 24",
        [parseInt(project_id), offset]);
    if (results) {
        return results;
    }

    throw new Error("layouts not found");
};


dbaccess.getAllLayoutListByProjectId = async function (project_id) {
    let results = await query("select distinct * from `layout_projects` as `project` "
        + "inner join `layouts` as `layout` on `project`.id = `layout`.`project_id` "
        + "inner join `layout_styles` as `style` on `style`.`id` = `layout`.`style_id` "
        + "left join `layouts_images` as `images` on `layout`.`id` = `images`.`layout_id` "
        + "where `layout`.`project_id`= ? and `layout`.`parent` = 0 and `layout`.`layout_json` is not null "
        + "and `layout`.`global`= 1 and `layout`.`status` = 1 "
        + "order by `layout`.`name` DESC limit 10000",
        [parseInt(project_id)]);
    if (results) {
        return results;
    }

    throw new Error("layouts not found");
};

dbaccess.getUnityLayoutById = async function (layoutId) {
    let rs = await query("select distinct "
        + "`layout`.*,  `style`.name "
        + "from `layouts` as `layout` "
        + "inner join `layout_styles` as `style` on `style`.`id` = `layout`.`style_id` "
        + "where `layout`.`id` = ? and `layout`.`layout_json` is not null LIMIT 1",
        [parseInt(layoutId)]);
    if (rs && rs.length > 0) {
        let layout = rs[0]['layout'];
        let style = rs[0]['style'];
        if (layout.layout_json === '\"{}\"' || layout.layout_json === '\"[]\"') {
            throw new Error("layout json is empty or null");
        }

        let data = JSON.parse(layout.layout_json);
        if (data === null || data["TTS"] === undefined) {
            throw new Error("layout json is wrong format");
        }

        return {
            id: layout["id"],
            name: layout["name"],
            area: layout["area"],
            project_id: layout["project_id"],
            panorama_url: layout["panorama_url"],
            price: layout["estimated_price"] || 0,
            published_at: layout["published_at"],
            status: layout["status"],
            construction_fee: layout["construction_fee"] || 0,
            fitinfurniture_fee: layout["fitinfurniture_fee"] || 0,
            bed_room_num: layout["room_number"],
            image: layout["image"],
            image_floor_plan: layout["image_floor_plan"],
            style_id: layout["style_id"],
            style_name: style["name"],
            unity_layout: data
        };
    }

    throw new Error("layout not found");
};

dbaccess.getListSKUFromObjectKeys = async function (array_keys) {
    let rs = await query("select distinct `products`.`sku`, `products`.`name`, `products`.`object_key`, `products`.`price`, `products`.`merchant_id`, `products`.`thumbnail_url`," +
        "`merchants`.`name`, `merchants`.`image` from `products` "
        + "inner join `merchants` on `merchants`.`id` = `products`.`merchant_id` "
        + "where `object_key` in (?)",
        [array_keys]);
    if (rs && rs.length > 0) {
        return rs;
    }

    return [];
};

dbaccess.getGalleryByLayoutID = async function (layoutID) {
    let rs = await query("select distinct `images`.`image_path`, `images`.`thumb_path` "
        + "from `layouts_images` as `images` "
        + "where `images`.`layout_id` = ? and `images`.`type` = 'gallery' and `images`.`image_path` is not null limit 24",
        [layoutID]);
    if (rs && rs.length > 0) {
        return rs;
    }

    return [];
};

dbaccess.getAllLayoutList = async function (page) {
    let offset = page > 0 ? (page - 1) * 10 : 0;
    let results = await query("select distinct * from `layout_projects` as `project` "
        + "inner join `layouts` as `layout` on `project`.id = `layout`.`project_id` "
        + "inner join `layout_styles` as `style` on `style`.`id` = `layout`.`style_id` "
        + "left join `layouts_images` as `images` on `layout`.`id` = `images`.`layout_id` "
        + "where `layout`.`parent` = 0 and `layout`.`layout_json` is not null "
        + "and `layout`.`global`= 1 and `layout`.`status` = 1 limit ? , 10",
        [offset]);
    if (results) {
        return results;
    }

    throw new Error("layouts not found");
};

dbaccess.getAllMetadataOfLayout = async function () {
    let results = await query("select distinct `id` ,`room_number`, `area` from `layouts` "
        + "where `layout_json` is not null "
        + "and `global`= 1 and `status` = 1 limit 5000",
        []);
    if (results) {
        return results;
    }

    throw new Error("layouts not found");
};

dbaccess.getAllGalleryOfLayouts = async function () {
    let rs = await query("select distinct `images`.`image_path`, `images`.`thumb_path` "
        + "from `layouts_images` as `images` "
        + "where `images`.`type` = 'gallery' and `images`.`image_path` is not null limit 24",
        []);
    if (rs && rs.length > 0) {
        return rs;
    }

    return [];
};

dbaccess.getLocations = async function () {
    let cities = await query("select distinct `id`, `name` from `location_city` as `city`");
    let districts = await query("select distinct `id`, `name`, `city_id` from `location_district` as `district`");
    let wards = await query("select distinct `id`, `name`, `district_id` from `location_ward` as `ward`");
    if (cities && districts && wards) {
        locations = {cities: {}, districts: {}, wards: {}};

        cities.forEach(row => {
            const id = row['city']['id'];
            locations.cities[id] = {name: row['city']['name'], id: id};
        });

        districts.forEach(row => {
            const id = row['district']['id'];
            locations.districts[id] = {name: row['district']['name'], id: id, city_id : row['district']['city_id']};
        });

        wards.forEach(row => {
            const id = row['ward']['id'];
            locations.wards[id] = {name: row['ward']['name'], id: id, district_id : row['ward']['district_id']};
        });

        locations.getCity = function (id) {
            if(id) {
                return locations.cities[id.toString()];
            }

            return null;
        };

        locations.getDistrict = function (id) {
            if(id) {
                return locations.districts[id.toString()];
            }

            return null;
        };

        locations.getWard = function (id) {
            if(id) {
                return locations.wards[id.toString()];
            }

            return null;
        };

        return locations;
    }

    throw new Error("locations not found");
};

/**
 * using DBMaster
 */
dbaccess.updatePanoramaLink = async function (layoutID, link) {
    await queryMaster("update `layouts` set `panorama_url` = ? where `id` = ? limit 1", [link, layoutID]);
};

module.exports = dbaccess;