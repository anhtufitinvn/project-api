const {debug} = require('../../core/logger');
const {poolSlave, poolMaster} = require('./pool');
const query = function(sql, values) {
    return new Promise((resolve, reject) => {
        poolSlave.getConnection(function(err, connection) {
            debug("use db-slave");
            if (err) {
                // not connected!
                reject(err);
                return null;
            }

            // Use the connection
            let args = values ? values : undefined;

            let q = connection.query({sql: sql, nestTables: true}, args, function (error, results, fields) {
                // When done with the connection, release it.
                connection.release();
                // Handle error after the release.
                if (error) {
                    reject(error);
                    return null;
                }

                // Don't use the connection here, it has been returned to the query.
                resolve(results);
            });

            // debug(q.sql);
        });
    });
};

const queryMaster = function(sql, values) {
    return new Promise((resolve, reject) => {
        poolMaster.getConnection(function(err, connection) {
            debug("use db-master");
            if (err) {
                // not connected!
                reject(err);
                return null;
            }

            // Use the connection
            let args = values ? values : undefined;

            let q = connection.query({sql: sql, nestTables: true}, args, function (error, results, fields) {
                // When done with the connection, release it.
                connection.release();
                // Handle error after the release.
                if (error) {
                    reject(error);
                    return null;
                }

                // Don't use the connection here, it has been returned to the query.
                resolve(results);
            });

            // debug(q.sql);
        });
    });
};

module.exports = {query, queryMaster};