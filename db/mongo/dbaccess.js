(function () {
    var mongoose = require('mongoose');
    let {ClientInboxModel, LayoutRenovationModel, LayoutModel} = require('./models');
    let {debug, Logger} = require('../../core/logger');
    let DataAccess = {
        DAO : {
            Project : require('./dao/project-dao'),
            Layout : require('./dao/layout-dao'),
            LayoutRenovation : require('./dao/layout-renovation-dao'),
            LayoutJson : require('./dao/layout-json-dao'),
            LayoutRenovationJson : require('./dao/layout-renovation-json-dao'),
            Room : require('./dao/room-dao'),
            Style : require('./dao/style-dao'),
            Location : require('./dao/location-dao'),
            RoomCategory : require('./dao/room-category-dao'),
            User : require('./dao/user-dao'),
            Cart : require('./dao/cart-dao'),
            SyncDataHistory : require('./dao/sync-data-history-dao'),
            LogSyncData : require('./dao/log-sync-data-dao'),
        }
    };

    DataAccess.generateObjectId = function() {
        return mongoose.Types.ObjectId();
    };

    DataAccess.addClientInbox = async function(payload) {
        try {
            let model = new ClientInboxModel(payload);
            let result = await model.save();
            // debug(result);
            return result._id;
        } catch (e) {
            Logger.error(e);
            return null;
        }
    };

    DataAccess.getClientInbox = function(email) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.find({email : email || null});
            query.limit(100);
            query.sort({createdAt: -1});
            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }

                let list = result.filter(e => {
                    return e["_doc"]["image_paths"].length > 0;
                });

                resolve(list);
            });
        });
    };

    //// get client inbox : images + panorama
    DataAccess.getClientInboxes = function(email) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.find({email : email || null});
            query.setOptions({ lean : true });
            query.limit(200);
            query.sort({createdAt: -1});
            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }

                let data = {
                    panoramas : [],
                    renders : []
                };


                result.forEach(e => {
                    if(e["panorama_url"]) {
                        data.panoramas.push(e);
                    } else if(e["image_paths"].length > 0) {
                        data.renders.push(e);
                    }
                });

                resolve(data);
            });
        });
    };

    DataAccess.getConsultantData = function(signature) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.findOne({"cart.cart_sign": signature });
            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                if(result) {
                    resolve(result["_doc"]);
                } else {
                    resolve(null);
                }
            });
        });
    };

    DataAccess.updateCartSign = function(signature, newSignature) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.updateOne({"cart.cart_sign": signature }, { "cart.cart_sign": newSignature });
            query.exec((err) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve();
            });
        });
    };

    DataAccess.getProjectList = function(offset, limit) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.findOne({"cart.cart_sign": signature });
            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                if(result) {
                    resolve(result["_doc"]);
                } else {
                    resolve(null);
                }
            });
        });
    };

    DataAccess.getConsultantRenovationData = function(args) {
        
        return new Promise((resolve, reject) => {
            let q = {};
            if(args.signature){
                q = {"extensions.cart_json.cart_sign": args.signature }
            }else{
                q = {"int_id": args.layout_id }
            }
            let query = LayoutRenovationModel.findOne(q);

            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                if(result) {
                    resolve(result["_doc"]);
                } else {
                    resolve(null);
                }
            });
        });
    };

    DataAccess.getConsultantLayoutData = function(args) {
        
        return new Promise((resolve, reject) => {
            let q = {};
            if(args.signature){
                q = {"extensions.cart_json.cart_sign": args.signature }
            }else{
                q = {"int_id": args.layout_id }
            }
            
            let query = LayoutModel.findOne(q);

            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                if(result) {
                    resolve(result["_doc"]);
                } else {
                    resolve(null);
                }
            });
        });
    };


    module.exports = DataAccess;
})();