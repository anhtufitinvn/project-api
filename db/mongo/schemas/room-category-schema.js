(function () {
    var mongoose = require('mongoose');
    
    var Schema = mongoose.Schema;
    module.exports = new Schema({
        name: {type: String, default: null},
       // room_id: {type: String, required: true, unique :true},
        type : {type: String, required: true}, 
        status: {type: String, default: "published"},
        sort : {type: Number, default: 0},
        image: {type: String, default: null},
    }, {
        timestamps: true,
        collection: 'RS_Room_Categories'
    });
})();