(function () {
    var mongoose = require('mongoose');
    var mongoosePaginate = require('mongoose-paginate-v2');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    

    const Any = new Schema({any: Schema.Types.Mixed});
    var LayoutSchema = new Schema({
        sort: {type: Number, required: false, default: 0},
        int_id: {type: Number, required: true, unique: false, index: true},
        parent_int_id: {type: Number, required: false, unique: false, default: 0},
        project_id: {type: mongoose.ObjectId, required: true, unique: false, index: true},
        project_int_id: {type: Number, required: true, unique: false, index: true},
        project_name: {type: String, required: true, unique: false, index: true},
        layout_key_name:{type: String, required: false, index: true},
        layout_name: {type: String, index: true, default: null},
        //layout_json: {type: Array, default: []},
        slug: {type: String, default: null},
        status: {type: String, default: "published"},
        is_template: {type: Boolean, default: false},
        image_path: {type: String, default: null},
        image_floor_plan_path: {type: String, default: null},
        desc: {type: String, default: null},
        area: {type: Float, default: 0.0},
        
        gallery: [
            {
                int_id : {type: Number, required: false, default: 0},
                image_path: {type: String, required: false, default: null},
                thumb_path: {type: String, required: false, default: null},
                index: {type: Number, required: false, default: 0},
                hidden: {type: Number, required: false, default: 0}
            }
        ],
        published_by: {type: String, required: false, default: null},
        published_at: {type: Number, required: false, default: 0},
        metadata: {
            title: {type: String, required: false, default: null},
            intro: {type: String, required: false, default: null},
            style: {
                name: {type: String, required: false, default: null},
                key: {type: String, required: false, default: null}
            },
            user_info : {
                name : {type: String, required: false, default: null},
                email : {type: String, required: false, default: null},
                address : {type: String, required: false, default: null},
                phone : {type: String, required: false, default: null}
            },
            price: {
                estimated: {type: Number, required: false, default: 0},
                max: {type: Number, required: false, default: 0},
                min: {type: Number, required: false, default: 0}
            },
            rooms: [
                {
                    name: {type: String, required: false, default: null},
                    type: {type: String, required: false, default: null},
                    room_id: {type: String, required: false, default: null},
                    qty: {type: Number, required: false, default: 0},
                    level: {type: Number, required: false, default: 0},
                    area: {type: Float, required: false, default: 0.0},
                }
            ],
            bedroom_num : {type: Number, required: false, default: 0},
            creator : {
                int_id : {type: Number, required: false , default: null},
                email : {type: String, required: false, default: null}
            },
            address : {
                block: {type: String, default: null},
                apartment_code : {type: String, default: null}
            }, 
        },
        attributes: [
            {
                label: {type: String, required: false, default: null},
                name: {type: String, required: false, default: null},
                desc: {type: String, required: false, default: null},
                type: {type: String, required: false, default: null},
                value: {type: Schema.Types.Mixed, required: false, default: null},
                value_int: {type: Number, required: false, default: null},
                value_float: {type: Float, required: false, default: null},
                value_string: {type: String, required: false, default: null},
                options: [
                    {
                        label: {type: String, required: false, default: null},
                        key: {type: String, required: false, default: null},
                        value: {type: String, required: false, default: null}
                    }
                ],
            }
        ],
        extensions: {
            cart_json: {
                metadata : {type: Schema.Types.Mixed},
                non_ecom_items : {type: Array , default : []},
                
                items : {type: Array , default : []},
                matDatas : {type: Array , default : []},
                cart_sign : {type: String, default: null}

            },
            panorama: {
                enable: {type: Number, required: false, default: 0},
                url: {type: String, required: false, default: null},
            },
            quotation: {
                url_download: {type: String, required: false, default: null},
                url: {type: String, required: false, default: null},
            },
            services_fee: [
                {
                    label: {type: String, required: false, default: null},
                    key: {type: String, required: false, default: null},
                    value: {type: Number, required: false, default: 0},
                    detail: [
                        {
                            label: {type: String, required: false, default: null},
                            key: {type: String, required: false, default: null},
                            qty: {type: Number, required: false, default: 0},
                            value: {type: Float, required: false, default: 0},
                            unit: {type: String, required: false, default: null},
                            area: {type: Float, required: false, default: 0}
                        }
                    ]
                }
            ]
        }
    }, {
        timestamps: true,
        collection: 'CT_Layouts'
    }).plugin(mongoosePaginate);

    LayoutSchema.index({project_name: 'text', layout_name : 'text', layout_key_name : 'text'});
    module.exports = LayoutSchema;

})();