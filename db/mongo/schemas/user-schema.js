(function () {
    var mongoose = require('mongoose');
    var mongoosePaginate = require('mongoose-paginate-v2');
    var bcrypt = require('bcrypt');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    

    const Any = new Schema({any: Schema.Types.Mixed});
    var UserSchema = new Schema({     
        int_id: {type: Number, required: true, unique: false, index: true},
        name: {type: String, required: false, unique: false, index: true},
        email: {type: String, required: true, unique: true, index: true, lowercase: true},
        password:{type: String, required: false, index: false,  minLength: 6},
        level: {type: Number, required: true, index: false}
        
    }, {
        timestamps: true,
        collection: 'RS_Users'
    });

    
    UserSchema.pre('save', async function (next) {
        // Hash the password before saving the user model
        const user = this
        if (user.isModified('password')) {
            user.password = await bcrypt.hash(user.password, 10);
        }
        next();
    })

    UserSchema.pre('create', async function (next) {
        // Hash the password before saving the user model
        const user = this
        user.password = await bcrypt.hash(user.password, 10);
        next();
    })

    // comparePassword
    UserSchema.methods.comparePassword = function (password) {
        return bcrypt.compareSync(password, this.password);
    }

    UserSchema.plugin(mongoosePaginate);

    module.exports = UserSchema;
})();