(function () {
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;
    module.exports = new Schema({
        seq: { type: Number, default: 100000 },
        name: { type: String, unique : true, required: true, index : true}
    }, {
        timestamps: false,
        collection: 'Auto_Increment'
    });
})();