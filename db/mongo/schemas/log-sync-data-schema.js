(function () {


    const mongoose = require('mongoose');
    const Schema = mongoose.Schema;

    module.exports = new Schema({
        total_second: {type: Number, required: true, default: 0},
        total: {type: Number, required: true, default: 0},
        processed: {type: Number, required: true, default: 0},
        count_success: {type: Number, required: true, default: 0},
        count_fail: {type: Number, required: true, default: 0},
        model:{type: String, required: true, index: true},
        sync_time: {type: Number, required: true, default: 0},
        data: {type: Schema.Types.Mixed, default: {}}
    }, {
        timestamps: true,
        collection: 'CT_Log_Sync_Data'
    });


})();