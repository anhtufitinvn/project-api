(function () {
    var mongoose = require('mongoose');
    var mongoosePaginate = require('mongoose-paginate-v2');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;

    const Any = new Schema({any: Schema.Types.Mixed});
    var ProjectSchema = new Schema({
        sort: {type: Number, required: false, default : 0},
        landing_url : {type: String, default: null},
        int_id: {type: Number, required: true, unique: false, index: true},
        project_ref_id: {type: String, index: true, unique: true},
        project_name: {type: String, default: null, index: true},
        slug: {type: String, default: null, index: true},
        status: {type: String, default: "published"},
        image_path: {type: String, default: null},
        desc: {type: String, default: null},
        is_renovation: {type: Boolean, default: false},
        location: {
            city_id: {type: Number, default: null},
            city_name: {type: String, default: null},
            district_id: {type: Number, default: null},
            district_name: {type: String, default: null},
            lat: {type: Float, default: null},
            long: {type: Float, default: null},
            address: {type: String, default: null}
        },
        metadata: {
            title: {type: String, required: false, default: null},
            title_desc: {type: String, required: false, default: null},
            creator : {
                int_id : {type: Number, required: false , default: null},
                email : {type: String, required: false, default: null}
            }
        },
        attributes: [
            {
                label: {type: String, required: false, default: null},
                name: {type: String, required: false, default: null},
                desc: {type: String, required: false, default: null},
                type: {type: String, required: false, default: null},
                value: {type: Schema.Types.Mixed, required: false, default: null},
                value_int: {type: Number, required: false, default: null},
                value_float: {type: Float, required: false, default: null},
                value_string: {type: String, required: false, default: null},
                options: [
                    {
                        label: {type: String, required: false, default: null},
                        key: {type: String, required: false, default: null},
                        value: {type: String, required: false, default: null}
                    }
                ],
            }
        ]
    }, {
        timestamps: true,
        collection: 'CT_Projects'
    }).plugin(mongoosePaginate);

    ProjectSchema.index({project_name: 'text'});
    module.exports = ProjectSchema;
})();