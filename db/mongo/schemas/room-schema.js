(function () {
    var mongoose = require('mongoose');
    var mongoosePaginate = require('mongoose-paginate-v2');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    

    const Any = new Schema({any: Schema.Types.Mixed});
    module.exports = new Schema({
        sort: {type: Number, required: false, default: 0},
        int_id: {type: Number, required: true, unique: false, index: true},
        layout_id: {type: mongoose.ObjectId, required: true, unique: false, index: true},
        layout_int_id: {type: Number, required: true, unique: false, index: true},
        layout_name: {type: String, required: true, unique: false, index: true},
        room_id:{type: String, required: true, index: true},
        room_name: {type: String, default: null},
        room_category_name : {type: String, default: null},
        room_category_key : {type: String, default: null},
        room_json: {type: Array, default: []},
        slug: {type: String, default: null},
        status: {type: String, default: "published"},
        image_path: {type: String, default: null},
        image_floor_plan_path: {type: String, default: null},
        desc: {type: String, default: null},
        area: {type: Float, default: 0.0},
        gallery: [
            {
                int_id : {type: Number, required: false, default: 0},
                image_path: {type: String, required: false, default: null},
                thumb_path: {type: String, required: false, default: null},
                index: {type: Number, required: false, default: 0},
                hidden: {type: Number, required: false, default: 0}
            }
        ],
        published_by: {type: String, required: false, default: null},
        published_at: {type: Number, required: false, default: 0},
        metadata: {
            title: {type: String, required: false, default: null},
            intro: {type: String, required: false, default: null},
            
            price: {
                estimated: {type: Number, required: false, default: 0},
                max: {type: Number, required: false, default: 0},
                min: {type: Number, required: false, default: 0}
            }
        },
        attributes: [
            {
                label: {type: String, required: false, default: null},
                name: {type: String, required: false, default: null},
                desc: {type: String, required: false, default: null},
                type: {type: String, required: false, default: null},
                value: {type: Schema.Types.Mixed, required: false, default: null},
                value_int: {type: Number, required: false, default: null},
                value_float: {type: Float, required: false, default: null},
                value_string: {type: String, required: false, default: null},
                options: [
                    {
                        label: {type: String, required: false, default: null},
                        key: {type: String, required: false, default: null},
                        value: {type: String, required: false, default: null}
                    }
                ],
            }
        ],
        extensions: {
            cart_json: {type: Schema.Types.Mixed},
            panorama: {
                enable: {type: Number, required: false, default: 0},
                url: {type: String, required: false, default: null},
            },
            services_fee: [
                {
                    label: {type: String, required: false, default: null},
                    key: {type: String, required: false, default: null},
                    value: {type: Number, required: false, default: 0},
                    detail: [
                        {
                            label: {type: String, required: false, default: null},
                            key: {type: String, required: false, default: null},
                            qty: {type: Number, required: false, default: 0},
                            value: {type: Float, required: false, default: 0},
                            unit: {type: String, required: false, default: null}
                        }
                    ]
                }
            ]
        }
    }, {
        timestamps: true,
        collection: 'CT_Rooms'
    }).plugin(mongoosePaginate);
})();