(function () {
    var mongoose = require('mongoose');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    module.exports = new Schema({
        email: {type: String, required: true, index: true},
        image_paths: {type: [{type: String}]},
        panorama_url: {type: String, default : null},
        cart: {
            items: {
                type: [
                    {
                        sku: {type: String, required: true, index : true},
                        quantity: {type: Number, required: true, default: 1},
                        group_id: {type: Number, required: false, default: 0},
                        group_name: {type: String, required: false, default: ""}
                    }
                ],
                default: []
            },
            cart_sign: {type: String, required: true, index : true}
        },
        metadata: {
            consultant_id: {type: Number, required: true, index: true},
            serial_key: {type: String, default: false},
            project_id: {type: Number, required: true},
            project_name: {type: String, required: true},
            layout_id: {type: Number, required: true},
            layout_name: {type: String, required: true},
            bed_room_num: {type: Number, required: true, default : 1},
            area: {type: String, required: true, default : "0.0"},
            discount: {type: Float, required: true, default : 0.0},
            construction_fee: {type: Number, required: true, default : 0.0},
            fitinfurniture_fee: {type: Number, required: true, default : 0.0}
        }
    }, {
        timestamps: true,
        collection: 'CT_Client_Inbox'
    });
})();