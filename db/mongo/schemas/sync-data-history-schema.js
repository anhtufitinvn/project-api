(function () {


    const mongoose = require('mongoose');
    const Schema = mongoose.Schema;

    module.exports = new Schema({
        sync_key: {type: String, required: true, unique: true, index: true},
        sync_time: {type: Number, required: false, default: 0},
    }, {
        timestamps: true,
        collection: 'CT_Sync_Data_Histories'
    });


})();