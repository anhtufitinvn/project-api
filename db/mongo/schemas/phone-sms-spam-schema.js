(function () {
    var mongoose = require('mongoose');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    module.exports = new Schema({
        phone: {type: String, required : true, unique: true, index: true},
        name: {type: String, default: null},
        status: {type: Number, default: 0}
    }, {
        timestamps: true,
        collection: 'RS_Phones_SMS_Spam'
    });
})();