(function () {
    var mongoose = require('mongoose');
    var mongoosePaginate = require('mongoose-paginate-v2');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    

    const Any = new Schema({any: Schema.Types.Mixed});
    module.exports = new Schema({
        int_id: {type: Number, required: true, unique: false, index: true},
    //    fitin_id: {type: mongoose.ObjectId, required: true, unique: false, index: true},
        
        metadata : {
            creator : {
                int_id : {type: Number, required: false , default: null},
                email : {type: String, required: false, default: null}
            }
        },
        original_json: {
            metadata : {
                
            },
            client: {
                email : {type: String, required: true, lowercase: true},
                phone : {type: String, required: false, default: null}
            },
            carts : {
                areas : [
                    {
                        id :  {type: Number, required: true},
                        name : {type: String, default: null},
                        area : {type: Float, required: true, default: 0.0},
                        items : [
                            {
                                sku :  {type: String, default: null},
                                name :  {type: String, default: null},
                                cost :  {type: Number, default: 0},
                                quantity :  {type: Number, default: 0},
                                key :  {type: String, default: null},
                                type :  {type: String, default: null},
                                thumb :  {type: String, default: null}
                            }
                        ]
                    }
                ],
                matDatas : []
            }

        },
        cart_json : {
            items : {type: Array, required: false, default: []},
            cart_sign :{type: String, required: false}
        }
    
    }, {
        timestamps: true,
        collection: 'CT_Carts'
    }).plugin(mongoosePaginate);
})();