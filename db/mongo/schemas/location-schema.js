(function () {
    var mongoose = require('mongoose');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    module.exports = new Schema({
        city_id: {type: Number, required: true, unique: true, index: true},
        city_name: {type: String, required: true},
        lat: {type: Float, default: null},
        long: {type: Float, default: null},
        districts: [
            {
                city_id: {type: Number, default: null},
                district_id: {type: Number, default: null},
                district_name: {type: String, default: null},
                lat: {type: Float, default: null},
                long: {type: Float, default: null},
                wards: [
                    {
                        city_id: {type: Number, default: null},
                        district_id: {type: Number, default: null},
                        ward_id: {type: Number, default: null},
                        ward_name: {type: String, default: null},
                        lat: {type: Float, default: null},
                        long: {type: Float, default: null}
                    }
                ]
            }
        ],
    }, {
        timestamps: true,
        collection: 'RS_Locations'
    });
})();