(function () {
    var mongoose = require('mongoose');
    var mongoosePaginate = require('mongoose-paginate-v2');
    var Schema = mongoose.Schema;
    

    
    var LayoutRenovationJsonSchema = new Schema({     
        layout_int_id: {type: Number, required: true, unique: true, index: true},
        //layout_id: {type: String, required: true, unique: true},
        creator : {
            int_id : {type: Number, required: false , default: null},
            email : {type: String, required: false, default: null}
        },
        data_json : {type: Schema.Types.Mixed, default: {}}
        
    }, {
        timestamps: true,
        collection: 'CT_Layouts_Renovation_Json'
    });


    LayoutRenovationJsonSchema.plugin(mongoosePaginate);

    module.exports = LayoutRenovationJsonSchema;
})();