(function () {
    var mongoose = require('mongoose');
    var Float = require('mongoose-float').loadType(mongoose);
    var Schema = mongoose.Schema;
    module.exports = new Schema({
        name: {type: String, default: null},
        slug: {type: String, default: null},
        status: {type: String, default: "published"},
        image: {type: String, default: null},
    }, {
        timestamps: true,
        collection: 'RS_Layout_Styles'
    });
})();