(function () {
    let {LayoutRenovationModel, AutoIncrement} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "CT_Layouts_Renovation"};
    let roomItem = require('../../../models/room-item');
    let galleryItem = require('../../../models/gallery-item');

    Dao.autoIncrement = async function () {
        let auto = await AutoIncrement.findOneAndUpdate({name: Dao.name}, {$inc: {seq: 1}});
        if (auto && auto['_doc']) {
            return auto['_doc']['seq'];
        }

        return null;
    };

    Dao.getList = async function (page, limit, params = []) {
        let id = params.layout_id;
        let intID = parseInt(id);

        let projectId = params.project_id;
        let intProjectId = parseInt(projectId);
        let estimated_price = params.estimated_price;
        let style = params.style;
        let bedroom = params.bedroom;
        let name = params.name;
        let room_type = params.room_type;
        let approved = params.approved;
        let status = params.status;
        let q = {};
        if (name) {
            //q.project_name = { $regex: '.*' + name + '.*' ,  $options: 'i'};
            q.$text = {$search: name}
        }
        if (intID) {
            q["int_id"] = intID;
        }
        if (intProjectId) {
            q["project_int_id"] = intProjectId;
        }
        if (style) {
            q["metadata.style.key"] = style.toLowerCase();
        }
        if (bedroom) {
            q["metadata.bedroom_num"] = parseInt(bedroom);
        }
        if (estimated_price) {
            q["metadata.price.estimated"] = parseInt(estimated_price);
        }
        if (room_type) {
            q["metadata.rooms.type"] = room_type.toLowerCase();
        }
        if (approved) {
            q["metadata.approved"] = approved;
        }
        if (status) {
            q["status"] = status;
        }

        return await LayoutRenovationModel.paginate(q, {
            page: page || 1,
            limit: limit || 1000,
            lean: true,
            sort: {createdAt: -1}
        });
        // let query = LayoutModel.find(q);
        // query.collation({locale: "en_US", numericOrdering: true});
        // query.setOptions({lean: true});
        // query.sort({project_name: 1});
        // query.skip(offset || 0);
        // query.limit(limit || 1000);
        // return await query.exec();
    };

    Dao.getDetail = async function (id) {
        let q = {
            '_id': id
        };
        if (!isNaN(id)) {
            q = {
                'int_id': parseInt(id)
            };
        }
        let query = LayoutRenovationModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.skip(0);
        query.limit(1);
        return await query.lean().exec();
    };

    Dao.getListByProjectId = async function (projectId, offset, limit) {
        let query = LayoutRenovationModel.find({project_id: projectId});
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort: -1, layout_name: 1});
        query.skip(offset || 0);
        query.limit(limit || 1000);
        return await query.exec();
    };

    Dao.getListByProjectIntId = async function (id, offset, limit) {
        let query = LayoutRenovationModel.find({project_int_id: id});
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort: -1, layout_name: 1});
        query.skip(offset || 0);
        query.limit(limit || 1000);
        return await query.exec();
    };

    Dao.getPublishedList = async function (offset, limit) {
        let query = LayoutRenovationModel.find({status: 'published'});
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort: -1, layout_name: 1});
        if (limit && offset) {
            query.skip(offset * limit);
            query.limit(limit);
        } else {
            query.skip(0);
            query.limit(1000);
        }

        return await query.exec();
    };

    Dao.create = async function (layout) {
        try {
            let seq = await Dao.autoIncrement();
            if (seq) {
                layout.int_id = seq;
                let model = new LayoutRenovationModel(layout);
                let result = await model.save();
                return result;
            }
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Dao.deleteByID = async function (id) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }
            let rs = await LayoutRenovationModel.deleteOne(q);
            if (rs && rs.deletedCount) {
                return id;
            }

            return null;
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Dao.update = async function (id, layout) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }

            let data = {
                "$set": {
                    sort: layout.sort,
                    //int_id: layout.int_id,
                    parent_int_id: layout.parent_int_id,
                    project_id: layout.project_id,
                    project_int_id: layout.project_int_id,
                    project_name: layout.project_name,
                    layout_key_name: layout.layout_key_name,
                    layout_name: layout.layout_name,
                    slug: layout.slug,
                    status: layout.status,
                    is_template: layout.is_template,
                    image_path: layout.image_path,
                    image_floor_plan_path: layout.image_floor_plan_path,
                    desc: layout.desc,
                    area: layout.area,
                    // gallery : layout.gallery || [],
                    published_by: layout.published_by,
                    published_at: layout.published_at,
                    //metadata : layout.metadata,
                    "metadata.title": layout.metadata ? layout.metadata.title : null,
                    "metadata.intro": layout.metadata ? layout.metadata.intro : null,
                    "metadata.style": layout.metadata ? layout.metadata.style : {name: null, key: null},
                    "metadata.price": layout.metadata ? layout.metadata.price : {estimated: 0, max: 0, min: 0},
                    "metadata.user_info": layout.metadata ? layout.metadata.user_info : {},
                    "metadata.bedroom_num": layout.metadata ? layout.metadata.bedroom_num : 0,
                    "metadata.creator": layout.metadata ? layout.metadata.creator : {},
                    "metadata.address": layout.metadata ? layout.metadata.address : null,
                    "metadata.approved": layout.metadata ? layout.metadata.approved : false,

                    //"extensions.services_fee": layout.extensions ? layout.extensions.services_fee : []
                }
            };


            let rs = await LayoutRenovationModel.updateOne(q, data);
            return (rs && rs.nModified);
        } catch (e) {
            logger.error(e);
            return false;
        }
    };

    Dao.updatePanoramaLink = async function (id, panorama_url) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }
            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    "$set": {
                        "extensions.panorama": {
                            url: panorama_url,
                            enable: true
                        }
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {
            logger.error(e);
            return false;
        }
    };

    Dao.updateQuotationLink = async function (id, url_quotation, url_download) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }
            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    "$set": {
                        "extensions.quotation": {
                            url: url_quotation,
                            url_download: url_download || null
                        }
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {
            logger.error(e);
            return false;
        }
    };

    Dao.addCart = async function (id, original_json, cart_json) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }

            let data_cart_metadata = original_json.metadata || {};
            let client_data = original_json.client || {};
            let matDatas = original_json.carts.matDatas || [];
            let items = cart_json.items || [];
            let cart_sign = cart_json.cart_sign || null;
            let non_ecom_items = cart_json.non_ecom_items || [];
            let data_fee = [];
            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    "$set": {
                        "extensions.cart_json": {
                            metadata: data_cart_metadata,
                            non_ecom_items: non_ecom_items,
                            items: items,
                            matDatas: matDatas,
                            cart_sign: cart_sign
                        },
                        "metadata.user_info": client_data,
                        "extensions.services_fee": data_fee
                    },

                }
            );
            return (rs && rs.nModified);
        } catch (e) {
            debug('add cart failed');
            logger.error(e);
            return false;
        }
    };

    Dao.addRoom = async function (id, data) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }
            room = roomItem(data.name, data.type, data.area);

            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    "$push": {
                        "metadata.rooms": room
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {
            debug('add room failed');
            logger.error(e);
            return false;
        }
    };

    Dao.addRooms = async function (id, array_data) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }

            let data_push = array_data.map(r => {
                return roomItem(r.name, r.type, r.area);
            });

            let promise = [];
            data_push.forEach(function (item){
                q["metadata.rooms.type"] = { $ne: item.type };
                promise.push(LayoutRenovationModel.updateOne(q,
                    {
                        $push: {
                            "metadata.rooms": item
                        }
                    }
                ));
            });

            await Promise.all(promise);
            // let rs = await LayoutRenovationModel.updateOne(q,
            //     {
            //         $push: {
            //             "metadata.rooms": {
            //                 $each: data_push
            //             }
            //         }
            //     }
            // );
            return true;
        } catch (e) {

            debug('add rooms failed');
            logger.error(e);
            return false;
        }
    };

    Dao.removeRoom = async function (id, room_id) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }

            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    "$pull": {
                        "metadata.rooms": {_id: room_id}
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {

            debug('remove room failed');
            logger.error(e);
            return false;
        }
    };


    Dao.addGallery = async function (id, array_data) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }

            let data_push = array_data.map(r => {
                return galleryItem(r.image_path, r.thumb_path, r.index, r.hidden);
            });

            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    $push: {
                        "gallery": {
                            $each: data_push
                        }
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {

            debug('add gallery failed');
            logger.error(e);
            return false;
        }
    };

    Dao.removeGallery = async function (id, gallery_id) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }

            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    "$pull": {
                        "gallery": {_id: gallery_id}
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {

            debug('remove gallery failed');
            logger.error(e);
            return false;
        }
    };

    Dao.import = async function (list) {
        let result = {
            imported: 0,
            errors: []
        };

        if (list) {
            try {
                let rs = await LayoutRenovationModel.insertMany(list);
                for (var r of rs) {
                    let layout = r['_doc'];
                    if (layout) {
                        if (layout['_id']) {
                            result.imported += 1;
                        } else {
                            result.errors.push({
                                layout_id: layout.int_id,
                                layout_name: layout.layout_name
                            });
                        }
                    }
                }
            } catch (e) {
                result.errors = list.map(m => {
                    return {layout_id: m.int_id, layout_name: m.layout_name}
                })
            }
        }

        return result;
    };

    /** function nay se replace gallery dang co  **/
    Dao.updateAllGallery = async function (id, gallery) {
        try {
            if (!gallery || !Array.isArray(gallery)) {
                return false;
            }

            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }
            let rs = await LayoutRenovationModel.updateOne(q,
                {
                    "$set": {
                        "gallery": gallery
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {
            logger.error(e);
            return false;
        }
    };

    module.exports = Dao;
})();