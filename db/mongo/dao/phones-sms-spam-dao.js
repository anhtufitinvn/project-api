(function () {
    let {PhoneSmsSpamModel} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "RS_Phones_SMS_Spam"};

    Dao.getList = async function () {
        let query = PhoneSmsSpamModel.find({
            status: 0
        });

        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({createdAt: -1});
        query.skip(0);
        query.limit(20);
        return await query.exec();
    };

    Dao.import = async function (list) {
        let result = {
            imported: 0,
            errors: []
        };

        if (list) {
            try {
                let rs = await PhoneSmsSpamModel.insertMany(list, {ordered: false});
                for (var r of rs) {
                    let doc = r['_doc'];
                    if (doc) {
                        if (doc['_id']) {
                            result.imported += 1;
                        } else {
                            result.errors.push({
                                phone: doc.phone
                            });
                        }
                    }
                }
            } catch (e) {
                if (e["writeErrors"] && Array.isArray(e.writeErrors)) {
                    result.errors = e.writeErrors.map(m => {
                        return {phone: m.err.op.phone}
                    });

                    result.imported = Math.max(list.length - result.errors.length, 0);
                } else if(e.op) {
                    result.errors.push({phone: e.op["phone"] || null});
                    result.imported = Math.max(list.length - 1, 0);
                } else {
                    result.errors = list.map(m => {
                        return {phone: m.phone}
                    })
                }
            }
        }

        return result;
    };

    Dao.updateSent = async function (phone) {
        let rs = await PhoneSmsSpamModel.updateOne({phone : phone},
            {
                "$set": {
                    "status": 1
                }
            }
        );
        return (rs && rs.nModified);
    };


    module.exports = Dao;
})();