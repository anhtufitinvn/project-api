(function () {
    let {RoomCategoryModel, AutoIncrement} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "RS_Room_Categories"};

    Dao.autoIncrement = async function() {
        let auto = await AutoIncrement.findOneAndUpdate({name: Dao.name}, {$inc: {seq: 1}});
        if(auto && auto['_doc']) {
            return auto['_doc']['seq'];
        }

        return null;
    };

    Dao.getList = async function (params = []) {
       
        let name = params['name'] || null;
        let status = params['status'] || null;
        let q = {};
        if(name){
            q.name = { $regex: '.*' + name + '.*' ,  $options: 'i'};
        }

        if(status){
            q.status = status;
        }

        let query = RoomCategoryModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort: -1});
        return await query.exec();
    };

    Dao.getByType = async function (type) {
        let q = {
            'type' : type
        };
       
        let query = RoomCategoryModel.findOne(q);
        query.collation({locale: "en_US", numericOrdering: true});
        return await query.lean().exec();
    };

    Dao.init = async function (){
        let list = [
                {
                    name : "Phòng khách",
                    type : "livingroom",
                    status : "published"
                },
                {
                    name : "Phòng trẻ em",
                    type : "kidroom",
                    status : "published"
                },
                {
                    name : "Phòng làm việc",
                    type : "workingroom",
                    status : "published"
                },
                {
                    name : "Phòng ngủ chính",
                    type : "bedroom",
                    status : "published"
                },
                {
                    name : "Phòng ăn",
                    type : "dinningroom",
                    status : "published"
                },
                {
                    name : "Phòng bếp",
                    type : "kitchenroom",
                    status : "unpublished"
                }
        ];

        try {
            return await RoomCategoryModel.bulkWrite(
                list.map((product) => 
                  ({
                    updateOne: {
                      filter: { type : product.type },
                      update: { $set: product },
                      upsert: true
                    }
                  })
                ));
            
            
        } catch (e) {
            
            return false;
        }
    };

    module.exports = Dao;
})();