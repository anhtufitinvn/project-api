(function () {
    let {LogSyncDataModel} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let LogSyncDataDao = {name: "CT_Log_Sync_Data"};


    LogSyncDataDao.create = async function (data) {
        try {
            let model = new LogSyncDataModel(data);
            let result = await model.save();
            return result;
        } catch (e) {
            logger.error('LogSyncDataDao.create', e, e.stack);
        }
        return null;
    };
    LogSyncDataDao.pushFail = async function (syncTime, data) {
        try {
            let q = {
                'sync_time': syncTime
            };
            let rs = await LogSyncDataModel.updateOne(q,
                {
                    "$push": {
                        "data.fail": data
                    },
                    "$inc": {
                        count_fail: 1,
                    },

                }
            );
            return (rs && rs.nModified);
        } catch (e) {
            debug('add room failed');
            logger.error(e);
            return false;
        }
    };
    LogSyncDataDao.pushSuccess = async function (syncTime, data) {
        try {
            let q = {
                'sync_time': syncTime
            };
            let rs = await LogSyncDataModel.updateOne(q,
                {
                    "$push": {
                        "data.success": data
                    },
                    "$inc": {
                        count_success: 1,
                    }
                }
            );
            return (rs && rs.nModified);
        } catch (e) {
            debug('add room failed');
            logger.error(e);
            return false;
        }
    };
    module.exports = LogSyncDataDao;
})();