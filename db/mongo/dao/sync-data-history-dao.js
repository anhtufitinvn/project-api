(function () {
    let {SyncDataHistoryModel} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let SyncDataHistoryDao = {name: "CT_Sync_Data_Histories"};


    SyncDataHistoryDao.getByKey = async function (sync_key) {
        let q = {
            'sync_key': sync_key
        };
        let query = SyncDataHistoryModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.skip(0);
        query.limit(1);
        return await query.lean().exec();
    };


    SyncDataHistoryDao.upsert = async function (data) {
        try {
            let q = {
                sync_key: data.sync_key
            };
            let rs = await SyncDataHistoryModel.findOneAndUpdate(q, data, {
                upsert: true,
                new: true,
                setDefaultsOnInsert: true
            });

            return rs._id;
        } catch (e) {
            logger.error(e);
        }
        return null;
    };

    module.exports = SyncDataHistoryDao;
})();