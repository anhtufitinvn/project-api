(function () {
    let {StyleModel, AutoIncrement} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "RS_Layout_Styles"};

    Dao.autoIncrement = async function() {
        let auto = await AutoIncrement.findOneAndUpdate({name: Dao.name}, {$inc: {seq: 1}});
        if(auto && auto['_doc']) {
            return auto['_doc']['seq'];
        }

        return null;
    };

    Dao.getList = async function ( params = []) {
       
        let name = params['name'] || null;

        let q = {};
        if(name){
            q.name = { $regex: '.*' + name + '.*' ,  $options: 'i'};
        }
        

        let query = StyleModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({createdAt: -1});
        return await query.exec();
    };


    module.exports = Dao;
})();