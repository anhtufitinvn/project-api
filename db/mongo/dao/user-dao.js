(function () {
    let {UserModel, AutoIncrement} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let bcrypt = require('bcrypt');
    let Dao = {name: "RS_Users"};

    Dao.autoIncrement = async function() {
        let auto = await AutoIncrement.findOneAndUpdate({name: Dao.name}, {$inc: {seq: 1}});
        
        if(auto && auto['_doc']) {
            return auto['_doc']['seq'];
        }

        return null;
    };

    Dao.getList = async function (page, limit,  params = []) {
       

        let name = params['name'] || null;

        let q = {};
        if(name){
            q.layout_name = { $regex: '.*' + name + '.*' ,  $options: 'i'};
        }
 
        let query = UserModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({createdAt: -1});
        return await query.exec();
    };

    Dao.getDetail = async function (id) {
        let q = {
            '_id' : id
        };
        if(!isNaN(id)){
            q = {
                'int_id' : parseInt(id)
            };
        }
        let query = UserModel.findOne(q);
        query.collation({locale: "en_US", numericOrdering: true});
        return await query.lean().exec();
    };
    
    
    Dao.create = async function (user) {
        try {
            let seq = await Dao.autoIncrement();
            if(seq) {
                user.int_id = seq;
                let model = new UserModel(user);
                let result = await model.save();
                return result._id;
            }
        } catch (e) {
            console.log(e);
            logger.error(e);
            return null;
        }
    };

    Dao.deleteByID = async function(id) {
        try {
            let q = {
                '_id' : id
            };
            if(!isNaN(id)){
                q = {
                    'int_id' : parseInt(id)
                };
            }
            let rs = await UserModel.deleteOne(q);
            if(rs && rs.deletedCount) {
                return id;
            }

            return null;
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Dao.update = async function (id, user) {
        try {
            let q = {
                '_id' : id
            };
            if(!isNaN(id)){
                q = {
                    'int_id' : parseInt(id)
                };
            }
            let rs = await UserModel.updateOne(q, user);
            return (rs && rs.nModified);
        } catch (e) {
            logger.error(e);
            return false;
        }
    };

    Dao.import = async function (list) {
        let result = {
            imported: 0,
            errors: []
        };

        if (list) {
            try {
                let rs = await UserModel.insertMany(list);
                for (var r of rs) {
                    let user = r['_doc'];
                    if (user) {
                        if (user['_id']) {
                            result.imported += 1;
                        } else {
                            result.errors.push({
                                user_id: user.int_id,
                                user_name: user.user_name
                            });
                        }
                    }
                }
            } catch (e) {
                result.errors = list.map(m => {
                    return {user_id: m.int_id, user_name: m.user_name}
                })
            }
        }

        return result;
    };

    Dao.init = async function (){
        let list = [
            {
                name : "Admin",
                email : "admin@fitin.vn",
                level : 1,
                int_id : 100000,
                password : 'Admin@nimad'
            },
            {
                name : "Consultant",
                email : "consultant@fitin.vn",
                level : 1,
                int_id : 100001,
                password : 'Consultant@^123xyz!'
            }
        ];

        try {
            await UserModel.create(list[0]);
            await UserModel.create( list[1]);
            
        } catch (e) {
            console.log('"error" : ' ,e);
            return false;
        }
    }

    module.exports = Dao;
})();