(function () {
    let {LocationModel} = require('../models');
    let {debug, Logger} = require('../../../core/logger');
    let Dao = {};

    Dao.getList = async function (offset, limit) {
        let query = LocationModel.find();
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({project_name: 1});
        query.skip(offset || 0);
        query.limit(limit || 250);
        return await query.exec();
    };

    Dao.import = async function (list) {
        let result = {
            imported: 0,
            errors: []
        };

        if (list) {
            try {
                let rs = await LocationModel.insertMany(list);
                for (var r of rs) {
                    let location = r['_doc'];
                    if(location) {
                        if (location['_id']) {
                            result.imported += 1;
                        } else {
                            result.errors.push({
                                city_id: location.city_id,
                                city_name: location.city_name
                            });
                        }
                    }
                }
            } catch (e) {
                result.errors = list.map(m => {
                    return {city_id: m.city_id, city_name: m.city_name}
                })
            }
        }

        return result;
    };

    module.exports = Dao;
})();