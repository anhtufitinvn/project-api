(function () {
    let {ProjectModel, AutoIncrement} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "CT_Projects"};

    Dao.autoIncrement = async function () {
        let auto = await AutoIncrement.findOneAndUpdate({name: Dao.name}, {$inc: {seq: 1}});
        if (auto && auto['_doc']) {
            return auto['_doc']['seq'];
        }

        return null;
    };

    Dao.getList = async function (page, limit, params = []) {
        let projectId = params.project_id;
        let name = params.name;
        let city_id = params.city_id;
        let district_id = params.district_id;
        let status = params.status;
        let is_renovation = params.is_renovation;
        let q = {};
        if (name) {
            q["$text"] = {$search: name};
        }
        if (!isNaN(projectId)) {
            q["int_id"] = projectId;
        }
        if (city_id) {
            q["location.city_id"] = parseInt(city_id);
        }
        if (district_id) {
            q["location.district_id"] = parseInt(district_id);
        }
        if (status) {
            q["status"] = status.toLowerCase();
        }

        if (is_renovation === true) {
            q["is_renovation"] = true;
        }
        if (is_renovation === false) {
            q["is_renovation"] = {"$in": [null, false, "false"]};
        }

        return await ProjectModel.paginate(q, {
            page: page || 1,
            limit: limit || 1000,
            lean: true,
            sort: {sort: -1, createdAt: -1}
        });
    };

    Dao.getDetail = async function (id) {
        let q = {
            '_id': id
        };
        if (!isNaN(id)) {
            q = {
                'int_id': parseInt(id)
            };
        }
        let query = ProjectModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.skip(0);
        query.limit(1);
        return await query.lean().exec();
    };

    Dao.getById = async function (id) {
        let q = {
            '_id': id
        };
        if (!isNaN(id)) {
            q = {
                'int_id': parseInt(id)
            };
        }
        let query = ProjectModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.skip(0);
        query.limit(1);
        let result = await query.lean().exec();
        if (result && result.length) {
            return result[0];
        }

        return null;
    };

    Dao.getPublishedList = async function (offset, limit) {
        let query = ProjectModel.find({status: 'published', is_renovation: {"$in": [null, false, "false"]}});
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort: -1, project_name: 1});
        if (limit && offset) {
            query.skip(offset * limit);
            query.limit(limit);
        } else {
            query.skip(0);
            query.limit(1000);
        }

        return await query.exec();
    };

    Dao.getListRenovation = async function (params = {}) {
        let query = ProjectModel.find({is_renovation: true});
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort: -1, createdAt: -1});

        return await query.exec();
    };

    Dao.create = async function (project) {
        try {
            let seq = await Dao.autoIncrement();
            if (seq) {
                project.int_id = seq;
                let model = new ProjectModel(project);
                let result = await model.save();
                return result;
            }
        } catch (e) {
            logger.error(e.stack);
            return null;
        }
    };

    Dao.deleteByID = async function (id) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }
            let rs = await ProjectModel.deleteOne(q);
            if (rs && rs.deletedCount) {
                return id;
            }

            return null;
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Dao.update = async function (id, project) {
        try {
            let q = {
                '_id': id
            };
            if (!isNaN(id)) {
                q = {
                    'int_id': parseInt(id)
                };
            }
            let rs = await ProjectModel.updateOne(q, project);
            return (rs && rs.nModified);
        } catch (e) {

            logger.error(e);
            return false;
        }
    };

    Dao.import = async function (list) {
        let result = {
            imported: 0,
            errors: []
        };

        if (list) {
            try {
                let rs = await ProjectModel.insertMany(list);
                for (var r of rs) {
                    let project = r['_doc'];
                    if (project) {
                        if (project['_id']) {
                            result.imported += 1;
                        } else {
                            result.errors.push({
                                project_id: project.int_id,
                                project_name: project.project_name
                            });
                        }
                    }
                }
            } catch (e) {
                result.errors = list.map(m => {
                    return {project_id: m.project_id, project_name: m.project_name}
                })
            }
        }

        return result;
    };

    Dao.getByProjectRefId = async function (project_ref_id) {
        let q = {
            'project_ref_id': project_ref_id
        };
        let query = ProjectModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.skip(0);
        query.limit(1);
        let result = await query.lean().exec();
        if (result && result.length) {
            return result[0];
        }

        return null;
    };
    Dao.updateByProjectRefId = async function (project_ref_id, project) {
        try {
            let q = {
                'project_ref_id': project_ref_id
            };
            let data = {
                "$set": {
                    location: project.location,
                    metadata: project.metadata,
                    sort: project.sort,
                    landing_url: project.landing_url,
                    project_name: project.project_name,
                    slug: project.slug,
                    status: project.status,
                    image_path: project.image_path,
                    desc: project.desc,
                    is_renovation: project.is_renovation,
                    attributes: project.attributes,
                }
            };
            let rs = await ProjectModel.updateOne(q, data);
            return (rs && rs.nModified);
        } catch (e) {

            logger.error(e);
            return false;
        }
    };
    Dao.updateImagePathByProjectRefId = async function (projectRefId, imagePath) {
        try {
            let q = {
                'project_ref_id': projectRefId
            };
            let rs = await ProjectModel.updateOne(q, {
                '$set': {
                    image_path: imagePath
                }
            });
            return (rs && rs.nModified);
        } catch (e) {

            logger.error(e);
            return false;
        }
    };
    module.exports = Dao;
})();