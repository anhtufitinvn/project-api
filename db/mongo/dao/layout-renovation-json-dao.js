(function () {
    let {LayoutRenovationJsonModel} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "CT_Layouts_Renovation_Json"};

    Dao.getList = async function (page, limit,  params = []) {
        let q = {};
        return await LayoutRenovationJsonModel.paginate(q, { page: page || 1, limit: limit || 1000 , lean : true, sort : {createdAt : -1}});
    };

    Dao.getDetail = async function (id) {
        let q = {
            '_id' : id
        };
        if(!isNaN(id)){
            q = {
                'layout_int_id' : parseInt(id)
            };
        }
        let query = LayoutRenovationJsonModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.skip(0);
        query.limit(1);
        return await query.lean().exec();
    };

    
    Dao.create = async function (data) {
        try {
           
            //let model = new LayoutRenovationJsonModel(data);
            let q = {
                layout_int_id : data.layout_int_id
            }
            let rs = await LayoutRenovationJsonModel.findOneAndUpdate(q, data, { upsert: true, new: true, setDefaultsOnInsert: true });
            
            return rs._id;
        } catch (e) {
           
            logger.error(e);
            return null;
        }
    };

    

    

    module.exports = Dao;
})();