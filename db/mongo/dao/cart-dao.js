(function () {
    let {CartModel, AutoIncrement} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "CT_Carts"};

    Dao.autoIncrement = async function() {
        let auto = await AutoIncrement.findOneAndUpdate({name: Dao.name}, {$inc: {seq: 1}});
        if(auto && auto['_doc']) {
            return auto['_doc']['seq'];
        }

        return null;
    };

    Dao.getList = async function (page, limit, params = []) {
       
        let q = {};
        return await CartModel.paginate(q, { page: page || 1, limit: limit || 1000 , lean : true, sort : {createdAt : -1}});
    };

    Dao.getDetail = async function (id) {
        let q = {
            '_id' : id
        };
        if(!isNaN(id)){
            q = {
                'int_id' : parseInt(id)
            };
        }
        let query = CartModel.findOne(q);
       
        return await query.lean().exec();
    };

    
    Dao.create = async function (data) {
        try {
            let seq = await Dao.autoIncrement();
            if(seq) {
                data.int_id = seq;
                let model = new CartModel(data);
                let result = await model.save();
                return result;
            }
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Dao.deleteByID = async function(id) {
        try {
            let q = {
                '_id' : id
            };
            if(!isNaN(id)){
                q = {
                    'int_id' : parseInt(id)
                };
            }
            let rs = await CartModel.deleteOne(q);
            if(rs && rs.deletedCount) {
                return id;
            }

            return null;
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Dao.update = async function (id, data) {
        try {
            let q = {
                '_id' : id
            };
            if(!isNaN(id)){
                q = {
                    'int_id' : parseInt(id)
                };
            }
            let rs = await CartModel.findOneAndUpdate(q, data);
            return rs;
        } catch (e) {
            
            logger.error(e);
            return false;
        }
    };
    module.exports = Dao;
})();