(function () {
    let {RoomModel, AutoIncrement} = require('../models');
    let {debug, logger} = require('../../../core/logger');
    let Dao = {name: "CT_Rooms"};

    Dao.autoIncrement = async function() {
        let auto = await AutoIncrement.findOneAndUpdate({name: Dao.name}, {$inc: {seq: 1}});
        
        if(auto && auto['_doc']) {
            return auto['_doc']['seq'];
        }

        return null;
    };

    Dao.getList = async function (page, limit,  params = []) {
        let id = params['room_id'] || null;
        let intID = parseInt(id);
        let layoutId = params['layout_id'] || null;
        let intLayoutId = parseInt(layoutId);

        let name = params['name'] || null;

        let q = {};
        if(name){
            q.layout_name = { $regex: '.*' + name + '.*' ,  $options: 'i'};
        }
        if(intID){
            q.int_id = intID;
        }

        let status = params['status'] || null;
        if(status){
            q.status = status;
        }
        if(intLayoutId){
            q.layout_int_id = intLayoutId;
        }
        return await RoomModel.paginate(q, { page: page || 1, limit: limit || 1000 , lean : true, sort : {createdAt : -1}});
    };

    Dao.getDetail = async function (id) {
        let q = {
            '_id' : id
        };
        if(!isNaN(id)){
            q = {
                'int_id' : parseInt(id)
            };
        }
        let query = RoomModel.find(q);
        query.collation({locale: "en_US", numericOrdering: true});
        query.skip(0);
        query.limit(1);
        return await query.lean().exec();
    };

    Dao.getListByLayoutId = async function (layoutId, offset, limit) {
        let query = RoomModel.find({layout_id : layoutId});
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort : -1 , room_name: 1});
        query.skip(offset || 0);
        query.limit(limit || 1000);
        return await query.exec();
    };

    Dao.getListByLayoutIntId = async function (id, offset, limit) {
        let query = RoomModel.find({layout_int_id : id});
        query.collation({locale: "en_US", numericOrdering: true});
        query.setOptions({lean: true});
        query.sort({sort : -1 , room_name: 1});
        query.skip(offset || 0);
        query.limit(limit || 1000);
        return await query.exec();
    };

    
    Dao.create = async function (room) {
        try {
            let seq = await Dao.autoIncrement();
            if(seq) {
                room.int_id = seq;
                let model = new RoomModel(room);
                let result = await model.save();
                return result._id;
            }
        } catch (e) {
            console.log(e);
            logger.error(e);
            return null;
        }
    };

    Dao.deleteByID = async function(id) {
        try {
            let q = {
                '_id' : id
            };
            if(!isNaN(id)){
                q = {
                    'int_id' : parseInt(id)
                };
            }
            let rs = await RoomModel.deleteOne(q);
            if(rs && rs.deletedCount) {
                return id;
            }

            return null;
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Dao.update = async function (id, room) {
        try {
            let q = {
                '_id' : id
            };
            if(!isNaN(id)){
                q = {
                    'int_id' : parseInt(id)
                };
            }
            let rs = await RoomModel.updateOne(q, room);
            return (rs && rs.nModified);
        } catch (e) {
            logger.error(e);
            return false;
        }
    };

    Dao.import = async function (list) {
        let result = {
            imported: 0,
            errors: []
        };

        if (list) {
            try {
                let rs = await RoomModel.insertMany(list);
                for (var r of rs) {
                    let room = r['_doc'];
                    if (room) {
                        if (room['_id']) {
                            result.imported += 1;
                        } else {
                            result.errors.push({
                                room_id: room.int_id,
                                room_name: room.room_name
                            });
                        }
                    }
                }
            } catch (e) {
                result.errors = list.map(m => {
                    return {room_id: m.int_id, room_name: m.room_name}
                })
            }
        }

        return result;
    };

    module.exports = Dao;
})();