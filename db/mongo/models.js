(function () {
    const mongoose = require('mongoose');
    module.exports = {
        AutoIncrement: mongoose.model('Auto_Increment', require('./schemas/auto-increment-schema')),
        ClientInboxModel: mongoose.model('CT_Client_Inbox', require('./schemas/client-inbox-schema')),
        PhoneSmsSpamModel: mongoose.model('RS_Phones_SMS_Spam', require('./schemas/phone-sms-spam-schema')),
        ProjectModel: mongoose.model('CT_Projects', require('./schemas/project-schema')),
        LocationModel: mongoose.model('RS_Location', require('./schemas/location-schema')),
        LayoutModel: mongoose.model('CT_Layouts', require('./schemas/layout-schema')),
        LayoutRenovationModel: mongoose.model('CT_Layouts_Renovation', require('./schemas/layout-renovation-schema')),
        RoomModel: mongoose.model('CT_Rooms', require('./schemas/room-schema')),
        RoomCategoryModel: mongoose.model('RS_Room_Categories', require('./schemas/room-category-schema')),
        StyleModel: mongoose.model('RS_Layout_Styles', require('./schemas/style-schema')),
        UserModel: mongoose.model('RS_Users', require('./schemas/user-schema')),
        CartModel: mongoose.model('CT_Carts', require('./schemas/cart-schema')),
        LayoutJsonModel: mongoose.model('CT_Layouts_Json', require('./schemas/layout-json-schema')),
        LayoutRenovationJsonModel: mongoose.model('CT_Layouts_Renovation_Json', require('./schemas/layout-renovation-json-schema')),
        SyncDataHistoryModel: mongoose.model('CT_Sync_Data_Histories', require('./schemas/sync-data-history-schema')),
        LogSyncDataModel: mongoose.model('CT_Log_Sync_Data', require('./schemas/log-sync-data-schema')),
    };
})();