(function () {
    let Providers = require('../server/providers');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    
    const UrlServices = require('../services/url-services');
    var StoreService = require('../services/store-services');


    class UploadHandler extends Handler {
        constructor() {
            super();
            this.name = "UploadHandler";
        }

        async uploadImage(req, res, next) {
            StoreService.uploadImageFile(req, res, async (err) => {
                try {
                    if(err) {
                        return res.json(this.makeReturnError(err));
                    }

                    let file = req.file;
                    if(file && file.relativePath) {
                        res.json(this.makeReturnMessage({
                            path : file.relativePath,
                            url :  UrlServices.resolveRelativePathLink(file.relativePath)
                        }));
                    } else {
                        return res.json(this.makeReturnError('Error when trying to upload'));
                    }
                } catch (e) {
                    return res.json(this.makeReturnError(e));
                }
            });
        };
    }

    module.exports = new UploadHandler();
})();