(function () {
    let Providers = require('../server/providers');
    let Handler = require('../core/handler');
    let jwt = require('jsonwebtoken');
    let {UserModel} = require('../db/mongo/models');
    let RedisService = Providers.RedisService;
    const JWT_SECRET = global.configuration.JWT_SECRET;
    const EXPIRE_TIME = 84600;
    class AuthHandler extends Handler {
        constructor() {
            super();
            this.name = "AuthHandler";
        }

       
        async login(req, res, next) {
            
            var email = req.body.email;
            var password = req.body.password;

            // find
            let user = await UserModel.findOne({
                'email': email
            });
            if (!user) {
                res.json(this.makeReturnError("User not exist"));
            } 

            if (user && user.comparePassword(password)) {
                var payload = {email: user.email};
                var jwtToken = jwt.sign(payload,JWT_SECRET, { expiresIn: EXPIRE_TIME });
                
                RedisService.setAsync(jwtToken, JSON.stringify(user),EXPIRE_TIME);
                
                var jsonResponse = {'access_token': jwtToken}
                res.json(this.makeReturnMessage(jsonResponse));
            } 
            else {
                res.json(this.makeReturnError("Login Error. Please check your credential"));
            }
           
        };
    }

    module.exports = new AuthHandler();
})();