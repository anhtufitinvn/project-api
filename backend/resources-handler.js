(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    let Handler = require('../core/handler');
    let DataServiceV2 = Providers.DataServiceV2;
    let Transform = require('../transform/transforms');
    let BackendDataService = Providers.BackendDataService;
    let TransformBackend = require('../transform/backend/index');

    class ResourcesHandler extends Handler {
        constructor() {
            super();
            this.name = "ResourcesHandler";
        }


        async importLocationFromMysql(req, res, next) {
            let result = await SynService.importLocationFromMysql();
            res.json(this.makeReturnMessage(result));
        };

        async getLocations(req, res, next) {
            let result = await global.locations;
            res.json(this.makeReturnMessage(result));
        };

        async getStyles(req, res, next) {
            
            let data = await BackendDataService.Styles.getStyleList(req.query);
            
            let result_data = TransformBackend.styles.list(data);
            let result = {
                list : result_data,
            }
            res.json(this.makeReturnMessage(result));
        };


        async getRoomCategories(req, res, next) {
            
            let data = await BackendDataService.RoomCategories.getRoomCategoryList(req.query);
            
            let result_data = TransformBackend.roomCategories.list(data);
            let result = {
                list : result_data,
            }
            res.json(this.makeReturnMessage(result));
        };

        async initRoomCategories(req, res, next) {
            
            let rs = await BackendDataService.RoomCategories.init();
           
            res.json(this.makeReturnMessage(rs));
        };
    }

    module.exports = new ResourcesHandler();
})();