(function () {
    module.exports = {
        SampleHandler : require('./sample-handler'),
        ProjectsHandler : require('./projects-handler'),
        LayoutsHandler : require('./layouts-handler'),
        LayoutsRenovationHandler : require('./layouts-renovation-handler'),
        RoomsHandler : require('./rooms-handler'),
        ResourcesHandler : require('./resources-handler'),
        UploadHandler : require('./upload-handler'),
        UsersHandler : require('./users-handler'),
        AuthHandler : require('./auth-handler'),
        CartsHandler : require('./carts-handler'),
        LayoutsJsonHandler : require('./layouts-json-handler'),
        LayoutsRenovationJsonHandler : require('./layouts-renovation-json-handler'),
        PanoramaHandler : require('./panorama-handler')
    }
})();