(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let BackendDataService = Providers.BackendDataService;
    let TransformBackend = require('../transform/backend/index');

    class RoomsHandler extends Handler {
        constructor() {
            super();
            this.name = "RoomsHandler";
        }

        async getRoomListByProjectId(req, res, next) {
            let page = req.query['page'] || null;
            let projectId = req.params['project_id'] || null;
            let intID = parseInt(projectId);
            let data = null;
            if(isNaN(intID)) {
                // search by string
                data = await DataServiceV2.Rooms.getRoomListByProjectId(projectId, page);
            } else {
                // search by number
                data = await DataServiceV2.Rooms.getRoomListByProjectIntId(intID);
            }

            res.json(this.makeReturnMessage(data));
        };

        async getRooms(req, res, next) {
            let data = await BackendDataService.Rooms.getRoomList(req.query);
            let result_data = TransformBackend.rooms.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            }
            res.json(this.makeReturnMessage(result));

        };

        async getRoom(req, res, next) {
            let roomId = req.params['room_id'];
            
            let data = await BackendDataService.Rooms.getRoomById(roomId);

            res.json(this.makeReturnMessage(data));
        };

        async createRoom(req, res, next) {
            let extensions = req.body.extensions || {};
            let metadata = req.body.metadata || {};
            
            let room = {
                room_name : req.body['room_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['room_name']),
                image_path : req.body['image_path'] || null,
                desc : req.body['desc'] || null,
                area : req.body['area'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                room_json : req.body['room_json'] || [],
                room_id : req.body['room_id'] || null,
                layout_name : req.body['layout_name'] || null,
                layout_id : req.body['layout_id'] || null,
                layout_int_id : req.body['layout_int_id'] || null,
                image_path : req.body['image_path'] || null,
                image_floor_plan_path : req.body['image_floor_plan_path'] || null,
                room_category_key : req.body['room_category_key'] || null,
                room_category_name : req.body['room_category_name'] || null,
                metadata : {
                    title : metadata.title || null,
                    intro : metadata.intro || null,
                  
                    price : metadata.price || {estimated : 0, max:0, min:0}
                },
                attributes : req.body.attributes,
                extensions : {
                    cart_json : extensions.cart_json || {},
                    panorama : extensions.panorama || { enable :0 , url :null},
                    services_fee : extensions.services_fee || []
                },
                sort : req.body.sort || 0
            };

            
            let rs = await BackendDataService.Rooms.create(room);
            if(rs) {
                room.room_id = rs;
                res.json(this.makeReturnMessage(
                    {
                        room_id : room.room_id,
                        int_id : room.int_id,
                        room_name : room.room_name
                    }
                    ));
            } else {
                res.json(this.makeReturnError("failed to insert room"));
            }

        };

        async updateRoom(req, res, next) {
            let roomId = req.params['room_id'];
            let extensions = req.body.extensions || {};
            let metadata = req.body.metadata || {};
            
            let room = {
                room_name : req.body['room_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['room_name']),
                image_path : req.body['image_path'] || null,
                desc : req.body['desc'] || null,
                area : req.body['area'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                room_json : req.body['room_json'] || [],
                room_id : req.body['room_id'] || null,
                layout_name : req.body['layout_name'] || null,
                layout_id : req.body['layout_id'] || null,
                layout_int_id : req.body['layout_int_id'] || null,
                image_path : req.body['image_path'] || null,
                image_floor_plan_path : req.body['image_floor_plan_path'] || null,
                room_category_key : req.body['room_category_key'] || null,
                room_category_name : req.body['room_category_name'] || null,
                metadata : {
                    title : metadata.title || null,
                    intro : metadata.intro || null,
                   
                    price : metadata.price || {estimated : 0, max:0, min:0},
                   
                },
                attributes : req.body.attributes,
                extensions : {
                    cart_json : extensions.cart_json || {},
                    panorama : extensions.panorama || { enable :0 , url :null},
                    services_fee : extensions.services_fee || []
                },
                sort : req.body.sort || 0
            };

            
            let rs = await BackendDataService.Rooms.update(roomId, room);
            if(rs) {
                let response = {
                    id : roomId,
                    room_name : room.room_name
                }
                res.json(this.makeReturnMessage(response));
            } else {
                res.json(this.makeReturnError("failed to update room"));
            }
        };

        async deleteRoomByID(req, res, next) {
            let id = req.params['room_id'];
            let rs = await BackendDataService.Rooms.deleteByID(id);
            if(rs) {
                res.json(this.makeReturnMessage({room_id : rs}));
            } else {
                res.json(this.makeReturnError(`could not delete room id : ${id}`));
            }
        };

        async importAllRoomsFromMySQL(req, res, next) {
            let result = await SynService.importRoomsFromMysql();
            res.json(this.makeReturnMessage(result));
        };
    }

    module.exports = new RoomsHandler();
})();