

(function () {
    var fs = require('fs-extra');
    var crypto = require('crypto');
    var {debug, logger} = require('../core/logger');
    //var controller = Object.create(require('./controller'));
    var storeService = require('../services/store-services');
    var urlServices = require('../services/url-services');
    var ecomService = require('../services/ecom-services');
    var dataService = require('../services/data-services');
    var renderService = require('../services/render-services');
    var cartService = require('../services/cart-services');
    var moment = require('moment');

    let Providers = require('../server/providers');
    let BackendDataService = Providers.BackendDataService;

    var Handler = require('../core/handler');

    class PanoramaHandler extends Handler {
        constructor() {
            super();
            this.name = "PanoramaHandler";
        }

        async uploadZipFile4Tool (req, res, next) {
            let self = this;

            storeService.uploadPanoramaZipFile4CMS(req, res, (async (err) => {
               
                try {
                    if (err) {
                        res.json(self.makeReturnError(err.message || "unzip failed", -1));
                        return;
                    }
        
                    let result = await this.unzipPipeline4Tool(req);
                    res.json(self.makeReturnMessage(result));
                } catch (e) {
                    res.json(self.makeReturnError(e, -1));
                    logger.error("uploadZipFile got exception ", e);
                }
            }));
        };

        async uploadZipFile4CMS (req, res, next) {
            let self = this;

            storeService.uploadPanoramaZipFile4CMS(req, res, (async (err) => {
               
                try {
                    if (err) {
                        res.json(self.makeReturnError(err.message || "unzip failed", -1));
                        return;
                    }
        
                    let result = await this.unzipPipeline4Tool(req);
                    res.json(self.makeReturnMessage(result));
                } catch (e) {
                    res.json(self.makeReturnError(e, -1));
                    logger.error("uploadZipFile got exception ", e);
                }
            }));
        };

        async uploadZipFile4CMSLayout (req, res, next) {
            let self = this;

            storeService.uploadPanoramaZipFile4CMS(req, res, (async (err) => {
               
                try {
                    if (err) {
                        res.json(self.makeReturnError(err.message || "unzip failed", -1));
                        return;
                    }
        
                    let result = await this.unzipPipeline4Tool(req, 'layout');
                    res.json(self.makeReturnMessage(result));
                } catch (e) {
                    res.json(self.makeReturnError(e, -1));
                    logger.error("uploadZipFile got exception ", e);
                }
            }));
        };
        
        async unzipPipeline4Tool(req, type = null) {
            
            let zipFile = req.file;
            
            if (zipFile.absolutePath === undefined) {
                throw new Error("Upload file got an exception");
            }
            
            const layoutId = zipFile.layout_id || req.body.layout_id;
            let UUID = zipFile.uuid;
            let now = moment();
            const sYear = now.year().toString();
            const md = now.format('M-D');
            let unzip = await storeService.unzip4CMS(zipFile.absolutePath, urlServices.join(sYear, md));
            let files = await storeService.getFilesInDirectory(unzip);
            if (files) {
                // get config.json
                let jsonFile = files.filter(f => {
                    return f.endsWith("config.json");
                });
                
                if (jsonFile === null || jsonFile.length === 0) {
                    throw new Error("panorama config.json not found");
                }

                let panorama = await this.createPanoramaConfigFromJsonFile(jsonFile[0]);
                if (panorama && panorama.file_name) {
                    let ticket = `${sYear}/${md}/${UUID}/${panorama.file_name}`;
                    debug("ticket", ticket);
                    ticket = Buffer.from(ticket).toString('base64');
                    let panoramaUrl = urlServices.resolvePanoramaLink(ticket);
                    if(type === 'layout'){
                        await BackendDataService.Layouts.updatePanoramaLink(layoutId, panoramaUrl);
                    }else{
                        await BackendDataService.LayoutsRenovation.updatePanoramaLink(layoutId, panoramaUrl);
                    }
                    
                    return {panorama_url: panoramaUrl};
                }

                throw new Error("unzip failed, files empty");
            }

            throw new Error("unzip failed");
        }




        async  createPanoramaConfigFromJsonFile(jsonConfigFile) {
            let parentDir = urlServices.dirname(jsonConfigFile);
            if (!await fs.pathExists(jsonConfigFile)) {
                throw new Error(file + " does not exist");
            }
        
            let template = await fs.readJson(jsonConfigFile);
            if (template && template['scenes']) {
                let scenes = template['scenes'];
                let views = Object.keys(scenes);
                if (views) {
                    let index = 0;
                    for (let i = 0, N = views.length; i < N; i++) {
                        index = index + 1;
                        //// resolve image url : convert image relative path to image url
                        scenes[views[i]]['panorama'] = urlServices.resolvePanoramaImageUrl(urlServices.join(parentDir, `${index}.jpg`));
                        debug(scenes[views[i]]['panorama']);
                    }
                }
        
                let rename = dataService.generateObjectId() + '.json';
                let panoPath = urlServices.join(parentDir, `${rename}`);
                debug(panoPath);
                await fs.writeJson(panoPath, template);
                return {file_name: `${rename}`, file_path: panoPath};
            } else {
                throw new Error(file + " was not a json format");
            }
        }



        async uploadCartJson4Renovation (req, res, next) {
    
            storeService.uploadCartJson4Renovation(req, res, (async (err) => {
                
                try {
                    if (err) {
                        res.json(this.makeReturnError(err.message || "upload failed", -1));
                        return;
                    }
                    let file = req.file;
            
                    let result = await this.handleJsonFile4Renovation(file);

                    res.json(this.makeReturnMessage(result));
                } catch (e) {
                    res.json(this.makeReturnError(e, -1));
                    logger.error("upload got exception ", e);
                }
            }));
        };
        
        async handleJsonFile4Renovation(file){
            let data_json = await storeService.readFileJson(file.path);
            
            let layout_id = file.layout_id;
            
            let payload = {
                email: data_json.client.email,
                panorama_url: null,
                cart: null,
                metadata: data_json.metadata
            };
           
            let cart = await cartService.buildCartFromJsonFile(file.path, payload);
            
            if (cart) {
                payload.cart = cart;
                let record_id = await BackendDataService.LayoutsRenovation.addCart(data_json, cart, layout_id);
                if (record_id) {
                    return {
                        //id: record_id,
                        signature: cart.cart_sign,
                        cart_url: urlServices.resolveCartLink(cart.cart_sign)
                    };
                }else{
                    throw new Error("failed to add cart to layout renovation");
                }

                
            }else{
                throw new Error("failed to add cart to layout renovation");
            }

        }


        async uploadCartJson4Layout(req, res, next) {
    
            storeService.uploadCartJson4Layout(req, res, (async (err) => {
                
                try {
                    if (err) {
                        res.json(this.makeReturnError(err.message || "upload failed", -1));
                        return;
                    }
                    let file = req.file;
            
                    let result = await this.handleJsonFile4Layout(file);

                    res.json(this.makeReturnMessage(result));
                } catch (e) {
                    res.json(this.makeReturnError(e, -1));
                    logger.error("upload got exception ", e);
                }
            }));
        };

        async handleJsonFile4Layout(file){
            let data_json = await storeService.readFileJson(file.path);
            
            let layout_id = file.layout_id;
            
            let payload = {
                email: data_json.client.email,
                panorama_url: null,
                cart: null,
                metadata: data_json.metadata
            };
           
            let cart = await cartService.buildCartFromJsonFile(file.path, payload);
            
            if (cart) {
                payload.cart = cart;
                let record_id = await BackendDataService.Layouts.addCart(data_json, cart, layout_id);
                if (record_id) {
                    return {
                        //id: record_id,
                        signature: cart.cart_sign,
                        cart_url: urlServices.resolveCartLink(cart.cart_sign)
                    };
                }else{
                    throw new Error("failed to add cart to layout ");
                }

                
            }else{
                throw new Error("failed to add cart to layout ");
            }

        }
    }

    

    module.exports = new PanoramaHandler();
})();