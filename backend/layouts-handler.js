(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    let EcomService = require('../services/ecom-services');
    let CacheService = require('../services/cached-services');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let DataServiceV2 = Providers.DataServiceV2;
    let BackendDataService = Providers.BackendDataService;
    let TransformBackend = require('../transform/backend/index');
    var StoreService = require('../services/store-services');
    let UrlServices = require('../services/url-services');
    let MongoAccess = require('../db/mongo/dbaccess');

    class LayoutsHandler extends Handler {
        constructor() {
            super();
            this.name = "LayoutsHandler";
        }

        async getLayoutListByProjectId(req, res, next) {
            let page = req.query['page'] || null;
            let projectId = req.params['project_id'] || null;
            let intID = parseInt(projectId);
            let data = null;
            if(isNaN(intID)) {
                // search by string
                data = await DataServiceV2.Layouts.getLayoutListByProjectId(projectId, page);
            } else {
                // search by number
                data = await DataServiceV2.Layouts.getLayoutListByProjectIntId(intID);
            }

            res.json(this.makeReturnMessage(data));
        };

        async getLayouts(req, res, next) {
            let query = {
                layout_id : req.query.layout_id,
                name : req.query.name,
                project_id : req.query.project_id,
                estimated_price : req.query.estimated_price,
                bedroom : req.query.bedroom,
                room_type : req.query.room_type,
                layout_key_name : req.query.layout_key_name,
                style : req.query.style,
                limit : req.query.limit,
                page : req.query.page
            };

            let data = await BackendDataService.Layouts.getLayoutList(query);
            let result_data = TransformBackend.layouts.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            }
            res.json(this.makeReturnMessage(result));

        };

        async getLayout(req, res, next) {
            let layoutId = req.params['layout_id'];
            
            let data = await BackendDataService.Layouts.getLayoutById(layoutId);

            res.json(this.makeReturnMessage(data));
        };

        async createLayout(req, res, next) {
            let extensions = req.body.extensions || {};
            let metadata = req.body.metadata || {};
            
            if(req.user){
                metadata.creator = {
                    int_id : req.user.int_id,
                    email :req.user.email
                };
            }

            let layout = {
                layout_name : req.body['layout_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['layout_name']),
                
                desc : req.body['desc'] || null,
                area : req.body['area'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                //layout_json : req.body['layout_json'] || [],
                layout_key_name : req.body['layout_key_name'] || null,
                project_name : req.body['project_name'] || null,
                project_id : req.body['project_id'] || null,
                project_int_id : req.body['project_int_id'] || null,
                image_path : req.body['image_path'] || null,
                image_floor_plan_path : req.body['image_floor_plan_path'] || null,
                
                is_template : req.body.is_template ? true : false,
                metadata : {
                    title : metadata.title || null,
                    intro : metadata.intro || null,
                    style : metadata.style || {name : null, key:null},
                    price : metadata.price || {estimated : 0, max:0, min:0},
                //    rooms : metadata.rooms || [],
                    user_info : metadata.user_info || [],
                    bedroom_num : metadata.bedroom_num || 0,
                    creator : metadata.creator || {},
                    address : metadata.address || {},
                },
                attributes : req.body.attributes,
                extensions : {
                    cart_json : extensions.cart_json || {},
                    panorama : extensions.panorama || { enable :0 , url :null},
                    services_fee : extensions.services_fee || []
                },
                sort : req.body.sort || 0
            };
            let data_rooms = metadata.rooms || [];

            let rs = await BackendDataService.Layouts.create(layout);
            if(rs) {
                //layout.layout_id = rs.layout_id;
                if (data_rooms.length){
                    await BackendDataService.Layouts.addRooms(rs.int_id, data_rooms);
                }

                res.json(this.makeReturnMessage(
                    {
                       
                        id : rs.int_id,
                        layout_name : layout.layout_name
                    }
                    ));
            } else {
                res.json(this.makeReturnError("failed to insert layout"));
            }

        };

        async updateLayout(req, res, next) {
            let layoutId = req.params['layout_id'];
            let extensions = req.body.extensions || {};
            let metadata = req.body.metadata || {};
           
            
            let layout = {
                layout_name : req.body['layout_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['layout_name']),
                
                desc : req.body['desc'] || null,
                area : req.body['area'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                //layout_json : req.body['layout_json'] || [],
                layout_key_name : req.body['layout_key_name'] || null,
                project_name : req.body['project_name'] || null,
                project_id : req.body['project_id'] || null,
                project_int_id : req.body['project_int_id'] || null,
                image_path : req.body['image_path'] || null,
                image_floor_plan_path : req.body['image_floor_plan_path'] || null,
              
                is_template : req.body.is_template ? true : false,
                metadata : {
                    title : metadata.title || null,
                    intro : metadata.intro || null,
                    style : metadata.style || {name : null, key:null},
                    price : metadata.price || {estimated : 0, max:0, min:0},
                //    rooms : metadata.rooms || [],
                    user_info : metadata.user_info || [],
                    bedroom_num : metadata.bedroom_num || 0,
                    creator : metadata.creator || {},
                    address : metadata.address || {}
                },
                attributes : req.body.attributes,
                extensions : {
                    cart_json : extensions.cart_json || {},
                    panorama : extensions.panorama || { enable :0 , url :null},
                    services_fee : extensions.services_fee || []
                },
                sort : req.body.sort || 0
            };

            
            let rs = await BackendDataService.Layouts.update(layoutId, layout);
            if(rs) {
                let response = {
                    id : layoutId,
                    layout_name : layout.layout_name
                }
                res.json(this.makeReturnMessage(response));
            } else {
                res.json(this.makeReturnError("failed to update layout"));
            }
        };

        async updateLayoutKey(req, res, next){
            let layoutId = req.params['layout_id'];
            let layout = {
                layout_key_name : req.body['layout_key_name'],
            };

            
            let rs = await BackendDataService.Layouts.update(layoutId, layout);
            if(rs) {
                let response = {
                    id : layoutId,
                    layout_name : layout.layout_name
                }
                res.json(this.makeReturnMessage(response));
            } else {
                res.json(this.makeReturnError("failed to update layout key name"));
            }
        };

        async deleteLayoutByID(req, res, next) {
            let id = req.params['layout_id'];
            let rs = await BackendDataService.Layouts.deleteByID(id);
            if(rs) {
                res.json(this.makeReturnMessage({layout_id : rs}));
            } else {
                res.json(this.makeReturnError(`could not delete layout id : ${id}`));
            }
        };

        async duplicateLayout(req, res, next) {
            let id = req.body['layout_id'];
            let rs = await BackendDataService.Layouts.duplicate(id);
            if(rs) {
                res.json(this.makeReturnMessage(rs));
            } else {
                res.json(this.makeReturnError(`could not clone to create renovation layout id : ${id}`));
            }
        };


        async addRooms(req, res, next) {
            let layout_id = req.body['layout_id'];
            let data_rooms = req.body.rooms || [];
            let rs = await BackendDataService.Layouts.addRooms(layout_id, data_rooms);
            if(rs) {
                res.json(this.makeReturnMessage({layout_id: layout_id}));
            } else {
                res.json(this.makeReturnError(`could not add room layout id : ${layout_id}`));
            }
        };

        async removeRoom(req, res, next) {
            let layout_id = req.body['layout_id'];
            let room_id = req.body['room_id'];
            
            let rs = await BackendDataService.Layouts.removeRoom(layout_id, room_id);
            if(rs) {
                res.json(this.makeReturnMessage({room_id : room_id}));
            } else {
                res.json(this.makeReturnError(`could not remove room of layout id : ${layout_id}`));
            }
        };

        async addGallery(req, res, next) {
            let layout_id = req.body['layout_id'];
            let data_gallery = req.body.gallery || [];
            let rs = await BackendDataService.Layouts.addGallery(layout_id, data_gallery);
            if(rs) {
                res.json(this.makeReturnMessage({layout_id: layout_id}));
            } else {
                res.json(this.makeReturnError(`could not add gallery of layout id : ${layout_id}`));
            }
        };

        async removeGallery(req, res, next) {
            let layout_id = req.body['layout_id'];
            let gallery_id = req.body['gallery_id'];
            
            let rs = await BackendDataService.Layouts.removeGallery(layout_id, gallery_id);
            if(rs) {
                res.json(this.makeReturnMessage({gallery_id : gallery_id}));
            } else {
                res.json(this.makeReturnError(`could not remove gallery of layout id : ${layout_id}`));
            }
        };

        /** Direct upload gallery by image */
        async uploadGallery(req, res, next){
            StoreService.uploadImageFile(req, res, async (err) => {
                try {
                    if(err) {
                        return res.json(this.makeReturnError(err));
                    }

                    let file = req.file;
                    if(file && file.relativePath) {
                        let layout_id = req.body['layout_id'];
                        let data_gallery = [
                            {
                                "index" : 1,
                                "hidden" : 0,
                                "image_path": file.relativePath,
                                "thumb_path": file.relativePath
                            }
                        ];
                        let rs = await BackendDataService.Layouts.addGallery(layout_id, data_gallery);
                        if(rs) {
                            res.json(this.makeReturnMessage({
                                layout_id : layout_id,
                                path : file.relativePath,
                                url :  UrlServices.resolveRelativePathLink(file.relativePath)
                            }));
                        } 
                        else {
                            return res.json(this.makeReturnError('Error when trying to upload'));
                        }
                        
                    } else {
                        return res.json(this.makeReturnError('Error when trying to upload'));
                    }
                } catch (e) {
                    return res.json(this.makeReturnError(e));
                }
            });
        }

        async importAllLayoutsFromMySQL(req, res, next) {
            let result = await SynService.importLayoutsFromMysql();
            res.json(this.makeReturnMessage(result));
        };

        async importLayoutJsonFromEcom(req, res, next) {
            let query = {
                limit: 50,
                page: req.query.page
            };
            let data = await BackendDataService.Layouts.getLayoutList(query);
            let layouts = data.docs || [];

            var i;
            let result = [];
            for (i = 0; i < layouts.length; i++) {
                let layout_id = layouts[i].int_id;
                let layout = await EcomService.getLayoutById(layout_id);    
                if(layout.layout_json && layout.layout_json.RoomId && !layouts[i].layout_key_name){

                    //UPDATE KEY NAME
                    let update_data_key_name = {
                        layout_key_name : layout.layout_json.RoomId,
                    };
                    await MongoAccess.DAO.Layout.updateKey(layout_id, update_data_key_name);

                    //UPDATE DATAJSON
                    let update_data_json = {
                        data_json : layout.layout_json,
                        layout_int_id : layout_id
                    };
                    
                    await BackendDataService.LayoutsJson.create(update_data_json);
                    result.push(layout_id);
                }
            }
            
            CacheService.Layout.flushAll();

            res.json(this.makeReturnMessage(result));
        };
    }

    module.exports = new LayoutsHandler();
})();