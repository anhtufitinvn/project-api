(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let DataServiceV2 = Providers.DataServiceV2;
    let BackendDataService = Providers.BackendDataService;
    let TransformBackend = require('../transform/backend/index');

    class ProjectsHandler extends Handler {
        constructor() {
            super();
            this.name = "ProjectsHandler";
        }

        async getProjects(req, res, next) {
            let page = req.query['page'];
            //page = Math.max(page, 1);
            let query = {
                project_id : req.query.project_id || NaN,
                name : req.query.name || "",
                city_id : req.query.city_id || NaN,
                district_id : req.query.district_id || NaN,
                status : req.query.status || "",
                is_renovation : false
            };

            let data = await BackendDataService.Projects.getProjectList(page, query);
            let array_int_id = data.docs.map(p => {
                return p.int_id;
            });
            let layouts = await BackendDataService.Layouts.getLayoutListByListProjectId(array_int_id);
            data.docs.map(l => {
                let aggr = layouts.find(x => x._id === l.int_id);
                
                if(aggr)
                    l.layouts_count = aggr.count;
                return l;
            });
            
            let result_data = TransformBackend.projects.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            };

            res.json(this.makeReturnMessage(result));
        };

        async getProjectsRenovation(req, res, next) {
            let page = req.query['page'] || 0;
            page = Math.max(page, 1);
            let query = {
                project_id : req.query.project_id || NaN,
                name : req.query.name || "",
                city_id : req.query.city_id || NaN,
                district_id : req.query.district_id || NaN,
                status : req.query.status || "",
                is_renovation: true
            };

            let data = await BackendDataService.Projects.getProjectList(page, query);

            let result_data = TransformBackend.projects.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            };

            res.json(this.makeReturnMessage(result));
        };

        async getProject(req, res, next) {
            let projectId = req.params['project_id'];
            //let intID = parseInt(projectId);
            // if(isNaN(intID)) {
            //     // search by string
            // } else {
            //     // search by number
            // }
            let data = await BackendDataService.Projects.getProjectById(projectId);
            res.json(this.makeReturnMessage(data));
        };

        async createProject(req, res, next) {
            let location = req.body.location || {};
            let metadata = req.body.metadata || {};
            let locations = global.locations;
            if(req.user){
                metadata.creator = {
                    int_id : req.user.int_id,
                    email :req.user.email
                };
            }
            let project = {
                project_name : req.body['project_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['project_name']),
                image_path : req.body['image_path'] || null,
                desc : req.body['desc'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                is_renovation : req.body.is_renovation ? true : false,
                sort : req.body.sort || 0,
                landing_url : req.body.landing_url || null,
                location : {
                    city_id : location['city_id'] || "79",
                    district_id : location['district_id'] || null,
                    lat : location['lat'] || null,
                    long : location['long'] || null,
                    address : location['address'] || null,
                },
                metadata : metadata,
                attributes : req.body.attributes
            };

            let city = locations.getCity(project.location.city_id);
            if(city) {
                project.location.city_name = city.name;
            }

            let district = locations.getDistrict(project.location.district_id);
            if(district) {
                project.location.district_name = district.name;
            }

            let rs = await BackendDataService.Projects.create(project);
            if(rs) {
                //project.project_id = rs;
                res.json(this.makeReturnMessage(
                    {
                        //project_id : project.project_id,
                        id : project.int_id,
                        project_name : project.project_name
                    }
                    ));
            } else {
                res.json(this.makeReturnError("failed to insert project"));
            }

        };

        async updateProject(req, res, next) {
            let projectId = req.params['project_id'];
            let location = req.body.location || {};
            let locations = global.locations;
            location = {
                city_id : location['city_id'] || "79",
                district_id : location['district_id'] || null,
                lat : location['lat'] || null,
                long : location['long'] || null,
                address : location['address'] || null
            };

            let project = {
                project_name : req.body['project_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['project_name']),
                desc : req.body['desc'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                is_renovation : req.body.is_renovation ? true : false,
                sort : req.body.sort || 0,
                landing_url : req.body.landing_url || null,
                image_path : req.body['image_path'] || null,
                metadata : req.body.metadata,
                attributes : req.body.attributes
            };

            let city = locations.getCity(location.city_id);
            if(city) {
                location.city_name = city.name;
            }

            let district = locations.getDistrict(location.district_id);
            if(district) {
                location.district_name = district.name;
            }

            project.location = location;
            let rs = await BackendDataService.Projects.update(projectId, project);
            if(rs) {
                let response = {
                    id : projectId,
                    project_name : project.project_name
                }
                res.json(this.makeReturnMessage(response));
            } else {
                res.json(this.makeReturnError("failed to update project"));
            }
        };

        async deleteProjectByID(req, res, next) {
            let id = req.params['project_id'];
            let rs = await BackendDataService.Projects.deleteByID(id);
            if(rs) {
                res.json(this.makeReturnMessage({project_id : rs}));
            } else {
                res.json(this.makeReturnError(`could not delete project id : ${id}`));
            }
        };

        async importAllProjectsFromMySQL(req, res, next) {
            let result = await SynService.importProjectsFromMysql();
            res.json(this.makeReturnMessage(result));
        };
    }

    module.exports = new ProjectsHandler();
})();