(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    var cartService = require('../services/cart-services');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    
    let ENUMS = global.ENUMS;
    let BackendDataService = Providers.BackendDataService;
    let TransformBackend = require('../transform/backend/index');

    class CartsHandler extends Handler {
        constructor() {
            super();
            this.name = "CartsHandler";
        }

        async getCarts(req, res, next) {
            let query = req.query;
            let data = await BackendDataService.Carts.getCartList(query);
           
            let result_data = TransformBackend.carts.list(data.docs);
            
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            }
            res.json(this.makeReturnMessage(result));

        };

        async getCart(req, res, next) {
            let cartId = req.params['cart_id'];
            
            let data = await BackendDataService.Carts.getCartById(cartId);

            res.json(this.makeReturnMessage(data));
        };

        async createCart(req, res, next) {
            
            let data_json = {
                metadata : req.body.metadata || {},
                client : req.body.client || {},
                carts: {
                    areas :  (req.body.carts) ? req.body.carts.areas : [],
                    matDatas : (req.body.carts) ? req.body.carts.matDatas : []
                },
            }
            
            let payload = {
                email: data_json.client.email,
                panorama_url: null,
                cart: null,
                metadata: data_json.metadata
            };

            let cart = await cartService.buildCartFromJson(data_json, payload);
            
            if (cart) {
                let cart_data = {
                    original_json : data_json,
                    metadata : {
                        creator : {}
                    },
                    cart_json : {
                        items : cart.items || [],
                        cart_sign : cart.cart_sign || ""
                    }
                };
    
                if(req.user){
                    cart_data.metadata.creator = {
                        int_id : req.user.int_id,
                        email :req.user.email
                    };
                }
                let rs = await BackendDataService.Carts.create(cart_data);
                if(rs) {
                    res.json(this.makeReturnMessage(
                        {
                            id : rs.int_id,
                        }
                        ));
                } else {
                    res.json(this.makeReturnError("failed to insert cart"));
                }

            }else{
                res.json(this.makeReturnError("failed to create cart"));
            }

            
        };

        async updateCart(req, res, next) {
            let cart_id = req.params['cart_id'];
            let data_json = {
                metadata : req.body.metadata || {},
                client : req.body.client || {},
                carts: {
                    areas :  (req.body.carts) ? req.body.carts.areas : [],
                    matDatas : (req.body.carts) ? req.body.carts.matDatas : []
                },
            }
            let cart_sign = req.body.cart_sign;
            let payload = {
                email: data_json.client.email,
                panorama_url: null,
                cart: null,
                metadata: data_json.metadata
            };

            let cart = await cartService.buildCartFromJson(data_json, payload, cart_sign);
            
            if (cart) {
                let cart_data = {
                    original_json : data_json,
                    metadata : {
                        creator : {}
                    },
                    cart_json : {
                        items : cart.items || [],
                        cart_sign : cart.cart_sign || ""
                    }
                };
    
                if(req.user){
                    cart_data.metadata.creator = {
                        int_id : req.user.int_id,
                        email :req.user.email
                    };
                }
                let rs = await BackendDataService.Carts.update(cart_id, cart_data);
                if(rs) {
                    res.json(this.makeReturnMessage(
                        {
                            id : rs.int_id,
                        }
                        ));
                } else {
                    res.json(this.makeReturnError("failed to insert cart"));
                }

            }else{
                res.json(this.makeReturnError("failed to create cart"));
            }
        };

        async deleteCartByID(req, res, next) {
            let id = req.params['cart_id'];
            let rs = await BackendDataService.Carts.deleteByID(id);
            if(rs) {
                res.json(this.makeReturnMessage({cart_id : rs}));
            } else {
                res.json(this.makeReturnError(`could not delete cart id : ${id}`));
            }
        };

    }

    module.exports = new CartsHandler();
})();