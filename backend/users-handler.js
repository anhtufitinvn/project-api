(function () {
    let Providers = require('../server/providers');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let BackendUserService = Providers.BackendUserService;
    let TransformBackend = require('../transform/backend/index');

    class UsersHandler extends Handler {
        constructor() {
            super();
            this.name = "UsersHandler";
        }

       
        async initUsers(req, res, next) {
            
            let result = await BackendUserService.Users.init();
            res.json(this.makeReturnMessage(result));
        };

        async getUsers(req, res, next) {
            
            let data = await BackendUserService.Users.getUserList(req.query);
            
            let result_data = TransformBackend.users.list(data);
            let result = {
                list : result_data,
            }
            res.json(this.makeReturnMessage(result));
        };

        async getUser(req, res, next) {
            let user_id = req.params['user_id'];
            
            let data = await BackendUserService.Users.getUserById(user_id);
           
            res.json(this.makeReturnMessage(data));
        };
    }

    module.exports = new UsersHandler();
})();