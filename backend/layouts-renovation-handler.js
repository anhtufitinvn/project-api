(function () {
    let Providers = require('../server/providers');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let DataServiceV2 = Providers.DataServiceV2;
    let BackendDataService = Providers.BackendDataService;
    let TransformBackend = require('../transform/backend/index');
    const UrlServices = require('../services/url-services');
    var StoreService = require('../services/store-services');

    class LayoutsRenovationHandler extends Handler {
        constructor() {
            super();
            this.name = "LayoutsRenovationHandler";
        }

        async getLayoutListByProjectId(req, res, next) {
            let page = req.query['page'] || null;
            let projectId = req.params['project_id'] || null;
            let intID = parseInt(projectId);
            let data = null;
            if(isNaN(intID)) {
                // search by string
                data = await DataServiceV2.Layouts.getLayoutListByProjectId(projectId, page);
            } else {
                // search by number
                data = await DataServiceV2.Layouts.getLayoutListByProjectIntId(intID);
            }

            res.json(this.makeReturnMessage(data));
        };

        async getLayouts(req, res, next) {
            let query = {
                layout_id : req.query.layout_id,
                name : req.query.name,
                project_id : req.query.project_id,
                estimated_price : req.query.estimated_price,
                bedroom : req.query.bedroom,
                room_type : req.query.room_type,
                style : req.query.style,
                approved : req.query.approved,
                page : req.query.page
            };
            let data = await BackendDataService.LayoutsRenovation.getLayoutList(query);
            let result_data = TransformBackend.layoutsRenovation.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            };
            res.json(this.makeReturnMessage(result));

        };

        async getLayouts4External(req, res, next) {
            let query = {
                layout_id : req.query.layout_id,
                name : req.query.name,
                project_id : req.query.project_id,
                estimated_price : req.query.estimated_price,
                bedroom : req.query.bedroom,
                room_type : req.query.room_type,
                style : req.query.style,
                approved : req.query.approved,
                page : req.query.page
            };
            let data = await BackendDataService.LayoutsRenovation.getLayoutList(query);
            let result_data = TransformBackend.layoutsRenovation.list4External(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            };
            res.json(this.makeReturnMessage(result));

        };

        async getLayout(req, res, next) {
            let layoutId = req.params['layout_id'];
            
            let data = await BackendDataService.LayoutsRenovation.getLayoutById(layoutId);

            res.json(this.makeReturnMessage(data));
        };

        async createLayout(req, res, next) {
            let extensions = req.body.extensions || {};
            let metadata = req.body.metadata || {};

            if(req.user){
                metadata.creator = {
                    int_id : req.user.int_id,
                    email :req.user.email
                };
            }
            let layout = {
                layout_name : req.body['layout_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['layout_name']),
                desc : req.body['desc'] || null,
                area : req.body['area'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                //layout_json : req.body['layout_json'] || [],
                layout_key_name : req.body['layout_key_name'] || null,
                project_name : req.body['project_name'] || null,
                project_id : req.body['project_id'] || null,
                project_int_id : req.body['project_int_id'] || null,
                image_path : req.body['image_path'] || null,
                image_floor_plan_path : req.body['image_floor_plan_path'] || null,
                is_template : !!req.body.is_template,
                metadata : {
                    title : metadata.title || null,
                    intro : metadata.intro || null,
                    style : metadata.style || {name : null, key:null},
                    price : metadata.price || {estimated : 0, max:0, min:0},
                //    rooms : metadata.rooms || [],
                    user_info : metadata.user_info || [],
                    bedroom_num : metadata.bedroom_num || 0,
                    creator : metadata.creator || {},
                    address : metadata.address || {},
                    approved : metadata.approved || false
                },
                attributes : req.body.attributes,
                extensions : {
                    //cart_json : extensions.cart_json || {},
                    panorama : extensions.panorama || { enable :0 , url :null},
                    services_fee : extensions.services_fee || []
                },
                sort : req.body.sort || 0
            };
            let data_rooms = metadata.rooms || [];

            let rs = await BackendDataService.LayoutsRenovation.create(layout);
            if(rs) {
                if (data_rooms.length){
                    await BackendDataService.LayoutsRenovation.addRooms(rs.int_id, data_rooms);
                }

                res.json(this.makeReturnMessage(
                    {
                        //layout_id : layout.layout_id,
                        id : rs.int_id,
                        layout_name : layout.layout_name
                    }
                    ));
            } else {
                res.json(this.makeReturnError("failed to insert layout"));
            }

        };

        async updateLayout(req, res, next) {
            let layoutId = req.params['layout_id'];
            let extensions = req.body.extensions || {};
            let metadata = req.body.metadata || {};

            let layout = {
                layout_name : req.body['layout_name'],
                slug : req.body['slug'] || Utils.createSlugFromName(req.body['layout_name']),
                
                desc : req.body['desc'] || null,
                area : req.body['area'] || null,
                status : req.body['status'] || ENUMS.STATUS.PUBLISHED,
                //layout_json : req.body['layout_json'] || [],
                layout_key_name : req.body['layout_key_name'] || null,
                project_name : req.body['project_name'] || null,
                project_id : req.body['project_id'] || null,
                project_int_id : req.body['project_int_id'] || null,
                image_path : req.body['image_path'] || null,
                image_floor_plan_path : req.body['image_floor_plan_path'] || null,
                is_template : !!req.body.is_template,
                metadata : {
                    title : metadata.title || null,
                    intro : metadata.intro || null,
                    style : metadata.style || {name : null, key:null},
                    price : metadata.price || {estimated : 0, max:0, min:0},
               //     rooms : metadata.rooms || [],
                    user_info : metadata.user_info || [],
                    bedroom_num : metadata.bedroom_num || 0,
                    creator : metadata.creator || {},
                    address : metadata.address || {},
                    approved : metadata.approved || false
                },
                attributes : req.body.attributes,
                extensions : {
                    //cart_json : extensions.cart_json || {},
                    panorama : extensions.panorama || { enable :0 , url :null},
                    //services_fee : extensions.services_fee || []
                },
                sort : req.body.sort || 0
            };

            let rs = await BackendDataService.LayoutsRenovation.update(layoutId, layout);
            if(rs) {
                let response = {
                    id : layoutId,
                    layout_name : layout.layout_name
                };

                res.json(this.makeReturnMessage(response));
            } else {
                res.json(this.makeReturnError("failed to update layout"));
            }
        };

        async deleteLayoutByID(req, res, next) {
            let id = req.params['layout_id'];
            let rs = await BackendDataService.LayoutsRenovation.deleteByID(id);
            if(rs) {
                res.json(this.makeReturnMessage({layout_id : rs}));
            } else {
                res.json(this.makeReturnError(`could not delete layout id : ${id}`));
            }
        };

        async duplicateLayout(req, res, next) {
            let id = req.body['layout_id'];
            let rs = await BackendDataService.LayoutsRenovation.duplicate(id);
            if(rs) {
                res.json(this.makeReturnMessage(rs));
            } else {
                res.json(this.makeReturnError(`could not duplicate layout id : ${id}`));
            }
        };



        async addRooms(req, res, next) {
            let layout_id = req.body['layout_id'];
            let data_rooms = req.body.rooms || [];
            let rs = await BackendDataService.LayoutsRenovation.addRooms(layout_id, data_rooms);
            if(rs) {
                res.json(this.makeReturnMessage({layout_id: layout_id}));
            } else {
                res.json(this.makeReturnError(`could not add room layout id : ${layout_id}`));
            }
        };

        async removeRoom(req, res, next) {
            let layout_id = req.body['layout_id'];
            let room_id = req.body['room_id'];
            
            let rs = await BackendDataService.LayoutsRenovation.removeRoom(layout_id, room_id);
            if(rs) {
                res.json(this.makeReturnMessage({room_id : room_id}));
            } else {
                res.json(this.makeReturnError(`could not remove room of layout id : ${layout_id}`));
            }
        };


        /** Add gallery type array path  [{}, {}] */
        async addGallery(req, res, next) {
            let layout_id = req.body['layout_id'];
            let data_gallery = req.body.gallery || [];
            let rs = await BackendDataService.LayoutsRenovation.addGallery(layout_id, data_gallery);
            if(rs) {
                res.json(this.makeReturnMessage({layout_id: layout_id}));
            } else {
                res.json(this.makeReturnError(`could not add gallery of layout id : ${layout_id}`));
            }
        };

        /** Remove gallery by _id */
        async removeGallery(req, res, next) {
            let layout_id = req.body['layout_id'];
            let gallery_id = req.body['gallery_id'];
            
            let rs = await BackendDataService.LayoutsRenovation.removeGallery(layout_id, gallery_id);
            if(rs) {
                res.json(this.makeReturnMessage({gallery_id : gallery_id}));
            } else {
                res.json(this.makeReturnError(`could not remove gallery of layout id : ${layout_id}`));
            }
        };

        /** Direct upload gallery by image */
        async uploadGallery(req, res, next){
            StoreService.uploadImageFile(req, res, async (err) => {
                try {
                    if(err) {
                        return res.json(this.makeReturnError(err));
                    }

                    let file = req.file;
                    if(file && file.relativePath) {
                        let layout_id = req.body['layout_id'];
                        let data_gallery = [
                            {
                                "index" : 1,
                                "hidden" : 0,
                                "image_path": file.relativePath,
                                "thumb_path": file.relativePath
                            }
                        ];
                        let rs = await BackendDataService.LayoutsRenovation.addGallery(layout_id, data_gallery);
                        if(rs) {
                            res.json(this.makeReturnMessage({
                                layout_id : layout_id,
                                path : file.relativePath,
                                url :  UrlServices.resolveRelativePathLink(file.relativePath)
                            }));
                        } 
                        else {
                            return res.json(this.makeReturnError('Error when trying to upload'));
                        }
                        
                    } else {
                        return res.json(this.makeReturnError('Error when trying to upload'));
                    }
                } catch (e) {
                    return res.json(this.makeReturnError(e));
                }
            });
        }

    }

    module.exports = new LayoutsRenovationHandler();
})();