(function () {
    let Providers = require('../server/providers');
    let Transport = require('../server/quotation-transporter');
    let T = require('../core/handler-object');
    let SampleHandler = Object.create(T("SampleHandler", __filename));

    SampleHandler.dump = async function (req, res, next) {
        res.json(this.makeReturnMessage(Providers.MockupService.dump()));
    }.bind(SampleHandler);

    SampleHandler.testWorker = async function(req, res, next) {
        Transport.queue({hnd : 1, action : 1, data : "hello you"});
        res.json(this.makeReturnMessage("done"));
    }.bind(SampleHandler);

    module.exports = SampleHandler;
})();