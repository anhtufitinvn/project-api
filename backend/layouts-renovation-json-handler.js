(function () {
    let Providers = require('../server/providers');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let {debug, logger} = require('../core/logger');
    let ENUMS = global.ENUMS;
    let BackendDataService = Providers.BackendDataService;
    var storeService = require('../services/store-services');
    let TransformBackend = require('../transform/backend/index');

    class LayoutsRenovationJsonHandler extends Handler {
        constructor() {
            super();
            this.name = "LayoutsRenovationJsonHandler";
        }

       
        async getLayoutJson(req, res, next) {
            let layoutId = req.params['layout_id'];
            let result = await BackendDataService.LayoutsRenovationJson.getLayoutJsonByLayoutId(layoutId);
            
            res.json(this.makeReturnMessage(result));
        };

        async postLayoutJson(req, res, next) {
            let layoutId = req.params['layout_id'];
            let data_json = req.body.data_json || {};
            
            let data = {
                data_json : data_json,
                layout_int_id : parseInt(layoutId)
            };
            if(req.user){
                data.creator = {
                    int_id : req.user.int_id,
                    email :req.user.email
                };
            }
            let rs = await BackendDataService.LayoutsRenovationJson.create(data);
            if(rs) {
                let response = {
                    layout_id : layoutId
                }
                res.json(this.makeReturnMessage(response));
            } else {
                res.json(this.makeReturnError("failed to update layout data_json"));
            }
        };

        async uploadLayoutJson(req, res, next) {
            let user = req.user;
            storeService.uploadJson(req, res, (async (err) => {
                
                try {
                    if (err) {
                        res.json(this.makeReturnError(err.message || "upload failed", -1));
                        return;
                    }
                    let file = req.file;
                    let result = await this.handleLayoutJson(file, user);

                    res.json(this.makeReturnMessage(result));
                } catch (e) {
                    res.json(this.makeReturnError(e, -1));
                    logger.error("upload got exception ", e);
                }
            }));
        };

        async handleLayoutJson(file, user){
            let data_json = await storeService.readFileJson(file.path);
            
            let layout_id = file.layout_id;
            
            let data = {
                data_json : data_json,
                layout_int_id : parseInt(layout_id)
            };
            if(user){
                data.creator = {
                    int_id : user.int_id,
                    email :user.email
                };
            }
            let rs = await BackendDataService.LayoutsRenovationJson.create(data);
            
            if(rs) {
                let response = {
                    layout_id : layout_id
                }
                return response;
            } else {
                throw new Error("failed to upload json layout renovation");
            }

        }
    }

    module.exports = new LayoutsRenovationJsonHandler();
})();