(function () {
    let Utils = {};

    Utils.createSlugFromName = function(name) {
        if(name) {
            let words = name.toLowerCase().trim();
            return words.replace(/ /g, "-");
        }

        return null;
    };

    module.exports = Utils;
})();