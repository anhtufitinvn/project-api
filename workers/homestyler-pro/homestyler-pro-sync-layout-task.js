(function () {
    const GetListLayoutAction = require('./get-list-layout-action');

    class TaskSyncLayout {
        constructor(name) {
            this.name = name;
            this.listAction = [];
            this.initAction();
        }

        initAction() {
            this.addAction(new GetListLayoutAction());
        }

        getName() {
            return this.name || __filename;
        }

        addAction(action) {
            if (action) {
                this.listAction.push(action);
            }
        }

        async run() {
            for (let action of this.listAction) {
                await action.run();
            }
        }
    }

    module.exports = TaskSyncLayout;
})();