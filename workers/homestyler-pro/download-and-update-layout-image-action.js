(function () {
    const fse = require('fs-extra');
    const fs = require('fs');
    const request = require('request');

    const BaseAction = require('./base-action');
    let path = require('path');
    const {debug, logger} = require('../../core/logger');
    let ImageService = require('../../services/image-services');
    let MongoAccess = require('../../db/mongo/dbaccess');

    class DownloadAndUpdateLayoutImageAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'DownloadAndUpdateLayoutImageAction';
        }

        async run() {
            if (this.args && this.args.layout) {
                let layout = this.args.layout;
                const url = layout.image_url;
                let image = {};
                debug('downloading image: '  + url);
                let {file_relative_path} = await ImageService.downloadImageByURL(url);
                debug('[OK] downloading image: '  + url + ', file_relative_path: ' + file_relative_path);

                image.file_relative_image = file_relative_path;
                image.file_relative_path_floor_plan = null;
                if (layout.image_floor_plan_url) {
                    let {file_relative_path} = await ImageService.downloadImageByURL(layout.image_floor_plan_url);
                    image.file_relative_path_floor_plan = file_relative_path;
                }
                // update
                await MongoAccess.DAO.Layout.updateImageByLayoutKeyName(layout.layout_key_name, image.file_relative_image, image.file_relative_path_floor_plan);
            }
        }
    }

    module.exports = DownloadAndUpdateLayoutImageAction;
})();