(function () {
    const BaseAction = require('./base-action');
    const {debug, logger} = require('../../core/logger');
    let Providers = require('../../server/providers');

    const MigrateService = Providers.MigrateService;
    let HoproLayoutTransform = require('../../transform/hopro-layout-transform');
    let MongoAccess = require('../../db/mongo/dbaccess');
    let ImageService = require('../../services/image-services');

    class GetListLayoutAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'GetListLayoutAction';
            this.currentPage = 1;
            this.limit = 300;
            this.timestampLastSync = 0;
            this.syncKey = 'sync_ct_layouts';
            this.logSyncLayoutData = {
                success: 1,
                type: null,
                layout_key_name: null,
                data: [], // log sync
            };
            this.logData = {
                total_second: 0,
                total: 0,
                processed: 0,
                count_fail: 0,
                count_success: 0,
                sync_time: 0,
                model: 'CT_Layouts', // create or update
                data: {
                    success: [],
                    fail: [],
                }, // log sync
            };
        }

        async run() {
            let t0 = Date.now();
            try {
                let syncData = await this.updateLastSync();
                this.logData.sync_time = syncData.sync_time;
                let rc = true;
                do {
                    rc = await this.doSyncData();
                    this.currentPage++;
                } while (rc);
            } finally {
                if (this.logData.processed > 0) {
                    this.logData.total_second = (Date.now() - t0) / 1000;
                    await MongoAccess.DAO.LogSyncData.create(this.logData);
                }
            }
        }

        async doSyncData() {
            let hasNext = false;
            try {
                ///// step 1 : load all data
                let listData = await MigrateService.getListLayout(this.currentPage, this.limit, this.timestampLastSync);
                let listLayout = HoproLayoutTransform.list(listData);
                if (listLayout) {
                    if (listLayout.length > 0) {
                        this.logData.total += listLayout.length;
                        for (let layout of listLayout) {
                            try {
                                this.logSyncLayoutData = {
                                    success: 1,
                                    type: null,
                                    layout_key_name: null,
                                    data: [], // log sync
                                };
                                await this.processLayout(layout);
                            } finally {
                                this.logData.processed++;
                                //
                                this.logSyncLayoutData.layout_key_name = layout.layout_key_name;
                                if (this.logSyncLayoutData.success === 1) {
                                    this.logData.count_success++;
                                    this.logData.data.success.push(this.logSyncLayoutData);
                                } else {
                                    this.logData.count_fail++;
                                    this.logData.data.fail.push(this.logSyncLayoutData);
                                }
                            }
                        }
                    } else {
                        if (this.currentPage === 1) {
                            debug('list layout is empty');
                        }
                    }
                } else {
                    if (this.currentPage === 1) {
                        debug('list layout is NULL');
                    }
                }
                hasNext = listLayout.length > 0;
            } catch (e) {
                debug(e);
            }
            return hasNext;
        }

        async processLayout(layout) {
            // insert or update
            let currentLayout = await MongoAccess.DAO.Layout.getByLayoutKeyName(layout.layout_key_name);
            this.logSyncLayoutData.layout_key_name = layout.layout_key_name;
            if (currentLayout) {
                this.logSyncLayoutData.type = 'update';
                this.logSyncLayoutData.project_int_id = layout.project_int_id;
                //
                debug('update current layout', currentLayout.layout_name, currentLayout.layout_key_name);
                await MongoAccess.DAO.Layout.updateByLayoutKeyName(currentLayout.layout_key_name, layout);
                //
                this.logSyncLayoutData.data.push({
                    step: 'upsert',
                    success: 1,
                    layout_int_id: currentLayout.int_id,
                });
                //
                await this.upsertLayoutJson(currentLayout);
                await this.processDownloadImage(layout);
                await this.processDownloadGalleryImage(layout);
                return;
            }
            // insert new layout
            debug('insert new layout', layout.layout_name, layout.layout_key_name);
            // query and set project_int_id
            let parentProject = await MongoAccess.DAO.Project.getByProjectRefId(layout.project_id);
            if (parentProject && parentProject.int_id) {
                this.logSyncLayoutData.type = 'create';
                //
                layout.project_int_id = parentProject.int_id;
                let dbLayout = await MongoAccess.DAO.Layout.create(layout);
                //
                this.logSyncLayoutData.data.push({
                    step: 'upsert',
                    success: 1,
                    layout_int_id: dbLayout.int_id,
                    layout_key_name: dbLayout.layout_key_name,
                    layout_name: dbLayout.layout_name,
                });
                //
                await this.upsertLayoutJson(dbLayout);
                //
                await this.processDownloadImage(layout);
                await this.processDownloadGalleryImage(layout);
            } else {
                this.logSyncLayoutData.success = 0;
                this.logSyncLayoutData.type = 'create';
                //
                this.logSyncLayoutData.data.push({
                    step: 'upsert',
                    success: 0,
                    layout_int_id: null,
                    error: null,
                    err_msg: 'parent project not found: ' + layout.layout_name + ', ' + layout.layout_key_name + ', ' + layout.project_id
                });
                logger.error('insert layout fail. parent project not found: ' + layout.layout_name + ', ' + layout.layout_key_name);
            }
        }

        async processDownloadImage(layout) {
            try {
                // download image
                let image = {file_relative_image: null, file_relative_path_floor_plan: null};
                if (layout.image_url) {
                    debug('downloading image:' + layout.image_url);
                    let {file_relative_path} = await ImageService.downloadImageByURL(layout.image_url);
                    debug('[OK] downloading image: ' + layout.image_url + ', file_relative_path: ' + file_relative_path);
                    image.file_relative_image = file_relative_path;
                }

                if (layout.image_floor_plan_url) {
                    debug("downloading image floor plan: " + layout.image_floor_plan_url);
                    let rc = await ImageService.downloadImageByURL(layout.image_floor_plan_url);
                    debug('[OK] downloading image floor plan: ' + layout.image_floor_plan_url + ', file_relative_path: ' + rc.file_relative_path);
                    image.file_relative_path_floor_plan = rc.file_relative_path;
                }
                // update
                await MongoAccess.DAO.Layout.updateImageByLayoutKeyName(layout.layout_key_name, image.file_relative_image, image.file_relative_path_floor_plan);
                //
                this.logSyncLayoutData.data.push({
                    step: 'download-image-url',
                    success: 1,
                    layout_int_id: layout.int_id,
                    error: null,
                    err_msg: null,
                });
            } catch (e) {
                logger.error('\x1b[41m', e, '\x1b[0m');
                this.logSyncLayoutData.success = 0;
                this.logSyncLayoutData.data.push({
                    step: 'download-image-url',
                    success: 0,
                    layout_int_id: layout.int_id,
                    error: e,
                    err_msg: e.stack,
                });
            }
        }

        async processDownloadGalleryImage(layout) {
            // download image from gallery
            try {
                if (layout.gallery && layout.gallery.length > 0) {
                    let newGallery = [];
                    let success = 1;
                    for (let gallery of layout.gallery) {
                        let images = {
                            image_path: null,
                            thumb_path: null,
                        };
                        try {
                            if (gallery.image_url) {
                                debug('downloading image-url: ' + gallery.image_url);
                                let resultDownloadImageUrl = await ImageService.downloadImageByURL(gallery.image_url);
                                debug('[OK] downloading image-url: ' + gallery.image_url + ', file_relative_path: ' + resultDownloadImageUrl.file_relative_path);
                                //
                                images.image_path = resultDownloadImageUrl.file_relative_path;
                            }
                        } catch (e) {
                            success = 0;
                            logger.error('', e);
                            this.logSyncLayoutData.success = 0;
                            this.logSyncLayoutData.data.push({
                                step: 'download-gallery-image',
                                data: {
                                    image_url: gallery.image_url
                                },
                                success: 0,
                                layout_int_id: layout.int_id,
                                error: e.stack,
                                err_msg: null,
                            });
                        }
                        try {

                            if (gallery.thumb_url) {
                                debug('downloading thumb-url: ' + gallery.thumb_url);
                                let resultDownloadThumbUrl = await ImageService.downloadImageByURL(gallery.thumb_url);
                                debug('[OK] downloading thumb-url: ' + gallery.thumb_url + ', file_relative_path: ' + resultDownloadThumbUrl.file_relative_path);
                                //
                                images.thumb_path = resultDownloadThumbUrl.file_relative_path;
                            }
                        } catch (e) {
                            success = 0;
                            logger.error('', e);
                            this.logSyncLayoutData.success = 0;
                            this.logSyncLayoutData.data.push({
                                step: 'download-gallery-thumb',
                                data: {
                                    thumb_url: gallery.thumb_url
                                },
                                success: 0,
                                layout_int_id: layout.int_id,
                                error: e.stack,
                                err_msg: null,
                            });
                        }
                        newGallery.push({
                            image_path: images.image_path,
                            thumb_path: images.thumb_path,
                            index: gallery['index'],
                            hidden: gallery['hidden'],
                            int_id: gallery['int_id'] || 0,
                        });
                    }
                    // then update
                    await MongoAccess.DAO.Layout.updateGalleryByLayoutKeyName(layout.layout_key_name, newGallery);
                    if (success) {
                        this.logSyncLayoutData.data.push({
                            step: 'download-gallery-image',
                            success: 1,
                            layout_int_id: layout.int_id,
                            error: null,
                            err_msg: null,
                        });
                    }
                }
            } catch (e) {
                logger.error('', e);
                this.logSyncLayoutData.success = 0;
                this.logSyncLayoutData.data.push({
                    step: 'download-gallery',
                    success: 0,
                    layout_int_id: layout.int_id,
                    error: e.stack,
                    err_msg: null,
                });
            }
        }

        async upsertLayoutJson(layout) {
            try {
                // get data
                let layoutJson = await MigrateService.getLayoutDetail(layout.layout_key_name);
                if (!layoutJson) {
                    this.logSyncLayoutData.success = 0;
                    this.logSyncLayoutData.data.push({
                        step: 'update-layout-json',
                        success: 0,
                        layout_int_id: layout.int_id,
                        error: null,
                        err_msg: 'layout-json not found',
                    });
                    logger.error('upsertLayoutJson. layout-json not found: ' + layout.layout_name + ', ' + layout.layout_key_name);
                    return null;
                }
                // insert or update
                let rc = MongoAccess.DAO.LayoutJson.getByLayoutIntId(layout.int_id);
                if (rc && rc.length > 0) {
                    let currentLayoutJson = rc[0];
                    await MongoAccess.DAO.LayoutJson.updateByLayoutIntId(layout.int_id, currentLayoutJson.data_json);
                    this.logSyncLayoutData.data.push({
                        step: 'update-layout-json',
                        success: 1,
                        layout_int_id: layout.int_id,
                        error: null,
                        err_msg: null,
                    });
                    return;
                }
                // insert
                let data = {
                    data_json: layoutJson.data_json,
                    layout_int_id: layout.int_id
                };
                await MongoAccess.DAO.LayoutJson.create(data);
                //
                this.logSyncLayoutData.data.push({
                    step: 'update-layout-json',
                    success: 1,
                    layout_int_id: layout.int_id,
                    error: null,
                });
            } catch (e) {
                this.logSyncLayoutData.success = 0;
                this.logSyncLayoutData.data.push({
                    step: 'update-layout-json',
                    success: 0,
                    layout_int_id: layout.int_id,
                    error: e
                });
            }
        }
    }

    module.exports = GetListLayoutAction;
})();