(function () {
    const {logger, debug} = require('../../core/logger');
    let MongoAccess = require('../../db/mongo/dbaccess');

    class BaseAction {
        constructor(args) {
            this.args = args;
            this.name = 'BaseAction';
            this.timestampLastSync = 0;
            this.syncKey = '';
        }

        getName() {
            return this.name || __filename;
        }

        async updateLastSync() {
            let result = await MongoAccess.DAO.SyncDataHistory.getByKey(this.syncKey);
            let syncData = {};
            if (result && result.length > 0) {
                syncData = result[0];
                this.timestampLastSync = syncData.sync_time;
                syncData.sync_time = Math.floor(Date.now() / 1000);
            } else {
                syncData = {
                    sync_key: this.syncKey,
                    sync_time: Math.floor(Date.now() / 1000)
                }
            }
            await MongoAccess.DAO.SyncDataHistory.upsert(syncData);
            return syncData;
        }
        async run(target) {
            return Promise.resolve(() => {
                debug('BaseAction.run');
            });
        }
    }

    module.exports = BaseAction;
})();