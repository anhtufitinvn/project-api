(function () {
    const BaseAction = require('./base-action');
    const {debug, logger} = require('../../core/logger');
    let Providers = require('../../server/providers');
    let ImportProjectTransporter = require('../../workers/homestyler-pro/import-project-transporter');
    let MongoAccess = require('../../db/mongo/dbaccess');
    let HoproProjectTransform = require('../../transform/hopro-project-transform');

    const MigrateService = Providers.MigrateService;

    class GetListProjectAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'GetListProjectAction';
            this.currentPage = 1;
            this.limit = 10;
            this.timestampLastSync = 0;
            this.syncKey = 'sync_ct_projects';
        }

        async run() {
            await this.updateLastSync();
            let rc = true;
            do {
                rc = await this.doSyncProject();
                this.currentPage++;
            } while (rc);
        }

        async doSyncProject() {
            let hasNext = false;
            try {
                ///// step 1 : load all data
                let listData = await MigrateService.getListProject(this.currentPage, this.limit, this.timestampLastSync);
                let listProject = HoproProjectTransform.list(listData);
                if (listProject) {
                    if (listProject.length > 0) {
                        //// step 2 : Add list to redis
                        listProject.forEach(q => {
                            ImportProjectTransporter.queue({
                                action: 'upsert-project',
                                params: {
                                    project: q
                                }
                            });
                        });
                    } else {
                        if (this.currentPage === 1) {
                            debug('list project is empty');
                        }
                    }
                } else {
                    if (this.currentPage === 1) {
                        debug('list project is NULl');
                    }
                }
                hasNext = listProject.length > 0;
            } catch (e) {
                debug(e);
            }
            return hasNext;
        }
    }

    module.exports = GetListProjectAction;
})();