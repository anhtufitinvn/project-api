(function () {
    const {logger, debug} = require('../../core/logger');

    class Task {
        constructor(name) {
            this.name = name;
            this.listAction = [];
        }

        getName() {
            return this.name || __filename;
        }

        addAction(action) {
            if (action) {
                this.listAction.push(action);
            }
            return this;
        }

        async run() {
            debug('--------------------------------------------------');
            for (let action of this.listAction) {
                debug('[RUN] action: ' + action.getName());
                await action.run(this);
                debug('[END] action: ' + action.getName());
                debug('--------------------------------------------------');
            }
        }
    }

    module.exports = Task;
})();