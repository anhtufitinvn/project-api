(function () {
    const GetListProjectAction = require('./get-list-project-action');

    class TaskSyncProject {
        constructor(name) {
            this.name = name;
            this.listAction = [];
            this.initAction();
        }

        initAction() {
            this.addAction(new GetListProjectAction());
        }

        getName() {
            return this.name || __filename;
        }

        addAction(action) {
            if (action) {
                this.listAction.push(action);
            }
        }

        async run() {
            for (let action of this.listAction) {
                await action.run();
            }
        }
    }

    module.exports = TaskSyncProject;
})();