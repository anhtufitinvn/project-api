(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let MongoAccess = require('../../../db/mongo/dbaccess');
    let Providers = require('../../../server/providers');

    const MigrateService = Providers.MigrateService;
    let HoproLayoutTransform = require('../../../transform/hopro-layout-transform');

    class GetListLayoutAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'GetListLayoutAction';
            this.currentPage = 1;
            this.limit = 100;
            this.timestampLastSync = 0;
            this.syncKey = 'sync_ct_layouts';
        }

        async run(target) {
            this.target = target;
            this.target.resultListLayout = [];
            //
            this.target.lastSyncData = await this.updateLastSync();
            let hasNext = true;
            do {
                debug('get layout. page: ' + this.currentPage + ', limit: ' + this.limit);
                let listData = await MigrateService.getListLayout(this.currentPage, this.limit, this.timestampLastSync);
                let listLayout = HoproLayoutTransform.list(listData);
                hasNext = listLayout.length === this.limit;
                this.currentPage++;
                //
                this.target.resultListLayout = [...this.target.resultListLayout, ...listLayout];
            } while (hasNext);
        }
    }

    module.exports = GetListLayoutAction;
})();