(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let ImageService = require('../../../services/image-services');

    class DownloadLayoutImageUrlAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'DownloadImageUrlAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            // this.target.image_urls.push({
            //     layout_key_name: layout.layout_key_name, // key for update
            //     image_url: layout.image_url,
            //     image_path: null,
            //     image_floor_plan_url: layout.image_floor_plan_url,
            //     image_floor_plan_path: null,
            // });
            //
            for (const image of this.target.image_urls) {
                if (image.image_url) {
                    debug('downloading image:' + image.image_url);
                    // image.image_path = await this.downloadImage('http://localhost:9020/storage/images/05/13/39659e0141e203f597c01f09ca55722f.jpg ') || image.image_path;
                    image.image_path = await this.downloadImage(image.image_url) || image.image_path;
                    debug('[OK] download: ' + image.image_url + ', file_relative_path: ' + image.image_path);
                }
                if (image.image_floor_plan_url) {
                    image.image_floor_plan_path = await this.downloadImage(image.image_floor_plan_url) || image.image_floor_plan_path;
                }
            }
        }

        async downloadImage(image_url) {
            try {
                let {file_relative_path} = await ImageService.downloadImageByURL(image_url);
                this.target.file_relative_image = file_relative_path;
                return file_relative_path;
            } catch (e) {
                logger.error(image_url, e, e.stack);
            }
            return null;
        }

        buildResult() {
            //TODO: implement data can thiet cho next-task
            // update image url
            // await MongoAccess.DAO.Layout.updateImageByLayoutKeyName(layout.layout_key_name, image.file_relative_image, image.file_relative_path_floor_plan);
        }
    }

    module.exports = DownloadLayoutImageUrlAction;
})();