(function () {
    const BaseAction = require('../base-action');
    const CachedService = require('../../../services/cached-services');
    const {debug, logger} = require('../../../core/logger');

    class ClearCacheLayoutAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'ClearCacheLayoutAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            if (this.target.resultListLayout && this.target.resultListLayout.length > 0) {
                debug('clear cache layout: ' + this.target.resultListLayout.length);
                CachedService.Layout.flushAll();
                CachedService.flushAll();
            }
        }

    }

    module.exports = ClearCacheLayoutAction;
})();