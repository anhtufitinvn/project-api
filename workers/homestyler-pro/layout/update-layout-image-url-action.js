(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let MongoAccess = require('../../../db/mongo/dbaccess');

    class UpdateLayoutImageUrlAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'UpdateLayoutImageUrlAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            // this.target.image_urls.push({
            //     layout_key_name: layout.layout_key_name, // key for update
            //     image_url: layout.image_url,
            //     image_path: null,
            //     image_floor_plan_url: layout.image_floor_plan_url,
            //     image_floor_plan_path: null,
            // });
            //
            for (const image of this.target.image_urls) {
                if (image.layout_key_name && (image.image_path || image.image_floor_plan_path)) {
                    // update
                    try {
                        await MongoAccess.DAO.Layout.updateImageByLayoutKeyName(image.layout_key_name, image.image_path, image.image_floor_plan_path);
                    } catch (e) {
                        logger.error('updateImageByLayoutKeyName', image.layout_key_name, e, e.stack);
                    }

                }
            }
        }

    }

    module.exports = UpdateLayoutImageUrlAction;
})();