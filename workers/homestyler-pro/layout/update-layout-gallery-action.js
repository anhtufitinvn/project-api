(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let MongoAccess = require('../../../db/mongo/dbaccess');

    class UpdateLayoutGalleryAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'UpdateLayoutGalleryAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            for (const image of this.target.image_urls) {
                if (image.layout_key_name && (image.image_path || image.image_floor_plan_path)) {
                    // update
                    await MongoAccess.DAO.Layout.updateGalleryByLayoutKeyName(image.layout_key_name, image.gallery);
                }
            }
        }

    }

    module.exports = UpdateLayoutGalleryAction;
})();