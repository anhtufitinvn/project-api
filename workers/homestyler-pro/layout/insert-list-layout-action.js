(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let MongoAccess = require('../../../db/mongo/dbaccess');

    class InsertListLayoutAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'InsertListLayoutAction';
            //
            this.target = null;
        }

        async run(target) {
            this.target = target;
            // result for step download-image-url
            this.target.image_urls = [];
            // then log
            this.target.logData = {
                total_second: 0,
                total: this.target.resultListLayout.length,
                processed: 0,
                count_fail: 0,
                count_success: 0,
                sync_time: this.target.lastSyncData.sync_time,
                model: 'CT_Layouts', // create or update
                data: {
                    success: [],
                    fail: [],
                },
            };
            await MongoAccess.DAO.LogSyncData.create(this.target.logData);
            //
            let listLayout = this.target.resultListLayout;
            if (listLayout) {
                if (listLayout.length > 0) {
                    for (let layout of listLayout) {
                        try {
                            await this.doInsertLayout(layout);
                        } catch (e) {
                            await MongoAccess.DAO.LogSyncData.pushFail(this.target.logData.sync_time, {
                                layout_key_name: layout.layout_key_name,
                                project_id: layout.project_id,
                                step: 'insert',
                                task: this.name,
                                success: 0,
                                msg: e.stack,
                            });
                        }
                    }
                } else {
                    debug('list layout is empty');
                }
            } else {
                debug('list layout is NULl');
            }
        }

        async doInsertLayout(layout) {
            this.buildResult(layout);
            // insert or update
            let currentLayout = await MongoAccess.DAO.Layout.getByLayoutKeyName(layout.layout_key_name);
            if (currentLayout) {
                //
                debug('update current layout', [currentLayout.layout_name, currentLayout.layout_key_name]);
                await MongoAccess.DAO.Layout.updateByLayoutKeyName(currentLayout.layout_key_name, layout);
                // then log
                await MongoAccess.DAO.LogSyncData.pushSuccess(this.target.logData.sync_time, {
                    layout_key_name: layout.layout_key_name,
                    project_id: layout.project_id,
                    step: 'update',
                    task: this.name,
                    success: 1,
                });
                return;
            }
            // insert new layout
            debug('insert new layout', layout.layout_name, layout.layout_key_name);
            // query and set project_int_id
            let parentProject = await MongoAccess.DAO.Project.getByProjectRefId(layout.project_id);
            if (parentProject && parentProject.int_id) {
                layout.project_int_id = parentProject.int_id;
                await MongoAccess.DAO.Layout.create(layout);
                // then log
                await MongoAccess.DAO.LogSyncData.pushSuccess(this.target.logData.sync_time, {
                    layout_key_name: layout.layout_key_name,
                    project_id: layout.project_id,
                    step: 'insert',
                    task: this.name,
                    success: 1,
                });
            } else {
                await MongoAccess.DAO.LogSyncData.pushFail(this.target.logData.sync_time, {
                    layout_key_name: layout.layout_key_name,
                    step: 'insert',
                    task: this.name,
                    success: 0,
                    msg: 'insert layout fail. Parent project not found',
                    project_id: layout.project_id
                });
                logger.error('\x1b[41m [ERR]insert layout fail  \x1b[0m. Parent project not found: ' + layout.layout_name + ', ' + layout.layout_key_name);
            }
        }

        buildResult(layout) {
            this.target.image_urls.push({
                layout_key_name: layout.layout_key_name, // key for update
                image_url: layout.image_url,
                image_path: null,
                image_floor_plan_url: layout.image_floor_plan_url,
                image_floor_plan_path: null,
                gallery: layout.gallery
            });
        }
    }

    module.exports = InsertListLayoutAction;
})();