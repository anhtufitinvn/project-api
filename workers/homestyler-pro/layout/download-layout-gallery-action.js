(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let ImageService = require('../../../services/image-services');

    class DownloadLayoutGalleryAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'DownloadImageUrlAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            // this.target.image_urls.push({
            //     layout_key_name: layout.layout_key_name, // key for update
            //     image_url: layout.image_url,
            //     image_path: null,
            //     image_floor_plan_url: layout.image_floor_plan_url,
            //     image_floor_plan_path: null,
            //     gallery: layout.gallery
            // });
            //
            for (const image of this.target.image_urls) {
                for (const gallery of image.gallery) {

                    if (gallery.image_url) {
                        debug('downloading layout gallery image_url:' + gallery.image_url);
                        gallery.image_path = await this.downloadImage(gallery.image_url) || gallery.image_path;
                        debug('[OK] download layout gallery image_url: ' + gallery.image_url + ', file_relative_path: ' + gallery.image_path);
                    }
                    if (gallery.thumb_url) {
                        debug('downloading layout gallery thumb_url:' + gallery.thumb_url);
                        gallery.thumb_path = await this.downloadImage(gallery.thumb_url) || gallery.thumb_path;
                        debug('[OK] download layout gallery thumb_url: ' + gallery.thumb_url + ', file_relative_path: ' + gallery.thumb_path);
                    }
                }
            }
        }

        async downloadImage(image_url) {
            try {
                let {file_relative_path} = await ImageService.downloadImageByURL(image_url);
                this.target.file_relative_image = file_relative_path;
                return file_relative_path;
            } catch (e) {
                logger.error(image_url, e, e.stack);
            }
            return null;
        }

        buildResult() {
            //TODO: implement data can thiet cho next-task
            // update image url
            // await MongoAccess.DAO.Layout.updateImageByLayoutKeyName(layout.layout_key_name, image.file_relative_image, image.file_relative_path_floor_plan);
        }
    }

    module.exports = DownloadLayoutGalleryAction;
})();