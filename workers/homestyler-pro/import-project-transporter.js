(function () {
    let Transporter = require('../../core/transport/redis-transporter');
    let worker = require('./homestyler-pro-worker');
    let instance = new Transporter('homestyler-pro-transporter');
    instance.setWorker(new worker());
    module.exports = instance;
})();