(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let Providers = require('../../../server/providers');
    let MongoAccess = require('../../../db/mongo/dbaccess');
    let HoproProjectTransform = require('../../../transform/hopro-project-transform');
    let ImageService = require('../../../services/image-services');

    const MigrateService = Providers.MigrateService;

    class GetListProjectAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'GetListProjectAction';
            this.currentPage = 1;
            this.limit = 10;
            this.timestampLastSync = 0;
            this.syncKey = 'sync_ct_projects';
            //
            this.logSyncProjectData = {
                success: 1,
                type: null,
                layout_key_name: null,
                data: [], // log sync
            };
            this.logData = {
                total_second: 0,
                total: 0,
                processed: 0,
                count_fail: 0,
                count_success: 0,
                sync_time: 0,
                model: 'CT_Projects',
                data: {
                    success: [],
                    fail: [],
                },
            };
            this.target = null;
        }

        async run(target) {
            this.target = target;
            this.target.listProject = [];
            //
            await this.updateLastSync();
            let hasNext = true;
            do {
                let listData = await MigrateService.getListProject(this.currentPage, this.limit, this.timestampLastSync);
                let listProject = HoproProjectTransform.list(listData);
                hasNext = listProject.length === this.limit;
                this.currentPage++;
                //
                this.target.listProject = [...this.target.listProject, ...listProject];
            } while (hasNext);
        }

        async doSyncProject() {
            let hasNext = false;
            let listData = await MigrateService.getListProject(this.currentPage, this.limit, this.timestampLastSync);
            let listProject = HoproProjectTransform.list(listData);
            return listProject;


            // let hasNext = false;
            // try {
            //     ///// step 1 : load all data
            //     let listData = await MigrateService.getListProject(this.currentPage, this.limit, this.timestampLastSync);
            //     let listProject = HoproProjectTransform.list(listData);
            //     if(listProject) {
            //         hasNext = listProject.length > 0;
            //     }
            //     return listProject;
            //     if (listProject) {
            //         if (listProject.length > 0) {
            //             this.logData.total += listProject.length;
            //             //// step 2 : Add list to redis
            //             for (let project of listProject) {
            //                 try {
            //                     this.logSyncProjectData = {
            //                         success: 1,
            //                         type: null,
            //                         project_ref_id: project.project_id,
            //                         project_slug: project.slug,
            //                         project_int_id: null,
            //                         data: [], // log sync
            //                     };
            //                     await this.onSyncProject(project);
            //                 } finally {
            //                     this.logData.processed++;
            //                     //
            //                     this.logSyncProjectData.project_ref_id = project.project_id;
            //                     if (this.logSyncProjectData.success === 1) {
            //                         this.logData.count_success++;
            //                         this.logData.data.success.push(this.logSyncProjectData);
            //                     } else {
            //                         this.logData.count_fail++;
            //                         this.logData.data.fail.push(this.logSyncProjectData);
            //                     }
            //                 }
            //             }
            //         } else {
            //             if (this.currentPage === 1) {
            //                 debug('list project is empty');
            //             }
            //         }
            //     } else {
            //         if (this.currentPage === 1) {
            //             debug('list project is NULl');
            //         }
            //     }
            //     hasNext = listProject.length > 0;
            // } catch (e) {
            //     debug(e);
            // }
            // return hasNext;
        }

        async onSyncProject(project) {
            await this.insertDB(project);
            await this.downloadImage(project);
        }

        async insertDB(project) {
            this.logSyncProjectData.step = 'upsert';
            let currentProject = await MongoAccess.DAO.Project.getByProjectRefId(project.project_ref_id);
            if (currentProject) {
                this.logSyncProjectData.type = 'update';
                debug('update current project', currentProject.project_ref_id);
                return await MongoAccess.DAO.Project.updateByProjectRefId(currentProject.project_ref_id, project);
            }
            this.logSyncProjectData.type = 'create';
            // insert or update project
            debug('insert new project', project.project_ref_id);
            return await MongoAccess.DAO.Project.create(project);
        }

        async downloadImage(project) {
            const url = project.image_url;
            let {file_relative_path} = await ImageService.downloadImageByURL(url);
            // update
            await MongoAccess.DAO.Project.updateImagePathByProjectRefId(project.project_ref_id, file_relative_path);
        }
    }

    module.exports = GetListProjectAction;
})();