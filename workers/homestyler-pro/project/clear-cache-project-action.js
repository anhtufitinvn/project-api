(function () {
    const BaseAction = require('../base-action');
    const CachedService = require('../../../services/cached-services');
    const {debug, logger} = require('../../../core/logger');

    class ClearCacheProjectAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'ClearCacheProjectAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            if (this.target.listProject && this.target.listProject.length > 0) {
                debug('clear cache project: ' + this.target.listProject.length);
                CachedService.Project.flushAll();
                CachedService.flushAll();
            }
        }

    }

    module.exports = ClearCacheProjectAction;
})();