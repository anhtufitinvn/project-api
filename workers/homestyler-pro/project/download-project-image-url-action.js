(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let ImageService = require('../../../services/image-services');

    class DownloadImageUrlAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'DownloadImageUrlAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            //
            for (const image of this.target.image_urls) {
                if (image.image_url) {
                    image.file_relative_path = await this.downloadImage(image.image_url);
                }
            }
        }

        async downloadImage(image_url) {
            debug('downloading image:' + image_url);
            let {file_relative_path} = await ImageService.downloadImageByURL(image_url);
            debug('[OK] downloading: ' + image_url + ', file_relative_path: ' + file_relative_path);
            this.target.file_relative_image = file_relative_path;
            return file_relative_path;
        }

        buildResult() {
            //TODO: implement data can thiet cho next-task
        }
    }

    module.exports = DownloadImageUrlAction;
})();