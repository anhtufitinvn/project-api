(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let MongoAccess = require('../../../db/mongo/dbaccess');

    class InsertListProjectAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'InsertListProjectAction';
            //
            this.target = null;
        }

        async run(target) {
            this.target = target;
            // result for step download-image-url
            this.target.image_urls = [];
            //
            try {
                let listProject = this.target.listProject;
                if (listProject) {
                    if (listProject.length > 0) {
                        for (let project of listProject) {
                            await this.doInsertProject(project);
                        }
                    } else {
                        debug('list project is empty');
                    }
                } else {
                    debug('list project is NULl');
                }
            } catch (e) {
                logger.error(e);
            }
        }

        async doInsertProject(project) {
            this.buildResult(project);
            //
            let currentProject = await MongoAccess.DAO.Project.getByProjectRefId(project.project_ref_id);
            if (currentProject) {
                debug('update current project', currentProject.project_ref_id);
                //
                await MongoAccess.DAO.Project.updateByProjectRefId(currentProject.project_ref_id, project);
            } else {
                // insert or update project
                debug('insert new project', project.project_ref_id);
                await MongoAccess.DAO.Project.create(project);
            }
        }

        buildResult(project) {
            this.target.image_urls.push({
                image_url: project.image_url,
                project_ref_id: project.project_ref_id
            });
        }
    }

    module.exports = InsertListProjectAction;
})();