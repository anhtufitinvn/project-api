(function () {
    const BaseAction = require('../base-action');
    const {debug, logger} = require('../../../core/logger');
    let MongoAccess = require('../../../db/mongo/dbaccess');

    class UpdateProjectImageUrlAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'UpdateProjectImageUrlAction';
            this.target = null;
        }

        async run(target) {
            this.target = target;
            //
            for (const image of this.target.image_urls) {
                if (image.file_relative_path && image.project_ref_id) {
                    // update
                    await MongoAccess.DAO.Project.updateImagePathByProjectRefId(image.project_ref_id, image.file_relative_path);
                }
            }
        }

    }

    module.exports = UpdateProjectImageUrlAction;
})();