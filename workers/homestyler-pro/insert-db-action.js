(function () {
    const BaseAction = require('./base-action');
    let MongoAccess = require('../../db/mongo/dbaccess');
    const {debug, logger} = require('../../core/logger');

    class InsertDBAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'InsertDBAction';
        }

        async run() {
            if (this.args && this.args.project) {
                let project = this.args.project;
                let currentProject = await MongoAccess.DAO.Project.getByProjectRefId(project.project_ref_id);
                if (currentProject) {
                    debug('update current project', currentProject.project_ref_id);
                    return await MongoAccess.DAO.Project.updateByProjectRefId(currentProject.project_ref_id, project);
                }
                //TODO: insert or update project
                debug('insert new project', project.project_ref_id);
                return await MongoAccess.DAO.Project.create(project);
            }
            throw new Error(`invalid args: ${this.args}`);
        }
    }

    module.exports = InsertDBAction;
})();