(function () {
    const fse = require('fs-extra');
    const fs = require('fs');
    const request = require('request');

    const BaseAction = require('./base-action');
    let path = require('path');
    const {debug, logger} = require('../../core/logger');
    let ImageService = require('../../services/image-services');
    let MongoAccess = require('../../db/mongo/dbaccess');

    class DownloadImageAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'DownloadImageAction';
        }

        async run() {
            if (this.args && this.args.project) {
                let project = this.args.project;
                const url = project.image_url;
                let {file_relative_path} = await ImageService.downloadImageByURL(url);
                // update
                await MongoAccess.DAO.Project.updateImagePathByProjectRefId(project.project_ref_id, file_relative_path);
            }
        }
    }

    module.exports = DownloadImageAction;
})();