(function () {
    let BaseWorker = require('../../core/transport/base-worker');
    let Task = require('./homestyler-pro-task');
    let InsertDBAction = require('./insert-db-action');
    let DownloadImageAction = require('./download-image-action');
    let DownloadAndUpdateLayoutImageAction = require('./download-and-update-layout-image-action');
    let UpsertLayoutAction = require('./upsert-layout-action');
    const {debug} = require('../../core/logger');

    class HomeStylerProWorker extends BaseWorker {
        constructor() {
            super();
        }

        async process(TMessage) {
            try {
                if (TMessage && TMessage.params) {
                    if (TMessage.action === 'upsert-project') {
                        await this.onSyncProject(TMessage.params);
                    } else if (TMessage.action === 'upsert-layout') {
                        await this.onSyncLayout(TMessage.params);
                    }
                }
            } catch (e) {
                debug(e);
            }
        }

        async onSyncProject(params) {
            let task = new Task('upsert-project');
            task.addAction(new InsertDBAction(params));
            task.addAction(new DownloadImageAction(params));
            //
            await task.run();
        }

        async onSyncLayout(params) {
            let task = new Task('upsert-layout');
            task.addAction(new UpsertLayoutAction(params));
            task.addAction(new DownloadAndUpdateLayoutImageAction(params));
            //
            await task.run();
        }

    }

    module.exports = HomeStylerProWorker;
})();