(function () {
    const BaseAction = require('./base-action');
    const {debug, logger} = require('../../core/logger');
    let importProjectTransporter = require('./import-project-transporter');

    class StartTransporterAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'StartTransporterAction';
        }

        async run() {
            debug(`${this.name} run`);
            importProjectTransporter.start();
        }
    }

    module.exports = StartTransporterAction;
})();