(function () {
    const BaseAction = require('./base-action');
    let MongoAccess = require('../../db/mongo/dbaccess');
    const {debug, logger} = require('../../core/logger');
    const MigrateService = require('../../services/migrate-services');

    class UpsertLayoutAction extends BaseAction {
        constructor(args) {
            super(args);
            this.name = 'UpsertLayoutAction';
        }

        async run() {
            if (this.args && this.args.layout) {
                let layout = this.args.layout;
                let currentLayout = await MongoAccess.DAO.Layout.getByLayoutKeyName(layout.layout_key_name);
                if (currentLayout) {
                    debug('update current layout', currentLayout.layout_name, currentLayout.layout_key_name);
                    await MongoAccess.DAO.Layout.updateByLayoutKeyName(currentLayout.layout_key_name, layout);
                    //
                    await this.upsertLayoutJson(currentLayout);
                    return currentLayout;
                }
                //TODO: insert or update project
                debug('insert new layout', layout.layout_name, layout.layout_key_name);
                // query and set project_int_id
                let parentProject = await MongoAccess.DAO.Project.getByProjectRefId(layout.project_id);
                if (parentProject && parentProject.int_id) {
                    layout.project_int_id = parentProject.int_id;
                    let dbLayout = await MongoAccess.DAO.Layout.create(layout);
                    //
                    await this.upsertLayoutJson(dbLayout);
                    return dbLayout;
                }
                logger.error('insert layout fail. parent project not found: ' + layout.layout_name + ', ' + layout.layout_key_name);
            } else {
                throw new Error(`invalid args: ${JSON.stringify(this.args)}`);
            }
        }

        async upsertLayoutJson(layout) {
            // get data
            let layoutJson = await MigrateService.getLayoutDetail(layout.layout_key_name);
            if (!layoutJson) {
                logger.error('upsertLayoutJson. layout-json not found: ' + layout.layout_name + ', ' + layout.layout_key_name);
                return null;
            }
            // insert or update
            let rc = MongoAccess.DAO.LayoutJson.getByLayoutIntId(layout.int_id);
            if (rc && rc.length > 0) {
                let currentLayoutJson = rc[0];
                return await MongoAccess.DAO.LayoutJson.updateByLayoutIntId(layout.int_id, currentLayoutJson.data_json);
            }
            // insert
            let data = {
                data_json: layoutJson.data_json,
                layout_int_id: layout.int_id
            };
            return await MongoAccess.DAO.LayoutJson.create(data);
        }
    }

    module.exports = UpsertLayoutAction;
})();