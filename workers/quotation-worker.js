(function () {
    let QuotationService = require('../services/quotation-services');
    let NofificationService = require('../services/notify-services');
    //
    let BaseWoker = require('../core/transport/base-worker');
    let {logger, debug} = require('../core/logger');
    let {ActionName} = require('./actions-names');

    async function report(lengthOfQueue, layoutID, status, desc, quotationUrl, downloadUrl) {
        try {
            return await NofificationService.CMS.broadcast("onBuildQtRenovation", {
                layout_id: layoutID,
                status: status || 0,
                desc: desc || "unknown",
                quotation_url: quotationUrl || null,
                download_url: downloadUrl || null,
                remain: lengthOfQueue || 0
            });
        } catch (e) {
            logger.error(e);
        }
    }

    class QuotationWorker extends BaseWoker {
        constructor() {
            super();
        }

        async process(TMessage) {
            try {
                if (TMessage && TMessage.params) {
                    if (TMessage.action === ActionName.GENERATE_QT_RENOVATION) {
                        await this.onGenerateQtRenovation(TMessage.params);
                    }
                }
            } catch (e) {
                debug(e);
                await report(this.countTaskInQueue(), layoutID, 1, "done", rs.quotation_url, rs.download_url);
            }
        }

        async onGenerateQtRenovation(params) {
            let layoutID = params.layout_id || null;
            layoutID = parseInt(layoutID);
            if(isNaN(layoutID)) {
                return;
            }

            debug(`onGenerateQtRenovation : [processing] layoutID = ${layoutID}`);

            await report(await this.countTaskInQueue(), layoutID, 0, "processing", null, null);

            let rs = await QuotationService.generatePDFFile(layoutID, true);

            debug(`onGenerateQtRenovation : [Finished] ${layoutID} url : ${rs.quotation_url}`);

            await report(0 , layoutID, 1, "done", rs.quotation_url, rs.download_url);

        }

    }

    module.exports = QuotationWorker;
})();