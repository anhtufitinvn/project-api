(function () {
    let URLServices = require('../services/url-services');
    let T = {};
    T.list = function(rs) {
        let locations = global.locations;
        return rs.map(row => {
            let city = locations.getCity(row["project"]["city_id"]);
            let district = locations.getDistrict(row["project"]["district_id"]);
            return {
                project_name: row["project"]["name"] || "no-name",
                project_id: row["project"]["id"],
                slug: row["project"]["slug"] || row["project"]["id"],
                status : row["project"]["status"] || null,
                sort : row["project"]["sort"] || 0,
                landing_url : row["project"]["landing_url"] || null,
                location: {
                    city_id : row["project"]["city_id"],
                    city_name : city ? city.name : null,
                    district_id : row["project"]["district_id"],
                    district_name : district ? district.name : null,
                    address : row["project"]["address"] || null
                },
                image_path : row["project"]["image"],
                project_image_url : URLServices.resolveConsultantImageUrl(row["project"]["image"])
            }
        });
    };

    /**
     * Transform Project Model Format Version 2
     * rs is a object from mongodb
     */
    T.listV2 = function(rs) {
        return rs.map(row => {
            let location = row.location || null;
            return {
                project_name: row.project_name,
                project_string_id: row._id,
                project_id : row.int_id,
                slug: row.slug,
                location: location ? {
                    city_id : location.city_id,
                    city_name : location.city_name,
                    district_id : location.district_id,
                    district_name : location.district_name,
                    address : location.address
                } : {
                    city_id : 79,
                    city_name : "Hồ Chí Minh",
                    district_id : 760,
                    district_name : "Quận 1",
                    address : null
                },
                sort : row.sort || 0,
                landing_url : row.landing_url || null,
                project_image_url : URLServices.resolveConsultantImageUrl(row.image_path)
            }
        });
    };

    module.exports = T;
})();