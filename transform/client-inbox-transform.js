(function () {
    let urlServices = require('../services/url-services');
    let moment = require('moment');
    let T = {};
    T.list = function (rs) {
        return rs.map(row => {
            let doc = row._doc;
            let m = moment(doc['updatedAt']);
            return {
                id: doc['_id'],
                image_urls: doc['image_paths'].map(img => {
                    return urlServices.resolveRelativePathLink(img);
                }),
                cart_url: urlServices.resolveCartLink(doc['cart']['cart_sign']),
                qt_download_url: urlServices.resolveDownloadQuotationLink(doc['cart']['cart_sign']),
                qt_url: urlServices.resolveQuotationHtmlLink(doc['cart']['cart_sign']),
                project_id: doc['metadata']['project_id'],
                project_name: doc['metadata']['project_name'],
                layout_id: doc['metadata']['layout_id'],
                layout_name: doc['metadata']['layout_name'],
                consultant_id: doc['metadata']['consultant_id'],
                create_time: m.format("YYYY-MM-DD HH:mm:ss"),
                unix_time: m.unix()
            }
        })
    };

    T.listEx = function (rs) {
        if (rs) {
            return rs.map(doc => {
                let m = moment(doc['updatedAt']);
                let d = {
                    id: doc['_id'],
                    cart_url: urlServices.resolveCartLink(doc['cart']['cart_sign']),
                    qt_download_url: urlServices.resolveDownloadQuotationLink(doc['cart']['cart_sign']),
                    qt_url: urlServices.resolveQuotationHtmlLink(doc['cart']['cart_sign']),
                    project_id: doc['metadata']['project_id'],
                    project_name: doc['metadata']['project_name'],
                    layout_id: doc['metadata']['layout_id'],
                    layout_name: doc['metadata']['layout_name'],
                    consultant_id: doc['metadata']['consultant_id'],
                    create_time: m.format("YYYY-MM-DD HH:mm:ss"),
                    unix_time: m.unix()
                };

                if (doc['panorama_url']) {
                    d.panorama_url = doc['panorama_url'];
                } else if (doc['image_paths']) {
                    d.image_urls = doc['image_paths'].map(img => {
                        return urlServices.resolveRelativePathLink(img);
                    });
                }

                return d;
            })
        } else {
            return [];
        }
    };

    module.exports = T;
})();