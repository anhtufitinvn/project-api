(function () {
    let contacts = {};
    contacts.list = function(rs) {
        return rs.map(row => {
            return {
                name : row['customers']["name"],
                email : row['customers']["email"] || null,
                phone : row['customers']["normalized_phone"] || null
            }
        })
    };

    module.exports = contacts;
})();