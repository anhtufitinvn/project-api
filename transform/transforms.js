(function () {
    module.exports = {
        contacts : require('./contact-transform'),
        layouts : require('./layout-transform'),
        projects : require('./project-transform'),
        clientInbox : require('./client-inbox-transform'),
        consultants : require('./consultants-transform'),
        consultantsLayout :require('./consultants-layout-transform'),
        consultantsRenovation : require('./consultants-renovation-transform'),
        Locations : require('./location-transform'),
        PhoneSpamSMS : require('./phone-sms-spam-transform')
    }
})();