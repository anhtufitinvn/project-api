(function () {
    let URLServices = require('../services/url-services');
    let T = {};
    T.list = function (rs) {
        return rs.map(row => {
            return {
                location: row["location"],
                metadata: row["metadata"],
                sort: row["sort"] || 0,
                landing_url: row["landing_url"] || null,
                project_name: row["project_name"],
                slug: row["project_slug"],
                status: row["status"],
                image_path: row["image_path"],
                image_url: row["image_url"],
                desc: row["desc"],
                is_renovation: row["is_renovation"] || null,
                attributes: row['attributes'],
                project_ref_id: row['project_id']
            }
        });
    };
    /**
     * Transform Project Model Format Version 2
     * rs is a object from mongodb
     */
    T.listV2 = function (rs) {
        return rs.map(row => {
            return {
                project_name: row.project_name,
                project_string_id: row._id,
                project_id: row.int_id,
                slug: row.slug,
                location: {
                    city_id: row.location.city_id,
                    city_name: row.location.city_name,
                    district_id: row.location.district_id,
                    district_name: row.location.district_name,
                    address: row.location.address
                },
                sort: row.sort || 0,
                landing_url: row.landing_url || null,
                project_image_url: URLServices.resolveConsultantImageUrl(row.image_path)
            }
        });
    };

    module.exports = T;
})();