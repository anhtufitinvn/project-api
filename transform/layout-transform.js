(function () {
    let URLServices = require('../services/url-services');
    let layouts = {};

    function resolveProjectLayout(row, project) {
        let layout = null;
        let layoutID = row["layout"]["id"];
        for (let i = 0, N = project.layouts.length; i < N; i++) {
            if (project.layouts[i].layout_id === layoutID) {
                layout = project.layouts[i];
                break;
            }
        }

        if (layout === null || layout === undefined) {
            let tts = row["layout"]["layout_json"] || null;
            let object_key = null;
            if (tts) {
                tts = JSON.parse(tts);
                object_key = tts ? tts["RoomId"] : null;
            }

            layout = {
                project_id: row["project"]["id"] || 0,
                project_name: row["project"]["name"] || "no-name",
                layout_id: row["layout"]["id"] || null,
                slug: row["layout"]["slug"] || row["project"]["id"],
                name: row["layout"]["name"],
                title: row["layout"]["title"] || "",
                desc: row["layout"]["desc"] || null,
                price: row["layout"]["estimated_price"] || 0.0,
                area: row["layout"]["area"] || 0.0,
                status: row["layout"]["status"] || 0,
                construction_fee: row["layout"]["construction_fee"] || 0.0,
                fitinfurniture_fee: row["layout"]["fitinfurniture_fee"] || 0.0,
                bed_room_num: row["layout"]["room_number"] || 1,
                style_name: row["style"]["name"] || "unknown style",
                has_editor: row["layout"]["editor"],
                panorama_url: row["layout"]["panorama_url"] || null,
                image_path: row["layout"]["image"],
                image_url: URLServices.resolveConsultantImageUrl(row["layout"]["image"]),
                image_floor_plan_path: row["layout"]["image_floor_plan"],
                image_floor_plan_url: URLServices.resolveConsultantImageUrl(row["layout"]["image_floor_plan"]),
                gallery: [],
                object_key: object_key
            };

            project.layouts.push(layout);
        }

        if (row["images"]["thumb_path"] && row["images"]["image_path"]) {
            layout.gallery.push({
                layout_id: layoutID,
                thumb_path: row["images"]["thumb_path"],
                thumb_url: URLServices.resolveConsultantImageUrl(row["images"]["thumb_path"]),
                image_path: row["images"]["image_path"],
                image_url: URLServices.resolveConsultantImageUrl(row["images"]["image_path"])
            });
        }
    }

    layouts.list = function (rs, noFilter) {
        let filter = null;
        if (noFilter) {
            filter = rs;
        } else {
            filter = rs.filter((row) => {
                return row["layout"]["layout_json"] !== "\"[]\"";
            });
        }

        let map = {};
        let list = [];
        filter.forEach(row => {
            let pid = row["project"]["id"].toString();
            let project = map[pid];
            if (project === undefined || project === null) {
                project = {
                    project_name: row["project"]["name"] || "no-name",
                    project_id: row["project"]["id"],
                    slug: row["project"]["slug"] || row["project"]["id"],
                    project_image_url: URLServices.resolveConsultantImageUrl(row["project"]["image"]),
                    layouts: []
                };

                map[pid] = project;
                list.push(project);
            }

            resolveProjectLayout(row, project);

        });

        return list;
    };

    layouts.listEx = function (rs) {
        let list = layouts.list(rs);
        return list.length ? list[0].layouts : [];
    };

    /**
     * Transform to list, version 2 from MongoDB
     */
    layouts.listV2 = function (rs) {
        return rs.map(row => {
            let t = {
                project_id: row.project_int_id,
                project_string_id: row.project_id,
                project_name: row.project_name,
                layout_id: row.int_id,
                layout_string_id: row._id,
                slug: row.slug,
                image_url: URLServices.resolveConsultantImageUrl(row.image_path),
                image_floor_plan_url: URLServices.resolveConsultantImageUrl(row.image_floor_plan_path),
                name: row.layout_name,
                title: null,
                intro: null,
                desc: row.desc,
                area: row.area,
                tower: row.tower,
                style_name: null,
                bed_room_num: 0,
                rooms: null,
                has_editor: 0,
                gallery: null
            };
            
            if (row.metadata) {
                t.title = row.metadata.title;
                t.intro = row.metadata.intro;
                t.style_name = row.metadata.style.name;
                t.bed_room_num = row.metadata.bedroom_num;
                t.price = row.metadata.price ? row.metadata.price.estimated : 0;
                t.rooms = row.metadata.rooms.map(r => {
                    // if (r.name === 'bed_room') {
                    //     t.bed_room_num = r.qty || 1;
                    // }

                    return {
                        name: r.name,
                        qty: r.qty,
                        level: r.level,
                        area: r.area
                    }
                })
            }

            if (row.attributes) {
                for (let a of row.attributes) {
                    if (a.name === 'has_editor') {
                        t.has_editor = a.value_int;
                    }
                }
            }

            if (row.extensions) {
                let panorama = row.extensions.panorama;
                if (panorama) {
                    t.panorama_url = panorama.url;
                }

                let quotation = row.extensions.quotation;
                if(quotation) {
                    t.quotation_url = quotation.url;
                    t.quotation_download_url = quotation.url_download;
                }
            }

            if (row.gallery && row.gallery.length > 0) {
                t.gallery = row.gallery.map(g => {
                    return {
                        layout_int_id: g.int_id,
                        thumb_url: URLServices.resolveConsultantImageUrl(g.thumb_path),
                        image_path_url: URLServices.resolveConsultantImageUrl(g.image_path),
                    }
                });
            }

            return t;
        });

    };

    module.exports = layouts;
})();