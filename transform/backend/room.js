(function () {
    let URLServices = require('../../services/url-services');
    let T = {};

    /**
     * Transform Layout Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            
            return {
                _id: obj._id,
                sort : obj.sort,
                int_id: obj.int_id,
                layout_id : obj.layout_id,
                layout_int_id: obj.layout_int_id,
                layout_name : obj.layout_name,
                room_id: obj.room_id,
                room_category_name : obj.room_category_name,
                room_category_key : obj.room_category_key,
                room_name: obj.room_name,
                room_json: obj.room_json || [],
                slug: obj.slug,
                status: obj.status,

                image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
                
                image_path : obj.image_path,
                desc : obj.desc,
                area : obj.area,
                metadata : obj.metadata,
                attributes : obj.attributes || [],
                extensions : obj.extensions,
                created_at : obj.createdAt,
                updated_at : obj.updatedAt,
                published_at : obj.published_at
            }
        });
    };

    T.detail  = function(obj) {
        
        return {
            _id: obj._id,
                sort : obj.sort,
                int_id: obj.int_id,
                layout_id : obj.layout_id,
                layout_int_id: obj.layout_int_id,
                layout_name : obj.layout_name,
                room_id: obj.room_id,
                room_category_name : obj.room_category_name,
                room_category_key : obj.room_category_key,
                room_name: obj.room_name,
                room_json: obj.room_json || [],
                slug: obj.slug,
                status: obj.status,

                image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
                
                image_path : obj.image_path,
                desc : obj.desc,
                area : obj.area,
                metadata : obj.metadata,
                attributes : obj.attributes || [],
                extensions : obj.extensions,
                created_at : obj.createdAt,
                updated_at : obj.updatedAt,
                published_at : obj.published_at
        }
    };
    module.exports = T;
})();