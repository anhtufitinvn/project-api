(function () {
    let URLServices = require('../../services/url-services');
    let T = {};
    /**
     * Transform Style Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            return {
                _id: obj._id,
                int_id: obj.int_id,
                original_json: obj.original_json,
                cart_json : obj.cart_json,
                created_at : obj.createdAt,
                updated_at : obj.updatedAt
            }
        });
    };

    T.detail  = function(obj) {
        return {
            _id: obj._id,
            int_id: obj.int_id,
            original_json: obj.original_json,
            cart_json : obj.cart_json,
            created_at : obj.createdAt,
            updated_at : obj.updatedAt
        }
    };
    module.exports = T;
})();