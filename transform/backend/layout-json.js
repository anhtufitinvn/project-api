(function () {
    let URLServices = require('../../services/url-services');
    let T = {};
    /**
     * Transform Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            return {
                layout_int_id: obj.layout_int_id,
                data_json: obj.data_json
            }
        });
    };

    T.detail  = function(obj) {
        return {
            layout_int_id: obj.layout_int_id,
            data_json: obj.data_json
        }
    };
    module.exports = T;
})();