(function () {
    let URLServices = require('../../services/url-services');
    let T = {};
    T.listV1 = function(rs) {
        let locations = global.locations;
        return rs.map(row => {
            let city = locations.getCity(row["project"]["city_id"]);
            let district = locations.getDistrict(row["project"]["district_id"]);
            return {
                project_name: row["project"]["name"] || "no-name",
                project_id: row["project"]["id"],
                slug: row["project"]["slug"] || row["project"]["id"],
                status : row["project"]["status"] || null,
                sort : row["project"]["sort"] || 0,
                landing_url : row["project"]["landing_url"] || null,
                location: {
                    city_id : row["project"]["city_id"],
                    city_name : city ? city.name : null,
                    district_id : row["project"]["district_id"],
                    district_name : district ? district.name : null,
                    address : row["project"]["address"] || null
                },
                image_path : row["project"]["image"],
                project_image_url : URLServices.resolveProjectImageUrl(row["project"]["image"])
            }
        });
    };

    /**
     * Transform Project Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            let location = obj.location || {};
            return {
                _id: obj._id,
                project_name: obj.project_name,
                project_id: obj._id,
                int_id : obj.int_id,
                slug: obj.slug,
                metadata :obj.metadata,
                status: obj.status,
                is_renovation: obj.is_renovation ? true : false,
                attributes : obj.attributes,
                desc : obj.desc,
                sort: obj.sort,
                landing_url: obj.landing_url,
                location: {
                    city_id : location.city_id,
                    city_name : location.city_name,
                    district_id : location.district_id,
                    district_name : location.district_name,
                    lat : location.lat,
                    long: location.long,
                    address : location.address
                },
                project_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
                image_path : obj.image_path,
                created_at : obj.createdAt,
                updated_at : obj.updatedAt,
                layouts_count :obj.layouts_count || 0
            }
        });
    };

    T.detail  = function(obj) {
        let location = obj.location || {};
        return {
            _id: obj._id,
            project_name: obj.project_name,
            project_id: obj._id,
            int_id : obj.int_id,
            slug: obj.slug,
            metadata :obj.metadata,
            status: obj.status,
            is_renovation: obj.is_renovation ? true : false,
            attributes : obj.attributes,
            desc : obj.desc,
            sort: obj.sort,
            landing_url: obj.landing_url,
            location: {
                city_id : location.city_id,
                city_name : location.city_name,
                district_id : location.district_id,
                district_name : location.district_name,
                lat : location.lat,
                long: location.long,
                address : location.address
            },
            project_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
            image_path : obj.image_path,
            created_at : obj.createdAt,
            updated_at : obj.updatedAt
        }
    };
    module.exports = T;
})();