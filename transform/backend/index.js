(function () {
    module.exports = {
        projects : require('./project'),
        layouts : require('./layout'),
        layoutsRenovation : require('./layout-renovation'),
        rooms : require('./room'),
        styles : require('./style'),
        roomCategories: require('./room-category'),
        users: require('./user'),
        layoutJson : require('./layout-json'),
        carts : require('./cart')
    }
})();