(function () {
    const DF = "default_value";
    const FNAME = "name";
    let T = {
        transform(fields, src) {
            let result = {};
            for(let f of fields) {
                if(f[FNAME] !== undefined) {
                    if(src[FNAME] !== undefined) {
                        result[FNAME] = src[FNAME];
                    } else {
                        result[FNAME] = src[DF] || null;
                    }
                } else if(src[f] !== undefined) {
                    result[f] = src[f] || null;
                }
            }
        }
    };

    module.exports = function (fields, src) {
        return T.transform(fields);
    };
})();