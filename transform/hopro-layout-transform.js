(function () {
    let URLServices = require('../services/url-services');
    let layouts = {};

    function resolveProjectLayout(row, project) {
        let layout = null;
        let layoutID = row["layout"]["id"];
        for (let i = 0, N = project.layouts.length; i < N; i++) {
            if (project.layouts[i].layout_id === layoutID) {
                layout = project.layouts[i];
                break;
            }
        }

        if (layout === null || layout === undefined) {
            let tts = row["layout"]["layout_json"] || null;
            let object_key = null;
            if (tts) {
                tts = JSON.parse(tts);
                object_key = tts ? tts["RoomId"] : null;
            }

            layout = {
                project_id: row["project"]["id"] || 0,
                project_name: row["project"]["name"] || "no-name",
                layout_id: row["layout"]["id"] || null,
                slug: row["layout"]["slug"] || row["project"]["id"],
                name: row["layout"]["name"],
                title: row["layout"]["title"] || "",
                desc: row["layout"]["desc"] || null,
                price: row["layout"]["estimated_price"] || 0.0,
                area: row["layout"]["area"] || 0.0,
                status: row["layout"]["status"] || 0,
                construction_fee: row["layout"]["construction_fee"] || 0.0,
                fitinfurniture_fee: row["layout"]["fitinfurniture_fee"] || 0.0,
                bed_room_num: row["layout"]["room_number"] || 1,
                style_name: row["style"]["name"] || "unknown style",
                has_editor: row["layout"]["editor"],
                panorama_url: row["layout"]["panorama_url"] || null,
                image_path: row["layout"]["image"],
                image_url: URLServices.resolveConsultantImageUrl(row["layout"]["image"]),
                image_floor_plan_path: row["layout"]["image_floor_plan"],
                image_floor_plan_url: URLServices.resolveConsultantImageUrl(row["layout"]["image_floor_plan"]),
                gallery: [],
                object_key: object_key
            };

            project.layouts.push(layout);
        }

        if (row["images"]["thumb_path"] && row["images"]["image_path"]) {
            layout.gallery.push({
                layout_id: layoutID,
                thumb_path: row["images"]["thumb_path"],
                thumb_url: URLServices.resolveConsultantImageUrl(row["images"]["thumb_path"]),
                image_path: row["images"]["image_path"],
                image_url: URLServices.resolveConsultantImageUrl(row["images"]["image_path"])
            });
        }
    }

    layouts.list = function (rs) {
        return rs.map(row => {
            let result = row;
            return result;
        });
    };


    module.exports = layouts;
})();