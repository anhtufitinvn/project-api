(function () {
    let urlServices = require('../services/url-services');
    let T = {};
    T.result = function (doc) {
        let project_id = doc['metadata']['project_id'];
        let layoutId = doc['metadata']['layout_id'];
        let cart_sign = doc['cart']['cart_sign'] || null;
        return {
            project_id: project_id,
            project_name: doc['metadata']['project_name'],
            layout_id: layoutId,
            layout_name: doc['metadata']['layout_name'],
            bed_room_num: doc['metadata']['bed_room_num'],
            area: doc['metadata']['area'],
            construction_fee: doc['metadata']['construction_fee'] || 0,
            fitinfurniture_fee: doc['metadata']['fitinfurniture_fee'] || 0,
            discount: doc['metadata']['discount'] || 0.0,
            image_urls: doc['image_paths'].map(img => {
                return urlServices.resolveRelativePathLink(img);
            }),
            items : doc['cart']['items'].map(item => {
                return {
                    quantity : item.quantity,
                    sku : item.sku,
                    group_id:  item.group_id || 0,
                    group_name: item.group_name || ""
                };
            }),
            cart_sign : cart_sign
        };
    };

    module.exports = T;
})();