(function () {
    let urlServices = require('../services/url-services');
    let T = {};
    T.result = function (doc) {
        let project_id = doc['project_int_id'];
        let layoutId = doc['layout_int_id'];
        let project_name = doc['project_name'];
        let layout_name = doc['layout_name'];
        let metadata = doc['metadata'] || {};
        let extensions = doc['extensions'] || {};
        let cart = extensions['cart_json'] || {};
        let bed_room_num = metadata['bedroom_num'] || 0;
        let area = doc['area'] || 0;
        let items = cart['items'] || [];
        let non_ecom_items = cart['non_ecom_items'] || [];
        let matDatas = cart['matDatas'] || [];
        let cart_meta = cart['metadata'] || {};
        let image_urls = (doc['gallery']) ? doc['gallery'].map(img => {
            let path = img.image_path;
            return urlServices.resolveRelativePathLink(path);
        }) : [];
        if(!image_urls.length && doc.image_path){
            image_urls  = [urlServices.resolveConsultantImageUrl(doc.image_path)];
        }
        return {
            project_id: project_id,
            project_name: project_name,
            layout_id: layoutId,
            layout_name: layout_name,
            bed_room_num: bed_room_num,
            area: area,
            cart_sign : cart['cart_sign'] || null,
            construction_fee:cart_meta['construction_fee'] || 0,
            fitinfurniture_fee: cart_meta['fitinfurniture_fee'] || 0,
            discount: cart_meta['discount'] || 0.0,
            image_urls: image_urls,
            cart_url : urlServices.resolveCartLink(cart['cart_sign']),

            non_ecom_items : non_ecom_items.map(item => {
                return {
                    quantity : item.quantity,
                    sku : item.sku,
                    group_id:  item.group_id || 0,
                    group_name: item.group_name || "",
                    cost : item.cost || 0
                };
            }),
            matDatas : matDatas.map(item => {
                return {
                   
                    sku : item.sku,
                    area : item.area,
                    group_id:  item.group_id || 0,
                    group_name: item.group_name || "",
                    cost : item.cost || 0
                };
            }),
            items : items.map(item => {
                return {
                    quantity : item.quantity,
                    sku : item.sku,
                    group_id:  item.group_id || 0,
                    group_name: item.group_name || ""
                };
            })
        };
    };

    module.exports = T;
})();