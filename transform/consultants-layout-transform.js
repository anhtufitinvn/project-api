(function () {
    let urlServices = require('../services/url-services');
    let T = {};
    T.result = function (doc) {
        let projectId = doc['project_int_id'];
        let layoutId = doc['layout_int_id'];
        let projectName = doc['project_name'];
        let layoutName = doc['layout_name'];
        let metadata = doc['metadata'] || {};
        let bedRoomNum = metadata['bedroom_num'] || 0;
        let area = doc['area'] || 0;
        let extensions = doc['extensions'] || {};
        let cart = extensions['cart_json'] || {};
        let items = cart['items'] || [];
        let nonEcomItems = cart['non_ecom_items'] || [];
        let materials = cart['matDatas'] || [];
        let cartMetadata = cart['metadata'] || {};
        let imageUrls = (doc['gallery']) ? doc['gallery'].map(img => {
            let path = img.image_path;
            return urlServices.resolveRelativePathLink(path);
        }) : [];
        if(!imageUrls.length && doc.image_path){
            imageUrls  = [urlServices.resolveConsultantImageUrl(doc.image_path)];
        }
        return {
            project_id: projectId,
            project_name: projectName,
            layout_id: layoutId,
            layout_name: layoutName,
            bed_room_num: bedRoomNum,
            area: area,
            construction_fee:cartMetadata['construction_fee'] || 0,
            fitinfurniture_fee: cartMetadata['fitinfurniture_fee'] || 0,
            discount: cartMetadata['discount'] || 0.0,
            image_urls: imageUrls,
            non_ecom_items : nonEcomItems.map(item => {
                return {
                    quantity : item.quantity,
                    sku : item.sku,
                    group_id:  item.group_id || 0,
                    group_name: item.group_name || "",
                    cost : item.cost || 0
                };
            }),
            matDatas : materials.map(item => {
                return {
                   
                    sku : item.sku,
                    area : item.area,
                    group_id:  item.group_id || 0,
                    group_name: item.group_name || "",
                    cost : item.cost || 0
                };
            }),
            items : items.map(item => {
                return {
                    quantity : item.quantity,
                    sku : item.sku,
                    group_id:  item.group_id || 0,
                    group_name: item.group_name || ""
                };
            })
        };
    };

    module.exports = T;
})();