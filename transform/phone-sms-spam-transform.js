(function () {
    let phones = {};
    phones.list = function(doc) {
        return doc.map(row => {
            return {
                name : row['name'] || null,
                phone : row['phone']
            }
        })
    };

    module.exports = phones;
})();