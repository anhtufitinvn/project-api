(function () {
    let urlServices = require('../services/url-services');
    let moment = require('moment');
    let T = {
        grid : {}
    };

    T.grid.layouts = function(rs) {
        let data = { total : rs.layouts.length};
        data.records = rs.layouts.map(row => {
            return {
                recid: row.layout_id,
                layout_id: row.layout_id,
                name: row.name,
                style_name: row.style_name,
                area: row.area,
                bed_room_num: row.bed_room_num,
                price: row.price,
                construction_fee: row.construction_fee,
                fitinfurniture_fee: row.fitinfurniture_fee,
            }
        });

        return data;
    };

    T.grid.projects = function(rs) {
        let data = { total : rs.length};
        data.records = rs.map(row => {
            return {
                recid: row.project_id,
                project_id: row.project_id,
                project_name: row.project_name,
                slug: row.project_name,
                city_name: row.location.city_name,
                district_name: row.location.district_name,
                project_image_url: row.project_image_url
            }
        });

        return data;
    };

    module.exports = T;
})();