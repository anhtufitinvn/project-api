(function () {
    let URLServices = require('../../services/url-services');
    let T = {};
    /**
     * Transform Style Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            return {
                _id: obj._id,
                name: obj.name,
              //  room_id: obj.room_id,
                type : obj.type,
                image : obj.image,
                image_url : URLServices.resolveConsultantImageUrl(obj.image),
                created_at : obj.createdAt,
                updated_at : obj.updatedAt
            }
        });
    };

    T.detail  = function(obj) {
        return {
            _id: obj._id,
            name: obj.name,
         //   room_id: obj.room_id,
            type : obj.type,
            image : obj.image,
            image_url : URLServices.resolveConsultantImageUrl(obj.image),
            created_at : obj.createdAt,
            updated_at : obj.updatedAt
        }
    };
    module.exports = T;
})();