(function () {
    let URLServices = require('../../services/url-services');
    let T = {};

    /**
     * Transform Layout Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            let gallery =  obj.gallery || [];
            gallery = gallery.map(image => {
                return {
                    "layout_id": obj.int_id,
                    "thumb_path": image.thumb_path,
                    "thumb_url": URLServices.resolveRelativePathLink(image.thumb_path),
                    "image_path": image.image_path,
                    "image_url": URLServices.resolveRelativePathLink(image.image_path)
                }
                
            });

            return {
                _id: obj._id,
                project_name: obj.project_name,
                parent_int_id : obj.parent_int_id, 
                project_id: obj.project_id,
                project_int_id: obj.project_int_id,
                layout_key_name : obj.layout_key_name,
                int_id : obj.int_id,
                layout_name : obj.layout_name,
                layout_json : obj.layout_json,
                slug: obj.slug,
                metadata :obj.metadata,
                status: obj.status,
                is_template: obj.is_template ? true : false,
                attributes : obj.attributes || [],
                gallery : gallery,
                desc : obj.desc,
                area : obj.area,
                extensions : obj.extensions,
                layout_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
                layout_image_floor_plan_url : URLServices.resolveConsultantImageUrl(obj.image_floor_plan_path),
                image_path : obj.image_path,
                image_floor_plan_path : obj.image_floor_plan_path,
                created_at : obj.createdAt,
                updated_at : obj.updatedAt,
                published_at : obj.published_at
            }
        });
    };

    T.detail  = function(obj) {
        let gallery =  obj.gallery || [];
            gallery = gallery.map(image => {
                return {
                    "layout_id": obj.int_id,
                    "thumb_path": image.thumb_path,
                    "thumb_url": URLServices.resolveRelativePathLink(image.thumb_path),
                    "image_path": image.image_path,
                    "image_url": URLServices.resolveRelativePathLink(image.image_path)
                }
                
            });

            return {
                _id: obj._id,
                project_name: obj.project_name,
                parent_int_id : obj.parent_int_id, 
                project_id: obj.project_id,
                project_int_id: obj.project_int_id,
                layout_key_name : obj.layout_key_name,
                int_id : obj.int_id,
                layout_name : obj.layout_name,
                layout_json : obj.layout_json,
                slug: obj.slug,
                metadata :obj.metadata,
                status: obj.status,
                is_template: obj.is_template ? true : false,
                attributes : obj.attributes || [],
                gallery : gallery,
                desc : obj.desc,
                area : obj.area,
                extensions : obj.extensions,
                layout_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
                layout_image_floor_plan_url : URLServices.resolveConsultantImageUrl(obj.image_floor_plan_path),
                image_path : obj.image_path,
                image_floor_plan_path : obj.image_floor_plan_path,
                created_at : obj.createdAt,
                updated_at : obj.updatedAt,
                published_at : obj.published_at
            }
    };
    module.exports = T;
})();