(function () {
    let URLServices = require('../../services/url-services');
    let T = {};
   
    /**
     * Transform Project Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            let location = obj.location || {};
            return {
                //_id: obj._id,
                project_name: obj.project_name,
                project_id: obj._id,
                int_id : obj.int_id,
                slug: obj.slug,
                //metadata :obj.metadata,
                status: obj.status,
                is_renovation: obj.is_renovation ? true : false,
                //attributes : obj.attributes,
                desc : obj.desc,
                sort: obj.sort,
                location: {
                    city_id : location.city_id,
                    city_name : location.city_name,
                    district_id : location.district_id,
                    district_name : location.district_name,
                    lat : location.lat,
                    long: location.long,
                    address : location.address
                },
                project_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
                //image_path : obj.image_path,
                //created_at : obj.createdAt,
                //updated_at : obj.updatedAt
            }
        });
    };

    T.detail  = function(obj) {
        let location = obj.location || {};
        return {
           // _id: obj._id,
            project_name: obj.project_name,
            project_id: obj._id,
            int_id : obj.int_id,
            slug: obj.slug,
            //metadata :obj.metadata,
            status: obj.status,
            is_renovation: obj.is_renovation ? true : false,
           // attributes : obj.attributes,
            desc : obj.desc,
            sort: obj.sort,
            landing_url: obj.landing_url,
            location: {
                city_id : location.city_id,
                city_name : location.city_name,
                district_id : location.district_id,
                district_name : location.district_name,
                lat : location.lat,
                long: location.long,
                address : location.address
            },
            project_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
            image_path : obj.image_path,
            //created_at : obj.createdAt,
           // updated_at : obj.updatedAt
        }
    };
    module.exports = T;
})();