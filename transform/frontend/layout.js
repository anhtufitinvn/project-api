(function () {
    let URLServices = require('../../services/url-services');
    let T = {};

    /**
     * Transform Layout Model Format Version 2
     * rs is a object from mongodb
     */
    T.list = function(rs) {
        return rs.map(obj => {
            let location = obj.location || {};
            return {
                _id: obj._id,
                project_name: obj.project_name,
                project_id: obj.project_id,
                project_int_id: obj.project_int_id,
                layout_key_name : obj.layout_key_name,
                int_id : obj.int_id,
                layout_name : obj.layout_name,
                layout_json : obj.layout_json,
                slug: obj.slug,
                metadata :obj.metadata,
                status: obj.status,
                is_template: obj.is_template ? true : false,
                attributes : obj.attributes || [],
                gallery : obj.gallery || [],
                desc : obj.desc,
                area : obj.area,
                extensions : obj.extensions,
                // location: {
                //     // city_id : location.city_id,
                //     // city_name : location.city_name,
                //     // district_id : location.district_id,
                //     // district_name : location.district_name,
                //     // lat : location.lat,
                //     // long: location.long,
                //     // address : location.address,
                //     block: location.block,
                //     apartment_code : location.apartment_code
                // },
                layout_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
                layout_image_floor_plan_url : URLServices.resolveConsultantImageUrl(obj.image_floor_plan_path),
                image_path : obj.image_path,
                image_floor_plan_path : obj.image_floor_plan_path,
                created_at : obj.createdAt,
                updated_at : obj.updatedAt,
                published_at : obj.published_at
            }
        });
    };

    T.detail  = function(obj) {
        let location = obj.location || {};
        return {
            _id: obj._id,
            project_name: obj.project_name,
            project_id: obj.project_id,
            project_int_id: obj.project_int_id,
            layout_key_name : obj.layout_key_name,
            int_id : obj.int_id,
            layout_name : obj.layout_name,
            layout_json : obj.layout_json,
            slug: obj.slug,
            metadata :obj.metadata,
            status: obj.status,
            is_template: obj.is_template ? true : false,
            attributes : obj.attributes || [],
            gallery : obj.gallery || [],
            desc : obj.desc,
            area : obj.area,
            extensions : obj.extensions,
            location: {
                // city_id : location.city_id,
                // city_name : location.city_name,
                // district_id : location.district_id,
                // district_name : location.district_name,
                // lat : location.lat,
                // long: location.long,
                // address : location.address
                block: location.block,
                apartment_code : location.apartment_code
            },
            layout_image_url : URLServices.resolveConsultantImageUrl(obj.image_path),
            layout_image_floor_plan_url : URLServices.resolveConsultantImageUrl(obj.image_floor_plan_path),
            image_path : obj.image_path,
            image_floor_plan_path : obj.image_floor_plan_path,
            created_at : obj.createdAt,
            updated_at : obj.updatedAt,
            published_at : obj.published_at
        }
    };
    module.exports = T;
})();