(function () {
    let LocationTransform = {};
    /** List location format v1, please don't use for v2 **/
    /**
     * @deprecated : Don't use for v2
     * @param locations
     * @returns {any[]}
     */
    LocationTransform.list = function (locations) {
        let citiesMap = {};
        let districtMap = {};

        Object.values(locations.wards).forEach(w => {
            let district = locations.districts[w.district_id];
            if (district) {
                let copyDistrict = districtMap[district.id];
                if (copyDistrict === undefined) {
                    copyDistrict = {
                        city_id: district.city_id,
                        district_id: district.id,
                        district_name: district.name,
                        wards: []
                    };
                    districtMap[district.id] = copyDistrict;
                }

                copyDistrict.wards.push({
                    ward_id: w.id,
                    ward_name: w.name,
                    district_id: w.district_id,
                    city_id: district.city_id
                });
            }
        });

        Object.values(locations.districts).forEach(d => {
            let city = locations.cities[d.city_id];
            if (city) {
                let copyCity = citiesMap[city.id];
                if (copyCity === undefined) {
                    copyCity = {
                        city_id: city.id,
                        city_name: city.name,
                        districts: []
                    };
                    citiesMap[city.id] = copyCity;
                }

                let district = districtMap[d.id];
                if (district === undefined) {
                    district = {
                        city_id: d.city_id,
                        district_id: d.id,
                        district_name: d.name,
                        wards: []
                    };

                    districtMap[d.id] = district;
                }

                copyCity.districts.push(district);
            }
        });

        return Object.values(citiesMap);
    };

    LocationTransform.listV2 = function (locations) {
        return locations.map(l => {
            let location = {
                lat: l.lat,
                long: l.long,
                city_id: l.city_id,
                city_name: l.city_name
            };

            location.districts = l.districts.map((d) => {
                return {
                    district_id: d.district_id,
                    district_name: d.district_name,
                    wards : d.wards.map(w => {
                        return {
                            ward_id:  w.ward_id,
                            ward_name: w.ward_name
                        }
                    })
                };
            });

            return location;
        });
    };

    module.exports = LocationTransform;
})();