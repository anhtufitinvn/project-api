(function () {

    const getLayoutJson = {
        layout_id: {
            in: ["params"],
            errorMessage: 'require param layout_id ',
            not: true,
            isEmpty: true
        }
    };

    const postLayoutJson = {
        layout_id: {
            in: ["params"],
            errorMessage: 'require param layout_id ',
            not: true,
            isEmpty: true
        },

        data_json :{
            in: ["body"],
            errorMessage: 'require param data_json',
            not: true,
            isEmpty: true
        }
    };


    const uploadLayoutJson = {
        layout_id: {
            in: ["body"],
            errorMessage: 'require param layout_id ',
            not: true,
            isEmpty: true
        }, 
        file :{
            in: ["body"],
            errorMessage: 'require param file : file',
            not: true,
            isEmpty: true
        }
    };


    module.exports = function (rules) {
        rules.getLayoutJson = getLayoutJson;
        rules.uploadLayoutJson = uploadLayoutJson;
        rules.postLayoutJson = postLayoutJson;
    }
})();