(function () {

    const indexLayout = {
        page: {
            in: ["query"],
            isInt: {
                options: {min: 1, max: 1000},
                errorMessage: 'require param page : integer > 0 and < 1000',
            },
            toInt: true
        },
        name: {
            in: ["query"],
            errorMessage: 'require param name : string',
            not: true,
            isEmpty: false
        },
        layout_id: {
            in: ["query"],
            errorMessage: 'require param layout_id : integer',
            isInt: true,
            toInt: true,
            optional: {options: {nullable: true}}
        },
        project_id: {
            in: ["query"],
            errorMessage: 'require param project_id : integer',
            isInt: true,
            toInt: true,
            optional: {options: {nullable: true}}
        },
        estimated_price: {
            in: ["query"],
            errorMessage: 'require param estimated_price : integer',
            isInt: true,
            toInt: true,
            optional: {options: {nullable: true}}
        },
        style: {
            in: ["query"],
            errorMessage: 'require param style : string',
            not: true,
            isEmpty: false,
            optional: {options: {nullable: true}}
        },
        bedroom: {
            in: ["query"],
            errorMessage: 'require param bedroom : integer',
            isInt: true,
            toInt: true,
            optional: {options: {nullable: true}}
        },
        room_type: {
            in: ["query"],
            errorMessage: 'require param room_type : string',
            not: true,
            isEmpty: false,
            optional: {options: {nullable: true}}
        },
        approved: {
            in: ["query"],
            errorMessage: 'require param approved : bool',
            not: true,
            isEmpty: false,
            optional: {options: {nullable: true}},
            toBoolean: true
        }
    };

    const deleteLayout = {
        layout_id: {
            in: ["params"],
            errorMessage: 'require layout_id : string',
            not: true,
            isEmpty: true
        }
    };

    const createLayout = {

        layout_name: {
            in: ['body'],
            errorMessage: 'require layout_name : string',
            not: true,
            isEmpty: true
        },
        status: {
            in: ['body'],
            errorMessage: 'require status : string',
            customSanitizer: {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        },
        slug: {
            in: ['body'],
            errorMessage: 'require slug : string',
            customSanitizer: {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    const updateLayout = {
        layout_id: {
            in: ["params"],
            errorMessage: 'require layout_id : string',
            not: true,
            isEmpty: true
        },
        layout_name: {
            in: ['body'],
            errorMessage: 'require layout_name : string',
            not: true,
            isEmpty: true
        },
        status: {
            in: ['body'],
            errorMessage: 'require status : string',
            customSanitizer: {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        },
        slug: {
            in: ['body'],
            errorMessage: 'require slug : string',
            customSanitizer: {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    const duplicateLayout = {
        layout_id: {
            in: ["body"],
            errorMessage: 'require layout_id : int',
            not: true,
            isEmpty: true
        }
    };

    const postAddLayoutRoom = {
        layout_id: {
            in: ["body"],
            errorMessage: 'require layout_id : int',
            not: true,
            isEmpty: true
        }
    };

    const deleteLayoutRoom = {
        layout_id: {
            in: ["body"],
            errorMessage: 'require layout_id : int',
            not: true,
            isEmpty: true
        },
        room_id: {
            in: ["body"],
            errorMessage: 'require room_id : string',
            not: true,
            isEmpty: true
        }
    };

    const postAddLayoutGallery = {
        layout_id: {
            in: ["body"],
            errorMessage: 'require layout_id : int',
            not: true,
            isEmpty: true
        },
        gallery: {
            in: ["body"],
            errorMessage: 'require gallery : array',
            not: true,
            isEmpty: true
        }
    };

    const deleteLayoutGallery = {
        layout_id: {
            in: ["body"],
            errorMessage: 'require layout_id : int',
            not: true,
            isEmpty: true
        },
        gallery_id: {
            in: ["body"],
            errorMessage: 'require gallery_id : string',
            not: true,
            isEmpty: true
        }
    };

    const uploadLayoutGallery = {
        layout_id: {
            in: ["body"],
            errorMessage: 'require layout_id : int',
            not: true,
            isEmpty: true
        }
    };

    module.exports = function (rules) {
        rules.deleteLayout = deleteLayout;
        rules.updateLayout = updateLayout;
        rules.indexLayout = indexLayout;
        rules.createLayout = createLayout;
        rules.duplicateLayout = duplicateLayout;
        rules.postAddLayoutRoom = postAddLayoutRoom;
        rules.deleteLayoutRoom = deleteLayoutRoom;
        rules.postAddLayoutGallery = postAddLayoutGallery;
        rules.uploadLayoutGallery = uploadLayoutGallery;
        rules.deleteLayoutGallery = deleteLayoutGallery;
    }
})();