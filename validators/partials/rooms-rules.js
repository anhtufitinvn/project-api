(function () {

    const indexRoom = {
        page: {
            in: ["query"],
            errorMessage: 'require param page : string',
            not: true,
            isEmpty: true
        },
        name: {
            in: ["query"],
            errorMessage: 'require param name : string',
            not: true,
            isEmpty: false
        },
    };

    const deleteRoom = {
        room_id: {
            in: ["params"],
            errorMessage: 'require room_id : string',
            not: true,
            isEmpty: true
        }
    };

    const createRoom = {

        room_name : {
            in : ['body'],
            errorMessage: 'require room_name : string',
            not: true,
            isEmpty: true
        },
        status : {
            in : ['body'],
            errorMessage: 'require status : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        },
        slug : {
            in : ['body'],
            errorMessage: 'require slug : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    const updateRoom = {
        room_id: {
            in: ["params"],
            errorMessage: 'require room_id : string',
            not: true,
            isEmpty: true
        },
        room_name : {
            in : ['body'],
            errorMessage: 'require room_name : string',
            not: true,
            isEmpty: true
        },
        status : {
            in : ['body'],
            errorMessage: 'require status : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    const duplicateRoom = {
        room_id: {
            in: ["body"],
            errorMessage: 'require room_id : int',
            not: true,
            isEmpty: true
        }
    };

    module.exports = function (rules) {
        rules.deleteRoom = deleteRoom;
        rules.updateRoom = updateRoom;
        rules.indexRoom = indexRoom;
        rules.createRoom = createRoom;
        rules.duplicateRoom = duplicateRoom;
    }
})();