(function () {

    const loginByEmail = {
        email: {
            in: ["body"],
            errorMessage: 'require param email : string',
            not: true,
            isEmpty: true
        },
        password :{
            in: ["body"],
            errorMessage: 'require param password : string',
            not: true,
            isEmpty: true
        }
    };


    module.exports = function (rules) {
        rules.loginByEmail = loginByEmail;
    }
})();