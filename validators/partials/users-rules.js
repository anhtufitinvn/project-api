(function () {

    const auth = {
        email: {
            in: ["body"],
            errorMessage: 'require param email : string',
            not: true,
            isEmpty: true
        },
        password: {
            in: ["body"],
            errorMessage: 'require param password : string',
            not: true,
            isEmpty: false
        },
    };

    const indexUser = {
        page: {
            in: ["query"],
            errorMessage: 'require param page : string',
            not: true,
            isEmpty: true
        },
        name: {
            in: ["query"],
            errorMessage: 'require param name : string',
            not: true,
            isEmpty: false
        },
    };

    const deleteUser = {
        user_id: {
            in: ["params"],
            errorMessage: 'require user_id : string',
            not: true,
            isEmpty: true
        }
    };

    const createUser = {

        user_name : {
            in : ['body'],
            errorMessage: 'require user_name : string',
            not: true,
            isEmpty: true
        },
        status : {
            in : ['body'],
            errorMessage: 'require status : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        },
        slug : {
            in : ['body'],
            errorMessage: 'require slug : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    const updateUser = {
        user_id: {
            in: ["params"],
            errorMessage: 'require user_id : string',
            not: true,
            isEmpty: true
        },
        user_name : {
            in : ['body'],
            errorMessage: 'require user_name : string',
            not: true,
            isEmpty: true
        },
        status : {
            in : ['body'],
            errorMessage: 'require status : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    const duplicateUser = {
        user_id: {
            in: ["body"],
            errorMessage: 'require user_id : int',
            not: true,
            isEmpty: true
        }
    };

    module.exports = function (rules) {
        rules.deleteUser = deleteUser;
        rules.updateUser = updateUser;
        rules.indexUser = indexUser;
        rules.createUser = createUser;
        rules.duplicateUser = duplicateUser;
        rules.auth = auth ;
    ;
    }
})();