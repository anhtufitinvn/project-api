(function () {

    const indexProject = {
        // page: {
        //     in: ["query"],
        //     isInt: {
        //         options: {min: 1, max: 1000},
        //         errorMessage: 'require param page : integer > 0 and < 1000',
        //     },
        //     toInt : true
        // },
        name: {
            in: ["query"],
            errorMessage: 'require param name : string',
            not: true,
            isEmpty: false
        },
        project_id: {
            in: ["query"],
            errorMessage: 'require param project_id : integer',
            isInt: true,
            toInt : true,
            optional: { options: { nullable: true } }
        },
        city_id: {
            in: ["query"],
            errorMessage: 'require param city_id : integer',
            isInt: true,
            toInt : true,
            optional: { options: { nullable: true } }
        },
        district_id: {
            in: ["query"],
            errorMessage: 'require param district_id : integer',
            isInt: true,
            toInt : true,
            optional: { options: { nullable: true } }
        },
        status: {
            in: ["query"],
            errorMessage: 'require param status : string',
            not: true,
            isEmpty: false,
            optional: { options: { nullable: true } }
        }
    };


    const deleteProject = {
        project_id: {
            in: ["params"],
            errorMessage: 'require project_id : string',
            not: true,
            isEmpty: true
        }
    };

    const createProject = {

        project_name : {
            in : ['body'],
            errorMessage: 'require project_name : string',
            not: true,
            isEmpty: true
        },
        status : {
            in : ['body'],
            errorMessage: 'require status : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        },
        slug : {
            in : ['body'],
            errorMessage: 'require slug : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    const updateProject = {
        project_id: {
            in: ["params"],
            errorMessage: 'require project_id : string',
            not: true,
            isEmpty: true
        },
        project_name : {
            in : ['body'],
            errorMessage: 'require project_name : string',
            not: true,
            isEmpty: true
        },
        status : {
            in : ['body'],
            errorMessage: 'require status : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        },
        slug : {
            in : ['body'],
            errorMessage: 'require slug : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    module.exports = function (rules) {
        rules.deleteProject = deleteProject;
        rules.updateProject = updateProject;
        rules.indexProject = indexProject;
    }
})();