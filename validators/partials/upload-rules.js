(function () {

    const uploadImage = {
        image: {
            in: ["body"],
            errorMessage: 'require param image : file',
            not: true,
            isEmpty: true
        }
    };


    module.exports = function (rules) {
        rules.uploadImage = uploadImage;
    }
})();