(function () {
    let Handler = require('../core/handler');
    class ValidatorHandler extends Handler {
        constructor() {
            super();
            this.name = "ValidatorHandler";
            this.loadAllRules();
        }
    }

    module.exports = new ValidatorHandler();
})();