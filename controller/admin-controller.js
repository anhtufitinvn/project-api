var {debug, logger} = require('../core/logger');
var controller = Object.create(require('./controller'));
var dataService = require('../services/data-services');
var w2uiTransform = require('../transform/w2ui-transform');
var components = require('../render/components');

controller.loadRules(["projectId", "layoutId"]);

function sortByField(sorts, data) {
    if(sorts === undefined || sorts === null || sorts.length === 0) {
        return data;
    }

    let sort = sorts[0];
    let field = sort.field;
    let isString = field === 'project_name';
    let isASC = sort.direction === 'asc';
    data.records.sort((a, b) => {
        if (isASC) {
            if (isString) {
                return a[field].localeCompare(b[field]);
            }
            return a[field] - b[field];
        } else {
            if (isString) {
                return b[field].localeCompare(a[field]);
            }

            return b[field] - a[field];
        }

    });

    return data;
}

controller.getLayoutsByProjectId = async function (req, res, next) {
    try {
        let projectId = req.params['project_id'];
        let result = await dataService.getLayoutListByProjectIdEx(projectId);
        if (result) {
            let records = w2uiTransform.grid.layouts(result);
            res.json(records);
        }
    } catch (e) {
        res.json({total: 0, records: []});
        logger.error("getClientInbox got exception ", e);
    }
};

controller.getProjectList = async function (req, res, next) {
    try {
        let result = await dataService.getProjectList();
        if (result) {
            let data = w2uiTransform.grid.projects(result);
            if (req.body && req.body.request) {
                let w2uiRequest = JSON.parse(req.body.request);
                sortByField(w2uiRequest.sort, data);
            }

            res.json(data);
        }
    } catch (e) {
        res.json({total: 0, records: []});
        logger.error("getClientInbox got exception ", e);
    }
};

controller.renderLayoutJsonViewer = async function (req, res, next) {
    try {
        let layoutId = req.params["layout_id"];
        let component = components.LayoutViewer;
        let data = await component.getContent({layout_id: layoutId});
        debug(data);
        res.render(component.view, data);
        // todo : redirect error page if needed.
    } catch (e) {
        next(e);
    }
};

controller.renderProjectList = async function (req, res, next) {
    try {
        let component = components.ProjectList;
        let data = await component.getContent();
        debug(data);
        res.render(component.view, data);
        // todo : redirect error page if needed.
    } catch (e) {
        next(e);
    }
};


module.exports = controller;
