(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let FrontendDataService = Providers.FrontendDataService;
    let Transform = require('../transform/frontend/index');

    class LayoutsController extends Handler {
        constructor() {
            super();
            this.name = "LayoutsController";
        }

        async getLayouts(req, res, next) {
            let query = req.query;
            let data = await FrontendDataService.Layouts.getLayoutList(query);
            let result_data = Transform.layouts.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            }
            res.json(this.makeReturnMessage(result));

        };

        async getLayout(req, res, next) {
            let layoutId = req.params['layout_id'];
            
            let data = await BackendDataService.Layouts.getLayoutById(layoutId);

            res.json(this.makeReturnMessage(data));
        };


    }

    module.exports = new LayoutsController();
})();