var {debug, logger} = require('../core/logger');
var controller = Object.create(require('./controller'));
var hubspotService = require('../services/hubspot-services');
var synService = require('../services/syn-services');
var quotationServices = require('../services/quotation-services');
var urlServices = require('../services/url-services');
var transform = require('../transform/transforms');

controller.loadRules(["pageNum", "name", "quickForm"]);

controller.getAllContacts = async function (req, res, next) {
    try {
        let page = req.query['page'] || 0;
        let list = await hubspotService.getAllContacts(page);
        res.json(list);
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};

controller.getContactList = async function (req, res, next) {
    try {
        let page = req.query['page'] || 0;
        let list = await hubspotService.getContactList(page);
        res.json(list);
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};

controller.createtContactList = async function (req, res, next) {
    try {
        let name = req.query['name'] || 0;
        let list = await hubspotService.createContactList(name);
        res.json(list);
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};

controller.synAllContacts = async function (req, res, next) {
    try {
        await synService.synContactFromDB();
        res.json(controller.makeReturnMessage());
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};

controller.synLastestContacts = async function (req, res, next) {
    try {
        await synService.synLastestContactFromDB();
        res.json(controller.makeReturnMessage());
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};

controller.submitQuickForm = async function (req, res, next) {
    try {
        let ipaddress =  req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const action = req.body.action || null;
        let form = {
            email: req.body.email,
            firstname: req.body.name,
            phone : req.body.phone,
            message : req.body.message,
            project : req.body.project,
            budget : req.body.budget || 0,
            area : req.body.area || 0,
            cookies : {
                hubspotutk : req.body.hubspotutk || req.cookies['hubspotutk'] || null
            },
            id_address : ipaddress,
            page_url : "fitin.vn",
            page_name : "fitin consultant"
        };

        await hubspotService.submitQuickForm(form, action);
        res.json(controller.makeReturnMessage());
        let layout_id = req.body.layout_id || null;
        layout_id = parseInt(layout_id);
        if(!isNaN(layout_id)) {
            let quotation_url = urlServices.resolveDownloadQuotationLink(layout_id);
            if(action && action.toLowerCase() === 'home_renovation') {
                quotation_url = urlServices.resolveQuotationRenovationHtmlLink(layout_id, true);
            }
            await quotationServices.sendQuotationToEmail(form.email, {
                quotation_url: quotation_url,
                firstname :form.firstname,
                project : form.project
            });
        }
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};


module.exports = controller;
