var {debug, logger} = require('../core/logger');
var controller = Object.create(require('./controller'));
var dataServices = require('../services/data-services');
var quotationServices = require('../services/quotation-services');

controller.loadRules(["layoutId"]);

controller.getItemsFromLayout = async function (req, res, next) {
    try {
        let layoutId = req.params['layout_id'];
        let result = await quotationServices.getUnityLayoutById(layoutId);
        if (result) {
            res.json(controller.makeReturnMessage(result));
        }
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getItemsFromLayout got exception ", e);
    }
};

module.exports = controller;
