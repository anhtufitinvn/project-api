(function () {
    let Providers = require('../server/providers');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let FrontendDataService = Providers.FrontendDataService;
    let Transform = require('../transform/frontend/index');

    class LayoutsRenovationController extends Handler {
        constructor() {
            super();
            this.name = "LayoutsRenovationController";
        }

        async getLayouts(req, res, next) {
            let query = req.query;
            query.status = ENUMS.STATUS.PUBLISHED;
            let data = await FrontendDataService.LayoutsRenovation.getLayoutList(query);
            let result_data = Transform.layoutsRenovation.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            }
            res.json(this.makeReturnMessage(result));

        };

        async getLayouts4Horo(req, res, next) {
            let query = req.query;
            query.page = 1;
            let data = await FrontendDataService.LayoutsRenovation.getLayoutList(query);
            let result_data = Transform.layoutsRenovation.list(data.docs);
            let result = {
                list : result_data,
                meta : {
                    total : data.totalDocs,
                    limit: data.limit,
                    page : data.page,
                    pages : data.totalPages,
                }
            }
            res.json(this.makeReturnMessage(result));

        };

        async getLayout(req, res, next) {
            let layoutId = req.params['layout_id'];
            
            let data = await FrontendDataService.LayoutsRenovation.getLayoutById(layoutId);

            res.json(this.makeReturnMessage(data));
        };


    }

    module.exports = new LayoutsRenovationController();
})();