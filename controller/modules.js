(function () {
    module.exports = {
        //SampleHandler : require('./sample-handler'),
        //ProjectsHandler : require('./projects-handler'),
        LayoutsController : require('./layouts-controller'),
        
        LayoutsRenovationController :  require('./layouts-renovation-controller'),
        //RoomsHandler : require('./rooms-handler'),
        ResourcesController : require('./resources-controller'),

        ProjectsController : require('./projects-controller')
        //UploadHandler : require('./upload-handler'),
        //UsersHandler : require('./users-handler'),
        //AuthHandler : require('./auth-handler'),
        //CartsHandler : require('./carts-handler'),
    }
})();