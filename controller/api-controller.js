var {debug, logger} = require('../core/logger');
var controller = Object.create(require('./controller'));
var dataService = require('../services/data-services');
var resourcesService = require('../services/resources-services');
var renderService = require('../services/render-services');
var urlsService = require('../services/url-services');
var cacheServices = require('../services/cached-services');
var quotationServices = require('../services/quotation-services');
var synService = require('../services/syn-services');
var fs = require('fs-extra');

controller.loadRules(["pageNum", "email", "findEmail", "projects", "signature"]);

controller.getContactList = async function (req, res, next) {
    try {
        let page = req.query['page'];
        let result = await dataService.getContactList(page);
        if (result) {
            res.json(controller.makeReturnMessage(result));
        }
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};

controller.findEmailContactStartWith = async function (req, res, next) {
    try {
        let email = req.query['email'];
        let result = await dataService.findEmailContactStartWith(email);
        if (result) {
            res.json(controller.makeReturnMessage(result));
        }
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getContactList got exception ", e);
    }
};

controller.getProjectList = async function (req, res, next) {
    try {
        let page = req.query['page'];
        let project_id = req.query['project_id'];
        if(project_id) {
            let result = await dataService.getLayoutListByProjectId(project_id, page);
            if (result) {
                res.json(controller.makeReturnMessage(result));
            }
        } else {
            let result = await dataService.getProjectList(page);
            if (result) {
                res.json(controller.makeReturnMessage(result));
            }
        }
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getProjectList got exception ", e);
    }
};

controller.getFormProperties = async function (req, res, next) {
    try {
        let result = await resourcesService.getFormProperties();
        res.json(controller.makeReturnMessage(result));
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getFormProperties got exception ", e);
    }
};

controller.downloadQuotation = async function (req, res, next) {
    try {
        let signature = req.params["signature"];
        let url = urlsService.resolveQuotationHtmlLink(signature);
        let result = await renderService.exportQuotation2Pdf(url, signature);
        res.writeHead(200, {
            "Content-Type": "application/octet-stream",
            "Content-Disposition": "attachment; filename=" + result.file_name
        });
        fs.createReadStream(result.file_path).pipe(res);
    } catch (e) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("ERROR File does not exist");
        logger.error("downloadLayoutQuotation got exception ", e);
    }
};

/**
 * @deprecated
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
controller.buildLayoutQuotation = async function (req, res, next) {
    try {
        let layout_id = req.params["layout_id"];
        let key = `quotation-${layout_id}.pdf`;
        let cache = cacheServices.get(key);
        if(cache) {
            res.json(controller.makeReturnMessage({quotation_url : cache.url}));
            return;
        }

        let url = urlsService.resolveLayoutQuotationHtmlLink(layout_id, true);
        let result = await renderService.exportQuotation2Pdf(url, layout_id);
        let quotation_url = urlsService.resolveDownloadQuotationLink(layout_id);
        if(result) {
            cacheServices.set(key, {file_name : result.file_name, file_path : result.file_path, url : quotation_url});
        }

        res.json(controller.makeReturnMessage({quotation_url : quotation_url}));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot generate quotation pdf file"));
    }
};

controller.buildLayoutTemplateQuotation = async function (req, res, next) {
    try {
        let layout_id = req.params["layout_id"];
        let key = `layout-template-quotation-${layout_id}.pdf`;
        let cache = cacheServices.get(key);
        if(cache) {
            res.json(controller.makeReturnMessage({quotation_url : cache.url}));
            return;
        }

        let url = urlsService.resolveLayoutTemplateQuotationHtmlLink(layout_id, true);
        let result = await renderService.exportQuotation2Pdf(url, layout_id);
        let quotation_url = urlsService.resolveDownloadQuotationLink(layout_id);
        if(result) {
            cacheServices.set(key, {file_name : result.file_name, file_path : result.file_path, url : quotation_url});
        }

        res.json(controller.makeReturnMessage({quotation_url : quotation_url}));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot generate quotation pdf file"));
    }
};

controller.buildLayoutRenovationQuotation = async function (req, res, next) {
    try {
        let layout_id = req.params["layout_id"];
        let key = `layout-renovation-quotation-${layout_id}.pdf`;
        let cache = cacheServices.get(key);
        if(cache) {
            res.json(controller.makeReturnMessage({quotation_url : cache.url}));
            return;
        }

        let url = urlsService.resolveQuotationRenovationHtmlLink(layout_id, true);
        let result = await renderService.exportQuotation2Pdf(url, layout_id, true);
        let quotation_url = urlsService.resolveDownloadRenovationQuotationLink(layout_id);
        if(result) {
            cacheServices.set(key, {file_name : result.file_name, file_path : result.file_path, url : quotation_url});
        }

        res.json(controller.makeReturnMessage({quotation_url : quotation_url}));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot generate quotation pdf file"));
    }
};

controller.flushAllCache = async function (req, res, next) {
     let accessKey = req.params['access_key'] || null;
     if(accessKey === "AhjakdasDdaidlatuyerit56e") {
         cacheServices.flushAll();
         res.json(controller.makeReturnMessage("cached has been cleared."));
     } else {
         res.json(controller.makeReturnError('access denied.'));
     }
};

controller.getLocations = async function (req, res, next) {
    try {
        let lc = await resourcesService.getLocations();
        res.json(controller.makeReturnMessage(lc));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot get locations"));
    }
};

/***
 * This function is used for HR
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
controller.getPhoneSMS4Spam = async function (req, res, next) {
    try {
        let lc = await resourcesService.getSpamSMSPhoneList();
        res.json(controller.makeReturnMessage({
            contents : [
                "Fitin.vn - Tuyen dung 20 ban chua co kinh nghiem ve Business Development.Dao tao duoc huong luong.Ban nao co nhu cau vui long gui CV den tuyendung@fitin.vn",
                "FITIN - Tuyen dung 20 ban chua co kinh nghiem ve Business Development.Dao tao duoc huong luong. Ban nao co nhu cau vui long gui CV den email tuyendung@fitin.vn",
                "Fitin.vn - Tuyen dung 20 ban chua co kinh nghiem ve Business Development.Dao tao duoc huong luong.Ban nao co nhu cau vui long gui CV den tuyendung@fitin.vn"
            ],
            list : lc
        }));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot get locations"));
    }
};

/***
 * This function is used for HR
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
controller.importPhoneSMS4Spam = async function (req, res, next) {
    try {
        let list = req.body["list"] || null;
        if(!list) {
            return res.json(controller.makeReturnError("list phone number is invalid"));
        }

        list = list.map(m => {
            return {
                name : null,
                phone : m
            }
        });

        let lc = await resourcesService.importPhoneSMS4Spam(list);
        res.json(controller.makeReturnMessage(lc));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot import phones"));
    }
};

/***
 * This function is used for HR
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
controller.updateSentPhoneSMS4Spam = async function (req, res, next) {
    try {
        let phone = req.body["phone"] || null;
        if(!phone) {
            return res.json(controller.makeReturnError("phone number is invalid"));
        }

        await resourcesService.updatePhoneSMS4Spam(phone);
        res.json(controller.makeReturnMessage());
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot import phones"));
    }
};

/**
 * @deprecated It will be removed in next version
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
controller.migrateProjectImagesFromECOM = async function(req, res, next) {
    try {
        let rs = await synService.migrateProjectImagesFromECOM();
        res.json(controller.makeReturnMessage(rs));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot get migrate images"));
    }
};

/***
 * @deprecated It will be removed in next version
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
controller.migrateLayoutImagesFromECOM = async function(req, res, next) {
    try {
        let rs = await synService.migrateLayoutImagesFromECOM();
        res.json(controller.makeReturnMessage(rs));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot get migrate images"));
    }
};

/***
 * @deprecated It will be removed in next version
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
controller.migrateLayoutMetadataFromECOM = async function(req, res, next) {
    try {
        let rs = await synService.migrateLayoutMetadataFromECOM();
        res.json(controller.makeReturnMessage(rs));
    } catch (e) {
        res.json(controller.makeReturnError(e.message || "cannot get migrate metadata"));
    }
};

module.exports = controller;
