(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    let Handler = require('../core/handler');
    let Transform = require('../transform/transforms');
    let FrontendDataService = Providers.FrontendDataService;
    let TransformFrontend = require('../transform/frontend/index');
    let ENUMS = global.ENUMS;

    class ResourcesController extends Handler {
        constructor() {
            super();
            this.name = "ResourcesController";
        }

        async getLocations(req, res, next) {
            let result = await global.locations;
            res.json(this.makeReturnList(result.list));
        };

        async getStyles(req, res, next) {
            
            let data = await BackendDataService.Styles.getStyleList(req.query);
            
            let result_data = TransformBackend.styles.list(data);
            let result = {
                list : result_data,
            }
            res.json(this.makeReturnMessage(result));
        };


        async getRoomCategories(req, res, next) {
            let query = req.query;
            query.status = ENUMS.STATUS.PUBLISHED;
            let data = await FrontendDataService.RoomCategories.getRoomCategoryList(req.query);
            
            let result_data = TransformFrontend.roomCategories.list(data);
            let result = {
                list : result_data,
            }
            res.json(this.makeReturnMessage(result));
        };

    }

    module.exports = new ResourcesController();
})();