(function () {
    let Providers = require('../server/providers');
    let QtTransporter = require('../server/quotation-transporter');
    let Handler = require('../core/handler');
    let DataServiceV2 = Providers.DataServiceV2;
    let QuotationService = Providers.QuotationService;
    let UrlService = Providers.UrlService;

    class APIV2Handler extends Handler {
        constructor() {
            super();
            this.name = "LayoutsHandler";
        }

        async getProjects(req, res, next) {
            let page = req.query['page'] || null;
            let projectId = req.query['project_id'] || null;
            let intID = parseInt(projectId);
            if (isNaN(intID)) {
                // search by string
            } else {
                // search by number
            }
            let data = await DataServiceV2.Projects.getProjectList(page);
            res.json(this.makeReturnMessage(data));
        };

        async getLayoutListByProjectId(req, res, next) {
            let page = req.query['page'] || null;
            let projectId = req.params['project_id'] || null;
            let intID = parseInt(projectId);
            let data = null;
            if (isNaN(intID)) {
                // search by string
                data = await DataServiceV2.Layouts.getLayoutListByProjectId(projectId, page);
            } else {
                // search by number
                data = await DataServiceV2.Layouts.getLayoutListByProjectIntId(intID);
            }

            res.json(this.makeReturnMessage(data));
        };

        async generateAllRenovationQuotation(req, res, next) {
            //// TODO need check access-key from header
            let queueLength = await QtTransporter.countTaskInQueue();
            if (queueLength > 0) {
                return res.json(this.makeReturnMessage({
                    amount_of_quotation: queueLength,
                    monitor_url: UrlService.resolveMonitorBuildQuotationUrl(true)
                }));
            }
            ///// step 1 : load all layoutID from DB
            let list = await DataServiceV2.DAO.LayoutRenovation.getPublishedList(0, 1000);
            if (list) {
                //// step 2 : Add list to redis
                list.forEach(q => {
                    QtTransporter.queue({
                        action: QtTransporter.ACTIONS.GENERATE_QT_RENOVATION,
                        params: {
                            layout_id: q.int_id
                        }
                    })
                });

                return res.json(this.makeReturnMessage({
                    amount_of_quotation: list.length || 0
                }));
            }

            return res.json(this.makeReturnError("failed to load layouts data"));
        }

        async generateRenovationQuotationLayout(req, res, next) {
            try {
                let layout_id = req.params["layout_id"];
                let rs = await QuotationService.generatePDFFile(layout_id, true);
                res.json(this.makeReturnMessage({quotation_url: rs.quotation_url, download_url: rs.download_url}));
            } catch (e) {
                res.json(this.makeReturnError(e.message || "cannot generate quotation pdf file"));
            }
        };

        async generateTemplateQuotationLayout(req, res, next) {
            try {
                let layout_id = req.params["layout_id"];
                let rs = await QuotationService.generatePDFFile(layout_id, false);
                res.json(this.makeReturnMessage({quotation_url: rs.quotation_url, download_url: rs.download_url}));
            } catch (e) {
                res.json(this.makeReturnError(e.message || "cannot generate quotation pdf file"));
            }
        };

        async broadcastNotificationToCMS(req, res, next) {
            let NotificationPlugin = getPlugin(PluginNames.NAME_NOTIFICATION);
            let event = req.body["event"];
            let data = req.body["data"];
            if(event && data && NotificationPlugin) {
                await NotificationPlugin.broadcast(event, data);
                return res.json(this.makeReturnMessage());
            }

            return res.json(this.makeReturnError("broadcast event message to CMS has been failed, invalid params"));
        }

    }

    module.exports = new APIV2Handler();
})();