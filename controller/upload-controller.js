var {debug, logger} = require('../core/logger');
var controller = Object.create(require('./controller'));
var storeService = require('../services/store-services');
var urlServices = require('../services/url-services');
var ecomService = require('../services/ecom-services');
var dataService = require('../services/data-services');
var renderService = require('../services/render-services');
var cartService = require('../services/cart-services');
let {cartItem} = require('../models/models');
const consultantService = require('../services/consultant-services');

controller.loadRules(["uploadResult"]);

async function unzipPipeline(req) {
    let zipFile = req.file;
    if (zipFile.absolutePath === undefined) {
        throw new Error("Upload file got an exception");
    }

    let unzip = await storeService.unzip(zipFile.absolutePath);
    let files = await storeService.getFilesInDirectory(unzip);
    if (files) {
        let fileUrls = files.map(f => {
            return urlServices.resolveFileStaticLink(f);
        });

        let imagesPath = files.filter(f => {
            return !f.endsWith(".json");
        });

        imagesPath = imagesPath.map(f => {
            return urlServices.resolveFileRelativePath(f);
        });

        const fitinfurnitureFee = zipFile.fitinfurniture_fee || 0;
        const constructionFee = zipFile.construction_fee || 0;

        let payload = {
            email: req.body.email,
            image_paths: imagesPath,
            cart: null,
            metadata: {
                serial_key: zipFile.serial_key || req.body.serial_key,
                project_id: zipFile.project_id || req.body.project_id,
                project_name: zipFile.project_name || req.body.project_name,
                layout_id: zipFile.layout_id || req.body.layout_id,
                layout_name: zipFile.layout_name || req.body.layout_name,
                consultant_id: zipFile.consultant_id || req.body.consultant_id,
                bed_room_num: zipFile.bed_room_num || 1,
                area: zipFile.area ? zipFile.area.toString() : "0.0",
                construction_fee: constructionFee,
                fitinfurniture_fee: fitinfurnitureFee,
                discount: zipFile.discount || 0
            }
        };

        // get carts.json
        let jsonFile = files.filter(f => {
            return f.endsWith(".json");
        });

        if (jsonFile === null || jsonFile.length === 0) {
            return {image_urls: fileUrls, cart_url: null};
        }

        let cart = await cartService.buildCartFromJsonFile(jsonFile[0], payload);
        if (cart) {
            payload.cart = cart;
            let record_id = await dataService.addClientInbox(payload);
            if (record_id) {
                return {
                    id: record_id,
                    signature: cart.cart_sign,
                    image_urls: fileUrls,
                    cart_url: urlServices.resolveCartLink(cart.cart_sign)
                };
            }

            throw new Error("could not insert data to client inbox");
        }
    }

    throw new Error("unzip failed");
}

controller.uploadZipFile = async function (req, res, next) {
    storeService.uploadZipFile(req, res, (async (err) => {
        try {
            if (err) {
                res.json(controller.makeReturnError(err.message || "unzip failed", -1));
                return;
            }

            let result = await unzipPipeline(req);
            res.json(controller.makeReturnMessage(result));
            let uid = await dataService.getUserIDByEmail(req.body.email);
            if (uid > 0) {
                renderService.pushAlertMessage(uid, "Fitin vừa gửi cho bạn 1 kết quả tư vấn !");
                if (result.signature) {
                    try {
                        let url = urlServices.resolveQuotationHtmlLink(result.signature);
                        await renderService.exportQuotation2Pdf(url, result.signature);
                    } catch (ex) {
                        logger.error("generate PFD got exception ", ex);
                    }
                }
            }
        } catch (e) {
            res.json(controller.makeReturnError(e, -1));
            logger.error("uploadZipFile got exception ", e);
        }
    }));
};

/** Handle Renovation Images **/
function checkAndOverrideMetadata(metadata, cartMetadata) {
    if (!metadata.project_name) {
        metadata.project_name = cartMetadata.project_name;
    }

    if (!metadata.layout_name) {
        metadata.layout_name = cartMetadata.layout_name;
    }

    if (!metadata.area) {
        metadata.area = cartMetadata.area ? cartMetadata.area.toString() : "0.0";
    }

    if (!metadata.bed_room_num) {
        metadata.bed_room_num = cartMetadata.bed_room_num || 1;
    }

    if (!metadata.consultant_id) {
        metadata.consultant_id = cartMetadata.consultant_id || 0;
    }

    if (!metadata.serial_key) {
        metadata.serial_key = cartMetadata.serial_key || null;
    }

    if (!metadata.construction_fee) {
        metadata.construction_fee = cartMetadata.construction_fee || null;
    }

    if (!metadata.fitinfurniture_fee) {
        metadata.fitinfurniture_fee = cartMetadata.fitinfurniture_fee || null;
    }
}

/** Ho tro tao project va layout khi import gallery  **/
/**
 * @deprecated hien tai khong con support phuong thuc nay nua.
 * @param req
 * @returns {Promise<{project_id: *, layout_id: *, project_name: *, layout_name: *, signature: (extensions.cart_json.cart_sign|{type, default}|cart.cart_sign|{type, required, index}|null), image_urls: Array, cart_url: string}>}
 */
async function unzipPipeline4RenovationCMSEx(req) {
    let zipFile = req.file;
    if (zipFile.absolutePath === undefined) {
        throw new Error("Upload file got an exception");
    }

    let unzip = await storeService.unzip(zipFile.absolutePath);
    let files = await storeService.getFilesInDirectory(unzip);
    if (files) {
        let imageUrls = [];
        let imagesPath = [];
        files.map(f => {
            if (!f.endsWith(".json")) {
                imageUrls.push(urlServices.resolveFileStaticLink(f));
                imagesPath.push(urlServices.resolveFileRelativePath(f));
            }
        });

        const fitinfurnitureFee = zipFile.fitinfurniture_fee || 0;
        const constructionFee = zipFile.construction_fee || 0;

        let payload = {
            email: req.body.email,
            image_paths: imagesPath,
            cart: null,
            metadata: {
                serial_key: zipFile.serial_key,
                project_id: zipFile.project_id,
                project_name: zipFile.project_name,
                layout_id: zipFile.layout_id,
                layout_name: zipFile.layout_name,
                consultant_id: zipFile.consultant_id,
                bed_room_num: zipFile.bed_room_num,
                area: zipFile.area ? zipFile.area.toString() : null,
                construction_fee: constructionFee,
                fitinfurniture_fee: fitinfurnitureFee,
                discount: zipFile.discount || 0
            }
        };

        ///// resolve files
        let jsonFiles = {};
        files.forEach(f => {
            if (f.endsWith("carts.json") && jsonFiles["cart"] === undefined) {
                jsonFiles["cart"] = f;
            } else if (f.endsWith("layout.json") && jsonFiles["layout"] === undefined) {
                jsonFiles["layout"] = f;
            } else if (f.endsWith("config.json") && jsonFiles["config"] === undefined) {
                jsonFiles["config"] = f;
            }
        });


        if (jsonFiles.config) {
            throw new Error("Image gallery didn't support panorama format, please remove config.json and retry again");
        }

        if (!jsonFiles.cart) {
            throw new Error("carts.json not found");
        }

        if (!jsonFiles.layout) {
            throw new Error("layout.json not found");
        }

        let layoutJSON = await storeService.readFileJson(jsonFiles.layout);
        let cartJson = await storeService.readFileJson(jsonFiles.cart);
        //// doc lai thong tin tu json cart , kiem tra metadata ten du an, project
        if (cartJson && cartJson.metadata) {
            checkAndOverrideMetadata(payload.metadata, cartJson.metadata);
        }

        if (cartJson && cartJson.client) {
            payload.client = {
                email: payload.email || cartJson.client.email || null,
                phone: payload.phone || cartJson.client.phone || null,
            }
        }


        //// !!! very importance, ignore layout_id and project_id if create mode
        if (req.params.isCreate) {
            payload.metadata.layout_id = null;
            payload.metadata.project_id = null;
        }

        //// check project and layout
        let pl = await consultantService.getOrCreateProjectAndLayoutRenovation(payload);
        if (!pl || !pl.int_id) {
            if (req.params.isCreate || req.method === "POST" || req.method === "POST") {
                throw new Error("project and layout not found");
            }

            throw new Error("failed to create project and layout");
        }

        const layoutId = pl.int_id;
        //// build cart
        let cartSign = null;
        if (pl.extensions && pl.extensions.cart_json) {
            cartSign = pl.extensions.cart_json.cart_sign || null;
        }
        //// create cart from ecom
        let cart = await cartService.buildCartFromJsonFile(jsonFiles.cart, payload, cartSign);
        if (cart) {
            payload.cart = cart;
            await Promise.all([
                consultantService.updateLayoutRenovationJson(layoutId, layoutJSON),
                consultantService.addLayoutRenovationCart(cartJson, cart, layoutId),
                consultantService.updateLayoutRenovationGallery(layoutId, imagesPath)
            ]);

            return {
                project_id: pl.project_int_id,
                layout_id: layoutId,
                project_name: pl.project_name,
                layout_name: pl.layout_name,
                signature: cart.cart_sign,
                image_urls: imageUrls,
                cart_url: urlServices.resolveCartLink(cart.cart_sign)
            };
        }

        throw new Error("failed to build cart");

    }

    throw new Error("unzip failed");
}

async function unzipPipeline4RenovationCMS(req) {
    let zipFile = req.file;
    if (zipFile.absolutePath === undefined) {
        throw new Error("Upload file got an exception");
    }

    let unzip = await storeService.unzip(zipFile.absolutePath);
    let files = await storeService.getFilesInDirectory(unzip);
    if (files) {
        let imageUrls = [];
        let imagesPath = [];
        files.map(f => {
            if (!f.endsWith(".json")) {
                imageUrls.push(urlServices.resolveFileStaticLink(f));
                imagesPath.push(urlServices.resolveFileRelativePath(f));
            }
        });

        const fitinfurnitureFee = zipFile.fitinfurniture_fee || 0;
        const constructionFee = zipFile.construction_fee || 0;

        let payload = {
            email: req.body.email,
            image_paths: imagesPath,
            cart: null,
            metadata: {
                serial_key: zipFile.serial_key,
                project_id: zipFile.project_id,
                project_name: zipFile.project_name,
                layout_id: zipFile.layout_id,
                layout_name: zipFile.layout_name,
                consultant_id: zipFile.consultant_id,
                bed_room_num: zipFile.bed_room_num,
                area: zipFile.area ? zipFile.area.toString() : null,
                construction_fee: constructionFee,
                fitinfurniture_fee: fitinfurnitureFee,
                discount: zipFile.discount || 0
            }
        };

        ///// resolve files
        let jsonFiles = {};
        files.forEach(f => {
            if (f.endsWith("carts.json") && jsonFiles["cart"] === undefined) {
                jsonFiles["cart"] = f;
            } else if (f.endsWith("layout.json") && jsonFiles["layout"] === undefined) {
                jsonFiles["layout"] = f;
            } else if (f.endsWith("config.json") && jsonFiles["config"] === undefined) {
                jsonFiles["config"] = f;
            }
        });


        if (jsonFiles.config) {
            throw new Error("Image gallery didn't support panorama format, please remove config.json and retry again");
        }

        if (!jsonFiles.cart) {
            throw new Error("carts.json not found");
        }

        let cartJson = await storeService.readFileJson(jsonFiles.cart);
        //// doc lai thong tin tu json cart , kiem tra metadata ten du an, project
        if (cartJson && cartJson.metadata) {
            checkAndOverrideMetadata(payload.metadata, cartJson.metadata);
        }

        if (cartJson && cartJson.client) {
            payload.client = {
                email: payload.email || cartJson.client.email || null,
                phone: payload.phone || cartJson.client.phone || null,
            }
        }

        //// check project and layout
        const layoutId = zipFile.layout_id;
        let rs = await consultantService.updateLayoutRenovationGallery(layoutId, imagesPath);
        if(rs) {
            return {
                project_id: payload.metadata.project_id,
                layout_id: layoutId,
                project_name:payload.metadata.project_name,
                layout_name: payload.metadata.layout_name,
                image_urls: imageUrls
            };
        }

        throw new Error("failed to import gallery, layout not found");

    }

    throw new Error("unzip failed");
}

controller.importRenovationZipFileCMS = async function (req, res, next) {
    req.params.is_cms_mode = true;
    storeService.uploadZipFileForCMS(req, res, (async (err) => {
        try {
            if (err) {
                res.json(controller.makeReturnError(err.message || "unzip failed", -1));
                return;
            }

            let result = await unzipPipeline4RenovationCMS(req);
            res.json(controller.makeReturnMessage(result));
        } catch (e) {
            res.json(controller.makeReturnError(e, -1));
            logger.error("uploadZipFile got exception ", e);
        }
    }));
};

module.exports = controller;
