var {debug, logger} = require('../core/logger');
var controller = Object.create(require('./controller'));
var dataService = require('../services/data-services');

controller.loadRules([]);


controller.getClientInbox = async function (req, res, next) {
    try {
        let email = req.query['email'];
        if(req.user && req.user.email.toLowerCase() === email.toLocaleString()) {
            let result = await dataService.getClientInbox(email);
            if (result) {
                res.json(controller.makeReturnMessage(result));
            }
        } else {
            res.json(controller.makeReturnError("email and token didn't match", -1));
        }
    } catch (e) {
        res.json(Handler.makeReturnError(e, -1));
        logger.error("getClientInbox got exception ", e);
    }
};

controller.getClientInboxes = async function (req, res, next) {
    try {
        let email = req.query['email'];
        if(req.user && req.user.email.toLowerCase() === email.toLocaleString()) {
            let result = await dataService.getClientInboxes(email);
            if (result) {
                res.json(controller.makeReturnMessage(result));
            }
        } else {
            res.json(controller.makeReturnError("email and token didn't match", -1));
        }
    } catch (e) {
        res.json(controller.makeReturnError(e, -1));
        logger.error("getClientInbox got exception ", e);
    }
};



module.exports = controller;
