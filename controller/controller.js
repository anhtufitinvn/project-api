const {matchedData, check, checkSchema, validationResult} = require('express-validator');
const ruleList = require('../validators/rules-schema');
const Controller = {
    ruleSchemas: {}
};

Controller.loadRules = function (rules) {
    if (rules && rules.length > 0) {
        rules.map(r => {
            if (ruleList[r] && !this.ruleSchemas[r]) {
                this.ruleSchemas[r] = checkSchema(ruleList[r]);
            }
        })
    }
};

Controller.errorHandler = function (req, res, next) {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        req.matchedData = matchedData(req);
        return next();
    }

    const extractedErrors = [];
    errors.array().map(err => extractedErrors.push({[err.param]: err.msg}));
    return res.status(200).json({
        code: -1,
        message: "invalid parameters",
        errors: extractedErrors,
    });
};

Controller.makeReturnMessage = function (data) {
    return {
        code: 0,
        message: "success",
        data: data
    };
};

Controller.makeReturnError = function (message, errorCode) {
    if(message instanceof Error) {
        return {
            code: -1,
            message: message.message || "got an exception",
            errors: [
                {
                    error: errorCode || -1,
                    message: message["message"] || "something went wrong"
                }
            ]
        };
    }

    return {
        code: -1,
        message: message,
        errors: [
            {
                error: errorCode || -1,
                message: message
            }
        ]
    };
};

Controller.validate = function (ruleName) {
    if (ruleName && this.ruleSchemas[ruleName]) {
        return [this.ruleSchemas[ruleName], this.errorHandler];
    }

    return [];
};

module.exports = Controller;