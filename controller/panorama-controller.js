var fs = require('fs-extra');
var {debug, logger} = require('../core/logger');
var controller = Object.create(require('./controller'));
var storeService = require('../services/store-services');
var urlServices = require('../services/url-services');
var consultantService = require('../services/consultant-services');
var dataService = require('../services/data-services');
var renderService = require('../services/render-services');
var cartService = require('../services/cart-services');
const discountServices = require('../services/discounts-services');
var moment = require('moment');


controller.loadRules(["uploadResult"]);

function checkAndOverrideMetadata(metadata, cartMetadata) {

    if (!metadata.project_name) {
        metadata.project_name = cartMetadata.project_name;
    }

    if (!metadata.layout_name) {
        metadata.layout_name = cartMetadata.layout_name;
    }

    if (!metadata.area) {
        metadata.area = cartMetadata.area ? cartMetadata.area.toString() : "0.0";
    }

    if (!metadata.bed_room_num) {
        metadata.bed_room_num = cartMetadata.bed_room_num || 1;
    }

    if (!metadata.consultant_id) {
        metadata.consultant_id = cartMetadata.consultant_id || 0;
    }

    if (!metadata.serial_key) {
        metadata.serial_key = cartMetadata.serial_key || null;
    }

    if (!metadata.construction_fee) {
        metadata.construction_fee = cartMetadata.construction_fee || null;
    }

    if (!metadata.fitinfurniture_fee) {
        metadata.fitinfurniture_fee = cartMetadata.fitinfurniture_fee || null;
    }
}

async function unzipPipeline4Consultant(req) {
    let zipFile = req.file;
    if (zipFile.absolutePath === undefined) {
        throw new Error("Upload file got an exception");
    }

    const layoutId = zipFile.layout_id || req.body.layout_id;
    let UUID = zipFile.uuid;
    let now = moment();
    const sYear = now.year().toString();
    const md = now.format('M-D');
    let unzip = await storeService.unzip4CMS(zipFile.absolutePath, urlServices.join(sYear, md));
    let files = await storeService.getFilesInDirectory(unzip);
    if (files) {
        const fitinfurnitureFee = zipFile.fitinfurniture_fee || 0;
        const constructionFee = zipFile.construction_fee || 0;

        let payload = {
            email: req.body.email,
            panorama_url: null,
            cart: null,
            metadata: {
                serial_key: zipFile.serial_key || req.body.serial_key,
                project_id: zipFile.project_id || req.body.project_id,
                project_name: zipFile.project_name || req.body.project_name,
                layout_id: layoutId,
                layout_name: zipFile.layout_name || req.body.layout_name,
                consultant_id: zipFile.consultant_id || req.body.consultant_id,
                bed_room_num: zipFile.bed_room_num || 1,
                area: zipFile.area ? zipFile.area.toString() : "0.0",
                construction_fee: constructionFee,
                fitinfurniture_fee: fitinfurnitureFee,
                discount: zipFile.discount || 0
            }
        };

        // get carts.json
        let jsonFile = files.filter(f => {
            return f.endsWith("config.json");
        });

        // get carts.json
        let cartFile = files.filter(f => {
            return f.endsWith("carts.json");
        });


        if (jsonFile === null || jsonFile.length === 0) {
            throw new Error("panorama config.json not found");
        }


        if (cartFile === null || cartFile.length === 0) {
            throw new Error("panorama carts.json not found");
        }

        let panorama = await createPanoramaConfigFromJsonFile(jsonFile[0]);
        if (panorama && panorama.file_name) {
            let ticket = `${sYear}/${md}/${UUID}/${panorama.file_name}`;
            debug("ticket", ticket);
            ticket = Buffer.from(ticket).toString('base64');
            payload.panorama_url = urlServices.resolvePanoramaLink(ticket);
            //// build cart
            let cart = await cartService.buildCartFromJsonFile(cartFile[0], payload);
            if (cart) {
                payload.cart = cart;
                let record_id = await dataService.addClientInbox(payload);
                if (record_id) {
                    return {
                        id: record_id,
                        signature: cart.cart_sign,
                        cart_url: urlServices.resolveCartLink(cart.cart_sign)
                    };
                }

                throw new Error("failed to add panorama to client inbox");
            }

            throw new Error("failed to build cart");
        }
    }

    throw new Error("unzip failed");
}

async function unzipPipeline4CMS(req) {
    let zipFile = req.file;
    if (zipFile.absolutePath === undefined) {
        throw new Error("Upload file got an exception");
    }

    const layoutId = zipFile.layout_id || req.body.layout_id;
    let UUID = zipFile.uuid;
    let now = moment();
    const sYear = now.year().toString();
    const md = now.format('M-D');
    let unzip = await storeService.unzip4CMS(zipFile.absolutePath, urlServices.join(sYear, md));
    let files = await storeService.getFilesInDirectory(unzip);
    if (files) {
        // get config.json
        let jsonFile = files.filter(f => {
            return f.endsWith("config.json");
        });

        if (jsonFile === null || jsonFile.length === 0) {
            throw new Error("panorama config.json not found");
        }

        let panorama = await createPanoramaConfigFromJsonFile(jsonFile[0]);
        if (panorama && panorama.file_name) {
            let ticket = `${sYear}/${md}/${UUID}/${panorama.file_name}`;
            debug("ticket", ticket);
            ticket = Buffer.from(ticket).toString('base64');
            let panoramaUrl = urlServices.resolvePanoramaLink(ticket);
            await dataService.updatePanoramaLink(layoutId, panoramaUrl);
            return {panorama_url: panoramaUrl};
        }

        throw new Error("unzip failed, files empty");
    }

    throw new Error("unzip failed");
}

async function createPanoramaConfigFromJsonFile(jsonConfigFile) {
    let parentDir = urlServices.dirname(jsonConfigFile);
    if (!await fs.pathExists(jsonConfigFile)) {
        throw new Error(file + " does not exist");
    }

    let template = await fs.readJson(jsonConfigFile);
    if (template && template['scenes']) {
        let scenes = template['scenes'];
        let views = Object.keys(scenes);
        if (views) {
            let index = 0;
            for (let i = 0, N = views.length; i < N; i++) {
                index = index + 1;
                //// resolve image url : convert image relative path to image url
                scenes[views[i]]['panorama'] = urlServices.resolvePanoramaImageUrl(urlServices.join(parentDir, `${index}.jpg`));
                debug(scenes[views[i]]['panorama']);
            }
        }

        let rename = dataService.generateObjectId() + '.json';
        let panoPath = urlServices.join(parentDir, `${rename}`);
        debug(panoPath);
        await fs.writeJson(panoPath, template);
        return {file_name: `${rename}`, file_path: panoPath};
    } else {
        throw new Error(file + " was not a json format");
    }
}

controller.uploadZipFile4Consultant = async function (req, res, next) {
    storeService.uploadZipFile(req, res, (async (err) => {
        try {
            if (err) {
                res.json(controller.makeReturnError(err.message || "unzip failed", -1));
                return;
            }

            let result = await unzipPipeline4Consultant(req);
            res.json(controller.makeReturnMessage(result));
            let uid = await dataService.getUserIDByEmail(req.body.email);
            if (uid > 0) {
                renderService.pushAlertMessage(uid, "Fitin vừa gửi cho bạn 1 kết quả tư vấn định dạng WebVR !");
                if (result.signature) {
                    try {
                        let url = urlServices.resolveQuotationHtmlLink(result.signature);
                        await renderService.exportQuotation2Pdf(url, result.signature);
                    } catch (ex) {
                        logger.error("generate PFD got exception ", ex);
                    }
                }
            }
        } catch (e) {
            res.json(controller.makeReturnError(e, -1));
            logger.error("uploadZipFile got exception ", e);
        }
    }));
};

controller.uploadZipFile4CMS = async function (req, res, next) {
    req.params.is_cms_mode = true;
    storeService.uploadPanoramaZipFile4CMS(req, res, (async (err) => {
        try {
            if (err) {
                res.json(controller.makeReturnError(err.message || "unzip failed", -1));
                return;
            }

            let result = await unzipPipeline4CMS(req);
            res.json(controller.makeReturnMessage(result));
        } catch (e) {
            res.json(controller.makeReturnError(e, -1));
            logger.error("uploadZipFile got exception ", e);
        }
    }));
};


/** Handle Panorama **/

async function unzipPipeline4CMS(req, type) {
    let zipFile = req.file;
    if (zipFile.absolutePath === undefined) {
        throw new Error("zip file was not found");
    }

    let UUID = zipFile.uuid;

    let projectId = req.body.project_id || null;

    let layoutId = req.body.layout_id || null;

    let consultantId = req.body.consultant_id || null;

    let bedRoomNum = req.body.bed_room_num || 0;

    let fitinfurnitureFee = req.body.fitinfurniture_fee || 0;

    let constructionFee = req.body.construction_fee || 0;

    projectId = parseInt(projectId);

    layoutId = parseInt(layoutId);

    bedRoomNum = parseInt(bedRoomNum);

    constructionFee = parseInt(constructionFee);

    fitinfurnitureFee = parseInt(fitinfurnitureFee);

    let discount = discountServices.getDiscountByProjectID(projectId);

    let layoutName = req.body.layout_name || null;


    let now = moment();
    const sYear = now.year().toString();
    const md = now.format('M-D');
    let unzip = await storeService.unzip4CMS(zipFile.absolutePath, urlServices.join(sYear, md));
    let files = await storeService.getFilesInDirectory(unzip);
    if (files) {
        let payload = {
            email: req.body.email,
            panorama_url: null,
            cart: null,
            metadata: {
                serial_key: zipFile.serial_key,
                project_id: projectId,
                project_name: zipFile.project_name,
                layout_id: layoutId,
                layout_name: layoutName,
                consultant_id: consultantId,
                bed_room_num: bedRoomNum,
                area: null,
                construction_fee: isNaN(constructionFee) ? 0 : constructionFee,
                fitinfurniture_fee: isNaN(fitinfurnitureFee) ? 0 : fitinfurnitureFee,
                discount: discount
            }
        };

        ///// resolve files
        let jsonFiles = {};
        files.forEach(f => {
            if (f.endsWith("config.json") && jsonFiles["config"] === undefined) {
                jsonFiles["config"] = f;
            } else if (f.endsWith("carts.json") && jsonFiles["cart"] === undefined) {
                jsonFiles["cart"] = f;
            } else if (f.endsWith("layout.json") && jsonFiles["layout"] === undefined) {
                jsonFiles["layout"] = f;
            }
        });

        //// valid required files
        if (!jsonFiles.config) {
            throw new Error("panorama config.json not found");
        }

        if (!jsonFiles.cart) {
            throw new Error("panorama carts.json not found");
        }

        if (!jsonFiles.layout) {
            throw new Error("panorama layout.json not found");
        }

        let layoutJSON = await storeService.readFileJson(jsonFiles.layout);
        let cartJson = await storeService.readFileJson(jsonFiles.cart);
        //// doc lai thong tin tu json cart , kiem tra metadata ten du an, project
        if (cartJson && cartJson.metadata) {
            //// uu tien lay trong tham so request, neu tham so trong request null se lay trong metadata.
            checkAndOverrideMetadata(payload.metadata, cartJson.metadata);
        }

        if (cartJson && cartJson.client) {
            payload.client = {
                email: payload.email || cartJson.client.email || null,
                phone: payload.phone || cartJson.client.phone || null,
            }
        }

        let panorama = await createPanoramaConfigFromJsonFile(jsonFiles.config);
        if (panorama && panorama.file_name) {
            let ticket = `${sYear}/${md}/${UUID}/${panorama.file_name}`;
            debug("ticket", ticket);
            ticket = Buffer.from(ticket).toString('base64');
            payload.panorama_url = urlServices.resolvePanoramaLink(ticket);
            //// check project and layout
            let pl = null;
            if(type === 'layout'){
                pl = await consultantService.getOrCreateProjectAndLayout(payload);
            }else{
                pl = await consultantService.getOrCreateProjectAndLayoutRenovation(payload);
            }

            if (!pl || !pl.int_id) {
                if (!isNaN(projectId) && !isNaN(layoutId)) {
                    throw new Error("project and layout not found");
                }

                throw new Error("failed to create project and layout");
            }

            const insertedLayoutId = pl.int_id;
            //// build cart
            let cartSign = null;
            if (pl.extensions && pl.extensions.cart_json) {
                cartSign = pl.extensions.cart_json.cart_sign || null;
            }
            //// create cart from ecom
            let cart = await cartService.buildCartFromJsonFile(jsonFiles.cart, payload, cartSign);
            if (cart) {
                payload.cart = cart;
                if(type === 'layout'){
                    await Promise.all([
                        consultantService.updateLayoutJson(insertedLayoutId, layoutJSON),
                        consultantService.addLayoutCart(cartJson, cart, insertedLayoutId),
                        consultantService.updateLayoutPanoramaLink(insertedLayoutId, payload.panorama_url)
                    ]);
                }
                else{
                    await Promise.all([
                        consultantService.updateLayoutRenovationJson(insertedLayoutId, layoutJSON),
                        consultantService.addLayoutRenovationCart(cartJson, cart, insertedLayoutId),
                        consultantService.updateLayoutRenovationPanoramaLink(insertedLayoutId, payload.panorama_url)
                    ]);
                }
                
                return {
                    project_id: pl.project_int_id,
                    layout_id: insertedLayoutId,
                    project_name: pl.project_name,
                    layout_name: pl.layout_name,
                    signature: cart.cart_sign,
                    panorama_url: payload.panorama_url,
                    cart_url: urlServices.resolveCartLink(cart.cart_sign)
                };
            }

            throw new Error("failed to build cart");
        }
    }

    throw new Error("unzip failed");
}

controller.importRenovationZipFileCMS = async function (req, res, next) {
    /** ! Importance select mode : create/update , if create new that will skip project_id and layout_id **/
    /*if (req.query.mode && req.query.mode.toLowerCase() === "update" || req.method.toLowerCase() === "put") {
        req.params.isCreate = false;
    } else {
        req.params.isCreate = req.method.toLowerCase() === "post";
    }*/

    req.params.is_cms_mode = true;
    storeService.uploadPanoramaZipFile4CMS(req, res, (async (err) => {
        try {
            if (err) {
                res.json(controller.makeReturnError(err.message || "unzip failed", -1));
                return;
            }

            let result = await unzipPipeline4CMS(req, 'renovation');
            res.json(controller.makeReturnMessage(result));
        } catch (e) {
            res.json(controller.makeReturnError(e, -1));
            logger.error("uploadRenovationZipFile got exception ", e);
        }
    }));
};

controller.importLayoutZipFileCMS = async function (req, res, next) {
    /** ! Importance select mode : create/update , if create new that will skip project_id and layout_id **/
    /*if (req.query.mode && req.query.mode.toLowerCase() === "update" || req.method.toLowerCase() === "put") {
        req.params.isCreate = false;
    } else {
        req.params.isCreate = req.method.toLowerCase() === "post";
    }*/

    req.params.is_cms_mode = true;
    storeService.uploadPanoramaZipFile4CMS(req, res, (async (err) => {
        try {
            if (err) {
                res.json(controller.makeReturnError(err.message || "unzip failed", -1));
                return;
            }

            let result = await unzipPipeline4CMS(req, 'layout');
            res.json(controller.makeReturnMessage(result));
        } catch (e) {
            res.json(controller.makeReturnError(e, -1));
            logger.error("uploadRenovationZipFile got exception ", e);
        }
    }));
};


module.exports = controller;
