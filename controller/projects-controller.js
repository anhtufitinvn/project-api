(function () {
    let Providers = require('../server/providers');
    let SynService = require('../services/syn-services');
    let Handler = require('../core/handler');
    let Utils = require('../utils/utils');
    let ENUMS = global.ENUMS;
    let FrontendDataService = Providers.FrontendDataService;
    let Transform = require('../transform/frontend/index');

    class ProjectsController extends Handler {
        constructor() {
            super();
            this.name = "ProjectsController";
        }

        async getProjectsRenovation(req, res, next) {
            let query = req.query;
            let data = await FrontendDataService.Projects.getProjectRenovationList(query);
            let result_data = Transform.projects.list(data);
            let result = {
                list : result_data
            }
            res.json(this.makeReturnMessage(result));

        };

    }

    module.exports = new ProjectsController();
})();