(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let logger = Service.logger;
    let debug = Service.debug;
    const Promise = require('bluebird');
    const uuidv4 = require('uuid/v4');
    let AdmZip = require('adm-zip');
    let moment = require('moment');
    let multer = require('multer');
    let path = require("path");
    let fs = require("fs-extra");
    const urlServices = require('../services/url-services');
    const dataServices = require('../services/data-services');
    const discountServices = require('../services/discounts-services');
    /***
     * Store file, could not access from CDN
     */
    let StorageDataDirectory = global.configuration.STORAGE["user_data"];

    let StaticDirectory = global.configuration.STORAGE["static"];

    let PanoramaDirectory = global.configuration.STORAGE["panorama"];

    function generateUUID() {
        return uuidv4();
    }

    function generatePathByMoment() {
        let now = moment();
        /** YYYY/M/ => 2020/12 **/
        return path.join(now.year().toString(), (now.month() + 1).toString());
    }

    function resolveFile(f, list) {
        let stat = fs.lstatSync(f);
        if (stat.isDirectory()) {
            if (list.length < 10) {
                return resolveDirectory(f, list);
            }
        }

        if (stat.isFile()) {
            list.push(f);
        }
    }

    function resolveDirectory(d, list) {
        let files = fs.readdirSync(d);
        files.forEach(f => {
            if (list.length < 50) {
                resolveFile(path.join(d, f), list);
            }
        });
    }

    /** valid param cho truong hop gui ket qua tu van cho user, nen doi hoi yeu cau phuc tap**/
    async function validParams(request) {
        let consultant_id = request.body.consultant_id || null;
        let project_id = request.body.project_id || null;
        let project_name = request.body.project_name || null;
        let layout_id = request.body.layout_id || null;
        let layout_name = request.body.layout_name || null;
        let email = request.body.email || null;
        let serial_key = request.body.serial_key || null;
        let bed_room_num = request.body.bed_room_num || 0;
        let area = request.body.area || null;
        let construction_fee = request.body.construction_fee || "0";
        let fitinfurniture_fee = request.body.fitinfurniture_fee || "0";
        if (project_id === null
            || layout_id === null
            || consultant_id === null
            // || project_name === null
            // || layout_name === null
            || email === null
            || serial_key === null) {
            return undefined;
        }

        let discount = discountServices.getDiscountByProjectID(project_id);
        project_id = parseInt(project_id);
        layout_id = parseInt(layout_id);
        bed_room_num = parseInt(bed_room_num);
        construction_fee = parseInt(construction_fee);
        fitinfurniture_fee = parseInt(fitinfurniture_fee);

        if (email === "" || serial_key === "" || isNaN(project_id)
            || isNaN(layout_id) || isNaN(bed_room_num)
            || isNaN(construction_fee) || isNaN(fitinfurniture_fee)) {
            return undefined;
        }

        let layoutData = await dataServices.getLayoutById(project_id, layout_id);
        /**
         * if found project in Database, override user submit data
         * **/
        if (layoutData) {
            layout_name = layoutData.name.trim();
            project_name = layoutData.project_name.trim();
            bed_room_num = layoutData.bed_room_num;
            area = layoutData.area || "0.0";
            construction_fee = layoutData.construction_fee || 0;
            fitinfurniture_fee = layoutData.fitinfurniture_fee || 0;
        }

        return {
            consultant_id: parseInt(consultant_id),
            project_id: parseInt(project_id),
            layout_id: parseInt(layout_id),
            layout_name: layout_name,
            project_name: project_name,
            bed_room_num: bed_room_num,
            area: area,
            construction_fee: construction_fee,
            fitinfurniture_fee: fitinfurniture_fee,
            discount: discount,
            serial_key: serial_key.toUpperCase().trim(),
            email: email.trim()
        }
    }

    /** valid param cho truong hop import data vao CMS **/
    async function validParamsForImportData(request) {
        let consultant_id = request.body.consultant_id || null;
        let project_id = request.body.project_id || null;
        let project_name = request.body.project_name || null;
        let layout_id = request.body.layout_id || null;
        let layout_name = request.body.layout_name || null;
        let email = request.body.email || null;
        let serial_key = request.body.serial_key || null;
        let bed_room_num = request.body.bed_room_num || 0;
        let area = request.body.area || null;
        let construction_fee = request.body.construction_fee || "0";
        let fitinfurniture_fee = request.body.fitinfurniture_fee || "0";
        let isUpdate = false;
        if(request.method.toLowerCase() === "put" || (request.query.mode && request.query.mode.toLowerCase() === "update")) {
            isUpdate = true;
        }

        if (isUpdate) {
            //// UPDATE MODE
            ///// case : neu truong hop update, khong phai tao moi, yeu cau chi ro project_id va layout_id
            if (project_id === null
                || layout_id === null
                || serial_key === null) {
                return undefined;
            }

            project_id = parseInt(project_id);
            layout_id = parseInt(layout_id);
            if (isNaN(project_id) || isNaN(layout_id)) {
                return undefined;
            }
        }

        //// CREATE MODE
        let discount = discountServices.getDiscountByProjectID(project_id);
        bed_room_num = parseInt(bed_room_num);
        construction_fee = parseInt(construction_fee);
        fitinfurniture_fee = parseInt(fitinfurniture_fee);

        if (serial_key === "" || isNaN(bed_room_num)
            || isNaN(construction_fee) || isNaN(fitinfurniture_fee)) {
            return undefined;
        }

        return {
            consultant_id: parseInt(consultant_id),
            project_id: parseInt(project_id),
            layout_id: parseInt(layout_id),
            layout_name: layout_name,
            project_name: project_name,
            bed_room_num: bed_room_num,
            area: area,
            construction_fee: construction_fee,
            fitinfurniture_fee: fitinfurniture_fee,
            discount: discount,
            serial_key: serial_key ? serial_key.toUpperCase().trim() : null,
            email: email ? email.trim() : null
        }
    }

    /**
     *  Dinh nghia disk store cho viec import images gallery vao layout
     */
    const ImportGalleryStorage = multer.diskStorage({
        destination: async function (request, file, callback) {
            try {
                let ps = await validParamsForImportData(request);
                if (ps === undefined) {
                    return callback(new Error("invalid params, require project_id, project_name, layout_id, layout_name ,serial_key"), null);
                }

                file.project_id = ps.project_id;
                file.layout_id = ps.layout_id;
                file.layout_name = ps.layout_name;
                file.project_name = ps.project_name;
                file.bed_room_num = ps.bed_room_num;
                file.area = ps.area || "0.0";
                file.construction_fee = ps.construction_fee || 0;
                file.fitinfurniture_fee = ps.fitinfurniture_fee || 0;
                file.email = ps.email;
                file.discount = ps.discount;
                // create directory and rename file.
                file.relativeDirectory = path.join("images", generatePathByMoment());
                file.absoluteDirectory = path.join(StaticDirectory, file.relativeDirectory);
                fs.ensureDir(file.absoluteDirectory).then(() => {
                    callback(null, file.absoluteDirectory);
                }, (e) => {
                    callback(e, null);
                });
            } catch (e) {
                callback(e, null);
            }
        },
        filename: function (request, file, callback) {
            let uuid = generateUUID();
            let ext = urlServices.getFileExt(file.originalname);
            file.uuid = uuid;
            file.rename = `${uuid}${ext}`;
            file.file_url = urlServices.resolveFileStaticLink(path.join(file.absoluteDirectory, file.rename));
            file.absolutePath = path.join(file.absoluteDirectory, file.rename);
            callback(null, file.rename);
        }
    });

    /**
     *  Dinh nghia disk store ket qua tu van
     */
    const ResultStorage = multer.diskStorage({
        destination: async function (request, file, callback) {
            try {
                let ps = await validParams(request);
                if (ps === undefined) {
                    return callback(new Error("invalid params, require consultant_id,project_id,project_name,layout_id,layout_name,email,serial_key"), null);
                }

                file.consultant_id = ps.consultant_id;
                file.project_id = ps.project_id;
                file.layout_id = ps.layout_id;
                file.layout_name = ps.layout_name;
                file.project_name = ps.project_name;
                file.bed_room_num = ps.bed_room_num;
                file.area = ps.area || "0.0";
                file.construction_fee = ps.construction_fee || 0;
                file.fitinfurniture_fee = ps.fitinfurniture_fee || 0;
                file.email = ps.email;
                file.discount = ps.discount;
                // create directory and rename file.
                file.relativeDirectory = path.join("images", generatePathByMoment());
                file.absoluteDirectory = path.join(StorageDataDirectory, file.relativeDirectory);
                fs.ensureDir(file.absoluteDirectory).then(() => {
                    callback(null, file.absoluteDirectory);
                }, (e) => {
                    callback(e, null);
                });
            } catch (e) {
                callback(e, null);
            }
        },
        filename: function (request, file, callback) {
            let uuid = generateUUID();
            let ext = urlServices.getFileExt(file.originalname);
            file.uuid = uuid;
            file.rename = `${uuid}${ext}`;
            file.file_url = urlServices.resolveFileStaticLink(path.join(file.absoluteDirectory, file.rename));
            file.absolutePath = path.join(file.absoluteDirectory, file.rename);
            callback(null, file.rename);
        }
    });

    /**
     *  Dinh nghia disk store Panorama
     */
    const PanoramaStorage = multer.diskStorage({
        destination: async function (request, file, callback) {
            try {
                let projectId = request.body.project_id || null;

                let layoutId = request.body.layout_id || null;

                projectId = parseInt(projectId);

                if(isNaN(projectId) && layoutId) {
                    //// truong hop co layoutId, nhung project is null
                    callback(new Error(`layout_id = ${layoutId}, but project_id is null`), null);
                    return;
                }

                file.project_id = projectId;
                // create directory and rename file.
                if(request && request.params.is_cms_mode) {
                    file.relativeDirectory = path.join("panoramas", generatePathByMoment(), "layouts");
                } else {
                    file.relativeDirectory = path.join("panoramas", generatePathByMoment(), "layouts", "client");
                }

                file.absoluteDirectory = path.join(StorageDataDirectory, file.relativeDirectory);
                fs.ensureDir(file.absoluteDirectory).then(() => {
                    callback(null, file.absoluteDirectory);
                }, (e) => {
                    callback(e, null);
                });
            } catch (e) {
                callback(e, null);
            }
        },
        filename: function (request, file, callback) {
            let uuid = generateUUID();
            let ext = urlServices.getFileExt(file.originalname);
            file.uuid = uuid;
            file.rename = `${uuid}${ext}`;
            file.file_url = urlServices.resolveFileStaticLink(path.join(file.absoluteDirectory, file.rename));
            file.absolutePath = path.join(file.absoluteDirectory, file.rename);
            callback(null, file.rename);
        }
    });


    /**
     *  Dinh nghia cho store hinh cho Project hoac layout
     */
    const ImagesStorage = multer.diskStorage({
        destination: async (req, file, callback) => {
            // Định nghia noi file upload se dc luu lai
            // cho nay dinh nghia folder name, khong thay doi ten file
            try {
                let dirName = generatePathByMoment();
                /** se tao ra relative path : images/{dirName} **/
                file.relativeDirectory = path.join('images', dirName);

                /** se tao ra absolute path : /var/www/tuvan.fitin.vn/static/images/{dirName} **/
                file.absoluteDirectory = path.join(StaticDirectory, file.relativeDirectory);

                /** kiem tra directory co ton tai hay chua, neu chua thi tao directory path**/
                await fs.ensureDir(file.absoluteDirectory);
                callback(null, file.absoluteDirectory);
            } catch (e) {
                callback(e, null);
            }
        },
        filename: (req, file, callback) => {
            /** lay file extension .jpg .png **/
            let ext = urlServices.getFileExt(file.originalname);
            // dat ten moi cho file : tao ra ten random su dung objectID
            file.rename = `${dataServices.generateObjectId()}${ext}`;
            file.relativePath = path.join(file.relativeDirectory, file.rename);
            callback(null, file.rename);
        }
    });

    /**
     *  Dinh nghia disk store CartJsonRenovationStorage
     */
    const CartJsonRenovationStorage = multer.diskStorage({
        destination: async function (request, file, callback) {
            try {
                let layout_id = request.body.layout_id || null;
                layout_id = parseInt(layout_id);
                if (layout_id === null || isNaN(layout_id)) {
                    return callback(new Error("invalid params, required layout_id"), null);
                }

                file.layout_id = layout_id;
                // create directory and rename file.
                file.relativeDirectory = path.join('carts', 'renovation', generatePathByMoment(), layout_id.toString());
                file.absoluteDirectory = path.join(StorageDataDirectory, file.relativeDirectory);


                fs.ensureDir(file.absoluteDirectory).then(() => {
                    callback(null, file.absoluteDirectory);

                }, (e) => {

                    callback(e, null);
                });
            } catch (e) {

                callback(e, null);
            }
        },
        filename: function (request, file, callback) {
            let uuid = generateUUID();
            let ext = urlServices.getFileExt(file.originalname);
            file.uuid = uuid;
            file.rename = `${uuid}${ext}`;
            file.file_url = urlServices.resolveFileStaticLink(path.join(file.absoluteDirectory, file.rename));
            file.absolutePath = path.join(file.absoluteDirectory, file.rename);
            callback(null, file.rename);
        }
    });

    /** Upload cartjson cho layout **/
    Service.uploadCartJson4Renovation = multer({
        fileFilter: async (req, file, cb) => {

            if (file.mimetype !== "application/json") {
                cb(new Error(`The file <strong>${file.originalname}</strong> is invalid. Only allowed json.`));
            } else {
                cb(null, true);
            }
        },
        storage: CartJsonRenovationStorage
    }).single("file");


    /**
     *  Dinh nghia disk store CartJsonStorage
     */
    const CartJsonStorage = multer.diskStorage({
        destination: async function (request, file, callback) {
            try {
                let layout_id = request.body.layout_id || null;
                layout_id = parseInt(layout_id);
                if (layout_id === null || isNaN(layout_id)) {
                    return callback(new Error("invalid params, required layout_id"), null);
                }

                file.layout_id = layout_id;
                // create directory and rename file.
                file.relativeDirectory = path.join('carts', 'template', generatePathByMoment(), layout_id.toString());
                file.absoluteDirectory = path.join(StorageDataDirectory, file.relativeDirectory);


                fs.ensureDir(file.absoluteDirectory).then(() => {
                    callback(null, file.absoluteDirectory);

                }, (e) => {

                    callback(e, null);
                });
            } catch (e) {

                callback(e, null);
            }
        },
        filename: function (request, file, callback) {
            let uuid = generateUUID();
            let ext = urlServices.getFileExt(file.originalname);
            file.uuid = uuid;
            file.rename = `${uuid}${ext}`;
            file.file_url = urlServices.resolveFileStaticLink(path.join(file.absoluteDirectory, file.rename));
            file.absolutePath = path.join(file.absoluteDirectory, file.rename);
            callback(null, file.rename);
        }
    });

    /** Upload cartjson cho layout **/
    Service.uploadCartJson4Layout = multer({
        fileFilter: async (req, file, cb) => {

            if (file.mimetype !== "application/json") {
                cb(new Error(`The file <strong>${file.originalname}</strong> is invalid. Only allowed json.`));
            } else {
                cb(null, true);
            }
        },
        storage: CartJsonStorage
    }).single("file");

    /**
     *  Dinh nghia disk store JSON file
     */
    const JsonStorage = multer.diskStorage({
        destination: async function (request, file, callback) {
            try {
                let layout_id = request.body.layout_id || null;
                layout_id = parseInt(layout_id);
                if (layout_id === null || isNaN(layout_id)) {
                    return callback(new Error("invalid params, required layout_id"), null);
                }

                file.layout_id = layout_id;
                // create directory and rename file.
                file.relativeDirectory = path.join('json', generatePathByMoment(),'layouts', layout_id.toString());
                file.absoluteDirectory = path.join(StorageDataDirectory, file.relativeDirectory);


                fs.ensureDir(file.absoluteDirectory).then(() => {
                    callback(null, file.absoluteDirectory);

                }, (e) => {

                    callback(e, null);
                });
            } catch (e) {

                callback(e, null);
            }
        },
        filename: function (request, file, callback) {
            let uuid = generateUUID();
            let ext = urlServices.getFileExt(file.originalname);
            file.uuid = uuid;
            file.rename = `${uuid}${ext}`;
            file.file_url = urlServices.resolveFileStaticLink(path.join(file.absoluteDirectory, file.rename));
            file.absolutePath = path.join(file.absoluteDirectory, file.rename);
            callback(null, file.rename);
        }
    });

    /** Upload json cho layout **/
    Service.uploadJson = multer({
        fileFilter: async (req, file, cb) => {

            if (file.mimetype !== "application/json") {
                cb(new Error(`The file <strong>${file.originalname}</strong> is invalid. Only allowed json.`));
            } else {
                cb(null, true);
            }
        },
        storage: JsonStorage
    }).single("file");


    /** Upload Images cho layout + project **/
    Service.uploadImageFile = multer({
        fileFilter: async (req, file, cb) => {
            let match = ["image/png", "image/jpeg"];
            if (match.indexOf(file.mimetype) === -1) {
                cb(new Error(`The file <strong>${file.originalname}</strong> is invalid. Only allowed to upload image jpeg or png.`));
            } else {
                cb(null, true);
            }
        },
        storage: ImagesStorage
    }).single("image");


    /**
     *  Upload file zip. Import vao CMS images + gallery cho project, layout
     */
    Service.uploadZipFileForCMS = multer({
        limits: {
            fieldNameSize: 200,
            files: 1,
            fileSize: 100 * 1024 * 1024 // 100MB
        },
        fileFilter: async (req, file, cb) => {
            if (!file.mimetype.includes("zip")) {
                cb(new Error("only zip file allowed"));

            } else {
                cb(null, true);
            }
        },
        storage: ImportGalleryStorage
    }).single('zfile');

    /**
     *  Upload Ket qua tu van zip file
     */
    Service.uploadZipFile = multer({
        limits: {
            fieldNameSize: 200,
            files: 1,
            fileSize: 100 * 1024 * 1024 // 100MB
        },
        fileFilter: async (req, file, cb) => {
            if (!file.mimetype.includes("zip")) {
                cb(new Error("only zip file allowed"));

            } else {
                cb(null, true);
            }
        },
        storage: ResultStorage
    }).single('zfile');

    /***
     * only use for Tool upload panorama. (inside FITIN team)
     */
    Service.uploadPanoramaZipFile4CMS = multer({
        limits: {
            fieldNameSize: 200,
            files: 1,
            fileSize: 100 * 1024 * 1024 // 100MB
        },
        fileFilter: async (req, file, cb) => {
            if (!file.mimetype.includes("zip")) {
                cb(new Error("only zip file allowed"));

            } else {
                cb(null, true);
            }
        },
        storage: PanoramaStorage
    }).single('zfile');

    /** Unzip ket qua tu van file.zip **/
    Service.unzip = function (file) {
        return new Promise((resolve, reject) => {
            let zipPath = path.normalize(file);
            const zip = new AdmZip(zipPath);
            const name = urlServices.getNameWithoutExt(zipPath);
            const target = path.join(StaticDirectory, generatePathByMoment(), name);
            debug(target);
            fs.ensureDir(target)
                .then(() => {
                    zip.extractAllToAsync(target, true, (err) => {
                        if (err) {
                            reject(err);
                            return;
                        }
                        resolve(target);
                    });
                })
                .catch(e => {
                    reject(e);
                })
        });
    };


    /** Unzip tool upload panorama.zip **/
    Service.unzip4CMS = function (file, directoryName) {
        return new Promise((resolve, reject) => {
            let zipPath = path.normalize(file);
            const zip = new AdmZip(zipPath);
            const name = urlServices.getNameWithoutExt(zipPath);
            const target = path.join(PanoramaDirectory, directoryName, name);
            debug(target);
            fs.ensureDir(target)
                .then(() => {
                    zip.extractAllToAsync(target, true, (err) => {
                        if (err) {
                            reject(err);
                            return;
                        }
                        resolve(target);
                    });
                })
                .catch(e => {
                    reject(e);
                })
        });
    };

    Service.getFilesInDirectory = function (directory) {
        return new Promise((resolve, reject) => {
            if (directory) {
                let dList = fs.readdirSync(directory);
                let list = [];
                dList.forEach(f => {
                    resolveFile(path.join(directory, f), list);
                });
                resolve(list);
            } else {
                reject("directory is null");
            }
        })
    };

    Service.readFileJson = function (file) {
        return new Promise((resolve, reject) => {
            fs.readFile(file, 'utf8', (err, jsonString) => {
                if (err) {
                    reject(err);
                    return;
                }
                try {
                    resolve(JSON.parse(jsonString));
                } catch (e) {
                    reject(e);
                }
            })
        });
    };

    Service.generateUUIDV4 = function () {
        return uuidv4();
    };

    Service.getImageStoreDirectory = function() {
        let relativeDirectory = path.join("images", generatePathByMoment());
        let absoluteDirectory = path.join(StaticDirectory, relativeDirectory);
        return {
            relative_path : relativeDirectory,
            absolute_path : absoluteDirectory
        }
    };

    module.exports = Service;
})();