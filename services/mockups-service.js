(function () {
    // load dependencies of this services
    let Discounts = require('../db/discount-mockup');
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    Service.getDiscountByProjectID = function (id) {
        let dc = Discounts.ct_discount[id] || Discounts.ct_discount[id.toString()] || Discounts.ct_discount["0"];
        if(dc) {
            return dc.percent || 0.0;
        }

        return 0.0;
    };

    Service.dump = function () {
        return Discounts;
    };

    Service.test = function() {
        let urlService = this.providers.get("UrlService");
        return {
            avatar_url: urlService.getStaticCDN()
        }
    };

    module.exports = Service;
})();