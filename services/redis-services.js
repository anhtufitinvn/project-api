(function () {
    const Promise = require('bluebird');
    const redis = Promise.promisifyAll(require('redis'));
    var configuration = global.configuration;
    var {logger, debug} = require('../core/logger');


    function makeClient() {
        return new Promise((resolve, reject) => {

            let redisOption = {
                prefix: configuration.REDIS.prefix || "consultant-",
                retry_strategy: function (options) {
                    if (options.error && options.error.code === 'ECONNREFUSED') {
                        // End reconnecting on a specific error and flush all commands with
                        // a individual error
                        return new Error('The server refused the connection');
                    }
                    if (options.total_retry_time > 1000 * 60 * 60) {
                        // End reconnecting after a specific timeout and flush all commands
                        // with a individual error
                        return new Error('Retry time exhausted');
                    }

                    if (options.attempt > 3) {
                        // End reconnecting with built in error
                        return undefined;
                    }
                    // reconnect after
                    return Math.min(options.attempt * 100, 3000);
                }
            };

            const client = redis.createClient(configuration.REDIS.port, configuration.REDIS.host, redisOption);

            client.on('error', (error) => {
                reject(error);
                onErrorHandler(error);
            });

            client.on('connect', onConnectHandler);

            client.on('ready', () => {
                resolve(client);
                onReadyHandler();
            });
        });
    }

    function onConnectHandler() {
        logger.info('redis is connecting ...');
        debug("[OK] redis connecting");
    }

    function onReadyHandler() {
        logger.info('redis is ready');
        debug("[OK] redis is ready");
    }

    function onErrorHandler(error) {
        logger.error("redis error", error);
        debug("redis error", error);
        if (error.code === 'NR_CLOSED' || error.code === 'ECONNREFUSED') {
            redisClient = null;
        }
    }

    let redisClient = null;

    let service = {
        getClient: async () => {
            if (redisClient && redisClient.connected) {
                return redisClient;
            }

            redisClient = await makeClient();
            return redisClient;
        },
        end : () => {
            return new Promise(resolve => {
                if (redisClient) {
                    redisClient.end(false);
                    return resolve(true);
                }

                resolve(false);
            });
        },
        createClient : async () => {
            return await makeClient();
        }
    };

    service.setAsync = async function(key, value, expire) {
        let client = await service.getClient();
        if(expire){
            return await client.setAsync(key, value, 'EX', expire);
        }else{
            return await client.setAsync(key, value);
        } 
    };

    service.getAsync = async function(key, defaultValue) {
        let client = await service.getClient();
        let r = await client.getAsync(key);
        if(r) {
            return r;
        }
        else{       
            return defaultValue || null;
        }
    };

    service.LPUSHAsync = async function(key, value) {
        let client = await service.getClient();
        return await client.LPUSHAsync(key, JSON.stringify(value));
    }.bind(service);

    service.RPOPAsync = async function(key) {
        let client = await service.getClient();
        return await client.RPOPAsync(key);
    }.bind(service);

    service.LLENAsync = async function(key) {
        let client = await service.getClient();
        return await client.LLENAsync(key);
    }.bind(service);

    module.exports = service;

})();