(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Logger = Service.logger;
    let MongoAccess = require('../db/mongo/dbaccess');
    let CacheService = require('./cached-services');
    let Utils = require('../utils/utils');
    let Transform = require('../transform/transforms');
    let DAO = MongoAccess.DAO;
    Service.DAO = DAO;

    Service.getConsultantRenovationData = async function (args) {
        try {
            let result = await MongoAccess.getConsultantRenovationData(args);
            if (result) {
                result = Transform.consultantsRenovation.result(result);
                return result;
            }

            return null;
        } catch (e) {
            Logger.error("getConsultantRenovationData", e);
            return [];
        }
    };

    /**create or get info project-renovation */
    Service.getOrCreateProject = async function (json) {
        let metadata = json.metadata;
        if (!metadata) {
            throw new Error("Json must contains metadata");
        }

        try {
            let project_id = metadata.project_id;
            if (!project_id) {
                let data_project = {
                    project_name: metadata.project_name,
                    slug: Utils.createSlugFromName(metadata.project_name),
                    is_renovation: true,

                    status: ENUMS.STATUS.UNPUBLISHED,
                    metadata: {}
                };

                let result = await DAO.Project.create(data_project);
                CacheService.Project.flushAll();
                return result;
            }
            else {
                let project = CacheService.Project.get(project_id);
                if (!project) {
                    let rs = await DAO.Project.getDetail(project_id);
                    if (rs.length) {
                        project = rs[0];
                        CacheService.Project.set(project_id, project);
                    } else {
                        return null;
                    }

                }

                return project || null;
            }
        } catch (e) {
            Logger.error("getOrCreateProject got exception ", e);
            return null;
        }
    };

    Service.getOrCreateProjectAndLayoutRenovation = async function (json) {
        let metadata = json.metadata;
        if (!metadata) {
            throw new Error("Json must contains metadata");
        }

        let project = await Service.getOrCreateProject(json);

        if (!project) {
            throw new Error("Error get project");
        }

        try {
            let layout_id = metadata.layout_id;
            if (!layout_id) {
                let data_layout = {
                    project_int_id: project.int_id,
                    project_name: project.project_name,
                    layout_name: metadata.layout_name,
                    slug: Utils.createSlugFromName(metadata.layout_name),
                    status: ENUMS.STATUS.UNPUBLISHED,
                    is_template: false,
                    metadata: {}
                };
                if (json.client) {
                    data_layout.metadata.user_info = {
                        email: json.client.email,
                        phone: json.client.phone
                    }
                }

                let layout = await DAO.LayoutRenovation.create(data_layout);
                CacheService.LayoutRenovation.flushAll();
                return layout;
            }

            let layout = CacheService.LayoutRenovation.get(layout_id);
            if (!layout) {
                let rs = await DAO.LayoutRenovation.getDetail(layout_id);
                if (rs.length) {
                    layout = rs[0];
                    CacheService.LayoutRenovation.set(layout_id, layout);
                } else {
                    return null;
                }

            }

            return layout || null;
        } catch (e) {
            Logger.error("Error getOrCreateProjectAndLayoutRenovation", e);
            return null;
        }
    };

    /**Tạo layout-json cho layout-renovation */
    Service.updateLayoutRenovationJson = async function (layout_id, json) {
        let data = {
            data_json: json,
            layout_int_id: parseInt(layout_id)
        };
        return await DAO.LayoutRenovationJson.create(data);
    };

    /**Update link paronama vào data layout-renovation */
    Service.updateLayoutRenovationPanoramaLink = async function (layout_id, link) {
        try {
            await DAO.LayoutRenovation.updatePanoramaLink(layout_id, link);
            CacheService.LayoutRenovation.flushAll();
            return true;
        } catch (e) {
            Logger.error("updatePanoramaLink", e);
            return false;
        }
    };

    /**Update thông tin và cart vào layout-renovation */
    Service.addLayoutRenovationCart = async function (original_cart_json, build_cart_json, layout_id) {
        try {
            let metadata = original_cart_json.metadata || {};
            if(metadata.room_type){
                let room_category = await DAO.RoomCategory.getByType(metadata.room_type);
                let room_data = [{
                    type : metadata.room_type,
                    name : room_category.name
                }];

                await DAO.LayoutRenovation.addRooms(layout_id, room_data);
            }
            let rs = await DAO.LayoutRenovation.addCart(layout_id, original_cart_json, build_cart_json);
            CacheService.LayoutRenovation.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add cart", e);
            return false;
        }
    };

    Service.updateLayoutRenovationGallery = async function(layout_id, imagesPathCollection) {
        if(imagesPathCollection && Array.isArray(imagesPathCollection)) {
            let index = 0;
            let gl = imagesPathCollection.map(imgPath => {
                return {
                    image_path: imgPath,
                    thumb_path: imgPath,
                    index: index++,
                    hidden: false
                }
            });

            let rs = await DAO.LayoutRenovation.updateAllGallery(layout_id, gl);
            CacheService.LayoutRenovation.flushAll();
            return rs;
        }

        return false;
    };



    /**
     * LAYOUT TEMPLATE
     */

    Service.getOrCreateProjectAndLayout = async function (json) {
        let metadata = json.metadata;
        if (!metadata) {
            throw new Error("Json must contains metadata");
        }

        let project = await Service.getOrCreateProject(json);

        if (!project) {
            throw new Error("Error get project");
        }

        try {
            let layout_id = metadata.layout_id;
            if (!layout_id) {
                let data_layout = {
                    project_int_id: project.int_id,
                    project_name: project.project_name,
                    layout_name: metadata.layout_name,
                    slug: Utils.createSlugFromName(metadata.layout_name),
                    status: ENUMS.STATUS.UNPUBLISHED,
                    is_template: false,
                    metadata: {}
                };
                if (json.client) {
                    data_layout.metadata.user_info = {
                        email: json.client.email,
                        phone: json.client.phone
                    }
                }

                let layout = await DAO.Layout.create(data_layout);
                CacheService.Layout.flushAll();
                return layout;
            }

            let layout = CacheService.Layout.get(layout_id);
            if (!layout) {
                let rs = await DAO.Layout.getDetail(layout_id);
                if (rs.length) {
                    layout = rs[0];
                    CacheService.Layout.set(layout_id, layout);
                } else {
                    return null;
                }

            }

            return layout || null;
        } catch (e) {
            Logger.error("Error getOrCreateProjectAndLayout", e);
            return null;
        }
    };

    /**Tạo layout-json cho layout */
    Service.updateLayoutJson = async function (layout_id, json) {
        let data = {
            data_json: json,
            layout_int_id: parseInt(layout_id)
        };
        return await DAO.LayoutJson.create(data);
    };

    /**Update link paronama vào data layout */
    Service.updateLayoutPanoramaLink = async function (layout_id, link) {
        try {
            await DAO.Layout.updatePanoramaLink(layout_id, link);
            CacheService.Layout.flushAll();
            return true;
        } catch (e) {
            Logger.error("updatePanoramaLink", e);
            return false;
        }
    };

    /**Update thông tin và cart vào layout */
    Service.addLayoutCart = async function (original_cart_json, build_cart_json, layout_id) {
        try {
            let metadata = original_cart_json.metadata || {};
            if(metadata.room_type){    
                let room_category = await DAO.RoomCategory.getByType(metadata.room_type);
                if(room_category){
                    let room_data = [{
                        type : metadata.room_type,
                        name : room_category.name
                    }];
                    await DAO.Layout.addRooms(layout_id, room_data);
                }
                
            }
            let rs = await DAO.Layout.addCart(layout_id, original_cart_json, build_cart_json);
            
            CacheService.Layout.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add cart", e);
            return false;
        }
    };

    Service.updateLayoutGallery = async function(layout_id, imagesPathCollection) {
        if(imagesPathCollection && Array.isArray(imagesPathCollection)) {
            let index = 0;
            let gl = imagesPathCollection.map(imgPath => {
                return {
                    image_path: imgPath,
                    thumb_path: imgPath,
                    index: index++,
                    hidden: false
                }
            });

            let rs = await DAO.Layout.updateAllGallery(layout_id, gl);
            CacheService.Layout.flushAll();
            return rs;
        }

        return false;
    };


    module.exports = Service;
})();