(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let logger = Service.logger;
    let debug = Service.debug;
    var Querystring = require('querystring');
    let Promise = require('bluebird');
    const CONFIG = global.configuration.HUBSPOT;
    const API_URL = CONFIG["api_url"];
    const API_KEY = CONFIG['api_key'];
    const HUBSPOT_ID = CONFIG['hubspot_id'];

    const request = require('request');


    Service.getAllContacts = async function (offset) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                qs: {
                    hapikey: API_KEY,
                    vidOffset: offset
                },
                url: `${API_URL}/contacts/v1/lists/all/contacts/all`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body["errorType"]) {
                        reject(new Error(body["errorType"]));
                    } else {
                        resolve(body);
                    }
                }
            });
        });
    };

    Service.createContactList = async function (listName) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                },
                body: {
                    name: listName
                },
                json: true,
                qs: {
                    hapikey: API_KEY
                },
                url: `${API_URL}/contacts/v1/lists`
            };

            debug("call :", options.url);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body["errorType"]) {
                        reject(new Error(body["errorType"]));
                    } else {
                        resolve(body);
                    }
                }
            });
        });
    };

    Service.getContactList = async function (offset) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                qs: {
                    hapikey: API_KEY,
                    offset: offset
                },
                url: `${API_URL}/contacts/v1/lists`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body["errorType"]) {
                        reject(new Error(body["errorType"]));
                    } else {
                        resolve(body);
                    }
                }
            });
        });
    };

    Service.importAllContacts = async function (contacts) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                },
                body: contacts,
                json: true,
                qs: {
                    hapikey: API_KEY
                },
                url: `${API_URL}/contacts/v1/contact/batch/`
            };

            debug("body :", options.body);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else if (body) {
                    if (body["status"] && body["status"] === "error") {
                        debug(JSON.stringify(body));
                        reject(new Error(body["message"]));
                    } else {
                        resolve(body);
                    }
                } else {
                    resolve({});
                }
            });
        });
    };

    Service.importContactsToClientList = async function (contacts) {
        return new Promise((resolve, reject) => {
            const listID = CONFIG["contact_list_id"] || 1;
            const options = {
                headers: {
                    "Content-Type": "application/json",
                },
                body: contacts,
                json: true,
                qs: {
                    hapikey: API_KEY
                },
                url: `${API_URL}/contacts/v1/lists/${listID}/add`
            };

            debug("body :", options.body);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else if (body) {
                    if (body["status"] && body["status"] === "error") {
                        debug(JSON.stringify(body));
                        reject(new Error(body["message"]));
                    } else {
                        resolve(body);
                    }
                } else {
                    resolve({});
                }
            });
        });
    };

    Service.getOwnerId = function () {
        return CONFIG["owner_id"] || 0;
    };

    Service.getContactListId = function () {
        return CONFIG["contact_list_id"] || 1;
    };

    Service.submitQuickForm = function (form, action) {
        const FID = getFormIDByAction(action);
        return new Promise((resolve, reject) => {
            if(FID === null) {
                return reject(new Error("formID not found"));
            }

            let data = {
                'email': form.email,
                'firstname': form.firstname,
                'mobilephone': form.phone,
                'request_support': form.message,
                'project': form.project,
                'hs_context': JSON.stringify({
                    "hutk": form.cookies.hubspotutk || null,
                    "ipAddress": form.id_address,
                    "pageUrl": form.page_url,
                    "pageName": form.page_name
                })
            };

            if(form.area) {
                data.area = form.area;
                data.budget_string = form.budget;
            }

            if(form.budget) {
                data.budget_string = form.budget;
            }

            let postData = Querystring.stringify(data);

            const options = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': postData.length
                },
                body: postData,
                url: `https://forms.hubspot.com/uploads/form/v2/${HUBSPOT_ID}/${FID}`
            };

            debug("call :", options.url);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    debug(body);
                    resolve();
                }
            });
        });
    };

    function getFormIDByAction(action) {
        if(action === undefined || action === null) {
            return CONFIG["form_quick_consultant"] || null;
        }

        let ACT = action.toLowerCase();
        if (ACT === 'download-lookbook') {
            return CONFIG["form_dl_lookbook"] || null;
        }

        if (ACT === 'download-project-info') {
            return CONFIG["form_dl_project_info"] || null;
        }

        if (ACT === 'home_renovation') {
            return CONFIG["form_home_renovation"] || null;
        }

        return null;
    }

    module.exports = Service;
})();