(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);

    //// Declare dependencies

    let Promise = require('bluebird');
    let ACCESS_KEY = global.configuration.ACCESS_KEY || "fitin";
    let Transform = require('../transform/transforms');
    let StoreService = require('../services/store-services');
    const STORAGE = global.configuration.STORAGE;
    const STATIC_STORAGE = STORAGE["static"];

    let logger = Service.logger;
    let debug = Service.debug;
    let request = require('request');
    let Fs = require('fs-extra');
    let Path = require('path');

    function createFileStore(fileName) {
        return new Promise((resolve) => {
            let store = StoreService.getImageStoreDirectory();
            Fs.ensureDir(store.absolute_path)
                .then(() => {
                    let target = Path.join(store.absolute_path, fileName);
                    let stream = Fs.createWriteStream(target);
                    resolve({
                        file_absolute_path: target,
                        file_relative_path: Path.join(store.relative_path, fileName),
                        stream: stream
                    });
                })
                .catch(e => {
                    resolve(null);
                });
        });


    }

    Service.downloadImageByURL = async function (imageURL) {
        return new Promise((resolve, reject) => {
            let url = decodeURI(imageURL).toLowerCase();
            let indexDot = url.lastIndexOf(".jpg");
            if (indexDot === -1) {
                indexDot = url.lastIndexOf(".png");
            }

            if (indexDot === -1) {
                indexDot = url.lastIndexOf(".webp");
            }

            if (indexDot === -1) {
                indexDot = url.lastIndexOf(".jpeg");
            }

            if (indexDot === -1) {
                return reject(new Error("url didn't contain a valid file name"))
            }

            let indexFlash = url.lastIndexOf("/");
            let ext = indexDot >= 0 ? url.substring(indexDot) : ".jpg";
            let n = ext.length;
            let name = indexDot ? url.substring(indexFlash + 1, indexDot + n) : `${StoreService.generateUUIDV4()}${ext}`;
            request
                .head(imageURL)
                .on('response', (res) => {
                    if (res.statusCode === 200) {
                        createFileStore(name).then((store) => {
                            store.stream.on('finish', function () {
                                // close() is async, call cb after close completes.
                                store.stream.close(() => {
                                    resolve({
                                        file_name: name,
                                        file_relative_path: store.file_relative_path,
                                        file_absolute_path: store.file_absolute_path
                                    });
                                });
                            });
                            store.stream.on('error', (err) => {
                                store.stream.end();
                                reject(err);
                            });
                            request(imageURL).pipe(store.stream);
                        }).catch((e) => {
                            reject(e);
                        });
                    } else {
                        reject(new Error(`[${res.statusCode || 404}] ${imageURL}`));
                    }
                })
                .on('error', function (err) {
                    reject(err);
                });

        });

    };


    module.exports = Service;
})();