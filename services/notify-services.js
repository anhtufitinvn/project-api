(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);

    //// Declare dependencies

    let Promise = require('bluebird');
    let ACCESS_KEY = global.configuration.ACCESS_KEY || "fitin";
    let API_URL = global.configuration.URLS["consultant-api-url"];

    let logger = Service.logger;
    let debug = Service.debug;
    let request = require('request');

    Service.CMS = {
        broadcast(event, data) {
            return new Promise((resolve, reject) => {
                const options = {
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Key" : ACCESS_KEY
                    },
                    body: {
                        access_key: ACCESS_KEY,
                        event :  event,
                        data : data
                    },
                    json: true,
                    method: 'POST',
                    url: `${API_URL}/cms/push/broadcast`
                };

                debug("call :", options.url);
                request.post(options, (err, res, body) => {
                    if (err) {
                        logger.error(err);
                        reject(err);
                    } else {
                        if (body.code === 0) {
                            resolve(true);
                        } else {
                            reject({message: body.message || "unknown error"});
                        }
                    }
                });
            });
        }
    };



    module.exports = Service;
})();