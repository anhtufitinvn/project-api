(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Discounts = require('../db/discount-mockup');

    Service.getDiscountByProjectID = function (id) {
        if(id === undefined || id === null) {
            return 0.0;
        }

        let dc = Discounts.ct_discount[id] || Discounts.ct_discount[id.toString()] || Discounts.ct_discount["0"];
        if(dc) {
            return dc.percent || 0.0;
        }

        return 0.0;
    };

    module.exports = Service;
})();