(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let logger = Service.logger;
    let debug = Service.debug;
    let path = require('path');
    let moment = require('moment');
    let URLS = global.configuration.URLS;
    let ROOT_PATH = global.configuration.STORAGE["root"];
    let STATIC_PATH = global.configuration.STORAGE["static"];
    let PANORAMA_PATH = global.configuration.STORAGE["panorama"];
    let UrlService = {};

    UrlService.getConsultantCDN = function () {
        return URLS["cdn-consultant"] || null;
    };

    UrlService.getStaticCDN = function () {
        return URLS["cdn"] || null;
    };

    UrlService.getEcomCDN = function () {
        return `${URLS["cdn"]}/${URLS["cms_ecom"]}`;
    };


    UrlService.isWindow = function () {
        return process.platform === "win32";
    };

    UrlService.getNameWithoutExt = function (file) {
        let name = this.basename(file);
        return name.replace(/\.[^/.]+$/, "");
    };

    UrlService.getFileExt = function (file) {
        let name = this.basename(file).toLowerCase();
        let i = name.lastIndexOf(".");
        if (i >= 0) {
            return name.substring(i, name.length);
        }

        return "";
    };

    UrlService.replaceAllSpace = function (string) {
        if (string) {
            return string.replace(/ /g, "");
        }

        return null;
    };

    UrlService.replaceAllSpaceWithUnderscore = function replaceAllSpaceWithUnderscore(string) {
        if (string) {
            return string.replace(/ /g, "_");
        }

        return null;
    };

    UrlService.basename = function (file) {
        if (file === undefined || file === null) {
            return null;
        }

        let f = path.normalize(file);
        if (process.platform === "win32") {
            return path.win32.basename(f);
        }

        return path.posix.basename(f);
    };

    UrlService.dirname = function (file) {
        if (file === undefined || file === null) {
            return null;
        }

        let f = path.normalize(file);
        return path.dirname(f);
    };

    UrlService.join = function (p1, p2) {
        return path.join(p1, p2);
    };

    UrlService.resolveProjectImageUrl = function (imageURL) {
        if (imageURL) {
            if (imageURL.startsWith("http")) {
                return imageURL;
            }

            if (imageURL.startsWith("/")) {
                return `${UrlService.getEcomCDN()}${imageURL}`;
            } else {
                return `${UrlService.getEcomCDN()}/${imageURL}`;
            }
        }

        return null;
    };

    UrlService.resolveConsultantImageUrl = function (imageURL) {
        if (imageURL) {
            if (imageURL.startsWith("http")) {
                return imageURL;
            }

            if (imageURL.startsWith("/")) {
                return `${UrlService.getConsultantCDN()}${imageURL}`
            } else {
                return `${UrlService.getConsultantCDN()}/${imageURL}`
            }
        }

        return null;
    };

    UrlService.resolveCDNEcomUrl = function (imageURL) {
        if (imageURL) {
            if (imageURL.startsWith("http")) {
                return imageURL;
            }

            if (imageURL.startsWith("/")) {
                return `${UrlService.getEcomCDN()}${imageURL}`
            } else {
                return `${UrlService.getEcomCDN()}/${imageURL}`
            }
        }

        return null;
    };

    UrlService.resolvePanoramaImageUrl = function (filePath) {
        let p = filePath.substring(PANORAMA_PATH.length);
        if (p && this.isWindow()) {
            p = p.replace(/\\/g, '/');
        }

        if (p && p.startsWith("/")) {
            return `${URLS["panorama_image_url"]}${p}`;
        } else {
            return `${URLS["panorama_image_url"]}/${p}`;
        }
    };

    UrlService.resolveCartLink = function (signature) {
        if (signature) {
            return `${URLS['cart_url']}?signature=${signature}&refer=${URLS['domain']}`
        }
        return null;
    };

    UrlService.resolveFileStaticLink = function (filePath) {
        if (filePath) {
            let p = filePath.substring(STATIC_PATH.length);
            if (p && this.isWindow()) {
                p = p.replace(/\\/g, '/');
            }

            if (p && p.startsWith("/")) {
                return `${UrlService.getConsultantCDN()}${p}`;
            }

            return `${UrlService.getConsultantCDN()}/${p}`;
        }

        return `${UrlService.getConsultantCDN()}`;
    };

    UrlService.resolveFileRelativePath = function (filePath) {
        let p = filePath.substring(STATIC_PATH.length);
        if (p && this.isWindow()) {
            p = p.replace(/\\/g, '/');
        }

        return p;
    };

    UrlService.resolveRelativePathLink = function (relativePath) {
        let p = relativePath;
        if (p && this.isWindow()) {
            p = p.replace(/\\/g, '/');
        }

        if (p && p.startsWith("/")) {
            return `${UrlService.getConsultantCDN()}${p}`;
        }

        return `${UrlService.getConsultantCDN()}/${p}`;
    };

    UrlService.resolvePanoramaLink = function (ticket) {
        return `${URLS['panorama_view_url']}/${ticket}`;
    };


    UrlService.resolveQuotationHtmlLink = function (signature) {
        return `${URLS['quotation_url']}/quotation/${signature}`;
    };

    UrlService.resolveQuotationRenovationHtmlLink = function (signature, update) {
        let isUpdate = update || false;
        if (signature) {
            if (isNaN(parseInt(signature))) {
                return `${URLS['quotation_url']}/quotation/renovation/${signature}?update=${isUpdate}`;
            }

            return `${URLS['quotation_url']}/quotation/renovation/layouts/${signature}?update=${isUpdate}`;
        }
        return null;
    };

    /***
     * @deprecated
     * use resolveLayoutTemplateQuotationHtmlLink instead
     * @param layout_id
     * @param update
     * @returns {string}
     */
    UrlService.resolveLayoutQuotationHtmlLink = function (layout_id, update) {
        let isUpdate = update || false;
        return `${URLS['quotation_url']}/quotation/layouts/${layout_id}?update=${isUpdate}`;
    };

    UrlService.resolveLayoutTemplateQuotationHtmlLink = function (layout_id, update) {
        let isUpdate = update || false;
        return `${URLS['quotation_url']}/quotation/template/layouts/${layout_id}?update=${isUpdate}`;
    };

    /**
     * @deprecated
     * used resolveDownloadLayoutTemplateQuotationStream instead
     * @param signature
     * @returns {string}
     */
    UrlService.resolveDownloadQuotationLink = function (signature) {
        let t = moment().unix();
        return `${UrlService.getConsultantCDN()}/qt/${signature}.pdf?t=${t}`;
    };

    UrlService.resolveDownloadLayoutTemplateQuotationStream = function (signature) {
        let t = moment().unix();
        return `${URLS['quotation_url']}/dl/quotation/template/layouts/${signature}?t=${t}`;
    };

    /**
     * @deprecated
     * used resolveDownloadRenovationQuotationStream instead
     * @param signature
     * @returns {string}
     */
    UrlService.resolveDownloadRenovationQuotationLink = function (signature) {
        let t = moment().unix();
        return `${UrlService.getConsultantCDN()}/qt/renovation/${signature}.pdf?t=${t}`;
    };

    UrlService.resolveDownloadRenovationQuotationStream = function (signature) {
        let t = moment().unix();
        return `${URLS['quotation_url']}/dl/quotation/renovation/layouts/${signature}?t=${t}`;
    };

    UrlService.resolveMonitorBuildQuotationUrl = function (isRenovation) {
        if(isRenovation) {
            return `${URLS['consultant-api-url']}/quotation/renovation/monitor/all`;
        }

        return `${URLS['consultant-api-url']}/quotation/template/monitor/all`;
    };

    module.exports = UrlService;
})();