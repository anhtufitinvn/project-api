(function () {
    let request = require('request');
    let RenderService = require('./render-services');
    let UrlService = require('../services/url-services');
    let CacheServices = require('../services/cached-services');
    let ConsultantService = require('../services/consultant-services');
    let CartService = require('../services/cart-services');
    ////
    let MongoAccess = require('../db/mongo/dbaccess');
    let Transform = require('../transform/transforms');
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let logger = Service.logger;
    let debug = Service.debug;


    let discountServices = require('./discounts-services');
    let DB = require('../db/mysql/dbaccess');

    const TTS_KEY_ITEMS = "Items";
    const TTS_KEY_ITEM_NAME = "Name";
    const TTS_KEY_LIST_ITEMS = "ListItems";
    const TTS_KEY_ROOM_ID = "RoomId";
    const MAIL_ACCESS_KEY = global.configuration.MAIL_ACCESS_KEY;
    const API_MAIL_URL = global.configuration.URLS["mail_api_url"] || "https://api-mail.fitin.vn/api/v2/send-email";

    Service.getUnityLayoutById = async function (layoutID) {
        let layoutData = await DB.getUnityLayoutById(layoutID);
        return getProductsFromUnityLayout(layoutData);
    };

    Service.sendQuotationToEmail = async function (mailto, quotation) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key": MAIL_ACCESS_KEY
                },
                body: {
                    email: mailto,
                    template_code: 'tai_file_pdf_du_an',
                    params: {
                        quotation_url: quotation.quotation_url,
                        quotation_layout: quotation.project,
                        full_name: quotation.firstname
                    }
                },
                json: true,
                method: 'POST',
                url: `${API_MAIL_URL}`
            };

            debug("call :", options.url);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        resolve();
                    } else {
                        logger.error("call api error", body);
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.generatePDFFile = async function (signature, isRenovation) {
        if(isRenovation) {
            let urlQuotation = UrlService.resolveQuotationRenovationHtmlLink(signature, true);
            let {file_path} = await RenderService.exportQuotation2Pdf(urlQuotation, signature, isRenovation);
            let urlDownload = UrlService.resolveDownloadRenovationQuotationStream(signature);
            await ConsultantService.DAO.LayoutRenovation.updateQuotationLink(signature, urlQuotation, urlDownload);
            return {
                quotation_url: urlQuotation,
                download_url: urlDownload,
                file_path: file_path
            };
        }

        let urlQuotation = UrlService.resolveLayoutTemplateQuotationHtmlLink(signature, true);
        let {file_path} = await RenderService.exportQuotation2Pdf(urlQuotation, signature);
        let urlDownload = UrlService.resolveDownloadLayoutTemplateQuotationStream(signature);
        await ConsultantService.DAO.Layout.updateQuotationLink(signature, urlQuotation, urlDownload);
        return {
            quotation_url: urlQuotation,
            download_url: urlDownload,
            file_path: file_path
        };
    };

    async function getProductsFromUnityLayout(layoutData) {
        let layout = layoutData["unity_layout"];
        let itemsMap = parseTTSLayout(layout);
        let objectKeys = Object.values(itemsMap).map(item => {
            return item.object_key;
        });

        let skus = await DB.getListSKUFromObjectKeys(objectKeys);
        let project_id = layoutData["project_id"];
        let discount = discountServices.getDiscountByProjectID(project_id);
        let products = skus.map(row => {
            let product = row["products"];
            let merchants = row["merchants"];
            let item = itemsMap[product.object_key];
            let quantity = 0;
            if (item) {
                quantity = item.count || 0;
            }

            let priceHasDiscounted = product.price - (product.price * discount);
            return {
                merchant_id: product.merchant_id,
                merchant_name: merchants.name,
                merchant_logo: merchants.image,
                name: product.name,
                sku: product.sku,
                thumbnail_url: product.thumbnail_url,
                quantity: quantity,
                price: priceHasDiscounted,
                market_price: product.price,
                total_price: quantity * priceHasDiscounted,
                total_market_price: quantity * product.price
            }
        });

        let metadata = {
            project_id: layoutData.project_id,
            project_name: "",
            layout_id: layoutData.id,
            layout_name: layoutData.name,
            consultant_id: 0,
            serial_key: "F-202005-100-ID",
            version: "1.2.4",
            area: layoutData.area,
            bed_room_num: layoutData.bed_room_num,
            fitinfurniture_fee: layoutData.fitinfurniture_fee,
            construction_fee: layoutData.construction_fee
        };

        let gallery = await DB.getGalleryByLayoutID(layoutData["id"]);

        let result = {
            layout_id: layoutData["id"],
            layout_name: layoutData["name"],
            area: `${layoutData['area']} m2`,
            bed_room_num: layoutData["bed_room_num"],
            construction_fee: layoutData["construction_fee"] || 0,
            fitinfurniture_fee: layoutData["fitinfurniture_fee"] || 0,
            project_id: project_id,
            discount: (discount * 100) + "%",
            image_url: UrlService.resolveProjectImageUrl(layoutData["image"]),
            gallery: []
        };

        if (gallery.length > 0) {
            result.gallery = gallery.map(g => {
                return {
                    thumb_url: UrlService.resolveProjectImageUrl(g["images"]["thumb_path"]),
                    image_url: UrlService.resolveProjectImageUrl(g["images"]["image_path"])
                }
            });
        }

        let group = groupByMerchant(products, result);
        //// convert items to cart json format
        // ConsultantService.addLayoutCart
        let cartJSON = buildCartJsonFromItems(metadata, products);
        let ecomItems = products.map(item => {
            return {
                sku : item.sku,
                quantity : item.quantity || 1
            }
        });

        let cartEcom = await CartService.buildCartFromJson(cartJSON, {
            email: "",
            panorama_url: null,
            cart: null,
            metadata: metadata
        });

        await ConsultantService.addLayoutCart(cartJSON, cartEcom, metadata.layout_id);
        return group;

    }

    function groupByMerchant(productList, result) {
        let merchants = {};
        let quotationPrice = 0;
        let quotationMarketPrice = 0;
        productList.forEach(p => {
            let id = p.merchant_id;
            let m = merchants[id];
            if (m === undefined || m === null) {
                m = {
                    merchant_id: id,
                    merchant_name: p.merchant_name,
                    merchant_logo: UrlService.resolveCDNEcomUrl(p.merchant_logo),
                    sum_price: 0,
                    product_list: []
                };

                merchants[id] = m;
            }

            m.sum_price += p.total_price;
            m.product_list.push({
                name: p.name,
                sku: p.sku,
                thumbnail_url: UrlService.resolveCDNEcomUrl(p.thumbnail_url),
                quantity: p.quantity,
                price: p.price,
                market_price: p.market_price,
                total_price: p.total_price,
                total_market_price: p.total_market_price,
            });

            quotationPrice += p.total_price;
            quotationMarketPrice += p.total_market_price;
        });

        result.quotation_price = quotationPrice;
        result.quotation_market_price = quotationMarketPrice;
        result.quotation_discount_price = quotationMarketPrice - quotationPrice;
        result.cart = Object.values(merchants);
        return result;
    }

    function parseTTSLayout(layout) {
        let items = layout[TTS_KEY_ITEMS];
        if (items === null || items.length === 0) {
            return null;
        }


        //// remove items are in library
        items = items.filter(item => {
            return !(item[TTS_KEY_ITEM_NAME] === undefined || item[TTS_KEY_ITEM_NAME] === null || item[TTS_KEY_ITEM_NAME].startsWith("library"));
        });

        let productsMap = {};
        items.forEach(item => {
            const object_key = item[TTS_KEY_ITEM_NAME];
            let product = productsMap[object_key];
            if (product === undefined || product === null) {
                product = {'object_key': object_key, 'count': 0};
                productsMap[object_key] = product;
            }
            product.count = item[TTS_KEY_LIST_ITEMS].length;
        });

        return productsMap;
    }

    function buildCartJsonFromItems(metadata, items) {
        let cartItems = [];
        let cartJSON = {
            metadata: metadata || {},
            client: {
                email: null,
                phone: null
            },
            carts: {
                areas: [
                    {
                        id: 0,
                        name: "OTHER FURNITURE",
                        area: 0.0,
                        items : cartItems
                    }
                ]
            }
        };


        items.forEach(item => {
            cartItems.push({
                sku: item.sku,
                name: item.name,
                cost: item.market_price,
                quantity: item.quantity || 1,
                key: item.sku,
                type: 'ecom',
            })
        });

        return cartJSON;
    }

    Service.getConsultantLayoutData = async function (args) {
        try {
            let result = await MongoAccess.getConsultantLayoutData(args);
            if (result) {
                result = Transform.consultantsLayout.result(result);
                return result;
            }

            return null;
        } catch (e) {
            return [];
        }
    };


    module.exports = Service;
})();