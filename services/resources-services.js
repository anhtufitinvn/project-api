(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Resources = require('../db/resources-mockup');
    let DB = require('../db/mysql/dbaccess');
    let LocationDAO = require('../db/mongo/dao/location-dao');
    let RoomCategoryDAO = require('../db/mongo/dao/room-category-dao');
    let PhoneSmsSpamDAO = require('../db/mongo/dao/phones-sms-spam-dao');
    let Transforms = require('../transform/transforms');

    /****/
    let LocationsV2 = null;
    let RoomCategory = null;

    /***
     * Danh sach Phone can duoc gui tin nhan quang cao tuyen dung FITIN
     * @returns {Promise<*>}
     */
    Service.getSpamSMSPhoneList = async function () {
        let list = await PhoneSmsSpamDAO.getList();
        list = Transforms.PhoneSpamSMS.list(list);
        return list;
    };


    Service.importPhoneSMS4Spam = async function(phones) {
        let result = {
            imported: 0,
            total: 0,
            errors: []
        };

        let inserted = await PhoneSmsSpamDAO.import(phones);
        result.imported = inserted.imported;

        if (inserted.errors) {
            inserted.errors.forEach(er => {
                result.errors.push(er);
            });
        }

        return result;
    };

    Service.updatePhoneSMS4Spam = async function(phone) {
        await PhoneSmsSpamDAO.updateSent(phone);
    };

    Service.getFormProperties = async function () {
        if (Resources['ct_forms']['ct_projects'] === undefined || Resources['ct_forms']['ct_projects'] === null) {
            Resources['ct_forms']['ct_projects'] = await DB.getAllProjectAsResources();
        }

        if (Resources['ct_forms']) {
            return Resources['ct_forms'];
        }

        return {}
    };

    Service.getLocations = async function () {
        if (LocationsV2 === null || LocationsV2 === undefined) {
            let LocationsV2 = await LocationDAO.getList(0, 250);

            LocationsV2 = LocationsV2.map(city => {
                return {
                    lat: city.lat,
                    long: city.long,
                    city_id: city.city_id,
                    city_name: city.city_name,
                    districts: city.districts.map(d => {
                        return {
                            district_name: d.district_name,
                            district_id: d.district_id,
                            wards: d.wards.map(w => {
                                return {
                                    ward_id: w.ward_id,
                                    ward_name: w.ward_name,
                                }
                            })
                        }
                    })
                };
            });

            let map = {cities: {}, districts: {}, wards: {}};

            LocationsV2.list = Transforms.Locations.listV2(LocationsV2);

            LocationsV2.forEach(row => {
                const id = row['city_id'];
                map.cities[id] = {name: row['city_name'], id: id};
                row['districts'].forEach(childRow => {
                    const dist_id = childRow['district_id'];
                    map.districts[dist_id] = {name: childRow['district_name'], id: dist_id, city_id: childRow['city_id']};
                    childRow['wards'].forEach(grandchildRow => {
                        const ward_id = grandchildRow['ward_id'];
                        map.wards[ward_id] = {
                            name: grandchildRow['ward_name'],
                            id: ward_id,
                            district_id: grandchildRow['district_id']
                        };
                    });
                });
            });


            LocationsV2.getCity = function (id) {
                if (id) {
                    return map.cities[id.toString()];
                }

                return null;
            };

            LocationsV2.getDistrict = function (id) {
                if (id) {
                    return map.districts[id.toString()];
                }

                return null;
            };

            LocationsV2.getWard = function (id) {
                if (id) {
                    return map.wards[id.toString()];
                }

                return null;
            };

            return LocationsV2;
        }

        return LocationsV2;
    };

    Service.loadRoomCategories = async function () {
        if (RoomCategory === null || RoomCategory === undefined) {
            RoomCategory = await RoomCategoryDAO.getList();
            return RoomCategory;
        }

        return RoomCategory;
    };

    Service.resolveRoomNameByGroupName = function (type) {
        if (!type) {
            return "Phòng khác";
        }

        let t = type.toLowerCase();
        if (RoomCategory) {
            let cate = RoomCategory.find((c) => {
                return c.type === t || c.type.startsWith(type) || c.type.endsWith(type) || t.indexOf(c.type) > -1;
            });

            return cate ? cate.name : type;
        }

        return type;
    };

    Service.clearCache = function () {
        RoomCategory = null;
        LocationsV2 = null;
    };

    Service.ENUMS = {
        STATUS: {
            PUBLISHED: "published",
            PENDING: "pending",
            UNPUBLISHED: "unpublished"
        }
    };

    module.exports = Service;
})();