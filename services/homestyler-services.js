(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Promise = require('bluebird');
    let ACCESS_KEY = global.configuration.ACCESS_KEY || "fitin";
    let HOMESTYLER_KEY = global.configuration.HOMESTYLER_KEY || "fitin";
    let ECOM_API_URL = global.configuration.URLS["ecom_api_url"];
    let HOMESTYLER_API_URL = global.configuration.URLS["homestyler_api_url"];
    let logger = Service.logger;
    let debug = Service.debug;
    let request = require('request');

    
    Service.getCartBySignature = async function(signature, constructionFee, furnitureFee) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key" : HOMESTYLER_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'POST',
                url: `${HOMESTYLER_API_URL}/v1/objects/keys`
            };

            debug("call :", options.url);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        resolve(body["data"]);
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.getItems = async function(items) {
       
        let keys = items.map (item => {
            return item.sku;
        });

        console.log('items   ', keys);
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key" : HOMESTYLER_KEY
                },
                body: {
                    keys: keys
                },
                json: true,
                method: 'POST',
                url: `${HOMESTYLER_API_URL}/v1/objects/keys`
            };

            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        resolve(body["data"]);
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    module.exports = Service;
})();