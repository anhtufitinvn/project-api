(function () {
    let ResourceService = require('./resources-services');
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    const NodeCache = require("node-cache");

    /**
     * COMMON CACHE
     * hien tai dang su dung cho fronted cache ket landing page
     */
    const MyCache = new NodeCache();

    /** cache danh rieng cho CMS **/
    const MyLayoutsCache = new NodeCache();

    /** cache danh rieng cho CMS **/
    const MyProjectCache = new NodeCache();

    /** cache danh rieng cho CMS **/
    const MyLayoutRenovationCache = new NodeCache();

    /** cache danh rieng cho CMS **/
    const MyCartCache = new NodeCache();

    /** cache danh rieng cho CMS **/
    const UserCache = new NodeCache();

    async function getAndLoad(cache, key, fn) {
        let value = cache.get(key) || null;
        if (value === undefined || value === null) {
            if (typeof fn === 'function' || fn instanceof Function) {
                value = await fn();
                if (value) {
                    cache.set(key, value);
                }
            }
        }

        return value;
    }

    Service.set = function (key, value, ttl) {
        MyCache.set(key, value, ttl || 600);
    };

    Service.get = function (key) {
        let value = MyCache.get(key);
        if (value === undefined) {
            return null;
        }

        return value;
    };

    Service.flushAll = function () {
        MyCache.flushAll();
        ResourceService.clearCache();
    };


    /** PROJECT CACHE - cache danh rieng cho Project CMS **/
    Service.Project = {
        set(key, value, ttl) {
            MyProjectCache.set(key, value, ttl || 900);
        },
        get(key) {
            let value = MyProjectCache.get(key);
            if (value === undefined) {
                return null;
            }

            return value;
        },
        flushAll() {
            MyProjectCache.flushAll();
        },
        async getAsync(key, loadFn) {
            return await getAndLoad(MyProjectCache, key, loadFn);
        }
    };

    /** cache danh rieng cho Layout cua CMS **/
    Service.Layout = {
        set(key, value, ttl) {
            MyLayoutsCache.set(key, value, ttl || 900);
        },
        get(key) {
            let value = MyLayoutsCache.get(key);
            if (value === undefined) {
                return null;
            }

            return value;
        },
        flushAll() {
            MyLayoutsCache.flushAll();
        },
        async getAsync(key, loadFn) {
            return await getAndLoad(MyLayoutsCache, key, loadFn);
        }
    };

    /**
     * LAYOUT RENOVATION CACHE
     */
    Service.LayoutRenovation = {
        set(key, value, ttl) {
            MyLayoutRenovationCache.set(key, value, ttl || 900);
        },
        get(key) {
            let value = MyLayoutRenovationCache.get(key);
            if (value === undefined) {
                return null;
            }

            return value;
        },
        flushAll() {
            MyLayoutRenovationCache.flushAll();
        },
        async getAsync(key, loadFn) {
            return await getAndLoad(MyLayoutRenovationCache, key, loadFn);
        }
    };


    /**
     * CART CACHE
     */
    Service.Cart = {
        set(key, value, ttl) {
            MyCartCache.set(key, value, ttl || 900);
        },
        get(key) {
            let value = MyCartCache.get(key);
            if (value === undefined) {
                return null;
            }

            return value;
        },
        flushAll() {
            MyCartCache.flushAll();
        },
        async getAsync(key, loadFn) {
            return await getAndLoad(MyCartCache, key, loadFn);
        }
    };


    /**
     * USER CACHE
     */
    Service.User = {
        set(key, value, ttl) {
            UserCache.set(key, value, ttl || 900);
        },
        get(key) {
            let value = UserCache.get(key);
            if (value === undefined) {
                return null;
            }

            return value;
        },
        flushAll() {
            UserCache.flushAll();
        },
        async getAsync(key, loadFn) {
            return await getAndLoad(UserCache, key, loadFn);
        }
    };

    module.exports = Service;
})();