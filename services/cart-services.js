(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let {logger, debug} = require('../core/logger');
    let storeService = require('./store-services');
    let ecomService = require('./ecom-services');
    let cartItem = require('../models/cart-item');

    Service.buildCartFromJsonFile = async function (cartJson, payload, signature) {
        let data = await storeService.readFileJson(cartJson);
        if (data && data['carts']) {
            // version 1.0
            let carts = data['carts'];
            if (carts && carts['items']) {
                let listItem = carts['items'].map(c => {
                    return cartItem(c.sku, c.quantity);
                });

                return await buildCartFromItems(listItem, [], payload, signature);
            }
            if (carts && carts['areas']) {
                let areas = carts['areas'];
                return await buildCartFromAreaItems(areas, payload, signature);
            }
        }

        throw new Error("carts.json is invalid format");
    };

    Service.buildCartFromJson = async function (data, payload, signature) {
        if (data && data['carts']) {
            // version 1.0
            let carts = data['carts'];
            if (carts && carts['items']) {
                let listItem = carts['items'].map(c => {
                    return cartItem(c.sku, c.quantity);
                });

                return await buildCartFromItems(listItem, [], payload, signature);
            }
            if (carts && carts['areas']) {
                let areas = carts['areas'];
                return await buildCartFromAreaItems(areas, payload, signature);
            }
        }

        throw new Error("carts.json is invalid format");
    };

    async function buildCartFromItems(items, non_ecom_items, payload, signature) {
        let metadata = payload.metadata;
        let cart = await ecomService.buildCart(items, signature || null, metadata.construction_fee, metadata.fitinfurniture_fee);
      
        if (cart && cart.signature) {
            return {
                non_ecom_items : non_ecom_items,
                items: items,
                cart_sign: cart.signature
            };
        }

        throw new Error("build cart got an exception");
    }

    async function buildCartFromAreaItems(areas, payload, signature) {
        let listItem = [];
        let listNonEcomItem = [];
        areas.forEach(area => {
            if (area['items']) {
                area.items.map(c => {
                    if(c['type'] && c['type'] === 'ecom') {
                        let i = cartItem(c.sku, c['quantity'] || c['Quantity'], area.id, area.name);
                        i.name = c.name;
                        // listItem.push(cartItem(c.sku, c['quantity'] || c['Quantity'], area.id, area.name));
                        listItem.push(i);
                    }

                    if(c['type'] && c['type'] === 'non_ecom') {
                        let i = cartItem(c.sku, c['quantity'] || c['Quantity'], area.id, area.name, c.cost);
                        i.name = c.name;
                        // listItem.push(cartItem(c.sku, c['quantity'] || c['Quantity'], area.id, area.name));
                        listNonEcomItem.push(i);
                    }
                })
            }
        });

        debug(listItem);
        return buildCartFromItems(listItem, listNonEcomItem, payload, signature);
    }

    module.exports = Service;
})();