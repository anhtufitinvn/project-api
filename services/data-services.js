(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Logger = Service.logger;
    let DB = require('../db/mysql/dbaccess');
    let MongoAccess = require('../db/mongo/dbaccess');
    let Transform = require('../transform/transforms');
    let CacheService = require('./cached-services');
    let Locations = null;

    Service.generateObjectId = function () {
        return MongoAccess.generateObjectId();
    };

    Service.getLocations = function () {
        return global.locations || Locations;
    };

    Service.getProjectList = async function (page) {
        try {
            let result = null;
            let key = "projects";
            if (page !== undefined && page !== null) {
                key = `projects-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DB.getProjectList(page);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DB.getAllProjectList();
            }

            if (result) {
                result = Transform.projects.list(result);
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };

    Service.getLayoutById = async function (projectId, layoutId) {
        let projects = await Service.getLayoutListByProjectId(projectId);
        if (projects && projects.length > 0) {
            let layouts = projects[0].layouts.filter(l => {
                return l.layout_id === layoutId;
            });

            if (layouts && layouts.length > 0) {
                return layouts[0];
            }

            return null;
        }

        return null;
    };

    Service.getLayoutByLayoutID = async function (layoutId) {
        let layoutData = await DB.getUnityLayoutById(layoutId);
        if (layoutData) {
            return layoutData;
        }

        return null;
    };

    /***
     * Return data as array has only one element inside
     * @param project_id
     * @param page
     * @returns {Promise<*>}
     */
    Service.getLayoutListByProjectId = async function (project_id, page) {
        try {
            let result = null;
            let key = `project-layouts-${project_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DB.getLayoutListByProjectId(project_id, page);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DB.getAllLayoutListByProjectId(project_id);
            }

            if (result) {
                result = Transform.layouts.list(result);
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectId", e);
            return [];
        }
    };

    /**
     * Return as a object has layout list inside
     * @param project_id
     * @param page
     * @returns {Promise<*>}
     */
    Service.getLayoutListByProjectIdEx = async function (project_id, page) {
        try {
            let result = null;
            let key = `project-layouts-ex-${project_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached[0];
                }
                result = await DB.getLayoutListByProjectId(project_id, page);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached[0];
                }
                result = await DB.getAllLayoutListByProjectId(project_id);
            }

            if (result) {
                result = Transform.layouts.list(result);
                CacheService.set(key, result, 900);
                return result[0];
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectIdEx", e);
            return null;
        }
    };

    Service.getContactList = async function (page) {
        try {

            let key = "contacts-";
            if (page === null || page === undefined) {
                key = key + `all`;
            } else {
                key = key + page;
            }

            let result = CacheService.get(key);
            if (result) {
                return result;
            }
            result = await DB.getContactList(0);
            if (result) {
                result = Transform.contacts.list(result);
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };

    Service.findEmailContactStartWith = async function (email) {
        try {
            if (email === "") {
                return Service.getContactList();
            }

            let key = 'find:' + email.toLowerCase();
            let result = CacheService.get(key);
            if (result) {
                return result;
            }

            result = await DB.findEmailContactStartWith(email);
            if (result) {
                result = Transform.contacts.list(result);
                CacheService.set(key, result, 300);
                return result;
            }
        } catch (e) {
            return [];
        }
    };

    Service.addClientInbox = async function (payload) {
        return await MongoAccess.addClientInbox(payload);
    };

    Service.getClientInbox = async function (email) {
        try {
            let result = await MongoAccess.getClientInbox(email);
            if (result) {
                result = Transform.clientInbox.list(result);
                return result;
            }
        } catch (e) {
            return [];
        }
    };

    Service.getClientInboxes = async function (email) {
        try {
            let result = await MongoAccess.getClientInboxes(email);
            if (result) {
                result.panoramas = Transform.clientInbox.listEx(result.panoramas);
                result.renders = Transform.clientInbox.listEx(result.renders);
                return result;
            }
        } catch (e) {
            return [];
        }
    };


    Service.getUserIDByEmail = async function (email) {
        let key = `${email}-uid`;
        let result = CacheService.get(key);
        if (result) {
            return result;
        }

        result = await DB.getUserIDByEmail(email);
        if (result > 0) {
            CacheService.set(key, result, 864000);
        }

        return result;

    };

    Service.getConsultantData = async function (signature) {
        try {
            let result = await MongoAccess.getConsultantData(signature);
            if (result) {
                result = Transform.consultants.result(result);
                return result;
            }

            return null;
        } catch (e) {
            Logger.error("getConsultantData", e);
            return [];
        }
    };


    Service.updateCartSign = async function (signature, newSignature) {
        try {
            await MongoAccess.updateCartSign(signature, newSignature);
            return true;
        } catch (e) {
            Logger.error("updateCartSign", e);
            return false;
        }
    };

    Service.updatePanoramaLink = async function (layoutID, link) {
        try {
            await DB.updatePanoramaLink(layoutID, link);
            return true;
        } catch (e) {
            Logger.error("updatePanoramaLink", e);
            return false;
        }
    };

    module.exports = Service;
})();