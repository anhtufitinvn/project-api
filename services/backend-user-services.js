(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Logger = Service.logger;
    let TransformBackend = require('../transform/backend/index');
    let MongoAccess = require('../db/mongo/dbaccess');
    let CacheService = require('./cached-services');
    let DAO = MongoAccess.DAO;
    Service.DAO = DAO;


    /**
     * Module User
     * @type {{}}
     */
    let Users = {};
    Service.Users = Users;
    Users.getUserList = async function (query) {
        try {
           
            let result = null;
            let name = query['name'] || null;
            let key = `cms-users-v2`;
            if(name){
                key = `${key}-${name}`;
            }
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.User.getList(query);

            if (result) {
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            console.log('error', e);
            return [];
        }
    };

    Users.init = async function () {
        try {
           
            result = await DAO.User.init();
            if (result) {    
                return result;
            }
        } catch (e) {
            console.log('error', e);
            return false;
        }
    };

    Users.getUserById = async function (id) {
        try {
            let result = null;
            let key =id.toString();
            let cached = CacheService.User.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.User.getDetail(id);
            
            if (result) {
                result = TransformBackend.users.detail(result);
                CacheService.User.set(key, result, 900);          
                return result;
            }
            else {
                return null;
            }
        } catch (e) {
            return null;
        }
    };

    Users.create = async function(layout) {
        if(layout) {
            let result =  await DAO.Layout.create(layout);
            CacheService.Layout.flushAll();
            return result;
        }

        return null;
    };

    Users.deleteByID = async function(id) {
        if(id) {
            let result =  await DAO.Layout.deleteByID(id);
            CacheService.Layout.flushAll();
            return result;
        }

        return null;
    };

    Users.update = async function(id, layout) {
        if(layout) {
            let result = await DAO.Layout.update(id, layout);
            CacheService.Layout.flushAll();
            return result;
        }

        return null;
    };


    module.exports = Service;
})();