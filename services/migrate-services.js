(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Promise = require('bluebird');
    let HOMESTYLER_API_URL = global.configuration.URLS["homestyler_api_url"];
    let HOMESTYLER_KEY = global.configuration.HOMESTYLER_KEY || "fitin";
    let logger = Service.logger;
    let debug = Service.debug;
    let request = require('request');

    /***
     *
     * @returns {Promise<void>}
     */
    Service.getListProject = async function (page, limit, timestamp) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key": HOMESTYLER_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                url: `${HOMESTYLER_API_URL}/homestyler/v1/projects?page=${page}&limit=${limit}&t=${timestamp}`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        if (body['data'] && body['data']['list']) {
                            resolve(body.data.list);
                        }
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.getListLayout = async function (page, limit, timestamp) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key": HOMESTYLER_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                url: `${HOMESTYLER_API_URL}/homestyler/v1/layouts?page=${page}&limit=${limit}&t=${timestamp}`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        if (body['data'] && body['data']['list']) {
                            resolve(body.data.list);
                        }
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };
    Service.getLayoutDetail = async function (layoutKeyName) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key": HOMESTYLER_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                url: `${HOMESTYLER_API_URL}/homestyler/v1/layouts/${layoutKeyName}`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        if (body['data']) {
                            resolve(body.data);
                        }
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };
    module.exports = Service;
})();