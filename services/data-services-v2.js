(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Logger = Service.logger;
    let DataAccess = require('../db/mysql/dbaccess');
    let MongoAccess = require('../db/mongo/dbaccess');
    let Transform = require('../transform/transforms');
    let CacheService = require('./cached-services');
    let URLServices = require('./url-services');
    let DAO = MongoAccess.DAO;
    Service.DAO = DAO;

    /**
     * Module Projects
     * @type {{}}
     */
    let Projects = {};
    Service.Projects = Projects;

    Projects.getProjectList = async function (page) {
        try {
            const limit = 24;
            let result = null;
            let key = "projects-v2";
            if (page !== undefined && page !== null) {
                key = `projects-v2-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }

                let offset = Math.max(page - 1, 0);
                result = await DAO.Project.getPublishedList(offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.Project.getPublishedList(0, 1000);
            }

            if (result) {
                result = Transform.projects.listV2(result);
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };

    Projects.create = async function(project) {
        if(project) {
            return await DAO.Project.create(project);
        }

        return null;
    };

    Projects.deleteByID = async function(id) {
        if(id) {
            return await DAO.Project.deleteByID(id);
        }

        return null;
    };

    Projects.update = async function(id, project) {
        if(project) {
            return await DAO.Project.update(id, project);
        }

        return null;
    };

    /** module layouts **/
    let Layouts = {};
    Service.Layouts = Layouts;
    /**
     * Return as a object has layout list inside
     * @param project_id : objectId
     * @param page
     * @returns {Promise<*>}
     */
    Layouts.getLayoutListByProjectId = async function (project_id, page) {
        try {
            let result = null;
            let key = `project-layouts-v2-${project_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached[0];
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Layout.getListByProjectId(project_id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Layout.getListByProjectId(project_id);
            }

            if (result) {
                result = Transform.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectId", e);
            return null;
        }
    };

    Layouts.getLayoutListByProjectIntId = async function (id, page) {
        try {
            let layoutList = null;
            let key = `project-layouts-v2-${id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                layoutList = await DAO.Layout.getListByProjectIntId(id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                layoutList = await DAO.Layout.getListByProjectIntId(id);
            }

            if (layoutList) {
                let project = await DAO.Project.getById(id);
                if(project === null || project === undefined || project.status !== ENUMS.STATUS.PUBLISHED) {
                    return null;
                }

                let result = {
                    project_name: project.project_name,
                    project_id: project.int_id,
                    project_string_id: project._id,
                    slug: project.slug,
                    project_image_url: URLServices.resolveConsultantImageUrl(project.image_path)
                };

                result.layouts = Transform.layouts.listV2(layoutList);
                if(result.layouts && result.layouts.length > 0) {
                    CacheService.set(key, result, 900);
                }

                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectIntId", e);
            return null;
        }
    };

    module.exports = Service;
})();