(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Logger = Service.logger;
    let debug = Service.debug;
    let Transform = require('../transform/transforms');
    const STORAGE = global.configuration.STORAGE;
    const ECOM_STATIC_STORAGE = STORAGE["static_ecom"];
    const STATIC_STORAGE = STORAGE["static"];
    let Path = require('path');
    let Moment = require('moment');
    let Promise = require('bluebird');
    let Fs = require('fs-extra');
    let DB = require('../db/mysql/dbaccess');
    let MonDB = require('../db/mongo/dbaccess');
    let Hubspot = require('./hubspot-services');
    const synFileCached = Path.resolve(Path.dirname(__dirname), "syn.json");
    const TIME_FORMAT = "YYYY-MM-DD HH:mm:ss";


    function lastSynMetadata() {
        return new Promise((resolve) => {
            let meta = {
                page: 0,
                last_sync_time: '2019-01-01 00:00:00',
                last_sync_unix: 1546300800
            };

            if (Fs.existsSync(synFileCached)) {
                try {
                    let data = Fs.readFileSync(synFileCached, "utf8");
                    if (data === undefined || data === null || data === "") {
                        return resolve(meta);
                    }

                    data = JSON.parse(data);
                    if (data && data['contacts']) {
                        meta.page = data.contacts.page || 0;
                        if (data.last_sync_time) {
                            meta.last_sync_unix = Moment(data.last_sync_time, TIME_FORMAT).unix();
                        }
                    }
                } catch (e) {
                    Logger.error("lastSynMetadata ", e);
                    return resolve(meta);
                }
            }

            return resolve(meta);
        });

    }

    async function copyImageFromECOM(image) {
        let imageEcomPath = Path.join(ECOM_STATIC_STORAGE, image);
        let imageTargetPath = Path.join(STATIC_STORAGE, image);
        let existed = await Fs.pathExists(imageTargetPath);
        if(existed) {
            return {
                success : true,
                existed : existed,
                image_src : imageEcomPath,
                image_target : imageTargetPath
            }
        }

        try {
            let srcExisted = await Fs.pathExists(imageEcomPath);
            if(!srcExisted) {
                return {
                    success : false,
                    existed : existed,
                    image_src : imageEcomPath,
                    image_target : imageTargetPath
                }
            }

            await Fs.copy(imageEcomPath, imageTargetPath);
            return {
                success : true,
                existed : existed,
                image_src : imageEcomPath,
                image_target : imageTargetPath
            }
        } catch (err) {
            Logger.error("copyImageFromECOM got exception ", err);
            return {
                success : false,
                existed : existed,
                image_src : imageEcomPath,
                image_target : imageTargetPath
            }
        }
    }

    Service.synContactFromDB = async () => {
        let metadata = await lastSynMetadata();
        const limit = 100;
        let p = metadata.page;
        let list = await DB.getContactListEx(limit, p);
        let ownerId = Hubspot.getOwnerId();
        while (list && list.length >= limit) {
            Logger.info(`------------ page ${p}-----------------`);
            list = list.filter(row => {
                return row['customers']['email'] !== null && row['customers']['email'] !== undefined && row['customers']['email'] !== ""
            });

            let emailList = [];

            let batched = list.map((row) => {
                emailList.push(row['customers']["email"]);
                return {
                    email: row['customers']["email"],
                    properties: [
                        {property: 'firstname', value: row['customers']["name"]},
                        {property: 'phone', value: row['customers']["phone"]},
                        {property: 'owneremail', value: "dev@fitin.vn"},
                        {property: 'website', value: "fitin.vn"},
                        {property: 'hubspot_owner_id', value: ownerId}
                    ]
                };
            });


            try {
                await Hubspot.importAllContacts(batched);
                await Hubspot.importContactsToClientList({emails: emailList});
            } catch (e) {
                throw e;
            }
            p = p + 1;
            Fs.writeFileSync(synFileCached, JSON.stringify({
                contacts: {
                    page: p,
                    last_sync_time: Moment().format(TIME_FORMAT)
                }
            }));
            list = await DB.getContactListEx(limit, p);
        }
    };

    Service.synLastestContactFromDB = async () => {
        let metadata = await lastSynMetadata();
        const limit = 100;
        let p = 0;
        let list = await DB.getContactListFromTime(limit, p, metadata.last_sync_unix);
        let ownerId = Hubspot.getOwnerId();
        while (list && list.length >= limit) {
            Logger.info(`------------ page ${p}-----------------`);
            list = list.filter(row => {
                return row['customers']['email'] !== null && row['customers']['email'] !== undefined && row['customers']['email'] !== ""
            });

            let emailList = [];

            let batched = list.map((row) => {
                emailList.push(row['customers']["email"]);
                return {
                    email: row['customers']["email"],
                    properties: [
                        {property: 'firstname', value: row['customers']["name"]},
                        {property: 'phone', value: row['customers']["phone"]},
                        {property: 'owneremail', value: "dev@fitin.vn"},
                        {property: 'website', value: "fitin.vn"},
                        {property: 'hubspot_owner_id', value: ownerId}
                    ]
                };
            });


            try {
                await Hubspot.importAllContacts(batched);
                await Hubspot.importContactsToClientList({emails: emailList});
            } catch (e) {
                throw e;
            }

            p = p + 1;
            Fs.writeFileSync(synFileCached, JSON.stringify({
                contacts: {
                    page: metadata.page,
                    last_sync_time: Moment().format(TIME_FORMAT)
                }
            }));
            list = await DB.getContactListFromTime(limit, p, metadata.last_sync_unix);
        }
    };

    Service.importProjectsFromMysql = async () => {
        let result = {
            imported: 0,
            total: 0,
            errors: []
        };

        let page = 1;
        let list = await DB.getProjectListWithoutStatusFilter(page);
        if (list === undefined || list === null || list.length === 0) {
            return result;
        }

        do {
            Logger.info(`------------ syn mysql projects page ${page} -----------------`);
            try {
                list = Transform.projects.list(list);
                let projects = list.map(p => {
                    return {
                        project_name: p.project_name,
                        int_id: p.project_id,
                        slug: p.slug,
                        image_path: p.image_path,
                        status: p.status,
                        location: {
                            city_id: p.location.city_id,
                            city_name: p.location.city_name,
                            district_id: p.location.district_id,
                            district_name: p.location.district_name
                        },
                    }
                });

                result.total += projects.length;
                let inserted = await MonDB.DAO.Project.import(projects);
                if (inserted.errors) {
                    inserted.errors.forEach(er => {
                        result.errors.push(er);
                    });
                }

                debug(inserted);
                debug(result);
                result.imported += inserted.imported;
            } catch (e) {
                throw e;
            }

            page = page + 1;
            list = await DB.getProjectListWithoutStatusFilter(page);
        } while (list && list.length > 0);

        return result;
    };

    Service.importLocationFromMysql = async () => {
        let result = {
            imported: 0,
            total: 0,
            errors: []
        };

        try {
            let locations = await DB.getLocations();
            if (locations === undefined || locations === null || locations.length === 0) {
                return result;
            }
            locations = Transform.Locations.list(locations);
            result.total = locations.length;
            let inserted = await MonDB.DAO.Location.import(locations);
            if (inserted.errors) {
                inserted.errors.forEach(er => {
                    result.errors.push(er);
                });
            }

            result.imported = inserted.imported;
        } catch (e) {
            throw e;
        }

        return result;
    };

    Service.importLayoutsFromMysql = async () => {
        let result = {
            imported: 0,
            total: 0,
            errors: []
        };

        Logger.info(`------------ load projects list -----------------`);
        let projects = await MonDB.DAO.Project.getList();
        if (projects && projects.docs) {
            projects = projects.docs;
        }
        let count = 0;
        for (let p of projects) {
            try {
                Logger.info(`------------ syn mysql layouts of project ${p.project_name} - ${p.int_id}------[${++count}/${projects.length}]-----------`);
                let layouts = await DB.getAllLayoutListByProjectId(p.int_id);
                let T = Transform.layouts.listEx(layouts);
                layouts = T.map(layout => {
                    let x = {
                        project_name: layout.project_name,
                        project_int_id: layout.project_id,
                        project_id: p._id || layout.project_id.toString(),
                        layout_name: layout.name,
                        int_id: layout.layout_id,
                        slug: layout.slug,
                        image_path: layout.image_path,
                        image_floor_plan_path: layout.image_floor_plan_path,
                        status: layout.status === 1 ? ENUMS.STATUS.PUBLISHED : ENUMS.STATUS.UNPUBLISHED,
                        desc: layout.desc,
                        area: layout.area,
                        metadata: {
                            title: layout.title,
                            intro: null,
                            price: {
                                estimated: layout.price,
                                max: layout.price,
                                min: layout.price
                            },
                            rooms: [
                                {name: "living_room", qty: 1, level: 0, area: 0.0},
                                {name: "bed_room", qty: layout.bed_room_num, level: 0, area: 0.0},
                                {name: "kitchen", qty: 1, level: 0, area: 0.0},
                                {name: "wc", qty: layout.bed_room_num, level: 0, area: 0.0}
                            ],
                            style: {
                                name: layout.style_name,
                                key: null
                            }
                        },
                        attributes: [
                            {
                                label: "Editor support",
                                name: "has_editor",
                                desc: "using editor",
                                type: "int",
                                value_int: layout.has_editor
                            }
                        ],
                        extensions: {
                            cart_json: null,
                            panorama: {
                                enable: layout.panorama_url ? 1 : 0,
                                url: layout.panorama_url
                            },
                            services_fee: [
                                {
                                    label: "Phí thi công",
                                    key: "construction_fee",
                                    value: layout.construction_fee || 0,
                                    detail: null
                                },
                                {
                                    label: "Phí vật tư",
                                    key: "fitinfurniture_fee",
                                    value: layout.fitinfurniture_fee || 0,
                                    detail: [
                                        {
                                            label: "Gỗ sàn công nghiệp",
                                            key: "mat_san_cn",
                                            quantity: 0,
                                            value: 0,
                                            unit: "m2"
                                        }
                                    ]
                                },
                            ]
                        },
                        gallery: layout.gallery.map((g, index) => {
                            return {
                                int_id: g.layout_id,
                                thumb_path: g.thumb_path,
                                image_path: g.image_path,
                                index: index,
                                hidden: 0
                            }
                        })
                    };

                    if (layout.layout_id === 1760) {
                        debug('===> 1760 <=== gallary :' + x.gallery.length);
                    }

                    return x;
                });

                result.total += layouts.length;
                let inserted = await MonDB.DAO.Layout.import(layouts);
                if (inserted.errors) {
                    inserted.errors.forEach(er => {
                        result.errors.push(er);
                    });
                }

                result.imported += inserted.imported;
                debug(result);
            } catch (e) {
                throw e;
            }
        }

        return result;
    };

    Service.migrateProjectImagesFromECOM = async () => {
        let report = {
            migrated : 0,
            existed : 0,
            total_images : 0,
            errors : []
        };

        let list = await DB.getAllProjectList();
        if(list) {
            for(let row of list) {
                let image = row.project["image"];
                if(image) {
                    report.total_images++;
                    let rs = await copyImageFromECOM(image);
                    if(rs.success) {
                        rs.existed ? report.existed++ : report.migrated++;
                    } else {
                        report.errors.push(rs.image_src);
                    }
                }
            }
        }

        return report;
    };

    Service.migrateLayoutImagesFromECOM = async () => {
        let report = {
            migrated : 0,
            existed : 0,
            total_images : 0,
            errors : []
        };

        let list = await MonDB.DAO.Layout.getPublishedList(0, 1000);
        if(list) {
            for(let row of list) {
                let gallery = row["gallery"];
                let image = row["image_path"];
                if(image) {
                    report.total_images++;
                    let rs = await copyImageFromECOM(image);
                    if(rs.success) {
                        rs.existed ? report.existed++ : report.migrated++;
                    } else {
                        report.errors.push(rs.image_src);
                    }
                }

                //// import gallery
                if(gallery && gallery.length > 0) {
                    for(let g of gallery) {
                        let image = g["image_path"];
                        if(image) {
                            report.total_images++;
                            let rs = await copyImageFromECOM(image);
                            if(rs.success) {
                                rs.existed ? report.existed++ : report.migrated++;
                            } else {
                                report.errors.push(rs.image_src);
                            }
                        }

                        let thumb = g["thumb_path"];
                        if(thumb) {
                            report.total_images++;
                            let rs = await copyImageFromECOM(thumb);
                            if(rs.success) {
                                rs.existed ? report.existed++ : report.migrated++;
                            } else {
                                report.errors.push(rs.image_src);
                            }
                        }
                    }
                }
                //// end gallery
            }
        }

        return report;
    };

    Service.migrateLayoutMetadataFromECOM = async () => {
        let report = {
            total : 0,
            updated : 0,
            errors : [],
            affected : []
        };
        let list = await DB.getAllMetadataOfLayout();
        if(list) {
            report.total = list.length;
            let layoutID = 0;
            for(let row of list) {
                layoutID = row['layouts']['id'];
                let rs = await MonDB.DAO.Layout.updateBedRoomQty(layoutID, row['layouts']['room_number'] || 1);
                if(rs) {
                    report.updated++;
                    report.affected.push({
                        layout_id : layoutID
                    });
                } else {
                    report.errors.push({
                        layout_id : layoutID
                    })
                }
            }
        }

        return report;
    };

    module.exports = Service;
})();