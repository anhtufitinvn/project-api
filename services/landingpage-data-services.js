(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Logger = Service.logger;
    let MongoAccess = require('../db/mongo/dbaccess');

    let Transform = require('../transform/frontend/index');
    let CacheService = require('./cached-services');
    let ENUMS = global.ENUMS;
    let DAO = MongoAccess.DAO;
    Service.DAO = DAO;


    /**
     * Module RoomCategories
     * @type {{}}
     */
    let RoomCategories = {};
    Service.RoomCategories = RoomCategories;
    RoomCategories.getRoomCategoryList = async function (query) {
        try {
           
            let result = null;
            let name = query['name'] || null;
            let status = query['status'] || null;
            
            let key = `room-categories-v2`;
            key = `${key}?name=${name}&status=&${status}`;
            
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.RoomCategory.getList(query);

            if (result) {
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };

    RoomCategories.init = async function () {
        try {
           
            result = await DAO.RoomCategory.init();
            if (result) {    
                return result;
            }
        } catch (e) {
            return false;
        }
    };



    /**
     * Module Styles
     * @type {{}}
     */
    let Styles = {};
    Service.Styles = Styles;
    Styles.getStyleList = async function (query) {
        try {
           
            let result = null;
            let name = query['name'] || null;
            let key = `styles-v2`;
            if(name){
                key = key + '-' + name;
            }
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Style.getList(query);

            if (result) {
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };

    /**
     * Module Projects
     * @type {{}}
     */
    let Projects = {};
    Service.Projects = Projects;

    Projects.getProjectList = async function (page, query) {
        try {
            //const limit = parseInt(query['limit'] || 24);
            let limit = 24;
            let result = null;
            let projectId = query['project_id'] || null;
            let intID = parseInt(projectId);
            let name = query['name'] || null;
            let key = `projects-v2-${limit}`;
            if(name){
                key = key + '-' + name;
            }
            if(intID){
                key = key + '-' + intID.toString();
            }

            if (page !== undefined && page !== null) {
                key = `${key}-${page}`;

                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                result = await DAO.Project.getList(page, limit, query);

            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.Project.getList(1, 1000, query);
            }

            if (result) {
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };


    Projects.getProjectRenovationList = async function (query) {
        try {
            let result = null;
            let name = query['name'] || null;
            let key = `projects-renovation-v2-${name}`;
        
            let cached = CacheService.Project.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Project.getListRenovation(query);

            if (result) {
                CacheService.Project.set(key, result, 900);
                
            }
            return result || [];
        } catch (e) {
            
            return [];
        }
    };

    Projects.getProjectById = async function (id) {
        try {
            let result = null;
            let key = "projects-v2-" + id.toString();
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Project.getDetail(id);

            if (result.length) {

                result = Transform.projects.detail(result[0]);
                CacheService.set(key, result, 900);

                return result;
            }
        } catch (e) {
            return null;
        }
    };

    Projects.create = async function(project) {
        if(project) {
            let result = await DAO.Project.create(project);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    Projects.deleteByID = async function(id) {
        if(id) {
            let result = await DAO.Project.deleteByID(id);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    Projects.update = async function(id, project) {
        if(project) {
            let result =  await DAO.Project.update(id, project);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    /** module layouts **/
    let Layouts = {};
    Service.Layouts = Layouts;
    /**
     * Return as a object has layout list inside
     * @param project_id : objectId
     * @param page
     * @returns {Promise<*>}
     */
    Layouts.getLayoutListByProjectId = async function (project_id, page) {
        try {
            let result = null;
            let key = `project-layouts-v2-${project_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached[0];
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Layout.getListByProjectId(project_id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Layout.getListByProjectId(project_id);
            }

            if (result) {
                result = Transform.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectId", e);
            return null;
        }
    };

    Layouts.getLayoutListByProjectIntId = async function (id, page) {
        try {
            let result = null;
            let key = `project-layouts-v2-${id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Layout.getListByProjectIntId(id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Layout.getListByProjectIntId(id);
            }

            if (result) {
                result = Transform.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectIntId", e);
            return null;
        }
    };


    Layouts.getLayoutList = async function (query) {
        try {
            let page = query.page;
            //const limit = parseInt(query.limit || 24);
            let limit = 24;
            let result = null;
            let layoutId = query.layout_id;
            //let intID = parseInt(layoutId);
            let name = query.name;
            let project_id = query.project_id;

            let estimated_price = query.estimated_price;
            let style = query.style;
            let bedroom = query.bedroom;
            let room_type = query.room_type;

            let key = `frontend-layouts-list-v2`;
           
            key = `${key}?&limit=${limit}&layout_id=${layoutId}&name=${name}&project_id=${project_id}&estimated_price=${estimated_price}&style=${style}&bedroom=${bedroom}&room_type=${room_type}`;


            if (page !== undefined && page !== null) {
                key = `${key}-${page}`;

                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                result = await DAO.Layout.getList(page, limit, query);

            } else {
                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.Layout.getList(1, 1000, query);
                
            }

            if (result) {
                //result = Transform.layouts.list(result);
                CacheService.Layout.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };


    Layouts.getLayoutById = async function (id) {
        try {
            let result = null;
            let key = id.toString();
            let cached = CacheService.Layout.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Layout.getDetail(id);
            
            if (result.length) {

                result = Transform.layouts.detail(result[0]);
                CacheService.Layout.set(key, result, 900);

                return result;
            }
        } catch (e) {
            console.log(e);
            return null;
        }
    };

    Layouts.create = async function(layout) {
        if(layout) {
            let result =  await DAO.Layout.create(layout);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    Layouts.deleteByID = async function(id) {
        if(id) {
            let result =  await DAO.Layout.deleteByID(id);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    Layouts.update = async function(id, layout) {
        if(layout) {
            let result = await DAO.Layout.update(id, layout);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    Layouts.duplicate = async function(id) {
        if(id) {
            let layout = await Service.Layouts.getLayoutById(id);
            delete layout._id;
            layout.is_template = false;
            layout.parent_int_id = layout.int_id; 
            return await Service.Layouts.create(layout);
        }

        return null;
    };

    /** module layouts **/
    let LayoutsRenovation = {};
    Service.LayoutsRenovation = LayoutsRenovation;
    /**
     * Return as a object has layout list inside
     * @param project_id : objectId
     * @param page
     * @returns {Promise<*>}
     */
    LayoutsRenovation.getLayoutListByProjectId = async function (project_id, page) {
        try {
            let result = null;
            let key = `project-layouts-v2-${project_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached[0];
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.LayoutRenovation.getListByProjectId(project_id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.LayoutRenovation.getListByProjectId(project_id);
            }

            if (result) {
                result = Transform.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectId", e);
            return null;
        }
    };

    LayoutsRenovation.getLayoutListByProjectIntId = async function (id, page) {
        try {
            let result = null;
            let key = `project-layouts-v2-${id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.LayoutRenovation.getListByProjectIntId(id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.LayoutRenovation.getListByProjectIntId(id);
            }

            if (result) {
                result = Transform.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectIntId", e);
            return null;
        }
    };


    LayoutsRenovation.getLayoutList = async function (query) {
        try {
            let page = query.page;
            //const limit = parseInt(query.limit || 24);
            let limit = 24;
            let result = null;
            let layoutId = query.layout_id;
            
            let name = query.name;
            let project_id = query.project_id;

            let estimated_price = query.estimated_price;
            let style = query.style;
            let bedroom = query.bedroom;
            let room_type = query.room_type;
            let approved = query.approved;
            let status = query.status || ENUMS.STATUS.PUBLISHED;
            let key = `frontend-layouts-renovation-list-v2`;
           

            key = `${key}?&limit=${limit}&layout_id=${layoutId}&name=${name}&project_id=${project_id}&estimated_price=${estimated_price}&style=${style}&bedroom=${bedroom}&room_type=${room_type}&approved=${approved}&status=${status}`;
            
            if (page !== undefined && page !== null) {
                key = `${key}-${page}`;

                let cached = CacheService.LayoutRenovation.get(key);
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                result = await DAO.LayoutRenovation.getList(page, limit, query);

            } else {
                let cached = CacheService.LayoutRenovation.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.LayoutRenovation.getList(1, 1000, query);
                
            }

            if (result) {
                //result = Transform.layouts.list(result);
                CacheService.LayoutRenovation.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    };


    LayoutsRenovation.getLayoutById = async function (id) {
        try {
            let result = null;
            let key = id.toString();
            let cached = CacheService.LayoutRenovation.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.LayoutRenovation.getDetail(id);
            
            if (result.length) {

                result = Transform.layoutsRenovation.detail(result[0]);
                CacheService.LayoutRenovation.set(key, result, 900);

                return result;
            }
            else{
                return null;
            }
        } catch (e) {
            return null;
        }
    };

    module.exports = Service;
})();