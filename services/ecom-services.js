(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Promise = require('bluebird');
    let ACCESS_KEY = global.configuration.ACCESS_KEY || "fitin";
    let ECOM_API_URL = global.configuration.URLS["ecom_api_url"];
    let logger = Service.logger;
    let debug = Service.debug;
    let request = require('request');

    /***
     * @deprecated Please don't use this function, it will be removed in next version.
     * @returns {Promise<void>}
     */
    Service.createCart = async function() {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key" : ACCESS_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'POST',
                url: `${ECOM_API_URL}/plugin/cart`
            };

            debug("call :", options.url);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        resolve(body["data"]);
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.getCartById = async function(cartId) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key" : ACCESS_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                url: `${ECOM_API_URL}/plugin/cart/${cartId}`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        if(body["data"] && body["data"].list) {
                            resolve(body["data"].list);
                        } else {
                            resolve(body["data"]);
                        }
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.buildCart = async function(items, signature, constructionFee, furnitureFee, discount) {
        return new Promise((resolve, reject) => {
            let discountPercent = 0.0;
            let discountValue = 0;
            if(discount && !isNaN(parseInt(discount.toString()))) {
                if(discount > 0.0 && discount <= 1.0) {
                    discountPercent = discount;
                } else {
                    discountValue = discount;
                }
            }
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key" : ACCESS_KEY
                },
                body: {
                    items : items,
                    construction_fee : parseInt(constructionFee) || 0,
                    fitinfurniture_fee : parseInt(furnitureFee) || 0,
                    signature : signature || null,
                    promotions : {
                        code: "FITIN-HR25",
                        value: discountValue || 0,
                        percent: discountPercent || 0.0
                    }
                },
                json: true,
                method: 'POST',
                url: `${ECOM_API_URL}/plugin/cart/build`
            };

            debug("call :", options.url);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        resolve(body["data"]);
                    } else {
                        logger.error("call api error", body);
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.getCartBySignature = async function(signature) {
        return new Promise((resolve, reject) => {
            if(signature === null || signature === undefined) {
                return resolve(null);
            }

            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key" : ACCESS_KEY
                },
                body: {},
                json: true,
                method: 'GET',
                url: `${ECOM_API_URL}/plugin/cart/${signature}`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        if(body["data"] && body["data"]['list']) {
                            resolve(body["data"].list);
                        } else {
                            resolve(body["data"]);
                        }
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.getLayoutById = async function(layout_id){
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key" : ACCESS_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                url: `${ECOM_API_URL}/layouts/${layout_id}`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        if(body["data"] && body["data"].list) {
                            resolve(body["data"].list);
                        } else {
                            resolve(body["data"]);
                        }
                    } else {
                        resolve({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    module.exports = Service;
})();