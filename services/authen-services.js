(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Promise = require('bluebird');
    let ACCESS_KEY = global.configuration.ACCESS_KEY || "fitin";
    let FITIN_ID_URL = global.configuration.URLS["fitin_id_url"];
    let {logger, debug} = require('../core/logger');
    let request = require('request');

    Service.verifyToken = async function(accessToken) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    'X-App-Id': "object-editor",
                    'X-App-Secret': "39f1590ebd5dd409df676f2f3f0ea8552c90fadc"
                },
                body: {
                    token: accessToken
                },
                json: true,
                method: 'POST',
                url: `${FITIN_ID_URL}/user/credential/token/verify`
            };

            debug("call ", options.url);
            request.post(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === -1 || body.data === undefined) {
                        reject(new Error(body.message || "invalid token"));
                    } else {
                        if (body['data']) {
                            const data = body.data;
                            resolve({
                                uid: data.uid || 0,
                                email: data.email,
                                token: data.token,
                                expire: data.expire
                            });
                        } else {
                            reject(new Error("invalid token"));
                        }
                    }
                }
            });
        });
    };

    module.exports = Service;
})();