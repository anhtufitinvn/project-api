(function () {
    let moment = require('moment');
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let logger = Service.logger;
    let debug = Service.debug;
    let Promise = require('bluebird');
    let Path = require('path');
    let fs = require('fs-extra');
    const Puppeteer = require('puppeteer');
    const STORAGE = global.configuration.STORAGE;
    let ACCESS_KEY = global.configuration.RENDER_ACCESS_KEY || "APNpLHHSJYQ9uV4R";
    let RENDER_SERVICE_URL = global.configuration.URLS["render_service_url"];
    let request = require('request');

    const quotationSavePath = Path.join(STORAGE.static, 'qt', 'template');

    const quotationRenovationSavePath = Path.join(STORAGE.static, 'qt', 'renovation');

    fs.ensureDirSync(quotationSavePath);

    fs.ensureDirSync(quotationRenovationSavePath);

    Service.pushAlertMessage = async function (uid, message) {
        return new Promise((resolve, reject) => {
            let m = encodeURI(message);
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Key": ACCESS_KEY
                },
                body: {
                    access_key: "token"
                },
                json: true,
                method: 'GET',
                url: `${RENDER_SERVICE_URL}/inbox/push?userId=${uid}&message=${m}`
            };

            debug("call :", options.url);
            request.get(options, (err, res, body) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                } else {
                    if (body.code === 0) {
                        resolve();
                    } else {
                        reject({message: body.message || "unknown error"});
                    }
                }
            });
        });
    };

    Service.exportQuotation2Pdf = async function (pageUrl, signature, isRenovation) {
        //// yum install libXScrnSaver
        const browser = await Puppeteer.launch({
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox'
            ]
        });

        //// TODO need check file existed and re-generate page if error
        const {file_name, file_path} = await Service.createQuotationPDFFilePath(signature, isRenovation);
        const page = await browser.newPage();
        await page.setViewport({ width: 1680, height: 1050 });
        await page.goto(pageUrl, {waitUntil: 'networkidle2', timeout: 30000});
        let size = await page.evaluate(()=>{
            return {w: document.documentElement.offsetWidth, h : document.documentElement.offsetHeight}
        });
        debug("size " , size);
        await page.addStyleTag({
            content: '@page { size: auto; }',
        });

        await page.pdf({
            path: file_path,
            landscape: false,
            printBackground: true,
            height : (size.h + 2)+ "px",
            width : size.w + "px",
            margin: {
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
            }
        });

        await browser.close();
        return {file_path : file_path, file_name : file_name};

    };

    Service.checkQuotationPDFExist = async function(signature, isRenovation) {
        if(!signature) {
            return false;
        }

        let {filePath} = await Service.createQuotationPDFFilePath(signature, isRenovation);
        return await fs.pathExists(filePath);
    };

    Service.pathExistsAsync = async function(filePath) {
        if(!filePath) {
            return false;
        }

        return await fs.pathExists(filePath);
    };

    Service.createQuotationPDFFilePath = async function(signature, isRenovation) {
        let now = moment();
        const newDirectory = now.format('YYYY/MM/DD');
        const subfix = now.format('HHmm');
        const fileName = `${signature}-${subfix}.pdf`;
        let fileDirectory = isRenovation ? Path.join(quotationRenovationSavePath, newDirectory) : Path.join(quotationSavePath, newDirectory);
        let filePath = Path.join(fileDirectory, fileName);
        await fs.ensureDir(fileDirectory);
        return {
            file_path : filePath,
            file_name: fileName
        };
    };

    module.exports = Service;
})();