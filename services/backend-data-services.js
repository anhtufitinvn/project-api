(function () {
    let Service = Object.create(require('./base-service')());
    Service.print(__filename);
    let Logger = Service.logger;
    let MongoAccess = require('../db/mongo/dbaccess');

    let TransformBackend = require('../transform/backend/index');
    let CacheService = require('./cached-services');
    let DAO = MongoAccess.DAO;
    Service.DAO = DAO;


    /**
     * Module RoomCategories
     * @type {{}}
     */
    let RoomCategories = {};
    Service.RoomCategories = RoomCategories;
    RoomCategories.getRoomCategoryList = async function (query) {
        try {
           
            let result = null;
            let name = query['name'] || null;
            let key = `room-categories-v2`;
            if(name){
                key = key + '-' + name;
            }
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.RoomCategory.getList(query);

            if (result) {
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
    
            return [];
        }
    };

    RoomCategories.init = async function () {
        try {
           
            result = await DAO.RoomCategory.init();
            if (result) {    
                return result;
            }
        } catch (e) {
            return false;
        }
    };



    /**
     * Module Styles
     * @type {{}}
     */
    let Styles = {};
    Service.Styles = Styles;
    Styles.getStyleList = async function (query) {
        try {
           
            let result = null;
            let name = query['name'] || null;
            let key = `styles-v2`;
            if(name){
                key = key + '-' + name;
            }
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Style.getList(query);

            if (result) {
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            
            return [];
        }
    };

    /**
     * Module Projects
     * @type {{}}
     */
    let Projects = {};
    Service.Projects = Projects;

    Projects.getProjectList = async function (page, query) {
        try {
            let limit = (page !== undefined && page !== null) ? 24 : 1000;
            let result = null;
            let projectId = query.project_id;
            let name = query.name;
            let city_id = query.city_id;
            let district_id = query.district_id;
            let status = query.status;
            let is_renovation = query.is_renovation;
            let key = `projects-list-v2`;
            key = `${key}?limit=${limit}&id=${projectId}&name=${name}&city_id=${city_id}&district_id=${district_id}&status=${status}&is_renovation=${is_renovation}`;

            if (page !== undefined && page !== null) {
                key = `${key}&page=${page}`;

                let cached = CacheService.Project.get(key);
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                result = await DAO.Project.getList(page, limit, query);

            } else {
                let cached = CacheService.Project.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.Project.getList(1, limit, query);
            }

            if (result) {
                CacheService.Project.set(key, result, 900);
                return result;
            }
        } catch (e) {
            
            return [];
        }
    };

    Projects.getProjectById = async function (id) {
        try {
            let key = id.toString();
            let cached = CacheService.Project.get(key);
            if (cached) {
                return cached;
            }

            let result = await DAO.Project.getDetail(id);

            if (result.length) {

                result = TransformBackend.projects.detail(result[0]);
                CacheService.Project.set(key, result, 900);
                return result;
                
            }
            else {
                return null;
            }
        } catch (e) {
            
            return null;
        }
    };

    Projects.create = async function(project) {
        if(project) {
            let result = await DAO.Project.create(project);
            CacheService.Project.flushAll();
            return result;
        }

        return null;
    };

    Projects.deleteByID = async function(id) {
        if(id) {
            let result = await DAO.Project.deleteByID(id);
            CacheService.Project.flushAll();
            return result;
        }

        return null;
    };

    Projects.update = async function(id, project) {
        if(project) {
            let result =  await DAO.Project.update(id, project);
            CacheService.Project.flushAll();
            return result;
        }

        return null;
    };

    /** module layouts **/
    let Layouts = {};
    Service.Layouts = Layouts;
    /**
     * Return as a object has layout list inside
     * @param project_id : objectId
     * @param page
     * @returns {Promise<*>}
     */
    Layouts.getLayoutListByProjectId = async function (project_id, page) {
        try {
            let result = null;
            let key = `project-layouts-v2-${project_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached[0];
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Layout.getListByProjectId(project_id, offset, limit);
            } else {
                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Layout.getListByProjectId(project_id);
            }

            if (result) {
                result = TransformBackend.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.Layout.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectId", e);
            return null;
        }
    };

    Layouts.getLayoutListByProjectIntId = async function (id, page) {
        try {
            let result = null;
            let key = `project-layouts-v2-${id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached;
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Layout.getListByProjectIntId(id, offset, limit);
            } else {
                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Layout.getListByProjectIntId(id);
            }

            if (result) {
                result = TransformBackend.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.Layout.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectIntId", e);
            return null;
        }
    };


    Layouts.getLayoutList = async function (query) {
        try {
            let page = query.page;
            let limit = (query.limit) ? Math.min(query.limit, 1000) : 24;
            let result = null;
            let layoutId = query.layout_id;
            //let intID = parseInt(layoutId);
            let name = query.name;
            let project_id = query.project_id;
            let layout_key_name = query.layout_key_name;
            let estimated_price = query.estimated_price;
            let style = query.style;
            let bedroom = query.bedroom;
            let room_type = query.room_type;
            let key = `layouts-list-v2`;
           
            // if(intID){
            //     key = key + '-id-' + intID.toString();
            // }
        
            key = `${key}?limit=${limit}&id=${layoutId}&name=${name}&project_id=${project_id}&estimated_price=${estimated_price}&style=${style}&bedroom=${bedroom}&room_type=${room_type}&layout_key_name=${layout_key_name}`;
            
            if (page !== undefined && page !== null) {
                key = `${key}&page=${page}`;

                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                result = await DAO.Layout.getList(page, limit, query);

            } else {
                let cached = CacheService.Layout.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.Layout.getList(1, limit, query);
                
            }

            if (result) {
                //result = TransformBackend.layouts.list(result);
                CacheService.Layout.set(key, result, 900);
                return result;
            }
        } catch (e) {
            
            return [];
        }
    };

    Layouts.getLayoutListByListProjectId = async function (array_int_id){
        try {
            let string = array_int_id.join('-');
            let key = `layouts-list-by-project-id:` + string;
           
            let cached = CacheService.Layout.get(key);
            if (cached) {
                return cached;
            }
            result = await DAO.Layout.getLayoutListByListProjectId(array_int_id);
            if (result) {
                CacheService.Layout.set(key, result, 900);
                return result;
            }
        } catch (e) {
            return [];
        }
    }

    Layouts.getLayoutById = async function (id) {
        try {
            let result = null;
            let key =id.toString();
            let cached = CacheService.Layout.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Layout.getDetail(id);
            
            if (result.length) {
                
                result = TransformBackend.layouts.detail(result[0]);
                CacheService.Layout.set(key, result, 900);
                return result;
            }
            else {
                return null;
            }
        } catch (e) {
            
            return null;
        }
    };

    Layouts.create = async function(layout) {
        if(layout) {
            let result =  await DAO.Layout.create(layout);
            CacheService.Layout.flushAll();
            return result;
        }

        return null;
    };

    Layouts.deleteByID = async function(id) {
        if(id) {
            let result =  await DAO.Layout.deleteByID(id);
            CacheService.Layout.flushAll();
            return result;
        }

        return null;
    };

    Layouts.update = async function(id, layout) {
        if(layout) {
            let result = await DAO.Layout.update(id, layout);
            CacheService.Layout.flushAll();
            return result;
        }

        return null;
    };

    Layouts.duplicate = async function(id) {
        if(id) {
            let layout = await Service.Layouts.getLayoutById(id);
            delete layout._id;
            layout.is_template = false;
            layout.parent_int_id = layout.int_id; 
            layout.metadata.approved = false;
            let result = await Service.LayoutsRenovation.create(layout);
            

            let layout_json_res = await DAO.LayoutJson.getDetail(id);
            
            if (layout_json_res.length) {
                let data_json = layout_json_res[0];
                delete data_json._id;
                await Service.LayoutsRenovationJson.create(data_json);
            }

            return result;
        }

        return null;
    };


    Layouts.updatePanoramaLink = async function (layoutID, link) {
        try {
            await DAO.Layout.updatePanoramaLink(layoutID, link);
            CacheService.Layout.flushAll();
            return true;
        } catch (e) {
            Logger.error("updatePanoramaLink", e);
            return false;
        }
    };

    Layouts.addCart = async function (original_json, cart_json, layout_id) {
        try {
            let rs = await DAO.Layout.addCart(layout_id, original_json, cart_json);
            CacheService.Layout.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add cart", e);
            return false;
        }
    };

    Layouts.addRooms = async function (layout_id, array_data_rooms) {
        try {
            let rs = await DAO.Layout.addRooms(layout_id, array_data_rooms);
            CacheService.Layout.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add rooms: ", e);
            return false;
        }
    };


    Layouts.removeRoom = async function (layout_id, room_id) {
        try {
            
            let rs = await DAO.Layout.removeRoom(layout_id, room_id);
            CacheService.Layout.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Remove rooms: ", e);
            return false;
        }
    };

    Layouts.addGallery = async function (layout_id, array_data_gallery) {
        try {
            let rs = await DAO.Layout.addGallery(layout_id, array_data_gallery);
            CacheService.Layout.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add gallery: ", e);
            return false;
        }
    };


    Layouts.removeGallery = async function (layout_id, gallery_id) {
        try {
            
            let rs = await DAO.Layout.removeGallery(layout_id, gallery_id);
            CacheService.Layout.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Remove gallery: ", e);
            return false;
        }
    };




    /** module layouts **/
    let LayoutsRenovation = {};
    Service.LayoutsRenovation = LayoutsRenovation;
    /**
     * Return as a object has layout list inside
     * @param project_id : objectId
     * @param page
     * @returns {Promise<*>}
     */
    LayoutsRenovation.getLayoutListByProjectId = async function (project_id, page) {
        try {
            let result = null;
            let key = `project-layouts-renovation-v2-${project_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.LayoutRenovation.get(key);
                if (cached) {
                    return cached[0];
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Layout.getListByProjectId(project_id, offset, limit);
            } else {
                let cached = CacheService.LayoutRenovation.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Layout.getListByProjectId(project_id);
            }

            if (result) {
                result = TransformBackend.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.LayoutRenovation.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectId", e);
            return null;
        }
    };

    LayoutsRenovation.getLayoutListByProjectIntId = async function (id, page) {
        try {
            let result = null;
            let key = `project-layouts-renovation-v2-${id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.LayoutRenovation.get(key);
                if (cached) {
                    return cached;
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Layout.getListByProjectIntId(id, offset, limit);
            } else {
                let cached = CacheService.LayoutRenovation.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Layout.getListByProjectIntId(id);
            }

            if (result) {
                result = TransformBackend.layouts.listV2(result);
                if(result && result.length > 0) {
                    CacheService.LayoutRenovation.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getLayoutListByProjectIntId", e);
            return null;
        }
    };


    LayoutsRenovation.getLayoutList = async function (query) {
        try {
            let page = query.page;
            let limit = 24;
            let result = null;
            let layoutId = query.layout_id;
            //let intID = parseInt(layoutId);
            let name = query.name;
            let project_id = query.project_id;

            let estimated_price = query.estimated_price;
            let style = query.style;
            let bedroom = query.bedroom;
            let room_type = query.room_type;
            let approved = query.approved;
            let key = `layouts-renovation-list-v2`;
           
            // if(intID){
            //     key = key + '-id-' + intID.toString();
            // }
        
            key = `${key}?limit=${limit}&id=${layoutId}&name=${name}&project_id=${project_id}&estimated_price=${estimated_price}&style=${style}&bedroom=${bedroom}&room_type=${room_type}&approved=${approved}`;
            
            if (page !== undefined && page !== null) {
                key = `${key}&page=${page}`;

                let cached = CacheService.LayoutRenovation.get(key);
                
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                
                result = await DAO.LayoutRenovation.getList(page, limit, query);

            } else {
                let cached = CacheService.LayoutRenovation.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.LayoutRenovation.getList(1, 1000, query);
                
            }

            if (result) {
                //result = TransformBackend.layouts.list(result);
                CacheService.LayoutRenovation.set(key, result, 900);
                return result;
            }
        } catch (e) {
            
            return [];
        }
    };


    LayoutsRenovation.getLayoutById = async function (id) {
        try {
            let result = null;
            let key = id.toString();
            let cached = CacheService.LayoutRenovation.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.LayoutRenovation.getDetail(id);
            
            if (result.length) {

                result = TransformBackend.layoutsRenovation.detail(result[0]);
                CacheService.LayoutRenovation.set(key, result, 900);
                return result;
            }
            else{
                return  null;
            }
        
        } catch (e) {
            
            return null;
        }
    };

    LayoutsRenovation.create = async function(layout) {
        if(layout) {
            let result =  await DAO.LayoutRenovation.create(layout);
            CacheService.LayoutRenovation.flushAll();
            return result;
        }

        return null;
    };

    LayoutsRenovation.deleteByID = async function(id) {
        if(id) {
            let result =  await DAO.LayoutRenovation.deleteByID(id);
            CacheService.LayoutRenovation.flushAll();
            return result;
        }

        return null;
    };

    LayoutsRenovation.update = async function(id, layout) {
        if(layout) {
            let result = await DAO.LayoutRenovation.update(id, layout);
            CacheService.LayoutRenovation.flushAll();
            return result;
        }

        return null;
    };



    LayoutsRenovation.updatePanoramaLink = async function (layoutID, link) {
        try {
            await DAO.LayoutRenovation.updatePanoramaLink(layoutID, link);
            CacheService.LayoutRenovation.flushAll();
            return true;
        } catch (e) {
            Logger.error("updatePanoramaLink", e);
            return false;
        }
    };

    LayoutsRenovation.addCart = async function (original_json, cart_json, layout_id) {
        try {
            let rs = await DAO.LayoutRenovation.addCart(layout_id, original_json, cart_json);
            CacheService.LayoutRenovation.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add cart", e);
            return false;
        }
    };

    LayoutsRenovation.addRooms = async function (layout_id, array_data_rooms) {
        try {
            let rs = await DAO.LayoutRenovation.addRooms(layout_id, array_data_rooms);
            CacheService.LayoutRenovation.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add rooms: ", e);
            return false;
        }
    };


    LayoutsRenovation.removeRoom = async function (layout_id, room_id) {
        try {
            
            let rs = await DAO.LayoutRenovation.removeRoom(layout_id, room_id);
            CacheService.LayoutRenovation.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Remove rooms: ", e);
            return false;
        }
    };

    LayoutsRenovation.addGallery = async function (layout_id, array_data_gallery) {
        try {
            let rs = await DAO.LayoutRenovation.addGallery(layout_id, array_data_gallery);
            CacheService.LayoutRenovation.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Add gallery: ", e);
            return false;
        }
    };


    LayoutsRenovation.removeGallery = async function (layout_id, gallery_id) {
        try {
            
            let rs = await DAO.LayoutRenovation.removeGallery(layout_id, gallery_id);
            CacheService.LayoutRenovation.flushAll();
            return rs;
        } catch (e) {
            Logger.error("Remove gallery: ", e);
            return false;
        }
    };

    /** module rooms **/
    let Rooms = {};
    Service.Rooms = Rooms;
    /**
     * Return as a object has room list inside
     * @param layout_id : objectId
     * @param page
     * @returns {Promise<*>}
     */
    Rooms.getRoomListByLayoutId = async function (layout_id, page) {
        try {
            let result = null;
            let key = `layout-rooms-v2-${layout_id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached[0];
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Room.getListByLayoutId(layout_id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Room.getListByLayoutId(layout_id);
            }

            if (result) {
                result = TransformBackend.rooms.listV2(result);
                if(result && result.length > 0) {
                    CacheService.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getRoomListByLayoutId", e);
            return null;
        }
    };

    Rooms.getRoomListByLayoutIntId = async function (id, page) {
        try {
            let result = null;
            let key = `layout-rooms-v2-${id}`;
            if (page !== undefined && page !== null) {
                key = `${key}-page-${page}`;
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                let limit = 24;
                let offset = page > 0 ? page * limit : 0;
                result = await DAO.Room.getListByLayoutIntId(id, offset, limit);
            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                result = await DAO.Room.getListByLayoutIntId(id);
            }

            if (result) {
                result = TransformBackend.rooms.listV2(result);
                if(result && result.length > 0) {
                    CacheService.set(key, result, 900);
                }
                return result || null;
            }
        } catch (e) {
            Logger.error("getRoomListByLayoutIntId", e);
            return null;
        }
    };


    Rooms.getRoomList = async function (query) {
        try {
            let page = query.page;
            //const limit = parseInt(query.limit || 24);
            let limit = 24;
            let result = null;
            let roomId = query.room_id;
            let intID = parseInt(roomId);
            let name = query.name;
            let key = `rooms-v2-${limit}`;
            let layout_id = query.layout_id;
            if(name){
                key = key + '-' + name;
            }
            if(intID){
                key = key + '-' + intID.toString();
            }
            if(layout_id){
                key = `${key}-layout:${layout_id}`;
            }
            if (page !== undefined && page !== null) {
                key = `${key}-${page}`;

                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                result = await DAO.Room.getList(page, limit, query);

            } else {
                let cached = CacheService.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.Room.getList(1, 1000, query);
                
            }

            if (result) {
                //result = TransformBackend.rooms.list(result);
                CacheService.set(key, result, 900);
                return result;
            }
        } catch (e) {
            
            return [];
        }
    };


    Rooms.getRoomById = async function (id) {
        try {
            let result = null;
            let key = "rooms-v2-" + id.toString();
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Room.getDetail(id);
            
            if (result.length) {

                result = TransformBackend.rooms.detail(result[0]);
                CacheService.set(key, result, 900);

                return result;
            }
        } catch (e) {
            
            return null;
        }
    };

    Rooms.create = async function(room) {
        if(room) {
           
            let result =  await DAO.Room.create(room);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    Rooms.deleteByID = async function(id) {
        if(id) {
            let result =  await DAO.Room.deleteByID(id);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    Rooms.update = async function(id, room) {
        if(room) {
            let result = await DAO.Room.update(id, room);
            CacheService.flushAll();
            return result;
        }

        return null;
    };


    /**
     * Module Layout Json
     * @type {{}}
     */
    let LayoutsJson = {};
    Service.LayoutsJson = LayoutsJson;
    LayoutsJson.getLayoutJsonByLayoutId = async function (id) {
        try {
            let result = null;
            let key = "layout-json-v2-layout_id-" + id.toString();
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.LayoutJson.getDetail(id);
            
            if (result.length) {
                result = TransformBackend.layoutJson.detail(result[0]);
                CacheService.set(key, result, 900);
                return result;
            }
            else {
                return null;
            }
        } catch (e) {
            
            return null;
        }
    };

    LayoutsJson.create = async function(data) {
        if(data) {
            let result =  await DAO.LayoutJson.create(data);
            CacheService.flushAll();
            return result;
        }

        return null;
    };

    
    /**
     * Module Layout Renovation Json
     * @type {{}}
     */
    let LayoutsRenovationJson = {};
    Service.LayoutsRenovationJson = LayoutsRenovationJson;
    LayoutsRenovationJson.getLayoutJsonByLayoutId = async function (id) {
        try {
            let result = null;
            let key = "layout-json-v2-layout_id-" + id.toString();
            let cached = CacheService.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.LayoutRenovationJson.getDetail(id);
            
            if (result.length) {
                result = TransformBackend.layoutJson.detail(result[0]);
                CacheService.set(key, result, 900);
                return result;
            }
            else {
                return null;
            }
        } catch (e) {
            
            return null;
        }
    };

    LayoutsRenovationJson.create = async function(data) {
        if(data) {
            let result =  await DAO.LayoutRenovationJson.create(data);
            CacheService.flushAll();
            return result;
        }

        return null;
    };


    /** module cart **/
    let Carts = {};
    Service.Carts = Carts;
    /**
     * Return as a object has cart list inside
     * @param cart_id : cartId
     * @param page
     * @returns {Promise<*>}
     */

    Carts.getCartList = async function (query) {
        try {
            let limit = 24;
            let result = null;
            let key = `carts-list-v2`;
            key = `${key}?limit=${limit}`;
            let page = query.page;
            if (page !== undefined && page !== null) {
                key = `${key}&page=${page}`;

                let cached = CacheService.Cart.get(key);
                if (cached) {
                    return cached;
                }
                //let offset = Math.max(page - 1, 0);
                result = await DAO.Cart.getList(page, limit, query);

            } else {
                let cached = CacheService.Cart.get(key);
                if (cached) {
                    return cached;
                }

                result = await DAO.Cart.getList(1, 1000, query);
            }
            if (result) {
                CacheService.Cart.set(key, result, 900);
                return result;
            }else{
                return [];
            }
            
            
        } catch (e) {
            return [];
        }
    };


    Carts.getCartById = async function (id) {
        try {
            let result = null;
            let key = id.toString();
            let cached = CacheService.Cart.get(key);
            if (cached) {
                return cached;
            }

            result = await DAO.Cart.getDetail(id);
            
            if (result) {

                result = TransformBackend.carts.detail(result);
                CacheService.Cart.set(key, result, 900);

                
            }
            return result;
        } catch (e) {
            
            return null;
        }
    };

    Carts.create = async function(cart) {
        if(cart) {
           
            let result =  await DAO.Cart.create(cart);
            CacheService.Cart.flushAll();
            return result;
        }

        return null;
    };

    Carts.deleteByID = async function(id) {
        if(id) {
            let result =  await DAO.Cart.deleteByID(id);
            CacheService.Cart.flushAll();
            return result;
        }

        return null;
    };

    Carts.update = async function(id, cart) {
        if(cart) {
            let result = await DAO.Cart.update(id, cart);
            CacheService.Cart.flushAll();
            return result;
        }

        return null;
    };


    module.exports = Service;
})();