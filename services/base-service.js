(function () {
    const {logger, debug} = require('../core/logger');
    const path = require('path');
    module.exports = function () {
        return {
            debug: debug,
            logger: logger,
            print : function (name) {
                let f = path.normalize(name);
                if (process.platform === "win32") {
                    name = path.win32.basename(f);
                } else {

                    name = path.posix.basename(f);
                }

                logger.info(`[OK] load module [${name}]`);
            }
        }
    };
})();