(function () {
    const {logger, debug} = require('../core/logger');
    const path = require('path');
    function print(name) {
        let f = path.normalize(name);
        if (process.platform === "win32") {
            name = path.win32.basename(f);
        } else {

            name = path.posix.basename(f);
        }

        logger.info(`[OK] load plugin [${name}]`);
    }

    module.exports = function (pluginName, fileName) {
        print(fileName);
        let plugin = {
            debug: debug,
            logger: logger,
            name : pluginName || "plugin",
            shutdown : async function () {
                if(plugin.onDestroy && typeof plugin.onDestroy === 'function') {
                    await plugin.onDestroy();
                }
            }
        };

        return plugin;
    };
})();