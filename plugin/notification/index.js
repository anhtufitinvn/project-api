(function () {

    let listener = Object.create(require('../base-plugin')(PluginNames.NAME_NOTIFICATION, __filename));

    /** declare dependencies **/
    const DEBUG_KEY = "debug.fitin.vn";
    let io = null;
    const adapter = require('socket.io-redis');
    let RedisService = require('../../services/redis-services');
    var {logger, debug} = require('../../core/logger');

    const NOTIFICATION_CHANNEL = "notification";

    let socketOps = {
        transports: ['polling', 'websocket'],
        /*origins: '*:*',
        handlePreflightRequest: (req, res) => {
            const headers = {
                "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
                "Access-Control-Allow-Credentials": true
            };
            res.writeHead(200, headers);
            res.end();
        }*/
    };

    let socket = null;

    let pub, sub = null;

    let nameSpace = null;

    let ChannelControllers = {};

    listener.initialize = function (httpServer) {
        socket = require('socket.io')(httpServer, socketOps);
        socket.origins('*:*');
        // middleware
        socket.use((client, next) => {
            return verifyUserToken(client, next);
        });
    };

    listener.onDestroy = async function() {
        new Promise((resolve) => {
            if(pub) {
                pub.end(false);
            }

            if(sub) {
                sub.end(false);
            }

            resolve(true);
        });
    };

    listener.start = async function () {
        pub = await RedisService.createClient();

        sub = await RedisService.createClient();

        socket.adapter(adapter({pubClient: pub, subClient: sub}));

        namespace = socket.of('/io');

        namespace.on('connection', async (client) => {
            try {
                //// binding event
                client.on('disconnect', async () => {
                    await onClientDisConnected(client)
                });

                client.on('event', async (data) => {
                    await onClientEvent(client, data);
                });

                //// forward event
                await onClientConnected(client);
            } catch (e) {
                debug("socket connection has error ", e);
            }
        });
    };

    listener.addEventHandler = function (event, handler) {
        if (event && typeof handler === 'function') {
            ChannelControllers[event.toLowerCase()] = {
                event : event.toLowerCase(),
                handler : handler
            };
        }
    };

    listener.makeEventMessage = function(event, data) {
        return {
            event : event,
            data : data
        }
    };

    listener.broadcast = function(event, msg) {
        return new Promise(resolve => {
            if (!msg) {
                socket.of('/io').to(NOTIFICATION_CHANNEL).emit("event", listener.makeEventMessage("message", event));
            } else {
                socket.of('/io').to(NOTIFICATION_CHANNEL).emit("event", listener.makeEventMessage(event || "message", msg));
            }

            resolve(true);
        });
    };

    async function onClientEvent(client, data) {
        if(data && data.event) {
            if(ChannelControllers[data.event.toLowerCase()]) {
                await ChannelControllers[data.event](client, data);
            } else {
                await onEventMessage(client, data);
            }
        }
    }

    async function onEventMessage(client, message) {
        debug("onEventMessage", message);
    }

    async function onClientConnected(client) {
        client.join(NOTIFICATION_CHANNEL);
        client.emit("event", listener.makeEventMessage("join", "welcome"));
        debug("onClientConnected - Auto join room");
    }

    async function onClientDisConnected(client) {
        debug("onClientDisConnected");
        client.leave(NOTIFICATION_CHANNEL, () => {
            debug(`${client.id} has left the room`);
        });
    }

    async function verifyUserToken(client, next) {
        try {
            let token = client.handshake.headers['token'];
            if(DEBUG_KEY === token) {
                return next();
            }

            return next(new Error('invalid token'));
        } catch (e) {
            return next(new Error('authentication error'));
        }
    }

    module.exports = function (httpServer) {
        listener.initialize(httpServer);
        return listener;
    };
})();