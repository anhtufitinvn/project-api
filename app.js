var configuration = global.configuration;
var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var {logger} = require('./core/logger');
// const requestIp = require('request-ip');
// const session= require('express-session');
const Handlebars = require('hbs');
Handlebars.registerHelper("counter", function (index){
    return index + 1;
});

Handlebars.registerHelper('formatVND', function(value) {
    if(value) {
        return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
    }

    return value;
});

Handlebars.registerHelper("format", function (index){
    return index + 1;
});

Handlebars.registerHelper('json', function(context) {
    return JSON.stringify(context);
});

/**
 * The {{#exists}} helper checks if a variable is defined.
 */
Handlebars.registerHelper('exists', function(variable, options) {
    if (typeof variable !== 'undefined') {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

/**
 * The {{#isDiscount}} helper checks if a variable is defined.
 */
Handlebars.registerHelper('isDiscount', function(variable, options) {
    if (variable === true) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

// var graphqlHTTP = require('express-graphql');
// var {resolver, schemas} = require('./graphs/graph');
// const UUIDV4 = require('uuid/v4');

// load shared services
var Providers = require('./server/providers');
var apiRouter = require('./routes/api');
var backendRouter = require('./routes/backend');
var frontendRouter = require('./routes/frontend');
var indexRouter = require('./routes/index');
var adminRouter = require('./routes/admin');
var app = express();

/** var SessionConfig = {
    name : 'fitin-cts',
    secret: 'f123321',
    resave: true,
    saveUninitialized: true,
    cookie: {maxAge: 60000},
    genid: function(req) {
        return UUIDV4() // use UUIDs for session IDs
    },
};

SessionConfig.cookie.secure = false;
if (configuration.HTTPS) {
    // trust first proxy
    app.set('trust proxy', 1) ;
    // serve secure cookies
    SessionConfig.cookie.secure = true
}

app.use(session(SessionConfig));
**/

const FormatErrorFn = error => ({
    message: error.message,
    locations: error.locations,
    stack: error.stack ? error.stack.split('\n') : [],
    path: error.path
});

// view engine setup
// Disable VIEW
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(require("morgan")("combined", {
    "stream": logger.stream
}));


app.use(cors({credentials: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());

app.use('/', indexRouter);
// API Routing
app.use('/api', express.json({limit: '100mb'}));
app.use('/api', express.urlencoded({extended: true, limit: '100mb'}));
app.use('/api', apiRouter);
//// CMS Routing
app.use('/cms', express.json({limit: '50mb'}));
app.use('/cms', express.urlencoded({extended: true, limit: '100mb'}));
app.use('/cms', backendRouter);


//// CMS Routing
app.use('/frontend', express.json());
app.use('/frontend', express.urlencoded({extended: true, limit: '100mb'}));
app.use('/frontend', frontendRouter);
// app.use('/graph/v2', graphqlHTTP(async (request, response, graphQLParams) => ({
//         schema: schemas,
//         context: {},
//         rootValue: resolver,
//         graphiql: true,
//         customFormatErrorFn: FormatErrorFn
//     }))
// );

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    if (req.app.get('env') === 'development') {
        if(res.locals.isRender) {
            res.render('404');
        } else {
            res.json({code: -1, message: err.message, stack: err.stack});
        }
    } else {
        if(res.locals.isRender) {
            res.render('404');
        } else {
            res.json({code: -1, message: err.message || "Something went wrong"});
        }
    }
});

///**** If run mixing worker + api
// require('./server/quotation-transporter').start();

module.exports = app;
