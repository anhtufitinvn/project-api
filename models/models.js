(function () {
    let cartItem = require('./cart-item');
    let roomItem = require('./room-item');
    module.exports = {cartItem, roomItem}
})();