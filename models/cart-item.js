(function () {
    module.exports = function (sku, quantity, groupId, groupName, cost) {
        if(sku === undefined && quantity === undefined) {
            return {
                sku : "null",
                quantity : 0,
                group_id : 0,
                group_name : "",
                cost : 0
            }
        }
        return {
            sku : sku ?  sku.toLowerCase() : "null",
            quantity : quantity === undefined || quantity === 0 ? 1 : Math.abs(quantity),
            group_id : groupId || 0,
            group_name : groupName || "",
            cost : cost || 0
        };
    }
})();