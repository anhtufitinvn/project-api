(function () {
    module.exports = function (image_path, thumb_path, index, hidden) {
        
        return {
            image_path : image_path ,
            thumb_path : thumb_path || image_path,
            index : index || 0,
            hidden : hidden || 0
        };
    }
})();
