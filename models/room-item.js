(function () {
    module.exports = function (name, type, area,  room_id, level, qty) {
        
        return {
            name : name || (type || "null"),
            type : type || null,
            room_id : room_id || 0,
            area : area || 0,
            qty : qty || 1,
            level : level || 1
        };
    }
})();
