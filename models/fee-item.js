(function () {
    module.exports = function (name, price) {
        if(name === undefined && price === undefined) {
            return {
                name : "null",
                price : 0
            }
        }
        return {
            name : name ?  name.toLowerCase() : "null",
            price : price === undefined ? 0 : Math.abs(price)
        };
    }
})();