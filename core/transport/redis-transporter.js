(function () {
    Redis = require('../../services/redis-services');
    let {debug, logger} = require('../../core/logger');
    class RedisTransporter {
        constructor(name) {
            this.name = name.toLowerCase();
            this.__channelIn = `${this.name}-in`;
            this.__channelOut = `${this.name}-out`;
        }

        setWorker(worker) {
            if(worker && worker.process && typeof worker.process === 'function') {
                this.worker = worker;
                this.worker.transporter = this;
                this.worker.countTaskInQueue = this.countTaskInQueue.bind(this);
                return this;
            }

            throw new Error('worker must be an instance of Worker or worker must have a method name process(Message)');
        }

        getWoker() {
            return this.worker || null;
        }

        /**
         * Send ket qua da xu ly sang 1 channel khac ChannelOut
         * @param TMessage
         * @returns {Promise<RedisTransporter>}
         */
        async output(TMessage) {
            try {
                let client = await Redis.getClient();
                let length = await client.LPUSHAsync(this.__channelOut, JSON.stringify(TMessage));
                debug(`${this.__channelOut} length : [${length}]`);
                return this;
            } catch (e) {
                logger.error(e);
            }
        }

        /**
         * Push TMessage len queue, cung 1 channel ChannelIn
         * @param TMessage
         * @returns {Promise<RedisTransporter>}
         */
        async queue(TMessage) {
            try {
                let client = await Redis.getClient();
                let length = await client.LPUSHAsync(this.__channelIn, JSON.stringify(TMessage));
                debug(`${this.__channelIn} length : [${length}]`);
                return this;
            } catch (e) {
                logger.error(e);
            }
        }

        async countTaskInQueue() {
            try {
                let client = await Redis.getClient();
                let length = await client.LLENAsync(this.__channelIn);
                debug(`${this.__channelIn} length : [${length}]`);
                return length;
            } catch (e) {
                logger.error(e);
            }
        }

        start() {
            if(this.timer) {
                clearInterval(this.timer);
                this.timer = null;
            }

            let self = this;
            this.timer = setInterval(async ()=> {
                try {
                    let client = await Redis.getClient();
                    let value = await client.RPOPAsync(self.__channelIn);
                    if(value) {
                        let message = JSON.parse(value);
                        if(message) {
                            if(message.action !== undefined) {
                                await self.dispatchMessage(message);
                            }
                        }
                    }
                } catch (e) {
                    debug(e);
                }
            }, 100);

            logger.info(`[OK] start transporter [${this.name}]`);
        }

        async dispatchMessage(TMessage) {
            if(this.worker) {
                await this.worker.process(TMessage);
            }
        }
    }

    module.exports = RedisTransporter;
})();