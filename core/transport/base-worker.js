(function () {
    let {debug, logger} = require('../logger');

    class BaseWorker {
        constructor() {
            this.debug = debug;
            this.logger = logger;
        }

        async process(TMessage) {
            this.debug(`process message`);
            this.debug(TMessage);
        }
    }

    module.exports = BaseWorker;
})();