(function () {
    const {matchedData, checkSchema, validationResult} = require('express-validator');
    const ruleList = require('../validators/rules-schema');
    const {logger, debug} = require('../core/logger');
    const path = require('path');

    module.exports = function (name, filename) {
        let Handler = {
            name: name,
            ruleSchemas: {},
            logger: logger,
            debug: debug,
            errorHandler(req, res, next) {
                const errors = validationResult(req);
                if (errors.isEmpty()) {
                    req.matchedData = matchedData(req);
                    return next();
                }

                const extractedErrors = [];
                errors.array().map(err => extractedErrors.push({[err.param]: err.msg}));
                return res.status(200).json({
                    code: -1,
                    message: "invalid parameters",
                    errors: extractedErrors,
                });
            },

            makeReturnMessage(data) {
                return {
                    code: 0,
                    message: "success",
                    data: data
                };
            },

            makeReturnError(message, errorCode) {
                if (message instanceof Error) {
                    return {
                        code: -1,
                        message: "got an exception",
                        errors: [
                            {
                                error: errorCode || -1,
                                message: message["message"] || "something went wrong"
                            }
                        ]
                    };
                }

                return {
                    code: -1,
                    message: message,
                    errors: [
                        {
                            error: errorCode || -1,
                            message: message
                        }
                    ]
                };
            },

            loadRules(rules) {
                if (rules && rules.length > 0) {
                    rules.map(r => {
                        if (ruleList[r] && !this.ruleSchemas[r]) {
                            this.ruleSchemas[r] = checkSchema(ruleList[r]);
                        }
                    })
                }
            },

            validate(ruleName) {
                if (ruleName && this.ruleSchemas[ruleName]) {
                    return [this.ruleSchemas[ruleName], this.errorHandler];
                }

                return [];
            },

            print(name) {
                let f = path.normalize(name);
                if (process.platform === "win32") {
                    name = path.win32.basename(f);
                } else {
                    name = path.posix.basename(f);
                }

                logger.info(`load handler [${name}]`);
            },
        };

        Handler.print(filename);
        return Handler;
    };
})();