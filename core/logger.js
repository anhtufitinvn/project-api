const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const myFormat = printf(({ level, message, timestamp }) => {
    return `${timestamp} ${level}: ${message}`;
});

const logger = createLogger({
    level: 'info',
    format: combine(
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        myFormat
    ),
    defaultMeta: {service: 'fitin-consultant'},
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log`
        // - Write all logs error (and below) to `error.log`.
        //
        new transports.File({filename: '../logs/fitin-consultant/error.log', level: 'error'}),
        new transports.File({ filename: '../logs/fitin-consultant/combined.log'}),
    ],
    exitOnError: false
});

const tracer = createLogger({
    level: 'info',
    format: combine(
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        myFormat
    ),
    defaultMeta: {service: 'fitin-consultant'},
    transports: [
        new transports.File({filename: '../logs/fitin-consultant/access.log', level: 'info'})
    ],
    exitOnError: false
});


logger.stream = {
    write: function(message){
        tracer.info(message);
    }
};

if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console({
        format: combine(
            timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            myFormat
        )
    }));
}

function debug(message, args) {
    if (process.env.NODE_ENV !== 'production') {
        if(args) {
            console.log(message, args);
        } else {
            console.log(message);
        }
    }
}

module.exports = {logger, debug, Debug : debug, Logger: logger};