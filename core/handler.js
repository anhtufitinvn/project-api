(function () {
    const {matchedData, checkSchema, validationResult} = require('express-validator');
    const ruleList = require('../validators/rules-schema');
    const Providers = require('../server/providers');
    const {logger, debug} = require('../core/logger');

    class Handler {
        constructor() {
            this.providers = Providers;
            this.ruleSchemas = {};
            this.logger = logger;
            this.debug = debug;
        }

        loadRules(rules) {
            if (rules && rules.length > 0) {
                rules.map(r => {
                    if (ruleList[r] && !this.ruleSchemas[r]) {
                        this.ruleSchemas[r] = checkSchema(ruleList[r]);
                    }
                })
            }
        };

        loadAllRules() {
            Object.keys(ruleList).forEach(name => {
                if(!name.startsWith("test")) {
                    if (ruleList[name] && !this.ruleSchemas[name]) {
                        this.debug(`load rule[${name}]`);
                        this.ruleSchemas[name] = checkSchema(ruleList[name]);
                    }
                }
            });
        };

        errorHandler(req, res, next) {
            const errors = validationResult(req);
            if (errors.isEmpty()) {
                req.matchedData = matchedData(req);
                return next();
            }

            const extractedErrors = [];
            errors.array().map(err => extractedErrors.push({[err.param]: err.msg}));
            return res.status(200).json({
                code: -1,
                message: "invalid parameters",
                errors: extractedErrors,
            });
        };

        makeReturnMessage (data) {
            return {
                code: 0,
                message: "success",
                data: data
            };
        };

        makeReturnList (data) {
            return {
                code: 0,
                message: "success",
                data: {
                    list : data
                }
            };
        };

        makeReturnError (message, errorCode) {
            if (message instanceof Error) {
                return {
                    code: -1,
                    message: "got an exception",
                    errors: [
                        {
                            error: errorCode || -1,
                            message: message["message"] || "something went wrong"
                        }
                    ]
                };
            }

            return {
                code: -1,
                message: message,
                errors: [
                    {
                        error: errorCode || -1,
                        message: message
                    }
                ]
            };
        };

        makeReturnLoginError (message, errorCode) {
            if (message instanceof Error) {
                return {
                    code: -401,
                    message: "Authenticated Error",
                    errors: [
                        {
                            error: errorCode || -1,
                            message: message["message"] || "something went wrong"
                        }
                    ]
                };
            }

            return {
                code: -401,
                message: message,
                errors: [
                    {
                        error: errorCode || -1,
                        message: message
                    }
                ]
            };
        };

        validate(ruleName) {
            if (ruleName && this.ruleSchemas[ruleName]) {
                return [this.ruleSchemas[ruleName], this.errorHandler];
            }

            return [];
        };
    }

    module.exports = Handler;
})();