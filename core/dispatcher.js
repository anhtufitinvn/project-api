(function () {
    const {debug, logger} = require('../core/logger');
    function dispatcher(handler, fn) {
        return async (req, res, next) => {
            try {
                if(handler) {
                    await fn.call(handler, req, res, next);
                } else {
                    next();
                }
            } catch (e) {
                debug(e);
                next(e);
            }
        }
    }

    module.exports = dispatcher;
})();