#!/bin/bash
echo "------------- FITIN CONSULTANT DEPLOYING SCRIPT  -------------"
echo "[-] check src folder"
#git reset --hard
PM2_NAME="fitin-consultant"
PM2_WORKER_NAME="fitin-consultant-worker"
NAME="src"
W="${PWD}"
D="${PWD}/${NAME}"
TEMP="${PWD}/${NAME}@temp"
echo "$D"
if [  -d "$D" ]; then
  echo "stop PM2" 
  pm2 stop "$PM2_NAME"
  pm2 stop "$PM2_WORKER_NAME"
fi

if [ ! -d "$TEMP" ]; then
  mkdir -p $TEMP
else 
	echo "[-] clean and reset"
 	rm -rf $TEMP	
 	mkdir -p $TEMP
fi

cd ${TEMP}
echo "[-] get src from git, please enter your password."
git clone git@bitbucket.org:fitin2018/consultant-services-api.git "."
echo "[-] install dependences"
npm install

sleep 1
if [  -d "$D" ]; then
 	rm -rf $D
fi

sleep 1
mv $TEMP $D
sleep 2
pm2 start "$D/process.json" --update-env
echo DONE
