#!/bin/bash
echo "------------- FITIN CONSULTANT UPDATING, DO NOT INSTALL NPM PACKAGES SCRIPT  -------------"
echo "[-] check src folder"
#git reset --hard
PM2_NAME="fitin-consultant"
PM2_WORKER_NAME="fitin-consultant-worker"
PM2_SYNC_NAME="fitin-consultant-sync"
NAME="src"
W="${PWD}"
D="${PWD}/${NAME}"
TEMP="${PWD}/${NAME}@temp"
echo "$D"
if [  -d "$D" ]; then
  echo "stop PM2" 
  pm2 stop "$PM2_NAME"
  pm2 stop "$PM2_WORKER_NAME"
  pm2 stop "$PM2_SYNC_NAME"
fi

if [ ! -d "$TEMP" ]; then
  mkdir -p $TEMP
else 
	echo "[-] clean and reset"
 	rm -rf $TEMP	
 	mkdir -p $TEMP
fi


cd ${TEMP}
echo "[-] get src from git, please enter your password."
git clone https://lamtranfitin@bitbucket.org/anhtufitinvn/project-api.git "."
echo "[-] copy node_modules"
# move folder node_modules to new src
mv $D/node_modules $TEMP

sleep 1
if [  -d "$D" ]; then
 	rm -rf $D
fi

sleep 1
mv $TEMP $D
sleep 2
pm2 start "$D/process-dev.json" --update-env
echo DONE
