var express = require('express');
var router = express.Router();
var pages = require('../render/renders');
var {debug} = require('../core/logger');
var Providers = require('../server/providers');
var adminController = require('../controller/admin-controller');


router.get('/', async function (req, res, next) {
    try {
        let renderPage = pages.AdminSite;
        let data = await renderPage.getContent();
        debug(data);
        res.render(renderPage.view, data);
    } catch (e) {
        next(e);
    }
});

router.get('/components/views/layouts/json/:layout_id/',
    adminController.validate("layoutId"),
    adminController.renderLayoutJsonViewer);

router.get('/components/views/projects',
    adminController.renderProjectList);

router.get('/components/data/projects',
    adminController.getProjectList);

router.post('/components/data/projects',
    adminController.getProjectList);

router.post('/components/data/projects/:project_id/layouts',
    adminController.validate("projectId"),
    adminController.getLayoutsByProjectId);

router.get('/admin/components/projects/:project_id/layouts', async function (req, res, next) {
    try {
        let id = req.params["project_id"] || 0;
        id = parseInt(id);
        let data = null;
        let component = components.LayoutList;
        if(!isNaN(id)) {
            data = await component.getContent({project_id: id});
            debug(data);
        }
        res.render(component.view, data);
        // todo : redirect error page if needed.
    } catch (e) {
        next(e);
    }
});

module.exports = router;
