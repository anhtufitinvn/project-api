var express = require('express');
var router = express.Router();
var configuration = global.configuration;

const Validator = require('../validators/validator');
const dispatch = require('../core/dispatcher');

router.get('/v1', (req, res, next) => {
    res.json({code: 0, message: "success", version: configuration.VERSION, name: "FITIN Consultant V1"});
});


/** project handler **/

// router.get('/v1/projects', Validator.validate('indexProject'),
//     dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.getProjects));

// router.get('/v1/projects/:project_id', Validator.validate('showProject'),
//     dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.getProject));




module.exports = router;