var express = require('express');
var router = express.Router();
const Validator = require('../validators/validator');
const dispatch = require('../core/dispatcher');

const Modules = require('../controller/modules');
var AuthenServices = require('../services/authen-services');
var ApiController = require('../controller/api-controller');
var ApiV2Controller = require('../controller/api-v2-controller');
var UploadController = require('../controller/upload-controller');
var PanoramaController = require('../controller/panorama-controller');
var ClientController = require('../controller/client-controller');
var HubSpotController = require('../controller/hubspot-controller');
var QuotationController = require('../controller/quotation-controller');

const ACCESS_KEY = configuration.ACCESS_KEY || "fitin";

router.get('/', async function (req, res, next) {
    res.json(ApiController.makeReturnMessage({version: configuration.VERSION, name : "FITIN Consultant -"}));
});

router.get('/v1/hubspot/contacts/sync', HubSpotController.synAllContacts);

router.get('/v1/hubspot/contacts/sync/lastest', HubSpotController.synLastestContacts);

router.get('/v1/hubspot/contacts/all', HubSpotController.validate("pageNum"), HubSpotController.getAllContacts);

router.get('/v1/hubspot/contacts/list', HubSpotController.validate("pageNum"), HubSpotController.getContactList);

router.get('/v1/hubspot/contacts/create-list', HubSpotController.validate("name"), HubSpotController.createtContactList);

router.post('/v1/hubspot/form/quick/submit', HubSpotController.validate("quickForm"), HubSpotController.submitQuickForm);

router.get('/v1/contacts', ApiController.validate("pageNum"), ApiController.getContactList);

router.get('/v1/contacts/find', ApiController.validate("findEmail"), ApiController.findEmailContactStartWith);

router.get('/v1/projects', ApiController.validate("projects"), ApiController.getProjectList);

router.get('/v1/quotation/layouts/:layout_id', QuotationController.validate('layout_id'),QuotationController.getItemsFromLayout);

router.get('/v1/resources/ct-forms', ApiController.getFormProperties);

router.get('/v1/resources/locations', ApiController.getLocations);

/***
 * This API is used for HR
 */
router.post('/v1/resources/phones-sms-spam/import', ApiController.importPhoneSMS4Spam);

/***
 * This API is used for HR
 */
router.put('/v1/resources/phones-sms-spam/sent', ApiController.updateSentPhoneSMS4Spam);

/***
 * This API is used for HR
 */
router.get('/v1/resources/phones-sms-spam/list', ApiController.getPhoneSMS4Spam);

/***
 * @deprecated It will be removed in next version
 */
router.get('/v1/resources/images/migrate/projects', ApiController.migrateProjectImagesFromECOM);

/***
 * @deprecated It will be removed in next version
 */
router.get('/v1/resources/images/migrate/layouts', ApiController.migrateLayoutImagesFromECOM);

/***
 * @deprecated It will be removed in next version
 * This function is temporary, it will be removed after migrating
 */
router.get('/v1/resources/layouts/migrate/metadata', ApiController.migrateLayoutMetadataFromECOM);

router.get('/v1/dl/quotation/layouts/:layout_id', ApiController.validate("layout_id"), ApiController.buildLayoutQuotation);

router.get('/v1/quotation/build/layouts/:layout_id', ApiController.validate("layout_id"), ApiController.buildLayoutQuotation);

router.get('/v1/quotation/build/renovation/layouts/:layout_id', ApiController.validate("layout_id"), ApiController.buildLayoutRenovationQuotation);

router.get('/v1/quotation/build/template/layouts/:layout_id', ApiController.validate("layout_id"), ApiController.buildLayoutTemplateQuotation);

router.post('/v1/upload/result', UploadController.uploadZipFile);

router.post('/v1/upload/panorama', PanoramaController.uploadZipFile4Consultant);

router.post('/v1/import/renovation/panorama', PanoramaController.importRenovationZipFileCMS);

router.put('/v1/import/renovation/panorama', PanoramaController.importRenovationZipFileCMS);

router.post('/v1/import/layout/panorama', PanoramaController.importLayoutZipFileCMS);

router.put('/v1/import/layout/panorama', PanoramaController.importLayoutZipFileCMS);

router.post('/v1/import/renovation/gallery', UploadController.importRenovationZipFileCMS);

router.put('/v1/import/renovation/gallery', UploadController.importRenovationZipFileCMS);

router.post('/v1/tool/upload/panorama', PanoramaController.uploadZipFile4CMS);

router.get('/v1/cache/flushall/:access_key', ApiController.flushAllCache);

router.get('/v1/client/inbox', verifyUserToken, ClientController.getClientInbox);

router.get('/v1/client/inboxes', verifyUserToken, ClientController.getClientInboxes);

/***
** API Version 2, Upgrade new format
 ***/
router.get('/v2/projects',
    dispatch(ApiV2Controller, ApiV2Controller.getProjects));

router.get('/v2/projects/:project_id',
    Validator.validate('projectStringId'),
    dispatch(ApiV2Controller, ApiV2Controller.getLayoutListByProjectId));

/** Layout */
router.get('/v2/layouts', Validator.validate('indexLayout'),
dispatch(Modules.LayoutsController, Modules.LayoutsController.getLayouts));

router.get('/v2/layouts/:layout_id', Validator.validate('showLayout'),
dispatch(Modules.LayoutsController, Modules.LayoutsController.getLayout));

/** Build quotation of renovation layout **/
router.get('/v2/quotation/renovation/build/layout/:layout_id',
    Validator.validate('layoutId'),
    dispatch(ApiV2Controller, ApiV2Controller.generateRenovationQuotationLayout));

/** Build all quotation of renovation layouts **/
router.get('/v2/quotation/renovation/build/all',
    dispatch(ApiV2Controller, ApiV2Controller.generateAllRenovationQuotation));

/** Build quotation of template layout **/
router.get('/v2/quotation/template/build/layout/:layout_id',
    Validator.validate('layoutId'),
    dispatch(ApiV2Controller, ApiV2Controller.generateTemplateQuotationLayout));

router.get('/v2/quotation/template/build/all',
    function (req, res, next) {
        res.json(ApiController.makeReturnMessage("this function is currently not supported"));
    });

/** Notification Message - Broadcast message to CMS */
router.post('/v2/cms/push/broadcast', verifyAccessKey,
    dispatch(ApiV2Controller, ApiV2Controller.broadcastNotificationToCMS));

/** Project Renovation */
router.get('/v2/renovation/projects',
    dispatch(Modules.ProjectsController, Modules.ProjectsController.getProjectsRenovation));

/** Layout Renovation */
router.get('/v2/renovation/layouts', Validator.validate('indexLayout'),
dispatch(Modules.LayoutsRenovationController, Modules.LayoutsRenovationController.getLayouts));

/** Only for Horo to get layout */
router.get('/v2/renovation/horo/layouts',
dispatch(Modules.LayoutsRenovationController, Modules.LayoutsRenovationController.getLayouts4Horo));

router.get('/v2/renovation/layouts/:layout_id', Validator.validate('showLayout'),
dispatch(Modules.LayoutsRenovationController, Modules.LayoutsRenovationController.getLayout));

/** Room Category */
router.get('/v2/resources/room-categories',
dispatch(Modules.ResourcesController, Modules.ResourcesController.getRoomCategories));

/** Location */
router.get('/v2/resources/locations',
dispatch(Modules.ResourcesController, Modules.ResourcesController.getLocations));


async function verifyUserToken(req, res, next) {
    try {
        const token = req.header("Token");
        if(token) {
            req.user = await AuthenServices.verifyToken(token);
            next();
        } else {
            res.json(ApiController.makeReturnError("require access token", -1));
        }
    } catch (e) {
        res.json(ApiController.makeReturnError(e.message || "invalid token", -1));
    }

}

async function verifyAccessKey(req, res, next) {
    try {
        const accessKey = req.header("Access-Key");
        if(accessKey && ACCESS_KEY === accessKey) {
            next();
        } else {
            res.json(ApiController.makeReturnError("require access key", -1));
        }
    } catch (e) {
        res.json(ApiController.makeReturnError(e.message || "invalid access key", -1));
    }

}

module.exports = router;
