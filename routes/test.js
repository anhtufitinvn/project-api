var express = require('express');
var router = express.Router();
var configuration = global.configuration;
var services = require('../server/providers');
let {cartItem} = require('../models/models');

router.get('/ecom/cart/create', async (req, res, next) => {
    let result = await services.EcomService.createCart();
    res.json({code : 0, data : result["data"]});
});

router.get('/ecom/cart/:id', async (req, res, next) => {
    let cartId = req.params['id'];
    let result = await services.EcomService.getCartById(cartId);
    res.json({code : 0, data : result["data"]});
});

router.get('/ecom/build/cart', async (req, res, next) => {
    try {
        let items = req.query['items'];
        if (items) {
            let list = [];
            items = items.split(",");
            items.forEach(e => {
                if (e !== "") {
                    list.push(cartItem(e, 1));
                }
            });

            let result = await services.ecom.buildCart(list);
            res.json({code: 0, data: result});
        } else {
            res.json({code: -1, message: "invalid parameters"});
        }
    } catch (e) {
        res.json({code: -1, message: e.message || null});
    }
});


module.exports = router;
