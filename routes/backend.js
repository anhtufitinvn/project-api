var express = require('express');
var router = express.Router();
var configuration = global.configuration;
const Modules = require('../backend/modules');
const {Authenticated, AccessKey} = require('../middleware/middleware');
const Validator = require('../validators/validator');
const dispatch = require('../core/dispatcher');


router.get('/v1', (req, res, next) => {
    res.json({code: 0, message: "success", version: configuration.VERSION, name: "FITIN Consultant-Backend V1"});
});

/** mockup handler **/

router.get('/v1/mockups',
    dispatch(Modules.SampleHandler, Modules.SampleHandler.dump));

router.get('/v1/mockups/email',
    Validator.validate('email'),
    dispatch(Modules.SampleHandler, Modules.SampleHandler.dump));

router.get('/v1/mockups/transport',
    dispatch(Modules.SampleHandler, Modules.SampleHandler.testWorker));


/** resources handler **/

router.get('/v1/resources/locations',
dispatch(Modules.ResourcesHandler, Modules.ResourcesHandler.getLocations));

router.get('/v1/resources/room-categories',
dispatch(Modules.ResourcesHandler, Modules.ResourcesHandler.getRoomCategories));

router.post('/v1/resources/room-categories/init',
dispatch(Modules.ResourcesHandler, Modules.ResourcesHandler.initRoomCategories));

router.post('/v1/users/init',
dispatch(Modules.UsersHandler, Modules.UsersHandler.initUsers));

/**
 * @Deprecated after v2
 */
router.get('/resources/locations/import',
dispatch(Modules.ResourcesHandler, Modules.ResourcesHandler.importLocationFromMysql));
/**
 * @Deprecated after v2
 */
router.get('/v1/projects/import',
    dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.importAllProjectsFromMySQL));
/**
 * @Deprecated after v2
 */
router.get('/v1/layouts/import',
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.importAllLayoutsFromMySQL));
/**
 * @Deprecated after v2
 */
router.get('/v1/layouts/datajson', 
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.importLayoutJsonFromEcom));




router.post('/v1/auth/login', Validator.validate('loginByEmail'),
    dispatch(Modules.AuthHandler, Modules.AuthHandler.login));



router.get('/v1/renovation/layouts', dispatch(AccessKey, AccessKey.isAccess),
    dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.getLayouts4External));



/** MIDDLEWARE CHECK AUTH */
router.use('/v1', dispatch(Authenticated, Authenticated.isAuth));

/** CMS ++++++++++++++++++++++++++++++++++++++++++++++++++++ */


/** project handler **/

router.get('/v1/projects', Validator.validate('indexProject'),
    dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.getProjects));

router.get('/v1/renovation/projects', Validator.validate('indexProject'),
    dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.getProjectsRenovation));

router.get('/v1/projects/:project_id', Validator.validate('showProject'),
    dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.getProject));

router.post('/v1/projects',
    Validator.validate('createProject'),
    dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.createProject));

router.put('/v1/projects/:project_id',
    Validator.validate('updateProject'),
    dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.updateProject));

router.delete('/v1/projects/:project_id',
    Validator.validate('deleteProject'),
    dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.deleteProjectByID));

/** Layout */
/**
 * LAYOUT  ROOM HANDLE
 */
router.post('/v1/layouts/rooms',
Validator.validate('postAddLayoutRoom'),
dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.addRooms));

router.delete('/v1/layouts/rooms',
Validator.validate('deleteLayoutRoom'),
dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.removeRoom));

/**
 * LAYOUT  GALLERY HANDLE
 */
router.post('/v1/layouts/gallery',
Validator.validate('postAddLayoutGallery'),
dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.addGallery));

router.delete('/v1/layouts/gallery',
Validator.validate('deleteLayoutGallery'),
dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.removeGallery));

/** LAYOUT  CRUD*/
router.get('/v1/layouts', Validator.validate('indexLayout'),
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.getLayouts));

router.get('/v1/layouts/:layout_id', Validator.validate('showLayout'),
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.getLayout));

router.post('/v1/layouts',
    Validator.validate('createLayout'),
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.createLayout));

router.put('/v1/layouts/:layout_id',
    Validator.validate('updateLayout'),
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.updateLayout));

router.delete('/v1/layouts/:layout_id',
    Validator.validate('deleteLayout'),
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.deleteLayoutByID));

router.post('/v1/layouts/duplicate',
    Validator.validate('duplicateLayout'),
    dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.duplicateLayout));

/**
 * LAYOUT JSON of LAYOUT
 */

router.get('/v1/layouts/json/:layout_id',
    Validator.validate('getLayoutJson'),
    dispatch(Modules.LayoutsJsonHandler, Modules.LayoutsJsonHandler.getLayoutJson));

    router.post('/v1/layouts/json/:layout_id',
    Validator.validate('postLayoutJson'),
    dispatch(Modules.LayoutsJsonHandler, Modules.LayoutsJsonHandler.postLayoutJson));

router.post('/v1/layouts/upload/json',
    dispatch(Modules.LayoutsJsonHandler, Modules.LayoutsJsonHandler.uploadLayoutJson));





/**
 * LAYOUT RENOVATION
 */

 /**
 * LAYOUT RENOVATION ROOM HANDLE
 */
router.post('/v1/layouts-renovation/rooms',
Validator.validate('postAddLayoutRoom'),
dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.addRooms));

router.delete('/v1/layouts-renovation/rooms',
Validator.validate('deleteLayoutRoom'),
dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.removeRoom));

 /**
 * LAYOUT RENOVATION GALLERY HANDLE
 */
router.post('/v1/layouts-renovation/gallery',
Validator.validate('postAddLayoutGallery'),
dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.addGallery));

router.delete('/v1/layouts-renovation/gallery',
Validator.validate('deleteLayoutGallery'),
dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.removeGallery));

/** CRUD LAYOUT RENOVATION */
router.get('/v1/layouts-renovation', Validator.validate('indexLayout'),
    dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.getLayouts));

router.get('/v1/layouts-renovation/:layout_id', Validator.validate('showLayout'),
    dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.getLayout));

router.post('/v1/layouts-renovation',
    Validator.validate('createLayout'),
    dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.createLayout));

router.put('/v1/layouts-renovation/:layout_id',
    Validator.validate('updateLayout'),
    dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.updateLayout));

router.delete('/v1/layouts-renovation/:layout_id',
    Validator.validate('deleteLayout'),
    dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.deleteLayoutByID));

router.post('/v1/layouts-renovation/duplicate',
    Validator.validate('duplicateLayout'),
    dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.duplicateLayout));


/**
 * LAYOUT JSON of LAYOUT RENOVATION
 */

router.get('/v1/layouts-renovation/json/:layout_id',
    Validator.validate('getLayoutJson'),
    dispatch(Modules.LayoutsRenovationJsonHandler, Modules.LayoutsRenovationJsonHandler.getLayoutJson));

    router.post('/v1/layouts-renovation/json/:layout_id',
    Validator.validate('postLayoutJson'),
    dispatch(Modules.LayoutsRenovationJsonHandler, Modules.LayoutsRenovationJsonHandler.postLayoutJson));

router.post('/v1/layouts-renovation/upload/json',
    dispatch(Modules.LayoutsRenovationJsonHandler, Modules.LayoutsRenovationJsonHandler.uploadLayoutJson));






/** Style */
router.get('/v1/styles',
    dispatch(Modules.ResourcesHandler, Modules.ResourcesHandler.getStyles));


/** Room */
router.get('/v1/rooms', Validator.validate('indexRoom'),
    dispatch(Modules.RoomsHandler, Modules.RoomsHandler.getRooms));

router.get('/v1/rooms/:room_id', Validator.validate('showRoom'),
    dispatch(Modules.RoomsHandler, Modules.RoomsHandler.getRoom));

router.post('/v1/rooms',
    Validator.validate('createRoom'),
    dispatch(Modules.RoomsHandler, Modules.RoomsHandler.createRoom));

router.put('/v1/rooms/:room_id',
    Validator.validate('updateRoom'),
    dispatch(Modules.RoomsHandler, Modules.RoomsHandler.updateRoom));

router.delete('/v1/rooms/:room_id',
    Validator.validate('deleteRoom'),
    dispatch(Modules.RoomsHandler, Modules.RoomsHandler.deleteRoomByID));



/** Users */


router.get('/v1/users',
    dispatch(Modules.UsersHandler, Modules.UsersHandler.getUsers));

router.get('/v1/users/:user_id', Validator.validate('showUser'),
    dispatch(Modules.UsersHandler, Modules.UsersHandler.getUser));

router.post('/v1/users',
    Validator.validate('createUser'),
    dispatch(Modules.UsersHandler, Modules.UsersHandler.createUser));

router.put('/v1/users/:user_id',
    Validator.validate('updateUser'),
    dispatch(Modules.UsersHandler, Modules.UsersHandler.updateUser));

router.delete('/v1/users/:user_id',
    Validator.validate('deleteUser'),
    dispatch(Modules.UsersHandler, Modules.UsersHandler.deleteUserByID));


// router.get('/v1/projects', Validator.validate('page'),
//     dispatch(Modules.ProjectsHandler, Modules.ProjectsHandler.getProjects));

/** CARTS */
router.get('/v1/carts', Validator.validate('indexCart'),
    dispatch(Modules.CartsHandler, Modules.CartsHandler.getCarts));

router.get('/v1/carts/:cart_id', Validator.validate('showCart'),
    dispatch(Modules.CartsHandler, Modules.CartsHandler.getCart));

router.post('/v1/carts',
    Validator.validate('createCart'),
    dispatch(Modules.CartsHandler, Modules.CartsHandler.createCart));

router.put('/v1/carts/:cart_id',
    Validator.validate('updateCart'),
    dispatch(Modules.CartsHandler, Modules.CartsHandler.updateCart));

router.delete('/v1/carts/:cart_id',
    Validator.validate('deleteCart'),
    dispatch(Modules.CartsHandler, Modules.CartsHandler.deleteCartByID));

/** Upload handler **/

router.post('/v1/upload/image',
    dispatch(Modules.UploadHandler, Modules.UploadHandler.uploadImage));
    
//router.post('/v1/layouts-renovation/upload/cart', 
//dispatch(Modules.UploadHandler, Modules.UploadHandler.uploadImage));

/** ADDITIONAL TOOL */
router.post('/v1/layouts/upload/gallery', //Validator.validate('uploadLayoutGallery'), 
dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.uploadGallery));
router.post('/v1/layouts-renovation/upload/gallery', //Validator.validate('uploadLayoutGallery'), 
dispatch(Modules.LayoutsRenovationHandler, Modules.LayoutsRenovationHandler.uploadGallery));

router.post('/v1/layouts-renovation/upload/panorama', dispatch(Modules.PanoramaHandler, Modules.PanoramaHandler.uploadZipFile4CMS));
router.post('/v1/layouts-renovation/upload/cart', dispatch(Modules.PanoramaHandler, Modules.PanoramaHandler.uploadCartJson4Renovation));

router.post('/v1/layouts/upload/panorama', dispatch(Modules.PanoramaHandler, Modules.PanoramaHandler.uploadZipFile4CMSLayout));
router.post('/v1/layouts/upload/cart', dispatch(Modules.PanoramaHandler, Modules.PanoramaHandler.uploadCartJson4Layout));

router.put('/v1/layouts/update/key/:layout_id', dispatch(Modules.LayoutsHandler, Modules.LayoutsHandler.updateLayoutKey));

module.exports = router;