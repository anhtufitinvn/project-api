var express = require('express');
var router = express.Router();
var pages = require('../render/renders');
var {debug, logger} = require('../core/logger');
var providers = require('../server/providers');

router.get('/', async function (req, res, next) {
    try {
        // const key = "test-key";
        // let result = await providers.RedisService.getAsync(key, null);
        // if(!result) {
        //     await services.redis.setAsync(key, "this is a test value");
        //     result = await services.redis.getAsync(key, null);
        // }

        res.json({
            code: 0,
            data: {
                version: configuration.VERSION,
                name: "FITIN CONSULTANT"
            }
        });
    } catch (e) {
        next(e);
    }
});

router.get('/test/sse', async function (req, res, next) {
    try {
        res.render('sse-test', {title: "test-sse"})
    } catch (e) {
        next(e);
    }
});

router.get('/quotation/:signature', async function (req, res, next) {
    try {
        res.locals.isRender = true;
        let signature = req.params.signature || null;
        let renderPage = pages.ConsultantQuote;
        if (signature) {
            let data = await renderPage.getContent({signature: signature, is_build : 'true' === req.query['update']});
            debug(data.data.groups);
            res.render(renderPage.view, data);
        } else {
            next(new Error("quotation not found"));
        }
    } catch (e) {
        next(e);
    }
});

router.get('/quotation/renovation/layouts/:layout_id', async function (req, res, next) {
    try {
        res.locals.isRender = true;
        let layoutId = req.params.layout_id || null;
        layoutId = parseInt(layoutId);
        let renderPage = pages.ConsultantQuoteRenovation;
        if (!isNaN(layoutId)) {
            let data = await renderPage.getContent({layout_id: layoutId, is_build : 'true' === req.query['update']});
            debug(data.data.groups);
            res.render(renderPage.view, data);
        } else {
            next(new Error("layout_id not found"));
        }
    } catch (e) {
        next(e);
    }
});

router.get('/quotation/template/layouts/:layout_id', async function (req, res, next) {
    try {
        res.locals.isRender = true;
        let layoutId = req.params.layout_id || null;
        layoutId = parseInt(layoutId);
        let renderPage = pages.ConsultantLayoutQuote;
        if (!isNaN(layoutId)) {
            let data = await renderPage.getContent({layout_id: layoutId, is_build : 'true' === req.query['update']});
            debug(data.data.groups);
            res.render(renderPage.view, data);
        } else {
            next(new Error("layout_id not found"));
        }
    } catch (e) {
        next(e);
    }
});

/***
 * @deprecated, this function will be removed in next version
 */
router.get('/quotation/layouts/:layout_id', async function (req, res, next) {
    try {
        res.locals.isRender = true;
        let layoutId = req.params.layout_id || null;
        layoutId = parseInt(layoutId);
        let renderPage = pages.LayoutQuote;
        if (!isNaN(layoutId)) {
            let data = await renderPage.getContent({layout_id: layoutId, is_build : 'true' === req.query['update']});
            debug(data);
            res.render(renderPage.view, data);
        } else {
            res.locals.isRender = false;
            next(new Error("layout_id not found"));
        }
    } catch (e) {
        next(e);
    }
});

/***
 * download quotation layout
 */
router.get('/dl/quotation/template/layouts/:layout_id', async function (req, res, next) {
    try {
        res.locals.isRender = true;
        let layoutId = req.params.layout_id || null;
        layoutId = parseInt(layoutId);
        if (!isNaN(layoutId)) {
            let rs = await providers.QuotationService.generatePDFFile(layoutId, false);
            if(rs && rs.file_path) {
                return res.download(rs.file_path);
            }
            next(new Error("file not found"));
        } else {
            next(new Error("layout_id not found"));
        }

    } catch (e) {
        next(e);
    }
});

/***
 * download quotation layout
 */
router.get('/dl/quotation/renovation/layouts/:layout_id', async function (req, res, next) {
    try {
        res.locals.isRender = true;
        let layoutId = req.params.layout_id || null;
        layoutId = parseInt(layoutId);
        if (!isNaN(layoutId)) {
            let rs = await providers.QuotationService.generatePDFFile(layoutId, true);
            if(rs && rs.file_path) {
                return res.download(rs.file_path);
            }
            next(new Error("file not found"));
        } else {
            next(new Error("layout_id not found"));
        }

    } catch (e) {
        next(e);
    }
});

router.get('/ct/client', async function (req, res, next) {
    try {
        let renderPage = pages.ChatClient;
        let data = await renderPage.getContent();
        debug(data);
        res.render(renderPage.view, data);
    } catch (e) {
        next(e);
    }
});

router.get('/admin', async function (req, res, next) {
    try {
        let renderPage = pages.AdminSite;
        let data = await renderPage.getContent();
        debug(data);
        res.render(renderPage.view, data);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
