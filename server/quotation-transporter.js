(function () {
    let Transporter = require('../core/transport/redis-transporter');
    let QuotationWorker = require('../workers/quotation-worker');
    let {ActionName} = require('../workers/actions-names');
    let QuotationTransporter = new Transporter('quotation-transporter');
    QuotationTransporter.setWorker(new QuotationWorker());
    QuotationTransporter.ACTIONS = ActionName;
    module.exports = QuotationTransporter;
})();