(function () {
    let container = {};
    module.exports = {
        CacheService : require('../services/cached-services'),
        AuthenService : require('../services/authen-services'),
        CartService : require('../services/cart-services'),
        DataService : require('../services/data-services'),
        BackendDataService : require('../services/backend-data-services'),
        FrontendDataService : require('../services/landingpage-data-services'),
        BackendUserService : require('../services/backend-user-services'),
        DataServiceV2 : require('../services/data-services-v2'),
        DiscountService : require('../services/discounts-services'),
        EcomService : require('../services/ecom-services'),
        StoreService : require('../services/store-services'),
        ResourceService : require('../services/resources-services'),
        UrlService : require('../services/url-services'),
        RenderService : require('../services/render-services'),
        RedisService : require('../services/redis-services'),
        MockupService : require('../services/mockups-service'),
        HubspotService : require('../services/hubspot-services'),
        ConsultantService :  require('../services/consultant-services'),
        QuotationService :  require('../services/quotation-services'),
        NotifyService :  require('../services/notify-services'),
        MigrateService :  require('../services/migrate-services'),
        setShared (name, instance) {
            if(name && instance) {
                let key = name.toLowerCase();
                container[key] = instance;
            }
        },
        get(name) {
            if(name) {
                return container[name.toLowerCase()] || null;
            }

            return null;
        }
    };
})();