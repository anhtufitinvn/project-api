(function () {
    module.exports = {
        Authenticated : require('./jwt-auth'),
        AccessKey : require('./access-key')
    };
})();