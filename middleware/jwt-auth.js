(function () {

    let Handler = require('../core/handler');
    let jwt = require('jsonwebtoken');
    let Providers = require('../server/providers');
    let {debug, logger} = require('../core/logger');
    let RedisService = Providers.RedisService;
    let jwt_secret = global.configuration.JWT_SECRET;

    class JWTAuthenticated extends Handler {
        constructor() {
            super();
            this.name = "JWTAuthenticated";
        }


        isAuth(req, res, next) {
            if (req.headers &&
                req.headers.authorization &&
                req.headers.authorization.startsWith('Bearer ')) {
                let header_token = req.headers.authorization;
                var jwt_token = header_token.slice(7, header_token.length);
                jwt.verify(jwt_token, jwt_secret, async (err, payload) => {
                    if (err) {
                        res.json(this.makeReturnLoginError(err.message));
                        return;
                    }

                    let user_string = await RedisService.getAsync(jwt_token);
                    if (user_string) {
                        req.user = JSON.parse(user_string);
                        next();
                    } else {
                        res.json(this.makeReturnLoginError("Please re-login"));
                    }
                });
            } else {
                res.json(this.makeReturnLoginError("Unauthenticated!"));
            }
        };
    }

    module.exports = new JWTAuthenticated();
})();