(function () {

    let Handler = require('../core/handler');
   
    let Providers = require('../server/providers');
    let {debug, logger} = require('../core/logger');
    let secret_key = global.configuration.ACCESS_SECRET_KEY;

    class AccessKeyAuthenticated extends Handler {
        constructor() {
            super();
            this.name = "AccessKeyAuthenticated";
        }


        isAccess(req, res, next) {
            if (req.headers &&
                req.headers.accesskey ) {
                let header_key = req.headers.accesskey;
                if(header_key === secret_key){                
                    next();
                }else{
                    res.json(this.makeReturnError('Wrong Access Key!'));
                }
            } else {
                res.json(this.makeReturnError("Need Access Key!"));
            }
        };
    }

    module.exports = new AccessKeyAuthenticated();
})();