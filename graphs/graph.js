const schemas = require('./schemas');
const resolver = require('./resolver');
module.exports = {schemas, resolver};