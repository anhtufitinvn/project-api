const products = require('../db/mockup').products;
const resolver = {
    products: () => {
        return products;
    }
};

resolver.productsWithPromotion = ({type}) => {
    return products.filter((e) => {
        const promotion = e['promotions'];
        if (promotion && promotion.length > 0) {
            for (let i = promotion.length; --i >= 0;) {
                if (promotion[i].type === type) {
                    return true;
                }
            }
        }
        return false;
    })
};



module.exports = resolver;